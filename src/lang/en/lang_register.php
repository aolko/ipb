<?php

$lang = array (

'lost_pass_form'   => "Lost Password Form",
'lp_header'        => "How to reset your password",

'lp_text'          => "<b>Step One: Entering your account username</b><br><br>Enter your account username in the field below. The username is case <b>in</b>sensitive.<br>Once you have submitted the form, you will receive an email asking for validation of this request to ensure that no malicious use has occured. This email will also contain your new password. Please note that you will not be able to log in with this password until you have validated your request.<br>You will be presented with a form.<br><br><b>Step Two: Validating the request</b><br><br>The email will contain instructions on how to validate this request.<br>Upon successful validation, you will be able to log in using your new password.</b>",

'lp_send'          => "Proceed",

'lp_user_name'     => "Enter your account username",

'lp_subject'       => "Password recovery information from",

'lp_redirect'     => "The password has been sent, taking you to the activate page...",

'activation_form' => "Activation Form",

registration_form					=>	"Registration Form",
user_validate_text					=>	"The board administrator wishes to preview all accounts before allowing you to register fully. Register now then please wait until you receive an email from the board administrator before attempting to log in.",
js_blanks					=>	"You must complete all of the form",
reg_header					=>	"Registration Instructions",
intro_proceed					=>	"Proceed to log in",
js_no_check					=>	"You cannot register unless you agree to the terms and check the 'I agree' checkbox",
title_reg					=>	"Registration",
email_validate_text					=>	"You will be sent an email to the email address you give after registering. Please read the email carefully, you will need to validate your account by clicking on a link in the email.",
done_reg_1					=>	"Your account has been activated! Proceeding to the board rules",
done_reg_2					=>	"Your account has been activated! Proceeding to log in",
board_rules					=>	"Board Rules",
dumb_text					=>	"Please ensure that you complete the form fully. The information you need will be in the email that was sent to you.",
registration_agree					=>	"Registration Agreement",
agree_submit					=>	"I agree",
email_address					=>	"<b>Please enter your email address</b><br>You will need to enter a <i>real</i> email address",
std_text					=>	"Please ensure that you complete all the fields fully, taking particular care over the password fields.",
user_name					=>	"<b>Please choose a Username</b><br>Usernames must be between 3 and 32 characters long",
reg_success					=>	"Registration Successful",
pass_word					=>	"<b>Please choose a Password</b><br>Passwords must be between 3 and 32 characters long",
re_enter_pass					=>	"<b>Please re-enter your password</b><br>It must match <i>exactly</i>",
thank_you					=>	"Thank you",
registration_process					=>	"Registration Process",
preview_reg_text					=>	"Your registration has been successful. The administrator wishes to preview all new registered accounts before posting permissions are granted. The administrator has been notified of your registration.",
dumb_email					=>	"Please enter the Email Address you registered with",
complete_form					=>	"Please complete the form fully",
auth_text					=>	"Your registration has been submitted.<br><br>The board administrator has chosen to require validation for all email addresses. Within the next 10 minutes (usually instantly) you'll receive an email with instructions on the next step. Don't worry, it won't take long before you can post!<br><br>The email has been sent to",
submit_form					=>	"Submit my registration",
nav_reg					=>	"Registration",
dumb_submit					=>	"Proceed",
dumb_header					=>	"Account Activation",

'val_key'  => "Validation Key:",
'user_id'  => "User ID:",

terms_service					=>	"Terms of Service",
terms_service_text				=>	"Please read fully and check the 'I agree' box ONLY if you agree to the terms",


email_address_two  => "<b>Please re-enter your email address</b>",

'click_wrap' => "Please remember that we are not responsible for any messages posted. We do not vouch for or warrant the accuracy, completeness or usefulness of any message, and are not responsible for the contents of any message. The messages express the views of the author of the message, not necessarily the views of this BB. Any user who feels that a posted message is objectionable is encouraged to contact us immediately by email. We have the ability to remove objectionable messages and we will make every effort to do so, within a reasonable time frame, if we determine that removal is necessary. You agree, through your use of this service, that you will not use this BB to post any material which is knowingly false and/or defamatory, inaccurate, abusive, vulgar, hateful, harassing, obscene, profane, sexually oriented, threatening, invasive of a person's privacy, or otherwise violative of any law. You agree not to post any copyrighted material unless the copyright is owned by you or by this BB.",

);
?>