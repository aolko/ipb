<?php

/*
+--------------------------------------------------------------------------
|   IBFORUMS v1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001,2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > Registration functions
|   > Module written by Matt Mecham
|   > Date started: 16th February 2002
|
|	> Module Version Number: 1.0.0
+--------------------------------------------------------------------------
*/


$idx = new Register;

class Register {

    var $output     = "";
    var $page_title = "";
    var $nav        = array();
    var $html       = "";
    var $email      = "";
    
    function Register() {
    	global $ibforums, $DB, $std, $print;
    	
    	//--------------------------------------------
    	// Make sure our code number is numerical only
    	//--------------------------------------------
    	
    	//$ibforums->input['CODE'] = preg_replace("/^([0-9]+)$/", "$1", $ibforums->input['CODE']);
    	
    	if ($ibforums->input['CODE'] == "") $ibforums->input['CODE'] = 00;
    	
    	//--------------------------------------------
    	// Require the HTML and language modules
    	//--------------------------------------------
    	
		$ibforums->lang = $std->load_words($ibforums->lang, 'lang_register', $ibforums->lang_id );
    	
    	require "./Skin/".$ibforums->skin_id."/skin_register.php";
    	$this->html = new skin_register();
    	
    	$this->base_url        = "{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}";
    	$this->base_url_nosess = "{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}";
    	
    	//--------------------------------------------
    	// Get the emailer module
		//--------------------------------------------
		
		require "./sources/lib/emailer.php";
		
		$this->email = new emailer();
    	
    	//--------------------------------------------
    	// What to do?
    	//--------------------------------------------
    	
    	switch($ibforums->input['CODE']) {
    		case '00':
    			$this->show_reg_form();
    			break;
    			
    		case '02':
    			$this->create_account();
    			break;
    			
    		case '03':
    			$this->validate_user();
    			break;

    		case '05':
    			$this->show_manual_form();
    			break;
    			
    		case '06':
    			$this->show_manual_form('lostpass');
    			break;
    			
    		case '07':
    			$this->show_manual_form('newemail');
    			break;
    			
    		case '10':
    			$this->lost_password_start();
    			break;
    		case '11':
    			$this->lost_password_end();
    			break;

    		default:
    			$this->show_reg_form();
    			break;
    	}
    	
    	// If we have any HTML to print, do so...
    	
    	$print->add_output("$this->output");
        $print->do_output( array( 'TITLE' => $this->page_title, 'JS' => 0, NAV => $this->nav ) );
    		
 	}
 	
 	/*****************************************************/
	// lost_password_start
	// ------------------
	// Simply shows the lostpassword form
	// What do you want? Blood?
	/*****************************************************/
	
	function lost_password_start()
	{
		global $ibforums, $DB, $std;
		
		$this->page_title = $ibforums->lang['lost_pass_form'];
		
    	$this->nav        = array( $ibforums->lang['lost_pass_form'] );

    	$this->output    .= $this->html->lost_pass_form();
    }
    
    
    
    
    function lost_password_end()
    {
    	global $ibforums, $DB, $std, $HTTP_POST_VARS, $print;
    	
    	if ($HTTP_POST_VARS['member_name'] == "")
    	{
    		$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_username' ) );
    	}
    	
    	//------------------------------------------------------------
		// Check for input and it's in a valid format.
		//------------------------------------------------------------
		
		$member_name = trim(strtolower($ibforums->input['member_name']));
		
		if ($member_name == "")
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_username' ) );
		}
    	
    	//------------------------------------------------------------
		// Attempt to get the user details from the DB
		//------------------------------------------------------------
		
		$DB->query("SELECT name, id, email, mgroup, validate_key FROM ibf_members WHERE LOWER(name)='$member_name'");
		
		if ( !$DB->get_num_rows() )
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_such_user' ) );
		}
		else
		{
			$member = $DB->fetch_row();
			
			//------------------------------------------------------------
			// Is there a validation key? If so, we'd better not touch it
			//------------------------------------------------------------
			
			if ($member['mgroup'] == $ibforums->vars['auth_group'])
			{
				$std->Error( array( 'LEVEL' => 1, 'MSG' => 'val_key_present' ) );
			}
			
			if ($member['id'] == "")
			{
				$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_such_user' ) );
			}
			
			$new_pass     = $std->make_password();
			$validate_key = $std->make_password();
			$md5_pass     = md5($new_pass);
			
			//------------------------------------------------------------
			// Update the DB for this member.
			//------------------------------------------------------------
			
			$DB->query("UPDATE ibf_members SET new_pass='$md5_pass', validate_key='$validate_key' WHERE id='".$member['id']."'");
			
			//------------------------------------------------------------
			// Send out the email.
			//------------------------------------------------------------
			
    		$this->email->get_template("lost_pass");
				
			$this->email->build_message( array(
												'NAME'         => $member['name'],
												'PASSWORD'     => $new_pass,
												'MAN_LINK'     => $this->base_url_nosess."?act=Reg&CODE=06",
												'EMAIL'        => $member['email'],
												'ID'           => $member['id'],
												'CODE'         => $validate_key,
											  )
										);
										
			$this->email->subject = $ibforums->lang['lp_subject'].' '.$ibforums->vars['BOARDNAME'];
			$this->email->to      = $member['email'];
			
			$this->email->send_mail();
			
			$std->my_setcookie( 'pass_hash' , '-1', 0 );
			
			$print->redirect_screen( $ibforums->lang['lp_redirect'], 'act=Reg&CODE=06' );
		}
    	
    }
 	
 	/*****************************************************/
	// show_reg_form
	// ------------------
	// Simply shows the registration form, no - really! Thats
	// all it does. It doesn't make the tea or anything.
	// Just the registration form, no more - no less.
	// Unless your server went down, then it's just useless.
	/*****************************************************/   
    
    function show_reg_form() {
    	global $ibforums, $DB, $std;
    	
    	if ($ibforums->vars['no_reg'] == 1)
    	{
    		$std->Error( array( 'LEVEL' => 1, 'MSG' => 'reg_off' ) );
    	}
    	
    	if ($ibforums->vars['reg_auth_type'])
    	{
    		$ibforums->lang['std_text'] .= "<br>" . $ibforums->lang['email_validate_text'];
    	}
    	
    	$this->page_title = $ibforums->lang['registration_form'];
    	$this->nav        = array( $ibforums->lang['registration_form'] );

    	$this->output    .= $this->html->ShowForm( array( 'TEXT' => $ibforums->lang['std_text'], 'RULES' => $ibforums->lang['click_wrap'] ) );
    
    }
    
   	/*****************************************************/
	// create_account
	// ------------------
	// Now this is a really good subroutine. It adds the member
	// to the members table in the database. Yes, really fancy
	// this one. It also finds the time to see if we need to
	// check any email verification type malarky before we
	// can use this brand new account. It's like buying a new
	// car and getting it towed home and being told the keys
	// will be posted later. Although you can't polish this
	// routine while you're waiting.
	/*****************************************************/ 
	
	function create_account() {
		global $ibforums, $std, $DB, $print;
		
		if ($ibforums->vars['no_reg'] == 1)
    	{
    		$std->Error( array( 'LEVEL' => 1, 'MSG' => 'reg_off' ) );
    	}
		
		// Trim off the username and password
		
		$in_username = trim($ibforums->input['UserName']);
		$in_password = trim($ibforums->input['PassWord']);
		$in_email    = strtolower( trim($ibforums->input['EmailAddress']) );
		
		$ibforums->input['EmailAddress_two'] = strtolower( trim($ibforums->input['EmailAddress_two']) );
		
		if ($ibforums->input['EmailAddress_two'] != $in_email)
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'email_addy_mismatch' ) );
		}
		
		//+--------------------------------------------
		//| Check for errors in the input.
		//+--------------------------------------------
		
		if (empty($in_username))    	                 $std->Error( array( LEVEL => 1, MSG => 'no_username' ) );
		if (strlen($in_username) < 3)               	 $std->Error( array( LEVEL => 1, MSG => 'username_short' ) );
		if (strlen($in_username) > 32)              	 $std->Error( array( LEVEL => 1, MSG => 'username_long' ) );
		
		if (empty($in_password))    	                 $std->Error( array( LEVEL => 1, MSG => 'pass_blank' ) );
		if (strlen($in_password) < 3)               	 $std->Error( array( LEVEL => 1, MSG => 'pass_too_short' ) );
		if (strlen($in_password) > 32)                   $std->Error( array( LEVEL => 1, MSG => 'pass_too_long' ) );
		if ($ibforums->input['PassWord_Check'] != $in_password) $std->Error( array( LEVEL => 1, MSG => 'pass_no_match' ) );
		
		if (strlen($in_email) < 6)                       $std->Error( array( LEVEL => 1, MSG => 'invalid_email' ) );
		
		//+--------------------------------------------
		//| Check the email address
		//+--------------------------------------------
		
		$in_email = $std->clean_email($in_email);
		
		if (! $in_email )
		{
			$std->Error( array( LEVEL => 1, MSG => 'invalid_email' ) );
		}
		
		//+--------------------------------------------
		//| Is this name already taken?
		//+--------------------------------------------
		
		$DB->query("SELECT id FROM ibf_members WHERE LOWER(name)='".strtolower($in_username)."'");
		$name_check = $DB->fetch_row();
		
		if ($name_check['id'])
		{
			$std->Error( array( LEVEL => 1, MSG => 'user_exists' ) );
		}
		
		if (strtolower($in_username) == 'guest')
		{
			$std->Error( array( LEVEL => 1, MSG => 'user_exists' ) );
		}
		
		//+--------------------------------------------
		//| Is this email addy taken?
		//+--------------------------------------------
		
		if (! $ibforums->vars['allow_dup_email'] )
		{
			$DB->query("SELECT id FROM ibf_members WHERE email='".$in_email."'");
			$email_check = $DB->fetch_row();
			if ($email_check['id'])
			{
				$std->Error( array( LEVEL => 1, MSG => 'email_exists' ) );
			}
		}
		
		//+--------------------------------------------
		//| Are they in the reserved names list?
		//+--------------------------------------------
		
		if ($ibforums->vars['ban_names'])
		{
			$names = explode( "|" , $ibforums->vars['ban_names'] );
			foreach ($names as $n)
			{
				if (preg_match( "/$n/i", $in_username ))
				{
					$std->Error( array( LEVEL => 1, MSG => 'user_exists' ) );
					break;
				}
			}
		}	
		
		//+--------------------------------------------
		//| Are they banned?
		//+--------------------------------------------
		
		if ($ibforums->vars['ban_ip'])
		{
			$ips = explode( "|", $ibforums->vars['ban_ip'] );
			foreach ($ips as $ip)
			{
				$ip = preg_replace( "/\*/", '.*' , $ip );
				if (preg_match( "/$ip/", $ibforums->input['IP_ADDRESS'] ))
				{
					$std->Error( array( LEVEL => 1, MSG => 'you_are_banned' ) );
				}
			}
		}
		
		if ($ibforums->vars['ban_email'])
		{
			$ips = explode( "|", $ibforums->vars['ban_email'] );
			foreach ($ips as $ip)
			{
				$ip = preg_replace( "/\*/", '.*' , $ip );
				if (preg_match( "/$ip/", $in_email ))
				{
					$std->Error( array( LEVEL => 1, MSG => 'you_are_banned' ) );
				}
			}
		}
		
		
		//+--------------------------------------------
		//| Build up the hashes
		//+--------------------------------------------
		
		$mem_group = $ibforums->vars['member_group'];
		
		//+--------------------------------------------
		//| Are we asking the member or admin to preview?
		//+--------------------------------------------
		
		if ($ibforums->vars['reg_auth_type'])
		{
			$mem_group = $ibforums->vars['auth_group'];
		}
		
		//+--------------------------------------------
		//| Find the highest member id, and increment it
		//| auto_increment not used for guest id 0 val.
		//+--------------------------------------------
		
		$DB->query("SELECT MAX(id) as new_id FROM ibf_members");
		$r = $DB->fetch_row();
		
		$member_id = $r['new_id'] + 1;
		
		$member = array(
						 'id'              => $member_id,
						 'name'            => $in_username,
						 'password'        => $in_password,
						 'email'           => $in_email,
						 'mgroup'          => $mem_group,
						 'posts'           => 0,
						 'avatar'          => 'noavatar',
						 'joined'          => time(),
						 'ip_address'      => $ibforums->input['IP_ADDRESS'],
						 'time_offset'     => $ibforums->vars['time_offset'],
						 'view_sigs'       => 1,
						 'email_pm'        => 1,
						 'view_img'        => 1,
						 'view_avs'        => 1,
						 'allow_post'      => 1,
						 'view_pop'        => 1,
						 'vdirs'           => "in:Inbox|sent:Sent Items",
						 'msg_total'       => 0,
						 'new_msg'         => 0,
					   );
					   
		
					   
		//+--------------------------------------------
		//| Insert into the DB
		//+--------------------------------------------
		
		$member['password'] = md5( $member['password'] );
		
		$db_string = $std->compile_db_string( $member );
		
		$DB->query("INSERT INTO ibf_members (" .$db_string['FIELD_NAMES']. ") VALUES (". $db_string['FIELD_VALUES'] .")");
		
		unset($db_string);
		
		$validate_key = $std->make_password();
		$time         = time();
		
		
		if ( ($ibforums->vars['reg_auth_type'] == 'user') or ($ibforums->vars['reg_auth_type'] == 'admin') ) {
		
			// We want to validate all reg's via email, after email verificiation has taken place,
			// we restore their previous group and remove the validate_key
			
			$DB->query("UPDATE ibf_members SET validate_key='$validate_key', prev_group='".$ibforums->vars['member_group']."' "
			          ."WHERE id='$member_id'");
			
			
			if ( $ibforums->vars['reg_auth_type'] == 'user' )
			{
			
				$this->email->get_template("reg_validate");
				
				$this->email->build_message( array(
													'THE_LINK'     => $this->base_url_nosess."?act=Reg&CODE=03&uid=".urlencode($member_id)."&aid=".urlencode($validate_key),
													'NAME'         => $member['name'],
													'PASSWORD'     => $in_password,
													'MAN_LINK'     => $this->base_url_nosess."?act=Reg&CODE=05",
													'EMAIL'        => $member['email'],
													'ID'           => $member_id,
													'CODE'         => $validate_key,
												  )
											);
											
				$this->email->subject = "Registration at ".$ibforums->vars['board_name'];
				$this->email->to      = $member['email'];
				
				$this->email->send_mail();
				
				$this->output     = $this->html->show_authorise( $member );
				
			}
			else if ( $ibforums->vars['reg_auth_type'] == 'admin' )
			{
				$this->output     = $this->html->show_preview( $member );
			}
			
			$this->page_title = $ibforums->lang['reg_success'];
			
			$this->nav        = array( $ibforums->lang['nav_reg'] );
		}

		else
		{
			
			// We don't want to preview, or get them to validate via email.
			
			$DB->query("UPDATE ibf_stats SET ".
			             "MEM_COUNT=MEM_COUNT+1, ".
			             "LAST_MEM_NAME='" . $member['name'] . "', ".
			             "LAST_MEM_ID='"   . $member['id']   . "'");
			             
			if ($ibforums->vars['new_reg_notify']) {
				
				$date = $std->get_date( time(), 'LONG' );
				
				$this->email->get_template("admin_newuser");
			
				$this->email->build_message( array(
													'DATE'         => $date,
													'MEMBER_NAME'  => $member['name'],
												  )
											);
											
				$this->email->subject = "New Registration at ".$ibforums->vars['board_name'];
				$this->email->to      = $ibforums->vars['email_in'];
				$this->email->send_mail();
			}
				
			$text = $ibforums->lang['done_reg_2'];
			$url  = 'act=Login&CODE=00';
			
			
			$print->redirect_screen( $text, $url );
		}
				
	} 
    
    /*****************************************************/
	// validate_user
	// ------------------
	// Leave a message after the tone, and I'll amuse myself
	// by pulling faces when hearing the message later.
	/*****************************************************/
	
	function validate_user() {
		global $ibforums, $std, $DB, $print;
		
		//------------------------------------------------------------
		// Check for input and it's in a valid format.
		//------------------------------------------------------------
		
		$in_user_id      = trim(urldecode($ibforums->input['uid']));
		$in_validate_key = trim(urldecode($ibforums->input['aid']));
		$in_type         = trim($ibforums->input['type']);
		
		if ($in_type == "")
		{
			$in_type = 'reg';
		}
		
		//------------------------------------------------------------
		
		if (! preg_match( "/^(?:[\d\w]){6,14}$/", $in_validate_key ) )
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'data_incorrect' ) );
		}
		
		//------------------------------------------------------------
		
		if (! preg_match( "/^(?:\d){1,}$/", $in_user_id ) )
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'data_incorrect' ) );
		}
		
		//------------------------------------------------------------
		// Attempt to get the profile of the requesting user
		//------------------------------------------------------------
		
		$DB->query("SELECT id, name, validate_key, prev_group, mgroup, email, new_pass FROM ibf_members WHERE id='$in_user_id'");
		
		if ( !$DB->get_num_rows() )
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'auth_no_mem' ) );
		}
		
		//------------------------------------------------------------
		
		$member = $DB->fetch_row();
		
		//------------------------------------------------------------
		
		if ( $member['id'] == "" )
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'auth_no_mem' ) );
		}
		
		//------------------------------------------------------------
		if ( $member['validate_key'] == "")
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'auth_no_key' ) );
		}
		else if ($member['validate_key'] != $in_validate_key)
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'auth_key_wrong' ) );
		}
		else
		{
			//------------------------------------------------------------
			// Update the member...
			//------------------------------------------------------------
			
			if ($in_type == 'reg')
			{
			
				if (empty($member['prev_group']))
				{
					$member['prev_group'] = $ibforums->vars['member_group'];
				}
				
				$DB->query("UPDATE ibf_members SET mgroup='".$member['prev_group']."', prev_group='', validate_key='', new_pass='' WHERE id='".$member['id']."'");
			
				//------------------------------------------------------------
				// Update the stats...
				//------------------------------------------------------------
			
				$DB->query("UPDATE ibf_stats SET ".
							 "MEM_COUNT=MEM_COUNT+1, ".
							 "LAST_MEM_NAME='" . $member['name'] . "', ".
							 "LAST_MEM_ID='"   . $member['id']   . "'");
							 
				if ($ibforums->vars['new_reg_notify']) {
					
					$date = $std->get_date( time(), 'LONG' );
					
					$this->email->get_template("admin_newuser");
				
					$this->email->build_message( array(
														'DATE'         => $date,
														'MEMBER_NAME'  => $member['name'],
													  )
												);
												
					$this->email->subject = "New Registration at ".$ibforums->vars['board_name'];
					$this->email->to      = $ibforums->vars['email_in'];
					$this->email->send_mail();
				}
			
			}
			else if ($in_type == 'lostpass')
			{
			
				if ($member['new_pass'] == "")
				{
					$std->Error( array( 'LEVEL' => 1, 'MSG' => 'lp_no_pass' ) );
				}
				
				$DB->query("UPDATE ibf_members SET password='".$member['new_pass']."', prev_group='', validate_key='', new_pass='' WHERE id='".$member['id']."'");
			
			}
			else if ($in_type == 'newemail')
			{
				if (empty($member['prev_group']))
				{
					$member['prev_group'] = $ibforums->vars['member_group'];
				}
				
				$DB->query("UPDATE ibf_members SET mgroup='".$member['prev_group']."', prev_group='', validate_key='', new_pass='' WHERE id='".$member['id']."'");
			}
				
			$text = $ibforums->lang['done_reg_2'];
			$url  = 'act=Login&CODE=00';
			
			
			$print->redirect_screen( $text, $url );
		} 
		
	} 
    
    /*****************************************************/
	// show_board_rules
	// ------------------
	// o_O  ^^
	/*****************************************************/
	
	function show_board_rules() {
		global $ibforums, $DB;
		
		$DB->query("SELECT RULES_TEXT from ib_forum_rules WHERE ID='00'");
		$rules = $DB->fetch_row();
		
		$this->output     = $this->html->show_rules($rules);
		$this->page_title = $ibforums->lang['board_rules'];
		$this->nav        = array( $ibforums->lang['board_rules'] );
	
	}
	
	/*****************************************************/
	// show_manual_form
	// ------------------
	// This feature is not available in an auto option
	/*****************************************************/
	
	function show_manual_form($type='reg') {
		global $ibforums;
		
		$this->output     = $this->html->show_dumb_form($type);
		$this->page_title = $ibforums->lang['activation_form'];
		$this->nav        = array( $ibforums->lang['activation_form'] );
	
	}
	
        
}

?>
