<?php

$lang = array (

'gzip_on' => "GZIP Enabled",
'gzip_off' => "GZIP Disabled",




'warn_offline' => "(OFFLINE)",

'all_emoticons' => "Show All",

'jmp_go'  => "Go!",
'forum_jump' => "Forum Jump",

'yes' => "Yes",
'no'  => "No",

'ct_size' => "SIZE",
'ct_lrg'  => "Large",
'ct_sml'  => "Small",
'ct_lest' => "Largest",

'ct_font' => "FONT",
'ct_arial' => "Arial",
'ct_times' => "Times",
'ct_courier' => "Courier",
'ct_impact'  => "Impact",
'ct_geneva'  => "Geneva",

'ct_color'  => "COLOR",
'ct_blue'   => "Blue",
'ct_red'    => "Red",
'ct_purple' => "Purple",
'ct_orange' => "Orange",
'ct_yellow' => "Yellow",
'ct_grey'   => "Gray",
'ct_green'  => "Green",

'msg_full' => "<span id='highlight'>Messenger FULL!</span>",

ba_edit => "Edit",
ba_delete => "Delete",

submit_li					=>	"Log in",
quote_text					=>	"Quote",
pass					=>	"Password",
thanks					=>	"Thanks",
forum_read					=>	"The forum has been marked as read",
guest_stuff					=>	"Welcome Guest",
offline_title					=>	"IBForums Offline Mode",
you_last_visit					=>	"You last visited on:",
code_sample					=>	"Code Sample",
transfer_you					=>	"Please wait while we transfer you...",
M_1					=>	"Jan.",
M_2					=>	"Feb.",
admin_login					=>	"Administrators Log in",
M_3					=>	"Mar.",
D_0					=>	"Sun",
new_posts					=>	"New Posts",
M_4					=>	"April",
D_1					=>	"Mon",
M_5					=>	"May",
D_2					=>	"Tue",
M_6					=>	"June",
you_running_time					=>	"Your last action was on:",
D_3					=>	"Wed",
M_7					=>	"July",
admin_cp					=>	"Admin CP",
D_4					=>	"Thur",
M_8					=>	"Aug.",
D_5					=>	"Fri",
M_9					=>	"Sep.",
your_cp					=>	"Your Control Panel",
D_6					=>	"Sat",
name					=>	"Name",
register					=>	"Register",
'logged_in_as'					=>	"Logged in as: ",
M_10					=>	"Oct.",
M_11					=>	"Nov.",
M_12					=>	"Dec.",
msg_new					=>	"%s new messages",
offline					=>	"Sorry, the boards are offline at the moment",
log_out					=>	"Log Out",
dont_wait					=>	"Or click here if you do not wish to wait",
stand_by					=>	"Please stand by...",
your_messenger					=>	"Your messenger",
log_in					=>	"Log In",
);
?>