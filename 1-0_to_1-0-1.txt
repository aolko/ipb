Upgrading from 1.0.0 to 1.0.1
-----------------------------


Files to update
----------------

index.php

sources/Boards.php
sources/Forums.php
sources/post.php

sources/lib/post_reply_post.php
sources/lib/post_q_reply_post.php
sources/search.php

sources/Admin/ad_member.php


Bugs Fixed
----------

Various Posting bugs fixed
Tighter permission checking when posting
ICQ number not being saved when editing a profile via the ACP
Incorrect error displayed when attempting to access a forum you don't have permission for
Shortened topic titles in board view display showing partial HTML entities (credit Chrisken)
