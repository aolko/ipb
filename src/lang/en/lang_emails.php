<?php

$lang = array (



message					=>	"Message",
title					=>	"Send a page to a friend",
'send_lang'             =>  "Send this email in",
to_name					=>	"Send to (Person's Name):",
redirect					=>	"The email has been sent",
to_email					=>	"Send to (Email Address):",
subject					=>	"Subject",
submit_send					=>	"Send Email",
send_email_to					=>	"Send an email to",
message					=>	"Message",
show_address_text					=>	"You can send this member an email by clicking on the link below. Invision Board does not show email addresses in the HTML code to prevent email addresses being harvested by 'spam bots'.",
send_header					=>	", please fill in this form fully to send an email to",
member_address_title					=>	"Members Email Address",
send_title					=>	"Email Form",
msg_txt					=>	"Note: By using this form, the recipient will be able to view your email address",
subject					=>	"Subject",
email_sent_txt					=>	"Thanks, the email has been successfully sent to",
email_sent					=>	"The email has been sent",

invite_subject					=>	"<#MEMBER_NAME#> thought you would like to see this",
subs_redirect					=>	"You have subscribed to this topic",
invite_redirect					=>	"The email has been sent to  ",
sub_added					=>	"Your subscription has been added",

icq_title					=>	"ICQ Pager",
submit					=>	"Send this message",
aol_title					=>	"AOL Pager",
msg					=>	"Enter your message",
email					=>	"Your Email",
name					=>	"Your Name",
forum_jump => "Forum Jump",
);
?>