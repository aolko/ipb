<?php

/*
+--------------------------------------------------------------------------
|   IBFORUMS v1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001,2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > CSS management functions
|   > Module written by Matt Mecham
|   > Date started: 4th April 2002
|
|	> Module Version Number: 1.0.0
+--------------------------------------------------------------------------
*/


$idx = new ad_settings();


class ad_settings {

	var $base_url;

	function ad_settings() {
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;

		switch($IN['code'])
		{
			case 'wrapper':
				$this->list_sheets();
				break;
				
			case 'add':
				$this->do_form('add');
				break;
				
			case 'edit':
				$this->do_form('edit');
				break;
				
			case 'doadd':
				$this->save_wrapper('add');
				break;
				
			case 'doedit':
				$this->save_wrapper('edit');
				break;
				
			case 'remove':
				$this->remove();
				break;
				
			case 'export':
				$this->export();
				break;
			
			//-------------------------
			default:
				$this->list_sheets();
				break;
		}
		
	}
	
	function export()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing CSS ID, go back and try again");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT * from ibf_css WHERE cssid='".$IN['id']."'");
		
		if ( ! $row = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the information from the database");
		}
		
		//+-------------------------------
		
		$archive_dir = $INFO['base_dir']."/archive_out";
		
		$css_file    = $INFO['base_dir']."/style_sheets/stylesheet_{$row['cssid']}.css";
		
		if (!is_dir($archive_dir))
		{
			$ADMIN->error("Could not locate $archive_dir, is the directory there?");
		}
		
		if (!is_writeable($archive_dir))
		{
			$ADMIN->error("Cannot write in $archive_dir, CHMOD via FTP to 0755 or 0777 to enable this script to write into it. IBF cannot do this for you");
		}
		
		if (!file_exists($css_file))
		{
			$ADMIN->error("Cannot locate $css_file, please ensure that the file is in the proper directory");
		}
		
		//+-------------------------------
		// Attempt to copy the files to the
		// working directory...
		//+-------------------------------
		
		$l_name = preg_replace( "/\s{1,}/", "_", $row['css_name'] );
		
		$file_name = "css-".$l_name.".css";
		
		if ( ! copy($css_file, $archive_dir."/".$file_name) )
		{
			$ADMIN->error("COPY FAILED ON STYLE SHEET $file_name, maybe the script has insufficient permissions?");
		}
		
		@chmod($archive_dir."/".$file_name, 0777);
		
		$ADMIN->done_screen("Style Sheet Export Created<br><br>You can download the CSS resource <a href='archive_out/$file_name' target='_blank'>here</a>", "Manage Style Sheets", "act=style" );
		
		
	}
	
	
	//-------------------------------------------------------------
	// REMOVE WRAPPERS
	//-------------------------------------------------------------
	
	function remove()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		//+-------------------------------
		
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing stylesheet ID, go back and try again");
		}
		
		$DB->query("DELETE FROM ibf_css WHERE cssid='".$IN['id']."'");
		
		@unlink( $root_path."style_sheets/stylesheet_{$IN['id']}.css" );
		
		$std->boink_it($SKIN->base_url."&act=style");
			
		exit();
		
		
	}
	
	
	
	//-------------------------------------------------------------
	// ADD / EDIT WRAPPERS
	//-------------------------------------------------------------
	
	function save_wrapper( $type='add' )
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		//+-------------------------------
		
		if ($type == 'edit')
		{
			if ($IN['id'] == "")
			{
				$ADMIN->error("You must specify an existing CSS ID, go back and try again");
			}
			
			$file = $root_path."style_sheets/stylesheet_".$IN['id'].".css";
			
			if ( ! is_writeable($file) )
			{
				$ADMIN->error("IBF does not have permission to write to that file. Try setting the CHMOD on $file to 0777 and try again. You can do this via FTP");
			}
			
		}
		
		if ($IN['name'] == "")
		{
			$ADMIN->error("You must specify a name for this stylesheet");
		}
		
		if ($IN['css'] == "")
		{
			$ADMIN->error("You can't have an empty stylesheet, can you?");
		}
		
		$css = stripslashes($HTTP_POST_VARS['css']);
		
		$barney = array( 'css_name'     => stripslashes($HTTP_POST_VARS['name']),
						 'css_text'     => $css,
					   );
					   
		if ($type == 'add')
		{
			$db_string = $DB->compile_db_insert_string( $barney );
			
			$DB->query("INSERT INTO ibf_css (".$db_string['FIELD_NAMES'].") VALUES(".$db_string['FIELD_VALUES'].")");
			
			$new_id = $DB->get_insert_id();
			
			$nfile = $root_path."style_sheets/stylesheet_".$new_id.".css";
			
			if ($fh = fopen( $nfile, 'w' ) )
			{
				fwrite( $fh, $css, strlen($css) );
				fclose( $fh );
				
				@chmod( $nfile, 0777 );
			}
			else
			{
				$DB->query("DELETE FROM ibf_css WHERE cssid='$new_id'");
			
				$ADMIN->error("Could not prepare $file for creation - please check the CHMOD value of the 'style_sheets' directory and where possible CHMOD to 0777 via FTP");
			}
			
			$std->boink_it($SKIN->base_url."&act=style");
			
			exit();
			
		}
		else
		{
			$db_string = $DB->compile_db_update_string( $barney );
			
			if ($fh = fopen( $file, 'w' ) )
			{
				fwrite( $fh, $css, strlen($css) );
				fclose( $fh );
				
				@chmod( $file, 0777 );
				
				$DB->query("UPDATE ibf_css SET $db_string WHERE cssid='".$IN['id']."'");
			
				$ADMIN->done_screen("Stylesheet updated", "Manage Style Sheets", "act=style" );
			}
			else
			{
				
				$ADMIN->error("Could not prepare $file for writing - please check the CHMOD value of the 'style_sheets' directory");
			}
			
		}
		
		
	}
	
	//-------------------------------------------------------------
	// ADD / EDIT WRAPPERS
	//-------------------------------------------------------------
	
	function do_form( $type='add' )
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		//+-------------------------------
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing wrapper ID, go back and try again");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT cssid, css_name FROM ibf_css WHERE cssid='".$IN['id']."'");
		
		if ( ! $cssinfo = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the CSS details from the database");
		}
		
		//+-------------------------------
		
		$file = $root_path."style_sheets/stylesheet_".$IN['id'].".css";
		
		if ( ! file_exists($file) )
		{
			$ADMIN->error("Could not load the stylesheet. No file found at $file");
		}
		
		if ( ! is_readable($file) )
		{
			$ADMIN->error("IBF does not have permission to read from that file. Try setting the CHMOD on $file to 0777 and try again. You can do this via FTP");
		}
		
		//+-------------------------------
		
		$fh  = fopen( $file, 'r' );
		$css = fread( $fh, filesize($file) );
		fclose($fh);
		
		if ($type == 'add')
		{
			$code = 'doadd';
			$button = 'Create StyleSheet';
			$cssinfo['css_name'] = $cssinfo['css_name'].".2";
		}
		else
		{
			// Availability test
		
			if ( ! is_writeable($file) )
			{
				$ADMIN->error("IBF does not have permission to write to that file. Try setting the CHMOD on $file to 0777 and try again. You can do this via FTP");
			}
			
			$code = 'doedit';
			$button = 'Edit Stylesheet';
		}
		
		//+-------------------------------
	
		$ADMIN->page_detail = "You may use CSS fully when adding or editing stylesheets.";
		$ADMIN->page_title  = "Manage Style Sheets";
		
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->js_no_specialchars();
		
		$ADMIN->html .= $SKIN->start_form( array( 1 => array( 'code'  , $code      ),
												  2 => array( 'act'   , 'style'      ),
												  3 => array( 'id'    , $IN['id']   ),
									     ), "theAdminForm", "onSubmit=\"return no_specialchars('csssheet')\""      );
									     
		//+-------------------------------
		
		$SKIN->td_header[] = array( "&nbsp;"  , "20%" );
		$SKIN->td_header[] = array( "&nbsp;"  , "80%" );

		//+-------------------------------
		
		$ADMIN->html .= $SKIN->start_table( $button );
		
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"Stylesheet Title",
													$SKIN->form_input('name', $cssinfo['css_name']),
									     )      );
									     
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"Content",
													$SKIN->form_textarea('css', $css, "70", "30"),
									     )      );
												 
		$ADMIN->html .= $SKIN->end_form($button);
										 
		$ADMIN->html .= $SKIN->end_table();
		
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->output();
		
		
	}
	
	//-------------------------------------------------------------
	// SHOW STYLE SHEETS
	//-------------------------------------------------------------
	
	function list_sheets()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		$form_array = array();
		$show_array = array();
	
		$ADMIN->page_detail = "You may add/edit and remove stylesheets.<br><br>Style Sheets are CSS files. This is where you can change the colours, fonts and font sizes throughout the board.";
		$ADMIN->page_title  = "Manage Stylesheets";
		
		//+-------------------------------
		
		$SKIN->td_header[] = array( "Title"  , "40%" );
		$SKIN->td_header[] = array( "Allocation"   , "30%" );
		$SKIN->td_header[] = array( "Export" , "10%" );
		$SKIN->td_header[] = array( "Edit"   , "10%" );
		$SKIN->td_header[] = array( "Remove" , "10%" );
		
		//+-------------------------------
		
		$DB->query("SELECT DISTINCT(c.cssid), c.css_name, s.sname from ibf_css c, ibf_skins s WHERE s.css_id=c.cssid ORDER BY c.css_name ASC");
		
		$used_ids = array();
		
		if ( $DB->get_num_rows() )
		{
		
			$ADMIN->html .= $SKIN->start_table( "Current Stylesheets In Use" );
			
			while ( $r = $DB->fetch_row() )
			{
			
				$show_array[ $r['cssid'] ] .= stripslashes($r['sname'])."<br>";
			
				if ( in_array( $r['cssid'], $used_ids ) )
				{
					continue;
				}
				
				$ADMIN->html .= $SKIN->add_td_row( array( "<b>".stripslashes($r['css_name'])."</b>",
														  "<#X-{$r['cssid']}#>",
														  "<center><a href='".$SKIN->base_url."&act=style&code=export&id={$r['cssid']}'>Export</a></center>",
														  "<center><a href='".$SKIN->base_url."&act=style&code=edit&id={$r['cssid']}'>Edit</a></center>",
														  "<i>Deallocate before removing</i>",
												 )      );
												   
				$used_ids[] = $r['cssid'];
				
				$form_array[] = array( $r['cssid'], $r['css_name'] );
				
			}
			
			foreach( $show_array as $idx => $string )
			{
				$string = preg_replace( "/<br>$/", "", $string );
				
				$ADMIN->html = preg_replace( "/<#X-$idx#>/", "$string", $ADMIN->html );
			}
			
			$ADMIN->html .= $SKIN->end_table();
		}
		
		if ( count($used_ids) > 0 )
		{
		
			$DB->query("SELECT cssid, css_name FROM ibf_css WHERE cssid NOT IN(".implode(",",$used_ids).")");
		
			if ( $DB->get_num_rows() )
			{
			
				$SKIN->td_header[] = array( "Title"  , "70%" );
				$SKIN->td_header[] = array( "Export" , "10%" );
				$SKIN->td_header[] = array( "Edit"   , "10%" );
				$SKIN->td_header[] = array( "Remove" , "10%" );
			
				$ADMIN->html .= $SKIN->start_table( "Current Unallocated Stylesheets" );
				
				$ADMIN->html .= $SKIN->js_checkdelete();
				
				
				while ( $r = $DB->fetch_row() )
				{
					
					$ADMIN->html .= $SKIN->add_td_row( array( "<b>".stripslashes($r['css_name'])."</b>",
					 										  "<center><a href='".$SKIN->base_url."&act=style&code=export&id={$r['cssid']}'>Export</a></center>",
															  "<center><a href='".$SKIN->base_url."&act=style&code=edit&id={$r['cssid']}'>Edit</a></center>",
															  "<center><a href='javascript:checkdelete(\"act=style&code=remove&id={$r['cssid']}\")'>Remove</a></center>",
													 )      );
													 
					$form_array[] = array( $r['cssid'], $r['css_name'] );
													   
				}
				
				$ADMIN->html .= $SKIN->end_table();
			}
		}
		
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->start_form( array( 1 => array( 'code'  , 'add'      ),
												  2 => array( 'act'   , 'style'    ),
									     )      );
		
		$SKIN->td_header[] = array( "&nbsp;"  , "40%" );
		$SKIN->td_header[] = array( "&nbsp;"  , "60%" );
		
		$ADMIN->html .= $SKIN->start_table( "Create New Stylesheet" );
			
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->add_td_row( array( "<b>Base new stylesheet on...</b>" ,
										  		  $SKIN->form_dropdown( "id", $form_array)
								 )      );
		
		$ADMIN->html .= $SKIN->end_form("Create new stylesheet");
										 
		$ADMIN->html .= $SKIN->end_table();
		
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->output();
	
	}
	
	
}


?>