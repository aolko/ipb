<?php

$lang = array (


'search_redirect' => "your search has been completed, please stand by as we take you to the search results.",

'search_pages'  => "Pages:",
'search_title'   => "Search Engine",
'key_search'    => "Search by Keywords",
'mem_search'    => "Search by Member Name",
'keysearch_text' => "<b>Simple Search:</b> Enter a keyword or phrase to search by<br><b>Advanced Search:</b> Join keywords with AND, OR to help define the search.<br>Asterisks '*' act as wildcards. (*bar* matches <i>foobarbaz</i>, etc).",
'match_name_ex'  => "Match Exact Name",
'match_name_pa'  => "Match Partial Name",
'keywords_title' => "Search Keywords",

'search_options' => "Search Options",
'search_forums'  => "Search in forum",
'search_cats'    => "Search in category",
'search_where'   => "Search Where?",
'search_refine'  => "Refine Search",

'search_results' => "Search Results",
'search_form'    => "Search Form",

'h_forum_name'   => "Forum",

'se_simple'      => "Simple Search",
'se_advanced'    => "Advanced",

'in_posts'       => "Search entire post",
'in_topics'      => "Search titles only",

'search_from'    => "Search posts from..",

'do_search'      => "Perform the search",

'today' => "Today",
'this_week' => "Last 7 days",
'this_month' =>  "Last 30 days",
'this_year'  =>  "Last 365 days",
'ever'       => "Any date",
'older'      => "Older",
'newer'      => "Newer",
'and'        => "and",

'sort_results'  => "Sort results by...",

'last_date'  => "Last Posting Date",
'number_topics'  => "Number of Replies",
'poster_name' => "Poster Name",
'forum_name'  => "Forum Name",

'in'  => "in",

'ascending' => 'ascending order',
'descending' => 'descending order',

'order'  => 'order',

'active_topics' => "Active Topics",

'active_no_topics'   => "There were no active topics during those date ranges",

'active_js_error' => "You cannot search for active topics using those dates. That'd just be silly. The first date MUST be older than the second date.",

'active_st_text' => 'Show me active topics posted from (oldest date):',
'active_end_text' => "to (newest date):",

'active_yesterday' => "Yesterday",
'active_today'     => "Today",
'active_week'      => "Last Week",
'active_month'     => "Last Month",
'active_days'      => "days ago",

);
?>