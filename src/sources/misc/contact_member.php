<?php

/*
+--------------------------------------------------------------------------
|   IBFORUMS v1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001,2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > ICQ / AIM / EMAIL functions
|   > Module written by Matt Mecham
|   > Date started: 28th February 2002
|
|	> Module Version Number: 1.0.0
+--------------------------------------------------------------------------
*/


$idx = new Contact;

class Contact {

    var $output    = "";
    var $base_url  = "";
    var $html      = "";
    
    var $nav       = array();
    var $page_title= "";
    var $email     = "";

    /***********************************************************************************/
	//
	// Our constructor, load words, load skin
	//
	/***********************************************************************************/
    
    function Contact() {
    
        global $ibforums, $DB, $std, $print, $skin_universal;
        
        
        // What to do?
        
        switch($ibforums->input['act']) {
        	case 'Mail':
        		$this->mail_member();
        		break;
        	case 'AOL':
        		$this->show_aim();
        		break;
        	case 'ICQ':
        		$this->show_icq();
        		break;
        	case 'Invite':
        		$this->invite_member();
        		break;
        	default:
        		$std->Error( array( 'LEVEL' => 1, 'MSG' => 'invalid_use' ) );
        		break;
        }
        
        $print->add_output("$this->output");
        $print->do_output( array( 'TITLE' => $this->page_title, 'JS' => 0, NAV => $this->nav ) );
        
	}
     
     
    //****************************************************************/
	// AOL CONSOLE:
	//
	//****************************************************************/
        
        
	function show_aim() {
		global $ibforums, $DB, $std, $print;
		
		require "./Skin/".$ibforums->skin_id."/skin_emails.php";

		$ibforums->lang    = $std->load_words($ibforums->lang, 'lang_emails', $ibforums->lang_id );

		$this->html    = new skin_emails();
		
		//----------------------------------
	
		if (empty($ibforums->member['id']))
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_guests' ) );
		}
	
		if ( empty($ibforums->input['MID']) )
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'invalid_use' ) );
		}
		
		//----------------------------------
		
		if (! preg_match( "/^(\d+)$/" , $ibforums->input['MID'] ) )
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'invalid_use' ) );
		}
		
		//----------------------------------
		
		$DB->query("SELECT name, id, aim_name from ibf_members WHERE id='".$ibforums->input['MID']."'");

		$member = $DB->fetch_row();
		
		//----------------------------------
		
		if (! $member['id'] )
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_such_user' ) );
		}
		
		//----------------------------------
		
		if (! $member['aim_name'] )
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_aol' ) );
		}
		
		$member['aim_name'] = str_replace(" ", "", $member['aim_name']);
		
		//----------------------------------
		
		$print->pop_up_window( "AOL CONSOLE", $this->html->aol_body( array( 'AOLNAME' => $member['aim_name'] ) ) );
	
	}
	
	//****************************************************************/
	// ICQ CONSOLE:
	//
	//****************************************************************/
	
	
	function show_icq() {
		global $ibforums, $DB, $std, $print;
		
		require "./Skin/".$ibforums->skin_id."/skin_emails.php";

		$ibforums->lang    = $std->load_words($ibforums->lang, 'lang_emails', $ibforums->lang_id);

		$this->html    = new skin_emails();
		
		//----------------------------------
	
		if (empty($ibforums->member['id'])) {
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_guests' ) );
		}
	
		if ( empty($ibforums->input['MID']) )
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'invalid_use' ) );
		}
		
		//----------------------------------
		
		if (! preg_match( "/^(\d+)$/" , $ibforums->input['MID'] ) )
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'invalid_use' ) );
		}
		
		//----------------------------------
		
		$DB->query("SELECT name, id, icq_number from ibf_members WHERE id='".$ibforums->input['MID']."'");

		$member = $DB->fetch_row();
		
		//----------------------------------
		
		if (! $member['id'] )
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_such_user' ) );
		}
		
		//----------------------------------
		
		if (! $member['icq_number'] )
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_icq' ) );
		}
		
		//----------------------------------
		
		$html  = $this->html->pager_header( array( $ibforums->lang['icq_title'] ) );
		
		$html .= $this->html->icq_body( array( 'UIN' => $member['icq_number'] ) );
		
		$html .= $this->html->end_table();
		
		$print->pop_up_window( "ICQ CONSOLE", $html );
	
	
	}
	
	//****************************************************************/
	// MAIL MEMBER:
	//
	// Handles the routines called by clicking on the "email" button when
	// reading topics
	//****************************************************************/
	
	
	function mail_member() {
		global $ibforums, $DB, $std, $print;
	
		require "./sources/lib/emailer.php";
		$this->email = new emailer();
		
		//----------------------------------
		
		require "./Skin/".$ibforums->skin_id."/skin_emails.php";

		$ibforums->lang    = $std->load_words($ibforums->lang, 'lang_emails', $ibforums->lang_id );

		$this->html    = new skin_emails();
		
		//----------------------------------
	
		if (empty($ibforums->member['id']))
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_guests' ) );
		}
		
		//----------------------------------
		
		if ($ibforums->input['CODE'] == '01')
		{
		
			// Send the email, yippee
			
			if ( empty($ibforums->input['to']) )
			{
				$std->Error( array( 'LEVEL' => 1, 'MSG' => 'invalid_use' ) );
			}
			
			//----------------------------------
			
			if (! preg_match( "/^(\d+)$/" , $ibforums->input['to'] ) )
			{
				$std->Error( array( 'LEVEL' => 1, 'MSG' => 'invalid_use' ) );
			}
			
			//----------------------------------
			
			$DB->query("SELECT name, id, email, hide_email from ibf_members WHERE id='".$ibforums->input['to']."'");
	
			$member = $DB->fetch_row();
			
			//----------------------------------
			
			if (! $member['id'] )
			{
				$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_such_user' ) );
			}
			
			/*if ($member['id'] == 1)
			{
				$std->Error( array( 'LEVEL' => 1, 'MSG' => 'private_email' ) );
			}*/
			
			//----------------------------------
			
			$check_array = array ( 
								   'message'   =>  'no_message',
								   'subject'   =>  'no_subject'
								 );
							 
			foreach ($check_array as $input => $msg)
			{
				if (empty($ibforums->input[$input]))
				{
					$std->Error( array( LEVEL => 1, MSG => $msg) );
				}
			}
			
			$this->email->get_template("email_member");
				
			$this->email->build_message( array(
												'MESSAGE'     => $ibforums->input['message'],
												'MEMBER_NAME' => $member['name'],
												'FROM_NAME'   => $ibforums->member['name']
											  )
										);
										
			$this->email->subject = $ibforums->input['subject'];
			$this->email->to      = $member['email'];
			$this->email->from    = $ibforums->member['email'];
			$this->email->send_mail();
			
			$forum_jump = $std->build_forum_jump();
		    $forum_jump = preg_replace( "!#Forum Jump#!", $ibforums->lang['forum_jump'], $forum_jump);
			
			$this->output  = $this->html->sent_screen($member['name']);
			
			$this->output .= $this->html->forum_jump($forum_jump);
			
			$this->page_title = $ibforums->lang['email_sent'];
			$this->nav        = array( $ibforums->lang['email_sent'] );
			
		}
		else
		{
			// Show the form, booo...
			
			if ( empty($ibforums->input['MID']) )
			{
				$std->Error( array( 'LEVEL' => 1, 'MSG' => 'invalid_use' ) );
			}
			
			//----------------------------------
			
			if (! preg_match( "/^(\d+)$/" , $ibforums->input['MID'] ) )
			{
				$std->Error( array( 'LEVEL' => 1, 'MSG' => 'invalid_use' ) );
			}
			
			//----------------------------------
			
			$DB->query("SELECT name, id, email, hide_email from ibf_members WHERE id='".$ibforums->input['MID']."'");
	
			$member = $DB->fetch_row();
			
			//----------------------------------
			
			if (! $member['id'] )
			{
				$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_such_user' ) );
			}
			
			if ($member['hide_email'] == 1)
			{
				$std->Error( array( 'LEVEL' => 1, 'MSG' => 'private_email' ) );
			}
			
			//----------------------------------
			
			$this->output = $ibforums->vars['use_mail_form']
						  ? $this->html->send_form(
													  array(
															  'NAME'   => $member['name'],
															  'TO'     => $member['id'],
														   )
						  						   )
						  : $this->html->show_address(
						  							  array(
															  'NAME'    => $member['name'],
															  'ADDRESS' => $member['email'],
														   )
													 );
													 
			$this->page_title = $ibforums->lang['member_address_title'];
			$this->nav        = array( $ibforums->lang['member_address_title'] );

		}
		
	}
        		
        		
        		
        		
}

?>





