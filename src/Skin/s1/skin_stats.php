<?php

class skin_stats {



function page_title($title) {
global $ibforums;
return <<<EOF
    <table cellpadding='0' cellspacing='0' border='0' width='{$ibforums->skin['tbl_width']}' align='center'>
      <tr>
         <td valign='middle' align='left'><span id='pagetitle'>{$title}</td>
      </tr>
     </table>
EOF;
}

function group_strip( $group ) {
global $ibforums;
return <<<EOF
<table cellpadding='0' cellspacing='0' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
        <tr>
            <td>
              <table cellpadding='5' cellspacing='1' border='0' width='100%'>
                <tr>
                   <td colspan='4' id='titlemedium' align='center' background='{$ibforums->vars['img_url']}/tile_sub.gif'>$group</td>
                </tr>
                <tr>
                   <td width='30%' align='left' id='category'   valign='middle'>{$ibforums->lang['leader_name']}</td>
                   <td width='40%' align='center' id='category' valign='middle'>{$ibforums->lang['leader_forums']}</td>
                   <td align='center' width='25%' id='category' valign='middle'>{$ibforums->lang['leader_location']}</td>
                   <td align='center' width='5%' id='category' valign='middle'>&nbsp;</td>
                </tr>
EOF;
}

function leader_row($info, $forums) {
global $ibforums;
return <<<EOF
                <tr>
                   <td align='left' id='row1' valign='middle'><a href='{$ibforums->base_url}&act=Profile&MID={$info['id']}'>{$info['name']}</a></td>
                   <td align='center' id='row1' valign='middle'>$forums</td>
                   <td align='center' id='row1' valign='middle'>{$info['location']}</td>
                   <td align='center' id='row2' valign='middle'>{$info['msg_icon']}</td>
                </tr>
EOF;
}

function close_strip() {
global $ibforums;
return <<<EOF
				</table>
			  </td>
			 </tr>
			</table>
		    <br>
EOF;
}

function top_poster_header() {
global $ibforums;
return <<<EOF
<table cellpadding='0' cellspacing='0' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
        <tr>
            <td>
              <table cellpadding='5' cellspacing='1' border='0' width='100%'>
                <tr>
                   <td width='30%' align='left' background='{$ibforums->vars['img_url']}/tile_sub.gif' id='titlemedium' valign='middle'>{$ibforums->lang['member']}</td>
                   <td width='20%' align='center' background='{$ibforums->vars['img_url']}/tile_sub.gif' id='titlemedium' valign='middle'>{$ibforums->lang['member_joined']}</td>
                   <td align='center' width='15%' background='{$ibforums->vars['img_url']}/tile_sub.gif' id='titlemedium' valign='middle'>{$ibforums->lang['member_posts']}</td>
                   <td align='center' width='15%' background='{$ibforums->vars['img_url']}/tile_sub.gif' id='titlemedium' valign='middle'>{$ibforums->lang['member_today']}</td>
                   <td align='center' width='20%' background='{$ibforums->vars['img_url']}/tile_sub.gif' id='titlemedium' valign='middle'>{$ibforums->lang['member_percent']}</td>
                </tr>
EOF;
}

function top_poster_row($info) {
global $ibforums;
return <<<EOF
                <tr>
                   <td align='left' id='row1' valign='middle'><a href='{$ibforums->base_url}&act=Profile&MID={$info['id']}'>{$info['name']}</a></td>
                   <td align='center' id='row1' valign='middle'>{$info['joined']}</td>
                   <td align='center' id='row1' valign='middle'>{$info['posts']}</td>
                   <td align='center' id='row2' valign='middle'>{$info['tpost']}</td>
                   <td align='center' id='row2' valign='middle'>{$info['today_pct']}%</td>
                </tr>
EOF;
}

function top_poster_footer($info) {
global $ibforums;
return <<<EOF
                <tr>
                   <td colspan='5' align='center' id='titlemedium' valign='middle'>{$ibforums->lang['total_today']} $info</td>
                </tr>
              </table>
            </td>
           </tr>
         </table>
EOF;
}

function top_poster_no_info() {
global $ibforums;
return <<<EOF
                <tr>
                   <td colspan='5' align='center' id='row1' valign='middle'>{$ibforums->lang['no_info']}</td>
                </tr>
EOF;
}


}
?>