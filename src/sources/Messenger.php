<?php

/*
+--------------------------------------------------------------------------
|   IBFORUMS v1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001,2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > Messenger functions
|   > Module written by Matt Mecham
|   > Date started: 26th February 2002
|
|	> Module Version Number: 1.0.0
+--------------------------------------------------------------------------
*/


$idx = new Messenger;

class Messenger {

    var $output     = "";
    var $page_title = "";
    var $nav        = array();
    var $html       = "";
    var $email      = "";
    
    var $msg_stats  = array();
    var $prefs      = "";
    
    var $member     = array();
    var $m_group    = array();
    
    var $to_mem     = array();
    
    var $jump_html  = "";
    
    var $vid        = "in";
    var $mem_groups = array();
    var $mem_titles = array();
    
    var $parser     = "";
    
    var $cp_html    = "";
    
    function Messenger() {
    	global $ibforums, $DB, $std, $print;
    	
    	//--------------------------------------------
    	// Make sure our code number is numerical only
    	//--------------------------------------------
    	
    	if ($ibforums->input['CODE'] == "") $ibforums->input['CODE'] = 00;
    	
    	//--------------------------------------------
    	// Require the HTML and language modules
    	//--------------------------------------------
    	
		$ibforums->lang = $std->load_words($ibforums->lang, 'lang_msg', $ibforums->lang_id);
		$ibforums->lang = $std->load_words($ibforums->lang, 'lang_ucp'   , $ibforums->lang_id);
    	
    	//--------------------------------------------
    	
    	require "./Skin/".$ibforums->skin_id."/skin_msg.php";
    	
    	$this->html = new skin_msg();
    	
    	//--------------------------------------------
    	
    	require "./Skin/".$ibforums->skin_id."/skin_ucp.php";
    	
    	$this->cp_html = new skin_ucp();
    	
    	//--------------------------------------------
    	
    	$this->base_url        = "{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}";
    	$this->base_url_nosess = "{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}";
    	
    	//--------------------------------------------
    	// Check viewing permissions, etc
		//--------------------------------------------
		
		$this->member  = $ibforums->member;
		
		if (empty($this->member['g_use_pm'])) {
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_use_messenger' ) );
		}
		
		if (empty($this->member['id'])) {
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_guests' ) );
		}
		
		//--------------------------------------------
    	// Get the member stats data
    	//--------------------------------------------
    	
    	$DB->query("SELECT vdirs, msg_total, new_msg, msg_msg_id FROM ibf_members WHERE id='".$this->member['id']."'");
    	$this->msg_stats = $DB->fetch_row();
    	
    	//--------------------------------------------
    	// Do a little set up, do a litle dance, get
    	// down tonight! *boogie*
    	//--------------------------------------------
    	
    	$this->jump_html = "<select name='VID' class='forminput'>\n";
    	
    	$this->msg_stats['dir_data'] = array();
    	
    	// Do we have VID?
    	// No, it's just the way we walk! Haha, etc.
    	
    	if ($ibforums->input['VID'])
    	{
    		$this->vid = $ibforums->input['VID'];
    	}
    	
    	if (empty($this->msg_stats['vdirs']))
    	{
    		$this->msg_stats['vdirs'] = "in:Inbox|sent:Sent Items";
    	}
    	
    	
    	foreach( explode( "|", $this->msg_stats['vdirs'] ) as $dir )
    	{
    		list ($id, $real) = explode( ":", $dir );
    		
    		if (empty($id))
    		{
    			continue;
    		}
    		
    		$this->msg_stats['dir_data'][] = array( 'id' => $id, 'real' => $real );
    		
    		if ($this->vid == $id)
    		{
    			$this->msg_stats['current_dir'] = $real;
    			$this->msg_stats['current_id']  = $id;
    			$this->jump_html .= "<option value='$id' selected>$real</option>\n";
    		}
    		else
    		{
    			$this->jump_html .= "<option value='$id'>$real</option>\n";
    		}
    	}
    	
    	$this->jump_html .= "<!--EXTRA--></select>\n\n";
		
		
		
    	$print->add_output( $this->cp_html->Menu_bar($this->base_url) );;
    	
    	
    	//--------------------------------------------
    	// What to do?
    	//--------------------------------------------
    	
    	
    	switch($ibforums->input['CODE']) {
    		case '01':
    			$this->msg_list();
    			break;
    		case '02':
    			$this->contact();
    			break;
    		case '03':
    			$this->view_msg();
    			break;
    		case '04';
    			$this->send();
    			break;
    		case '05':
    			$this->delete();
    			break;
    		case '06':
    			$this->multi_act();
    			break;
    		case '07':
    			$this->prefs();
    			break;
    		case '08':
    			$this->do_prefs();
    			break;
    		case '09':
    			$this->add_member();
    			break;
    		case '10':
    			$this->del_member();
    			break;
    		case '11':
    			$this->edit_member();
    			break;
    		case '12':
    			$this->do_edit();
    			break;
    		case '14':
    			$this->archive();
    			break;
    		case '15':
    			$this->do_archive();
    			break;
    		case '99':
    			$this->pm_popup();
    			break;
    		default:
    			$this->msg_list();
    			break;
    	}
    	
    	// If we have any HTML to print, do so...
    	
    	$fj = $std->build_forum_jump();
		$fj = preg_replace( "!#Forum Jump#!", $ibforums->lang['forum_jump'], $fj);
		
		$this->output .= $this->cp_html->CP_end();
		
		$this->output .= $this->cp_html->forum_jump($fj);
    	
    	$print->add_output("$this->output");
        $print->do_output( array( 'TITLE' => $this->page_title, 'JS' => 0, NAV => $this->nav ) );
    		
 	}
 	
 	/**********************************************************/
 	// PM Pop up:
 	//
 	// Simpy display the pop up window
 	/**********************************************************/
 	
 	function pm_popup() {
 	
 		global $DB, $std, $print, $ibforums;
 		
 		// Get the last message stuff
 		
 		$DB->query("SELECT m.name, msg.title, msg.msg_date, msg.from_id FROM ibf_members m, ibf_messages msg ".
 				   "WHERE msg.member_id='".$ibforums->member['id']."' AND msg_id='".$this->msg_stats['msg_msg_id']."' ".
 				   "AND m.id=msg.from_id");
 				   
 		$row = $DB->fetch_row();
 				   
 		// Fix up the text string...
 		
 		$row['msg_date'] = $std->get_date( $row['msg_date'], 'LONG' );
 		
 		$text = preg_replace( "/<#NAME#>/" , $row['name']    , $ibforums->lang['pmp_string'] );
 		$text = preg_replace( "/<#TITLE#>/", $row['title']   , $text );
 		$text = preg_replace( "/<#DATE#>/" , $row['msg_date'], $text );
 		
 		$html = $this->html->pm_popup($text);
 		
 		$print->pop_up_window( "PM", $html );
 	
 	}
 	
 	
 	/**********************************************************/
 	// ARCHIVE:
 	//
 	// Allows a user to archive and email a HTML file
 	/**********************************************************/
 	
 	function archive() {
 		global $ibforums, $DB, $std, $print;
 		
 		$this->jump_html = preg_replace("/<!--EXTRA-->/", "<option value='all'>".$ibforums->lang['all_folders']."</option>", $this->jump_html );
 		
 		$this->output .= $this->html->archive_form( $this->jump_html );
 		
 		$this->page_title = $ibforums->lang['t_welcome'];
 		$this->nav        = array( "<a href='".$this->base_url."&act=UserCP&CODE=00'>".$ibforums->lang['t_title']."</a>" );
 		
 	}
 	
 	function do_archive() {
 		global $ibforums, $DB, $std, $print;
 		
 		require "./sources/lib/emailer.php";
		
		$this->email = new emailer();
 		
 		//----------------------------------------
 		// Did we specify a folder, or choose all?
 		//----------------------------------------
 		
 		$folder_query  = "";
 		$msg_ids       = array();
 		
 		if ($ibforums->input['VID'] != 'all')
 		{
 			$folder_query = " AND vid='".$ibforums->input['VID']."'";
 		}
 		
 		if ( $ibforums->input['dateline'] == 'all' )
 		{
 			$time_cut = 0;
 		}
 		else
 		{
 			$time_cut = time() - ($ibforums->input['dateline'] * 60 * 60 *24);
 		}
 		
 		//----------------------------------------
 		// Check the input...
 		//----------------------------------------
 		
 		$ibforums->input['number'] = preg_replace( "/^(\d+)$/", "\\1", $ibforums->input['number'] );
 		
 		if ($ibforums->input['number'] < 5)
 		{
 			$ibforums->input['number'] = 5;
 		}
 		
 		if ($ibforums->input['number'] > 50)
 		{
 			$ibforums->input['number'] = 50;
 		}
 		
 		$type      = 'html';
 		$file_name = "pm_archive.html";
 		$ctype     = "text/html";
 		
 		if ($ibforums->input['type'] == 'xls')
 		{
 			$type      = 'xls';
 			$file_name = "xls_importable.txt";
 			$ctype     = "text/plain";
 		}
 		
 		$output = "";
 		
 		//----------------------------------------
 		// Start the datafile..
 		//----------------------------------------
 		
 		if ($type == 'html')
 		{
 			$output .= $this->html->archive_html_header();
 		}
 		
 		require "./sources/lib/post_parser.php";
        
        $this->parser = new post_parser();
 		
 		//----------------------------------------
 		// Get the messages...
 		//----------------------------------------
 		
 		$archive_query = $DB->query("SELECT mg.*, m.name, m.id from ibf_messages mg, ibf_members m WHERE mg.member_id=m.id AND mg.member_id='".$this->member['id']."' AND mg.msg_date > $time_cut".$folder_query." ORDER BY mg.msg_date LIMIT 0,".$ibforums->input['number']);
 		
 		if ( $DB->get_num_rows($archive_query) )
 		{
 			while ( $r = $DB->fetch_row($archive_query) )
 			{
 				$info = array();
 				
 				$msg_ids[] = $r['msg_id'];
 				
 				$from_member = $DB->query("SELECT id, name FROM ibf_members WHERE id='".$r['from_id']."'");
 				
 				$from_mem = $DB->fetch_row($from_member);
 				
 				$info['msg_date']    = $std->get_date( $r['msg_date'], 'LONG' );
 				$info['msg_title']   = $r['title'];
 				$info['msg_sender']  = $from_mem['name'];
				$info['msg_content'] = $this->parser->convert( array( 'TEXT'    => $r['message'],
																	  'SMILIES' => 0,
																	  'CODE'    => $ibforums->vars['msg_allow_code'],
																	  'HTML'    => $ibforums->vars['msg_allow_html']
																	)
															 );
 				
 				if ($type == 'xls')
 				{
 					$output .= '"'.$this->strip_quotes($info['msg_title']).'","'.$this->strip_quotes($info['msg_date']).'","'.$this->strip_quotes($info['msg_sender']).'","'.$this->strip_quotes($info['msg_content']).'"'."\r";
 				}
 				else
 				{
 					$output .= $this->html->archive_html_entry($info);
 				}
 				
 			}
 			
 			if ($type == 'html')
			{
				$output .= $this->html->archive_html_footer();
			}
			
			$num_msg = count( $msg_ids );
			
			if ($ibforums->input['delete'] == 'yes')
			{
				$msg_str = implode( ",", $msg_ids );
				
				if (!empty($msg_str))
				{
					$DB->query("DELETE FROM ibf_messages WHERE msg_id IN ($msg_str)");
					
					$DB->query("UPDATE ibf_members SET msg_total=msg_total-$num_msg WHERE id ='".$this->member['id']."'");
				}
				
			}
			
			$this->email->get_template("pm_archive");
			
			$this->email->build_message( array(
												'NAME'         => $this->member['name'],
											  )
										);
										
			$this->email->subject = $ibforums->lang['arc_email_subject'];
			$this->email->to      = $this->member['email'];
			$this->email->add_attachment( $output, $file_name, $ctype );
			$this->email->send_mail();
			
			
			$ibforums->lang['arc_complete'] = preg_replace( "/<#NUM#>/", "$num_msg", $ibforums->lang['arc_complete'] );
			
			$this->output .= $this->html->archive_complete();
 		
			$this->page_title = $ibforums->lang['t_welcome'];
			$this->nav        = array( "<a href='".$this->base_url."&act=UserCP&CODE=00'>".$ibforums->lang['t_title']."</a>" );
			
		}
		else
		{
			$std->Error( array(  'LEVEL' => 1, 'MSG' => 'no_archive_messages' ) );
		}
 		
 		
 	}
 
 	function strip_quotes($text) {
 	
 		return preg_replace( "/\"/", '\\\"', $text );
 		
 	}	
 	
 	/**********************************************************/
 	// PREFS:
 	//
 	// Create/delete/edit messenger folders
 	/**********************************************************/
 	
 	function prefs() {
 		global $ibforums, $DB, $std, $print;
 		
 		$this->output .= $this->html->prefs_header();
 		
 		$count = 0;
 		
 		foreach( $this->msg_stats['dir_data'] as $k => $v )
 		{
 			$extra = "";
 			if ($v['id'] == 'in' or $v['id'] == 'sent')
 			{
 				$extra = "&nbsp;&nbsp;( ".$v['real']." - ".$ibforums->lang['cannot_remove']." )";
 			}
 			$this->output .= $this->html->prefs_row( array( 'ID' => $v['id'], 'REAL' => $v['real'], 'EXTRA' => $extra ) );
 			$count++;
 		}
 		
 		$this->output .= $this->html->prefs_add_dirs();
 		
 		for ($i = $count; $i < $count+3; $i++)
 		{
 			$this->output .= $this->html->prefs_row( array( 'ID' => 'dir_'.$i, 'REAL' => '', 'EXTRA' => '' ) );
 		}
 		
 		$this->output .= $this->html->prefs_footer();
 		
 		$this->page_title = $ibforums->lang['t_welcome'];
 		$this->nav        = array( "<a href='".$this->base_url."&act=UserCP&CODE=00'>".$ibforums->lang['t_title']."</a>" );
 		
 	}
 	
 	function do_prefs() {
 		global $ibforums, $DB, $std, $print;
 		
 		// Check to ensure than we've not tried to remove the inbox and sent items directories.
 		
 		if ( ($ibforums->input['sent'] == "") or ($ibforums->input['in'] == "") )
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'cannot_remove_dir' ) );
 		}
 		
 		
 		$v_dir = 'in:'.$ibforums->input['in'].'|sent:'.$ibforums->input['sent'];
 		
 		// Fetch the rest of the dirs
 		
 		
 		$ids = array();
 		
 		foreach ($ibforums->input as $key => $value)
 		{
 			if ( preg_match( "/^dir_(\d+)$/", $key, $match ) )
 			{
 				if ($ibforums->input[$match[0]])
 				{
 					$v_dir .= '|'.$match[0].':'.trim($ibforums->input[$match[0]]);
 				}
 			}
 		}
 		
 		$DB->query("UPDATE ibf_members SET vdirs='$v_dir' WHERE id='".$this->member['id']."'");
 		
 		$std->boink_it($ibforums->base_url."&act=Msg&CODE=07");
		exit;
 		
 	}
 	
 	/**********************************************************/
 	// DELETE_MEMBER:
 	//
 	// Removes a member from address book.
 	/**********************************************************/
 	
 	function del_member() {
 		global $ibforums, $DB, $std, $print;
 		
 		if (!$ibforums->input['MID'])
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_user' ) );
 		}
 		
 		if (! preg_match( "/^(\d+)$/", $ibforums->input['MID'] ) )
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_user' ) );
 		}
 		
 		$DB->query("DELETE FROM ibf_contacts WHERE member_id='".$this->member['id']."' AND contact_id='".$ibforums->input['MID']."'");
 		
 		$std->boink_it($this->base_url."&act=Msg&CODE=02");
		exit;
	}
	
	/**********************************************************/
 	// EDIT_MEMBER:
 	//
 	// Edit a member from address book.
 	/**********************************************************/
 	
 	function edit_member() {
 		global $ibforums, $DB, $std, $print;
 		
 		if (!$ibforums->input['MID'])
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_user' ) );
 		}
 		
 		if (! preg_match( "/^(\d+)$/", $ibforums->input['MID'] ) )
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_user' ) );
 		}
 		
 		
 		$DB->query("SELECT * FROM ibf_contacts WHERE member_id='".$this->member['id']."' AND contact_id='".$ibforums->input['MID']."'");
 		$memb = $DB->fetch_row();
 		
 		if (!$memb['contact_id'])
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_user' ) );
 		}
 		
 		$html = "<select name='allow_msg' class='forminput'>";
 		
 		if ($memb['allow_msg'])
 		{
 			$html .= "<option value='yes' selected>{$ibforums->lang['yes']}</option><option value='no'>{$ibforums->lang['no']}";
 		}
 		else
 		{
 			$html .= "<option value='yes'>{$ibforums->lang['yes']}</option><option value='no' selected>{$ibforums->lang['no']}";
 		}
 		
 		$html .= "</select>";
 		
 		$this->output .= $this->html->address_edit( array( 'SELECT' => $html, 'MEMBER' => $memb ) );
 		
 		
 		$this->page_title = $ibforums->lang['t_welcome'];
 		$this->nav        = array( "<a href='".$this->base_url."&act=UserCP&CODE=00'>".$ibforums->lang['t_title']."</a>",
 								   "<a href='".$this->base_url."&act=Msg&CODE=02'>".$ibforums->lang['t_book']."</a>"  );
 		
 	}
 	
	/**********************************************************/
 	// DO_EDIT_MEMBER:
 	//
 	// Edit a member from address book.
 	/**********************************************************/
 	
 	function do_edit() {
 		global $ibforums, $DB, $std, $print;
 		
 		if (!$ibforums->input['MID'])
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_user' ) );
 		}
 		
 		if (! preg_match( "/^(\d+)$/", $ibforums->input['MID'] ) )
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_user' ) );
 		}
 		
 		$ibforums->input['allow_msg'] = $ibforums->input['allow_msg'] == 'yes' ? 1 : 0;
 		
 		$DB->query("SELECT * FROM ibf_contacts WHERE member_id='".$this->member['id']."' AND contact_id='".$ibforums->input['MID']."'");
 		$memb = $DB->fetch_row();
 		
 		if (!$memb['contact_id'])
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_user' ) );
 		}
 		
 		$DB->query("UPDATE ibf_contacts SET contact_desc='".$ibforums->input['mem_desc']."', allow_msg='".$ibforums->input['allow_msg']."' WHERE id='".$memb['id']."'");
 		
 		$std->boink_it($this->base_url."&act=Msg&CODE=02");
		exit;
 		
 	}
 	
 	
 		
 	/**********************************************************/
 	// CONTACT:
 	//
 	// Shows the address book.
 	/**********************************************************/
 	
 	function contact() {
 		global $ibforums, $DB, $std, $print;
 		
 		$this->output .= $this->html->Address_header();
 		
 		$DB->query("SELECT * FROM ibf_contacts WHERE member_id='".$this->member['id']."' ORDER BY contact_name ASC");
 		
 		if ( $DB->get_num_rows() )
 		{
 		
 			$this->output .= $this->html->Address_table_header();
 			while ( $row = $DB->fetch_row() )
 			{
 				$row['text'] = $row['allow_msg']
 							 ? $ibforums->lang['can_contact']
 							 : $ibforums->lang['cannot_contact'];
 							 
 				$this->output .= $this->html->render_address_row($row);
 			}
 			$this->output .= $this->html->end_address_table();
 			
 		}
 		else
 		{
 			$this->output .= $this->html->Address_none();
 			
 		}
 		
 		// Do we have a name to enter?
 		
 		$name_to_enter = "";
 		
 		if ($ibforums->input['MID'])
 		{
 			if ( preg_match( "/^(\d+)$/", $ibforums->input['MID'] ) )
 			{
 				$DB->query("SELECT name, id FROM ibf_members WHERE id='".$ibforums->input['MID']."'");
 				$memb = $DB->fetch_row();
 				
 				if ($memb['id'])
 				{
 					$name_to_enter = $memb['name'];
 				}
 			}
 		}
 		
 		$this->output .= $this->html->address_add($name_to_enter);
 		
 		$this->page_title = $ibforums->lang['t_welcome'];
 		$this->nav        = array( "<a href='".$this->base_url."&act=UserCP&CODE=00'>".$ibforums->lang['t_title']."</a>" );
 		
 	}
 	
 	
 	/**********************************************************/
 	// ADD MEMBER:
 	//
 	// Adds a member to the addy book.
 	/**********************************************************/
 	
 	function add_member() {
 		global $ibforums, $DB, $std, $print;
 		
 		if (! $ibforums->input['mem_name'])
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_user' ) );
 		}
 		
 		$DB->query("SELECT name, id FROM ibf_members WHERE LOWER(name)='".$ibforums->input['mem_name']."'");
 		$memb = $DB->fetch_row();
 		
 		if (! $memb['id'])
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_user' ) );
 		}
 		
 		//--------------------------------------
 		// Do we already have this member in our
 		// address book?
 		//--------------------------------------
 		
 		$DB->query("SELECT contact_id FROM ibf_contacts WHERE member_id='".$this->member['id']."' AND contact_id='".$memb['id']."'");
 		
 		if ( $DB->get_num_rows() )
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'member_in_add_book' ) );
 		}
 		
 		//--------------------------------------
 		// Insert it into the DB
 		//--------------------------------------
 		
 		$ibforums->input['allow_msg'] = $ibforums->input['allow_msg'] == 'yes' ? 1 : 0;
 		
 		$db_string = $std->compile_db_string( array( 
													 'member_id'      => $this->member['id'],
													 'contact_name'   => $memb['name'],
													 'allow_msg'      => $ibforums->input['allow_msg'],
													 'contact_desc'   => $ibforums->input['mem_desc'],
													 'contact_id'     => $memb['id']
											)      );
		
		$DB->query("INSERT INTO ibf_contacts (" .$db_string['FIELD_NAMES']. ") VALUES (". $db_string['FIELD_VALUES'] .")");
		unset($db_string);
		
		// BUH BYE!
		
		$std->boink_it($this->base_url."&act=Msg&CODE=02");
		exit;
	}
 		

 	
 	/********************************************************************************************************************/
 	// Mutli Act:
 	//
 	// Removes or moves messages.
 	/**********************************************************/
 	
 	function multi_act() {
 		global $ibforums, $DB, $std, $print;
 		
 		//--------------------------------------
 		// Get the ID's to delete
 		//--------------------------------------
 		
 		$ids = array();
 		
 		foreach ($ibforums->input as $key => $value)
 		{
 			if ( preg_match( "/^msgid-(\d+)$/", $key, $match ) )
 			{
 				if ($ibforums->input[$match[0]])
 				{
 					$ids[] = $match[1];
 				}
 			}
 		}
 		
 		$affected_ids = count($ids);
 		
 		if ( $affected_ids > 0 )
 		{
 			$id_string = implode( ",", $ids );
 			
 			if ($ibforums->input['delete'])
 			{
 				$DB->query("DELETE FROM ibf_messages WHERE member_id='".$this->member['id']."' AND msg_id IN ($id_string)");
 				$DB->query("UPDATE ibf_members SET msg_total=msg_total-$affected_ids WHERE id='".$this->member['id']."'");
 				$std->boink_it($this->base_url."&act=Msg&CODE=01&VID={$this->vid}");
 				exit;
 				
 			}
 			else if ($ibforums->input['move'])
 			{
 				$DB->query("UPDATE ibf_messages SET vid='".$this->vid."' WHERE member_id='".$this->member['id']."' AND msg_id IN ($id_string)");
 				$std->boink_it($this->base_url."&act=Msg&CODE=01&VID={$this->vid}");
 				exit;
 			}
 			else
 			{
 				$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_msg_chosen' ) );
 			}
 		}
 		else
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_msg_chosen' ) );
 		}
 		
 	}
 	
 	
 	
 	/**********************************************************/
 	// DELETE MESSAGE:
 	//
 	// Removes a message.
 	// Yes. there is no small print.
 	/**********************************************************/
 	
 	function delete() {
 		global $ibforums, $DB, $std, $print;
 	
 		//--------------------------------------
 		// check for a msg ID
 		//--------------------------------------
 		
 		if (!$ibforums->input['MSID'])
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_msg_chosen' ) );
 		}
 		
 		if (! preg_match( "/^\d+$/" , $ibforums->input['MSID'] ) )
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'invalid_use' ) );
 		}
 		
 		//--------------------------------------
 		// Delete it from the DB
 		//--------------------------------------
 		
 		$DB->query("DELETE FROM ibf_messages WHERE msg_id='".$ibforums->input['MSID']."' AND member_id='".$this->member['id']."'");
 		
 		$DB->query("UPDATE ibf_members SET msg_total=msg_total-1 WHERE id='".$this->member['id']."'");
 		
 		// BYE!
 		
 		$std->boink_it($this->base_url."&act=Msg&CODE=01&VID={$this->vid}");
 		exit;
 	}
 	
 	
 	/**********************************************************/
 	// VIEW MESSAGE:
 	//
 	// Views a message, thats it. No, it doesn't do anything else
 	// I don't know why. It just does. Accept it and move on dude.
 	/**********************************************************/
 	
 	function view_msg() {
 		global $ibforums, $DB, $std, $print;
 		
 		//--------------------------------------
 		// check for a msg ID
 		//--------------------------------------
 		
 		if (!$ibforums->input['MSID'])
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_such_msg' ) );
 		}
 		
 		if (! preg_match( "/^\d+$/" , $ibforums->input['MSID'] ) )
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'invalid_use' ) );
 		}
 		
 		//--------------------------------------
 		// Get the message from the DB
 		// Check to make sure it exists
 		//--------------------------------------
 		
 		$DB->query("SELECT * FROM ibf_messages WHERE msg_id='".$ibforums->input['MSID']."' and member_id='".$this->member['id']."'");
 		$msg = $DB->fetch_row();
 		
 		if (!$msg['msg_id'])
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_such_msg' ) );
 		}
 		
 		//--------------------------------------
 		// Are we tracking this message?
 		//--------------------------------------
 		
 		if ($msg['read_state'] == -1)
 		{
 		
 			$r_time = $std->get_date( $msg['msg_date'], 'LONG' );
 			$r_post = "<b>{$ibforums->lang['read_rec_text']} $r_time</b><br>{$msg['message']}<br><br><b>{$this->member['name']} {$ibforums->lang['mem_read_msg']}</b>";
 		
 			$db_string = $std->compile_db_string( array( 
														 'member_id'      => $msg['from_id'],
														 'msg_date'       => time(),
														 'read_state'     => 0,
														 'title'          => $ibforums->lang['read_rec_title'].' '.$msg['title'],
														 'message'        => $r_post,
														 'from_id'        => $this->member['id'],
														 'vid'            => 'in',
														 'recipient_id'   => 'N/A',
												)      );
			
			$DB->query("INSERT INTO ibf_messages (" .$db_string['FIELD_NAMES']. ") VALUES (". $db_string['FIELD_VALUES'] .")");
			$new_id = $DB->get_insert_id();
			unset($db_string);
			
			//-----------------------------------------------------
		
			$DB->query("UPDATE ibf_members SET ".
						"msg_total = msg_total + 1, ".
						"new_msg = new_msg + 1, ".
						"msg_from_id='"    .$this->member['id']  .  "', ".
						"msg_msg_id='"     . $new_id                    .  "' ".
						"WHERE id='"       . $msg['from_id']            .  "'");
						
		}
		
		//--------------------------------------
 		// Is this an unread message?
 		//--------------------------------------
 		
 		if ($msg['read_state'] < 1)
 		{
 			$DB->query("UPDATE ibf_messages SET read_state='1' WHERE msg_id='".$ibforums->input['MSID']."'");
 		}
 		
 		
 		//--------------------------------------
 		// Start formatting the member and msg
 		//--------------------------------------
 		
 		require "./sources/lib/post_parser.php";
        
        $this->parser = new post_parser();
        
 		
 		$msg['msg_date'] = $std->get_date( $msg['msg_date'], 'LONG' );
 		
 		
 		$DB->query("SELECT g.g_title as member_group, m.id, m.joined, m.name, m.title, m.posts, m.mgroup, m.email, m.signature, m.avatar, m.avatar_size, m.hide_email, m.aim_name, m.icq_number ".
 				   "FROM ibf_members m, ibf_groups g WHERE id='".$msg['from_id']."' and g.g_id=m.mgroup");
 				   
 		$member = $DB->fetch_row();
 		
 		$member = $this->parse_member( $member, $msg );
 		
 		$msg['message'] = $this->parser->convert( array( TEXT    => $msg['message'],
														 SMILIES => 1,
														 CODE    => $ibforums->vars['msg_allow_code'],
														 HTML    => $ibforums->vars['msg_allow_html']
													   )
												);
												
		if (!$ibforums->vars[SIG_SEP]) $ibforums->vars[SIG_SEP] = "<br><br>--------------------<br>";
												
		if ($this->member['view_sigs'])
		{
			$member['signature'] = $ibforums->vars[SIG_SEP] . $this->parser->convert( array( TEXT    => $member['signature'],
																					   SMILIES => 0,
																					   CODE    => $ibforums->vars['sig_allow_ibc'],
																					   HTML    => $ibforums->vars['sig_allow_html'],
																					   SIGNATURE     => 1,
																					 )
																			  );
		}
		else
		{
			$member['signature'] = "";
		}
		
		$member['VID'] = $this->msg_stats['current_id'];
		
		$this->output .= $this->html->Render_msg( array( 
														 'msg'    => $msg,
														 'member' => $member,
														 'jump'   => $this->jump_html
											    )      );
											   
		
		$this->page_title = $ibforums->lang['t_welcome'];
		
		$this->nav        = array( "<a href='".$this->base_url."&act=UserCP&CODE=00'>".$ibforums->lang['t_title']."</a>",
								   "<a href='".$this->base_url."&act=Msg&CODE=01&VID={$member['VID']}'>".$this->msg_stats['current_dir']."</a>"
								 );						   
 	
 	}
 	
 	/**********************************************************/
 	// SEND MESSAGE:
 	//
 	// Sends a message. Yes, it's that simple. Why so much code?
 	// Because typing "send a message to member X" doesnt actually
 	// do anything.
 	/**********************************************************/
 	
 	function send() {
 		global $ibforums;
 		
 		$ibforums->input['MODE'] ? $this->send_msg() : $this->send_form();
 		
 	}
 	
 	//+-----------------------------------------------------------
 	
 	function send_form($preview=0) {
 		global $ibforums, $DB, $std, $print, $HTTP_POST_VARS;
 		
 		//--------------------------------------
 		// Get some more HTML and words, oh yes.
 		//--------------------------------------
 		
 		$ibforums->lang = $std->load_words($ibforums->lang, 'lang_post', $ibforums->lang_id);
    	
    	require "./Skin/".$ibforums->skin_id."/skin_post.php";
    	$post_html = new skin_post();
    	
    	if ($preview == 1)
    	{
    	
    		require "./sources/lib/post_parser.php";
        
        	$this->parser = new post_parser();
        	
    		$old_msg = $this->parser->convert( array( TEXT    => $ibforums->input['Post'],
													  SMILIES => 1,
													  CODE    => $ibforums->vars['msg_allow_code'],
													  HTML    => $ibforums->vars['msg_allow_html']
													)
											 );
											 
			$this->output .= $this->html->preview($old_msg);
    	
    	}
    	
    	//--------------------------------------
 		// Load the contacts
 		//--------------------------------------
 		
 		$contacts = "";
 		
 		$DB->query("SELECT * FROM ibf_contacts WHERE member_id='".$this->member['id']."' ORDER BY contact_name");
 		
 		if ( $DB->get_num_rows() )
 		{
 			$contacts = "<select name='from_contact' class='forminput'><option value='-'>".$ibforums->lang['other']."</option>\n<option value='-'>--------------------</option>\n";
 			
 			while ( $entry = $DB->fetch_row() )
 			{
 				$contacts .= "<option value='".$entry['contact_id']."'>".$entry['contact_name']."</option>\n";
 			}
 			
 			$contacts .= "</select>\n";
 		}
 		else
 		{
 			$contacts = $ibforums->lang['address_list_empty'];
 		}
 		
 		$this->html->class['LANG']['max_msg']     = preg_replace( "/<#MAX_MSG_SIZE#>/", $ibforums->vars['MAX_MSG_SIZE'], $this->html->class['LANG']['max_msg'] );
 		$this->html->class['LANG']['the_max_msg'] = $ibforums->vars['MAX_MSG_SIZE'] * 1024;
 		
 		$name_to_enter = "";
 		$old_message   = "";
 		$old_title     = "";
    	
    	//--------------------------------------
 		// Did we come from a button with a user ID?
 		//--------------------------------------
 		
 		if (!empty($ibforums->input['MID']))
 		{
 			$DB->query("SELECT name, id FROM ibf_members WHERE id='".$ibforums->input['MID']."'");
 			$name = $DB->fetch_row();
 			
 			if ($name['id'])
 			{
 				$name_to_enter = $name['name'];
 			}
 		}
 		
 		//--------------------------------------
 		// Are we quoting an old message?
 		//--------------------------------------
 		
 		if ($preview == 1)
 		{
 			$old_message = stripslashes($HTTP_POST_VARS['Post']);
 			$old_title   = preg_replace( "/'/", "&#39;", stripslashes($HTTP_POST_VARS['msg_title']) );
 			
 		}
 		else if (!empty($ibforums->input['MSID']))
 		{
 			$DB->query("SELECT message, title from ibf_messages WHERE msg_id='".$ibforums->input['MSID']."' and member_id='".$this->member['id']."'");
 			$old_msg = $DB->fetch_row();
 			if ($old_msg['title'])
 			{
 				$old_message = '[QUOTE]'.$old_msg['message'].'[/QUOTE]'."\n";
 				$old_message = preg_replace( "/<br>/", "\n", $old_message );
 				$old_title   = "Re:".$old_msg['title'];
 				$old_title   = preg_replace( "/^(?:Re\:){1,}/i", "Re:", $old_title );
 			}
 		}
 		
 		
 		//--------------------------------------
 		// Build up the HTML for the send form
 		//--------------------------------------
 		
 		$this->output .= $this->html->Send_form( array (
 														 'CONTACTS'  => $contacts,
 														 'MEMBER'    => $this->member,
 														 'N_ENTER'   => $name_to_enter,
 														 'O_TITLE'   => $old_title,
 											   )       );
 		
 		$post_html->class['LANG']['the_max_length'] = $ibforums->vars['MAX_MSG_SIZE'] * 1024;
 		$post_html->class['LANG']['ib_state']       = $ibforums->vars['MSG_ALLOW_CODE'] ? $post_html->class['LANG']['ib_on']   : $post_html->class['LANG']['ib_off'];
 		$post_html->class['LANG']['html_state']     = $ibforums->vars['MSG_ALLOW_HTML'] ? $post_html->class['LANG']['html_on'] : $post_html->class['LANG']['html_off'];
 		
 		$ibforums->lang['the_max_length'] = $ibforums->vars['max_post_length'] * 1024;
 		
 		$this->output .= $post_html->postbox_buttons($old_message);
 		
 		$this->output .= $this->html->send_form_footer();
 		
 		//--------------------------------------
 		// Add in the smilies box
 		//--------------------------------------
 		
 		$show_table = 0;
		$count      = 0;
		$smilies    = "<tr align='center'>\n";
		
		// Get the smilies from the DB
		
		$DB->query("SELECT * FROM ibf_emoticons WHERE clickable='1'");
		
		while ($elmo = $DB->fetch_row() ) {
		
			$show_table++;
			$count++;
			
			$smilies .= "<td><a href=\"javascript:emoticon('".$elmo['typed']."')\"><img src=\"".$ibforums->vars['EMOTICONS_URL']."/".$elmo['image']."\" alt='smilie' border='0'></a>&nbsp;</td>\n";
			
			if ($count == $ibforums->vars['emo_per_row']) {
				$smilies .= "</tr>\n\n<tr align='center'>";
				$count = 0;
			}
		}
		
		if ($count != $ibforums->vars['emo_per_row']) {
			for ($i = $count ; $i < $ibforums->vars['emo_per_row'] ; ++$i) {
				$smilies .= "<td>&nbsp;</td>\n";
			}
			$smilies .= "</tr>";
		}
		
		$table = $post_html->smilie_table();
		
		if ($show_table != 0) {
			$table = preg_replace( "/<!--THE SMILIES-->/", $smilies, $table );
			$this->output = preg_replace( "/<!--SMILIE TABLE-->/", $table, $this->output );
		}
		
		
		$this->page_title = $ibforums->lang['t_welcome'];
 		$this->nav        = array( "<a href='".$this->base_url."&act=UserCP&CODE=00'>".$ibforums->lang['t_title']."</a>" );
		
 		
 	}
 	
 	
 	
 	
 	
 	//+-----------------------------------------------------------
 	
 	
 	
 	
 	
 	function send_msg() {
 		global $ibforums, $DB, $std, $print;
 		
 		$ibforums->input['from_contact'] = $ibforums->input['from_contact'] ? $ibforums->input['from_contact'] : '-';
 		
 		//----------------------------------------------------------------
 		if ( strlen($ibforums->input['msg_title']) < 2 )
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_msg_title' ) );
 		}
 		//----------------------------------------------------------------
 		if ( strlen($ibforums->input['Post']) < 2 )
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_msg' ) );
 		}
 		//----------------------------------------------------------------
 		if ($ibforums->input['from_contact'] == '-' and $ibforums->input['entered_name'] == "")
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_chosen_member' ) );
 		}
 		//----------------------------------------------------------------
 		
 		//--------------------------------------
 		// Attempt to get the reciepient details
 		//--------------------------------------
 		
 		$to_member = array();
 		
 		if ($ibforums->input['from_contact'] == '-')
 		{
 			$query = "LOWER(name)='".$ibforums->input['entered_name']."'";
 		}
 		else
 		{
 			$query = "id='".$ibforums->input['from_contact']."'";
 		}
 		
 		$DB->query("SELECT name, id, view_pop, mgroup FROM ibf_members WHERE ".$query);
 		$to_member = $DB->fetch_row();
 		
 		if (empty($to_member['id']))
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_to_member' ) );
 		}
 		
 		//--------------------------------------
 		// Can the reciepient use the PM system?
 		//--------------------------------------
 		
 		$DB->query("SELECT g_use_pm FROM ibf_groups WHERE g_id='".$to_member['mgroup']."'");
 		$allow = $DB->fetch_row();
 		
 		if ($allow['g_use_pm'] != 1)
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_usepm_member' ) );
 		}
 		
 		//--------------------------------------
 		// Has the reciepient blocked us?
 		//--------------------------------------
 		
 		$DB->query("SELECT contact_id, allow_msg FROM ibf_contacts WHERE contact_id='".$this->member['id']."' AND member_id='".$to_member['id']."'");
 		$can_msg = $DB->fetch_row();
 		
 		if ( (isset($can_msg['contact_id'])) and ($can_msg['allow_msg'] != 1) )
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'msg_blocked' ) );
 		}
 		
 		//--------------------------------------
 		// Do we have enough room to store a
 		// saved copy?
 		//--------------------------------------
 		
 		if ($ibforums->input['add_sent'] and $ibforums->vars['max_messages'])
 		{
			if ( ($this->msg_stats['msg_total'] + 1) >= $ibforums->vars['max_messages'] )
			{
				$std->Error( array( 'LEVEL' => 1, 'MSG' => 'max_message_from' ) );
			}
 		}
 		
 		//--------------------------------------
 		// Is this a preview?
 		//--------------------------------------
 		
 		if ($ibforums->input['preview'] != "")
 		{
 			$ibforums->input['MID'] = $to_member['id'];
 			$this->send_form(1);
 			return;
 		}
 		
 		//--------------------------------------
 		// Sort out tracking and pop us status
 		//--------------------------------------
 		
 		$show_popup =  $to_member['view_pop'];
 		
 		$track_id = 0;
 		
 		if ($ibforums->input['add_tracking'])
 		{
 			$track_id = -1;
 		}
 		
 		//--------------------------------------
 		// Get the recipient msg_stats
 		//--------------------------------------
 		
 		$DB->query("SELECT msg_total FROM ibf_members WHERE id='".$to_member['id']."'");
 		$to_msg_stats = $DB->fetch_row();
 		
 		//--------------------------------------
 		// Does the target member have enough room
 		// in their inbox for a new message?
 		//--------------------------------------
 		
		if ( (($to_msg_stats['msg_total']) >= $ibforums->vars['max_messages']) and ($ibforums->vars['max_messages'] != "") )
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'max_message_to' ) );
		}
		
		//--------------------------------------
 		// Enter the info into the DB
 		// Target user side.
 		//--------------------------------------
 		
 		$db_string = $std->compile_db_string( array( 
 													 'member_id'      => $to_member['id'],
 													 'msg_date'       => time(),
 													 'read_state'     => $track_id,
 													 'title'          => $ibforums->input['msg_title'],
 													 'message'        => $ibforums->input['Post'],
 													 'from_id'        => $this->member['id'],
 													 'vid'            => 'in',
 													 'recipient_id'   => $to_member['id'],
 											)      );
		
		$DB->query("INSERT INTO ibf_messages (" .$db_string['FIELD_NAMES']. ") VALUES (". $db_string['FIELD_VALUES'] .")");
		$new_id = $DB->get_insert_id();
		unset($db_string);
		
		//-----------------------------------------------------
		
		$DB->query("UPDATE ibf_members SET ".
					"msg_total = msg_total + 1, "                    .
					"new_msg = new_msg + 1, "                        .
					"msg_from_id='"     .$this->member['id']         .  "', ".
					"msg_msg_id='"      . $new_id                    .  "', ".
					"show_popup='"      . $show_popup                .  "' ".
					"WHERE id='"        . $to_member['id']           .  "'");
 		
 		//-----------------------------------------------------
 		// Add the data to the current members DB if we are
 		// adding it to our "sent items" folder
 		//-----------------------------------------------------
 		
 		if ($ibforums->input['add_sent'])
 		{
 		
			$DB->query("UPDATE ibf_members SET ".
						"msg_total = msg_total + 1 ".
						"WHERE id='" . $this->member['id'] . "'");
						
			$db_string = $std->compile_db_string( array( 
														 'member_id'      => $this->member['id'],
														 'msg_date'       => time(),
														 'read_state'     => 0,
														 'title'          => $ibforums->lang['saved_sent_msg'].' '.$ibforums->input['msg_title'],
														 'message'        => $ibforums->input['Post'],
														 'from_id'        => $this->member['id'],
														 'vid'            => 'sent',
														 'recipient_id'   => $to_member['id'],
												)      );
			
			
			$DB->query("INSERT INTO ibf_messages (" .$db_string['FIELD_NAMES']. ") VALUES (". $db_string['FIELD_VALUES'] .")");
			unset($db_string);
		}
		
		$text = preg_replace( "/<#FROM_MEMBER#>/"   , $this->member['name'] , $ibforums->lang['sent_text'] );
		$text = preg_replace( "/<#TO_MEMBER#>/"     , $to_member['name']    , $text );
		$text = preg_replace( "/<#MESSAGE_TITLE#>/" , $ibforums->input['msg_title'], $text );
		
		$print->redirect_screen( $text , "&act=Msg&CODE=01" );
 		
 	}
 	
 	
 	/**********************************************************/
 	// MSG LIST:
 	//
 	// Views the inbox / folder of choice
 	/**********************************************************/
 	
 	function msg_list() {
 		global $ibforums, $DB, $std, $print;
 		
 		$sort_key    = $ibforums->input['sort_key']   ? $ibforums->input['sort_key']   : 'msg_date';
 		$sort_order  = $ibforums->input['sort_order'] ? $ibforums->input['sort_order'] : 'Z-A';
 		
 		$sort_order  = 'Z-A' ? 'DESC' : 'ASC';
 		
 		//---------------------------------------------
 		// Get the number of messages we have in total.
 		//---------------------------------------------
 		
 		$DB->query("SELECT COUNT(*) as msg_total FROM ibf_messages WHERE member_id='".$this->member['id']."'");
 		$total = $DB->fetch_row();
 		
 		$total['msg_total'] = $total['msg_total'] > 0 ? $total['msg_total'] : 0;
 		
 		if ($ibforums->member['msg_total'] == "")
 		{
 			$DB->query("UPDATE ibf_members SET msg_total='".$total['msg_total']."' WHERE id='".$this->member['id']."'");
 		}
 		
 		//---------------------------------------------
 		// Make sure we've not exceeded our alloted allowance.
 		//---------------------------------------------
 		
 		$info['full_messenger'] = "";
 		$info['full_percent']   = "";
 		$info['space_free']     = "Unlimited";
 		$info['total_messages'] = $total['msg_total'];
 		
 		if ($ibforums->vars['max_messages'] > 0)
 		{
 			if ($total['msg_total'] >= $ibforums->vars['max_messages'])
 			{
 				$info['full_messenger'] = "<span id='highlight'>".$ibforums->lang['folders_full']."</span>";
 			}
 			
 			$info['full_percent'] = $total['msg_total'] ? sprintf( "%.0f", ( ($total['msg_total'] / $ibforums->vars['max_messages']) * 100) ) : 0;
 			$info['full_percent'] = "(". $info['full_percent'] .'% '. $ibforums->lang['total_capacity'] . ")";
 			$info['space_free']   = $ibforums->vars['max_messages'] - $total['msg_total'];
 		}
 		
 		
 		//---------------------------------------------
 		// Print the header
 		//---------------------------------------------
 		
 		if ($this->vid == 'sent')
 		{
 			$ibforums->lang['message_from'] = $ibforums->lang['message_to'];
 			
 			$DB->query("SELECT m.*, mp.name as from_name FROM ibf_messages m, ibf_members mp WHERE member_id='".$this->member['id']."' AND vid='".$this->vid."' and mp.id=m.recipient_id ORDER BY $sort_key $sort_order");
 		
 		}
 		else
 		{
 			$DB->query("SELECT m.*, mp.name as from_name FROM ibf_messages m, ibf_members mp WHERE member_id='".$this->member['id']."' AND vid='".$this->vid."' and mp.id=m.from_id ORDER BY $sort_key $sort_order");
 		}
 		
 		$this->output .= $this->html->inbox_table_header( $this->msg_stats['current_dir'], $info );
 		
 		//---------------------------------------------
 		// Get the messages
 		//---------------------------------------------
 		
 		
 		if ( $DB->get_num_rows() )
 		{
 			while( $row = $DB->fetch_row() )
 			{
 				$row['icon'] = $row['read_state'] == 1 ? $ibforums->skin['M_READ'] : $ibforums->skin['M_UNREAD'];
 				$row['date'] = $std->get_date( $row['msg_date'] , 'LONG' );
 				
 				if ($this->vid != 'sent')
 				{
 					$row['add_to_contacts'] = "[ <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Msg&CODE=02&MID={$row['from_id']}'>{$ibforums->lang[add_to_book]}</a> ]";
 				}
 				else
 				{
 					$row['from_id'] = $row['recipient_id'];
 				}
 				
 				$d_array = array( 'msg' => $row, 'member' => $this->member, 'stat' => $this->msg_stats );
 				
 				$this->output .= $this->html->inbox_row( $d_array );
 			}
 		}
 		else
 		{
 			$this->output .= $this->html->No_msg_inbox();
 		}
 		
 		$sorted_by = 'from_' . $sort_key;
 		$this->output .= $this->html->end_inbox( $this->jump_html );
 		
 		//---------------------------------------------
 		// Update the message stats if we have to
 		//---------------------------------------------
 		
 		if ($this->msg_stats['current_id'] == 'in')
 		{
 			$DB->query("UPDATE ibf_members SET ".
 					     "new_msg='0' ".
 					     "WHERE id='".$this->member['id']."'");
 		}
 		
 		$this->page_title = $ibforums->lang['t_welcome'];
 		$this->nav        = array( "<a href='".$this->base_url."&act=UserCP&CODE=00'>".$ibforums->lang['t_title']."</a>" );
 		
 		
 	}


//+------------------------------------------------------------------------------------

	/*********************************************************************/
	// Parse the member info
	/*********************************************************************/
	
	function parse_member($member=array(), $row=array()) {
		global $ibforums, $std, $DB;
		
		$member['name'] = "<a href='{$this->base_url}&act=Profile&CODE=03&MID={$member['id']}'>{$member['name']}</a>";
	
		$member['avatar'] = $std->get_avatar( $member['avatar'], $ibforums->member['view_avs'], $member['avatar_size'] );
		
		$member['MEMBER_JOINED'] = $ibforums->lang['m_joined'].' '.$std->get_date( $member['joined'], 'JOINED' );
		
		$member['MEMBER_GROUP'] = $ibforums->lang['m_group'].' '.$member['member_group'];
		
		$member['MEMBER_POSTS'] = $ibforums->lang['m_posts'].' '.$member['posts'];
		
		$member['PROFILE_ICON'] = "<a href='{$this->base_url}&act=Profile&CODE=03&MID={$member['id']}'>{$ibforums->skin['P_PROFILE']}</a>&nbsp;";
		
		$member['MESSAGE_ICON'] = "<a href='{$this->base_url}&act=Msg&CODE=04&MID={$member['id']}'>{$ibforums->skin['P_MSG']}</a>&nbsp;";
		
		if (!$member['hide_email']) {
			$member['EMAIL_ICON'] = "<a href='{$this->base_url}&act=Mail&CODE=00&MID={$member['id']}'>{$ibforums->skin['P_EMAIL']}</a>&nbsp;";
		}
		
		if ( $member['website'] and $member['website'] = preg_match( "/^http:\/\/\S+$/", $member['WEBSITE'] ) ) {
			$member['WEBSITE_ICON'] = "<a href='{$member['website']}' target='_blank'>{$ibforums->skin['P_WEBSITE']}</a>&nbsp;";
		}
		
		if ($member['icq_number']) {
			$member['ICQ_ICON'] = "<a href=\"javascript:PopUp('{$this->base_url}&act=ICQ&MID={$member['id']}','Pager','450','330','0','1','1','1')\">{$ibforums->skin[P_ICQ]}</a>&nbsp;";
		}
		
		if ($member['aim_name']) {
			$member['AOL_ICON'] = "<a href=\"javascript:PopUp('{$this->base_url}&act=AOL&MID={$member['id']}','Pager','450','330','0','1','1','1')\">{$ibforums->skin[P_AOL]}</a>&nbsp;";
		}
		
		//-----------------------------------------------------
		
		return $member;
	
	}


        
}

?>
