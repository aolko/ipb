<?php

/*
+--------------------------------------------------------------------------
|   IBFORUMS v1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001,2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > Moderation core module
|   > Module written by Matt Mecham
|   > Date started: 19th February 2002
|
|   > Module Version 1.0.0
+--------------------------------------------------------------------------
*/


$idx = new Moderate;

class Moderate {

    var $output    = "";
    var $base_url  = "";
    var $html      = "";

    var $moderator = "";
    var $forum     = array();
    var $topic     = array();
    
    var $upload_dir = "";

    
    /***********************************************************************************/
	//
	// Our constructor, load words, load skin, print the topic listing
	//
	/***********************************************************************************/
    
    function Moderate() {
    
        global $ibforums, $DB, $std, $print, $skin_universal, $HTTP_POST_VARS;
        
        // Make sure this is a POST request, not a naughty IMG redirect
        
        if ($ibforums->input['CODE'] != '04' && $ibforums->input['CODE'] != '20' && $ibforums->input['CODE'] != '22')
        {
        
			if ($HTTP_POST_VARS['act'] == '')
			{
				$std->Error( array( 'LEVEL' => 1, 'MSG' => 'incorrect_use') );
			}
			
		}
        
        require "./Skin/".$ibforums->skin_id."/skin_mod.php";
        
        //-------------------------------------
		// Compile the language file
		//-------------------------------------
		
        $ibforums->lang  = $std->load_words($ibforums->lang, 'lang_mod', $ibforums->lang_id);

        $this->html      = new skin_mod();
        
        //-------------------------------------
        // Check the input
        //-------------------------------------
        
        if ($ibforums->input['t'])
        {
        	$ibforums->input['t'] = $std->is_number($ibforums->input['t']);
        	if (! $ibforums->input['t'] )
        	{
        		$std->Error( array( 'LEVEL' => 1, 'MSG' => 'missing_files') );
        	}
        	else
        	{
        		$DB->query("SELECT tid, title, description, posts, state, starter_id, pinned, forum_id from ibf_topics WHERE tid='".$ibforums->input['t']."'");
        		$this->topic = $DB->fetch_row();
        		
        		if (empty($this->topic['tid']))
        		{
        			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'missing_files') );
        		}
        	}
        }
        
        if ($ibforums->input['p'])
        {
        	$ibforums->input['p'] = $std->is_number($ibforums->input['p']);
        	if (! $ibforums->input['p'] )
        	{
        		$std->Error( array( 'LEVEL' => 1, 'MSG' => 'missing_files') );
        	}
        }
        
        $ibforums->input['f'] = $std->is_number($ibforums->input['f']);
        if (! $ibforums->input['f'] )
        {
        	$std->Error( array( 'LEVEL' => 1,'MSG' => 'missing_files') );
        }
        
        $ibforums->input['st'] = $ibforums->input['st'] ? $std->is_number($ibforums->input['st']) : 0;
        
        
        //-------------------------------------
        // Get the forum info based on the forum ID, get the category name, ID, and get the topic details
        //-------------------------------------
        
        $DB->query("SELECT f.*, c.name as cat_name, c.id as cat_id from ibf_forums f, ibf_categories c WHERE f.id=".$ibforums->input['f']." and c.id=f.category");
        
        $this->forum = $DB->fetch_row();
        
        
        //-------------------------------------
        // Error out if we can not find the forum
        //-------------------------------------
        
        if (!$this->forum['id'])
        {
        	$std->Error( array( LEVEL => 1, MSG => 'missing_files') );
        }
        
        $this->base_url = "{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}";
        
        if ($ibforums->member['id'])
        {
        	if ($ibforums->member['g_is_supmod'] != 1)
        	{
        		$DB->query("SELECT * FROM ibf_moderators WHERE forum_id='".$this->forum['id']."' AND member_id='".$ibforums->member['id']."'");
        		$this->moderator = $DB->fetch_row();
        	}
        }
        
        $this->upload_dir = $ibforums->vars['upload_dir'];
        
        //-------------------------------------
        // Convert the code ID's into something
        // use mere mortals can understand....
        //-------------------------------------
        
        switch ($ibforums->input['CODE']) {
        	case '02':
        		$this->move_form();
        		break;
        	case '03':
        		$this->delete_form();
        		break;
        	case '04':
        		$this->delete_post();
        		break;
        	case '05':
        		$this->edit_form();
        		break;
        	case '00':
        		$this->close_topic();
        		break;
        	case '01':
        		$this->open_topic();
        		break;
        	case '08':
        		$this->delete_topic();
        		break;
        	case '12':
        		$this->do_edit();
        		break;
        	case '14':
        		$this->do_move();
        		break;
        	case '15':
        		$this->pin_topic();
        		break;
        	case '16':
        		$this->unpin_topic();
        		break;
        	case '17':
        		$this->rebuild_topic();
        		break;
        	//-------------------------
        	case '20':
        		$this->poll_edit_form();
        		break;
        	case '21':
        		$this->poll_edit_do();
        		break;
        	//-------------------------
        	case '22':
        		$this->poll_delete_form();
        		break;
        	case '23':
        		$this->poll_delete_do();
        		break;
        	//-------------------------
        	default:
        		$this->moderate_error();
        		break;
        }
        
        // If we have any HTML to print, do so...
    	
    	$print->add_output("$this->output");
        $print->do_output( array( 'TITLE' => $this->page_title, 'JS' => 0, NAV => $this->nav ) );
      
	}
	
	
	/*************************************************/
	// EDIT POLL FORM:
	// ---------------
	//
	/*************************************************/
	
	
	function poll_delete_form() {
		global $std, $ibforums, $DB, $print;
		
		$passed = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1) {
			$passed = 1;
		}
		
		else if ($this->moderator['delete_topic'] == 1) {
			$passed = 1;
		}
		else {
			$passed = 0;
		}
		
		if ($passed != 1) $this->moderate_error();
		
		if (empty($this->topic['tid']))
		{
			$this->moderate_error();
		}
		
		$DB->query("SELECT * FROM ibf_polls WHERE tid='".$this->topic['tid']."'");
        $poll_data = $DB->fetch_row();
        
        if (! $poll_data['pid'])
        {
        	$this->moderate_error();
        }
		
		$this->output = $this->html_start_form( array( 1 => array( 'CODE', '23' ),
												       2 => array( 't' , $this->topic['tid'] ),
												       3 => array( 'f' , $this->forum['id']  ),
		 								      )      );
		 								      
		$this->output .= $this->html->table_top( $ibforums->lang['pd_top']." ".$this->forum['name']." &gt; ".$this->topic['title'] );
		
		$this->output .= $this->html->mod_exp( $ibforums->lang['pd_text'] );		
        		
		$this->output .= $this->html->end_form( $ibforums->lang['pd_submit'] );
		
		$this->page_title = $ibforums->lang['pd_top'].$this->topic['title'];
		
		$this->nav = array ( "<a href='{$this->base_url}&act=SF&f={$this->forum['id']}'>{$this->forum['name']}</a>",
							 "<a href='{$this->base_url}&act=ST&f={$this->forum['id']}&t={$this->topic['tid']}'>{$this->topic['title']}</a>"
						   );
	}
	
	function poll_delete_do() {
		global $std, $ibforums, $DB, $print;
		
		$passed = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1) {
			$passed = 1;
		}
		
		else if ($this->moderator['delete_topic'] == 1) {
			$passed = 1;
		}
		else {
			$passed = 0;
		}
		
		if ($passed != 1) $this->moderate_error();
		
		if (empty($this->topic['tid']))
		{
			$this->moderate_error();
		}
		
		// Remove the poll
		
		$DB->query("DELETE FROM ibf_polls WHERE tid='".$this->topic['tid']."'");
		
		// Remove from poll votes
		
		$DB->query("DELETE FROM ibf_voters WHERE tid='".$this->topic['tid']."'");
		
		// Update topic
		
		$DB->query("UPDATE ibf_topics SET poll_state='', last_vote='', total_votes='' WHERE tid='".$this->topic['tid']."'");
		
		// Boing!
		
		$print->redirect_screen( $ibforums->lang['pd_redirect'], "act=ST&f=".$this->forum['id']."&t=".$this->topic['tid']."&st=".$ibforums->input['st'] );
		
	}
	
	
	function poll_edit_do() {
		global $std, $ibforums, $DB, $print;
		
		$passed = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1)
		{
			$passed = 1;
		}
		
		else if ($this->moderator['edit_post'] == 1)
		{
			$passed = 1;
		}
		else
		{
			$passed = 0;
		}
		
		if ($passed != 1) $this->moderate_error();
		
		if (empty($this->topic['tid']))
		{
			$this->moderate_error();
		}
		
		$DB->query("SELECT * FROM ibf_polls WHERE tid='".$this->topic['tid']."'");
        $poll_data = $DB->fetch_row();
        
        if (! $poll_data['pid'])
        {
        	$this->moderate_error();
        }
        
        $poll_answers = unserialize(stripslashes($poll_data['choices']));
        
		reset($poll_answers);
		$new_poll_array = array();
		
		foreach ($poll_answers as $entry)
		{
			$id     = $entry[0];
			$choice = $ibforums->input['POLL_'.$id];
			$votes  = $entry[2];
			
			$new_poll_array[] = array( $id, $choice, $votes);
		}
		
		$poll_data['choices'] = addslashes(serialize($new_poll_array));
		
		$DB->query("UPDATE ibf_polls SET ".
					 "choices='"  . $poll_data['choices'] . "' ".
					 "WHERE tid='" . $this->topic['tid']    . "'");
					 
		//------------------------
		
		// Update the topic table to change the poll_only value.
		
		$poll_state = $ibforums->input['pollonly'] == 1 ? 'closed' : 'open';
		
		$DB->query("UPDATE ibf_topics SET poll_state='$poll_state' WHERE tid='".$this->topic['tid']."'");
		
		$this->moderate_log("Edited a Poll");
	
		$print->redirect_screen( $ibforums->lang['pe_done'], "act=ST&f=".$this->forum['id']."&t=".$this->topic['tid']."&st=".$ibforums->input['st'] );

    }
    
    
    //--------------------------------------
	
	
	function poll_edit_form() {
		global $std, $ibforums, $DB, $print;
		
		$passed = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1) {
			$passed = 1;
		}
		
		else if ($this->moderator['edit_post'] == 1) {
			$passed = 1;
		}
		else {
			$passed = 0;
		}
		
		if ($passed != 1) $this->moderate_error();
		
		if (empty($this->topic['tid']))
		{
			$this->moderate_error();
		}
		
		$DB->query("SELECT * FROM ibf_polls WHERE tid='".$this->topic['tid']."'");
        $poll_data = $DB->fetch_row();
        
        if (! $poll_data['pid'])
        {
        	$this->moderate_error();
        }
		
		$this->output = $this->html_start_form( array( 1 => array( 'CODE', '21' ),
												       2 => array( 't' , $this->topic['tid'] ),
												       3 => array( 'f' , $this->forum['id']  ),
		 								      )      );
		 								      
		$this->output .= $this->html->table_top( $ibforums->lang['pe_top']." ".$this->forum['name']." &gt; ".$this->topic['title'] );
		
		$poll_answers = unserialize(stripslashes($poll_data['choices']));
		
		reset($poll_answers);
		
		foreach ($poll_answers as $entry)
		{
			$id     = $entry[0];
			$choice = $entry[1];
			$votes  = $entry[2];
			
			$this->output .= $this->html->poll_entry($id, $choice);
			
		}
        		
        $this->output .= $this->html->poll_select_form();		
        		
		$this->output .= $this->html->end_form( $ibforums->lang['pe_submit'] );
		
		$this->page_title = $ibforums->lang['pe_top'].$this->topic['title'];
		
		$this->nav = array ( "<a href='{$this->base_url}&act=SF&f={$this->forum['id']}'>{$this->forum['name']}</a>",
							 "<a href='{$this->base_url}&act=ST&f={$this->forum['id']}&t={$this->topic['tid']}'>{$this->topic['title']}</a>"
						   );
	}
	
	/*************************************************/
	// MOVE FORM:
	// ---------------
	//
	/*************************************************/
	
	function move_form() {
		global $std, $ibforums, $DB, $print;
		
		$passed = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1) {
			$passed = 1;
		}
		
		else if ($this->moderator['move_topic'] == 1) {
			$passed = 1;
		}
		else {
			$passed = 0;
		}
		
		if ($passed != 1) $this->moderate_error();
		
		if (empty($this->topic['tid']))
		{
			$this->moderate_error();
		}
		
		$this->output = $this->html_start_form( array( 1 => array( 'CODE', '14' ),
												       2 => array( 'tid' , $this->topic['tid'] ),
												       3 => array( 'sf'  , $this->forum['id']  ),
		 								      )      );
		 								      
		$jump_html = $std->build_forum_jump('no_html');
		 								
		$this->output .= $this->html->table_top( $ibforums->lang['top_move']." ".$this->forum['name']." &gt; ".$this->topic['title'] );
		$this->output .= $this->html->mod_exp( $ibforums->lang['move_exp'] );
		$this->output .= $this->html->move_form( $jump_html , $this->forum['name']);
		$this->output .= $this->html->end_form( $ibforums->lang['submit_move'] );
		
		$this->page_title = $ibforums->lang['t_move'].": ".$this->topic['title'];
		
		$this->nav = array ( "<a href='{$this->base_url}&act=SF&f={$this->forum['id']}'>{$this->forum['name']}</a>",
							 "<a href='{$this->base_url}&act=ST&f={$this->forum['id']}&t={$this->topic['tid']}'>{$this->topic['title']}</a>"
						   );
	}
	
	/*************************************************/
	
	function do_move() {
		global $std, $ibforums, $DB, $print;
		
		$passed = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1) {
			$passed = 1;
		}
		
		else if ($this->moderator['move_topic'] == 1) {
			$passed = 1;
		}
		else {
			$passed = 0;
		}
		
		if ($passed != 1) $this->moderate_error();
		
		//----------------------------------
		// Check for input..
		//----------------------------------
		
		if ($ibforums->input['sf'] == "")
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'move_no_source' ) );
		}
		
		//----------------------------------
		
		if ($ibforums->input['move_id'] == "" or $ibforums->input['move_id'] == -1)
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'move_no_forum' ) );
		}
		
		//----------------------------------
		
		if ($ibforums->input['move_id'] == $ibforums->input['sf'])
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'move_same_forum' ) );
		}
		
		//----------------------------------
		
		$DB->query("SELECT id, subwrap FROM ibf_forums WHERE id IN(".$ibforums->input['sf'].",".$ibforums->input['move_id'].")");
		
		if ($DB->get_num_rows() != 2)
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'move_no_forum' ) );
		}
		
		//-----------------------------------
		// Check for an attempt to move into a subwrap forum
		//-----------------------------------
		
		while ( $f = $DB->fetch_row() )
		{
			if ($f['subwrap'] == 1)
			{
				$std->Error( array( 'LEVEL' => 1, 'MSG' => 'move_no_forum' ) );
			}
		}
		
		
		$DB->query("SELECT * FROM ibf_topics WHERE tid='".$ibforums->input['tid']."'");
		
		if ( ! $this->topic = $DB->fetch_row() )
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'move_no_forum' ) );
		}
		
		//----------------------------------
		// We know that $this->topic is set..
		//----------------------------------
		
		$source = $ibforums->input['sf'];
		$moveto = $ibforums->input['move_id'];
		$tid    = $ibforums->input['tid'];
		
		//----------------------------------
		// Update the topic
		//----------------------------------
		
		$DB->query("UPDATE ibf_topics SET forum_id='$moveto' WHERE forum_id='$source' AND tid='$tid'");
		
		//----------------------------------
		// Update the posts
		//----------------------------------
		
		$DB->query("UPDATE ibf_posts SET forum_id='$moveto' WHERE forum_id='$source' AND topic_id='$tid'");
		
		//----------------------------------
		// Update the polls
		//----------------------------------
		
		$DB->query("UPDATE ibf_polls SET forum_id='$moveto' WHERE forum_id='$source' AND tid='$tid'");
		
		//----------------------------------
		// Are we leaving a link?
		//----------------------------------
		
		if ($ibforums->input['leave'] == 'y')
		{
			//----------------------------------
			// Insert a new "link" topic...
			//----------------------------------
			
			$db_string = $DB->compile_db_insert_string( array (
																 'title'            => $this->topic['title'],
																 'description'      => $this->topic['description'],
																 'state'            => 'link',
																 'posts'            => 0,
																 'views'            => 0,
																 'starter_id'       => $this->topic['starter_id'],
																 'start_date'       => $this->topic['start_date'],
																 'starter_name'     => $this->topic['starter_name'],
																 'last_post'        => $this->topic['last_post'],
																 'forum_id'         => $source,
																 'approved'         => 1,
																 'pinned'           => 0,
																 'moved_to'         => $this->topic['tid'].'&'.$moveto,
																 'last_poster_id'   => $this->topic['last_poster_id'],
																 'last_poster_name' => $this->topic['last_poster_name']
													 )        );
													 
			$DB->query("INSERT INTO ibf_topics (" .$db_string['FIELD_NAMES']. ") VALUES (". $db_string['FIELD_VALUES'] .")");
			
		}
		
		$this->moderate_log("Moved a topic");
		
		// Resync the forums..
		
		$this->recount($source);
		
		$this->recount($moveto);
	
		$print->redirect_screen( $ibforums->lang['p_moved'], "act=SF&f=".$this->forum['id']."&st=".$ibforums->input['st'] );
		
	}
	
	
	/*************************************************/
	
	function delete_post() {
		global $std, $ibforums, $DB, $print;
		
		$passed = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1)
		{
			$passed = 1;
		}
		else if ($ibforums->member['g_delete_own_posts'] == 1)
		{
			$passed = 1;
		}
		else if ($this->moderator['delete_post'] == 1)
		{
			$passed = 1;
		}
		else 
		{
			$passed = 0;
		}
		
		if ($passed != 1) $this->moderate_error();
		
		
		// Get this post id.
		
		$DB->query("SELECT pid,attach_file, attach_id, post_date, new_topic from ibf_posts WHERE forum_id='".$this->forum['id']."' AND topic_id='".$this->topic['tid']."' and pid='".$ibforums->input['p']."'");
		if ( ! $post = $DB->fetch_row() )
		{
			$this->moderate_error();
		}
		
		// Check to make sure that this isn't the first post in the topic..
		
		if ($post['new_topic'] == 1)
		{
			$this->moderate_error('no_delete_post');
		}
		
		//---------------------------------------
		// Is there an attachment to this post?
		//---------------------------------------
		
		if ($post['attach_id'] != "")
		{
			if (is_file($this->upload_dir."/".$post['attach_id']))
			{
				unlink ($this->upload_dir."/".$post['attach_id']);
			}
		}
		
		//---------------------------------------
		// delete the post
		//---------------------------------------
		
		$DB->query("DELETE from ibf_posts WHERE topic_id='".$this->topic['tid']."' and pid='".$post['pid']."'");
		
		//---------------------------------------
		// Update the stats
		//---------------------------------------
		
		$DB->query("UPDATE ibf_stats SET TOTAL_REPLIES=TOTAL_REPLIES-1");
		
		//---------------------------------------
		// Get the latest post details
		//---------------------------------------
		
		$DB->query("SELECT post_date, author_id, author_name from ibf_posts WHERE topic_id='".$this->topic['tid']."' and queued <> 1 ORDER BY pid DESC");
		$last_post = $DB->fetch_row();
			
		$DB->query("UPDATE ibf_topics SET last_post='"        .$last_post['post_date']   ."', ".
										 "last_poster_id='"   .$last_post['author_id']   ."', ".
										 "last_poster_name='" .$last_post['author_name'] ."', ".
										 "posts=posts-1 WHERE tid='".$this->topic['tid']."'");
										 
		//---------------------------------------
		// If we deleted the last post in a topic that was
		// the last post in a forum, best update that :D
		//---------------------------------------
		
		if ($this->forum['last_id'] == $this->topic['tid'])
		{
			$DB->query("SELECT title, tid, last_post, last_poster_id, last_poster_name "
					  ."FROM ibf_topics WHERE forum_id='".$this->forum['id']."' AND approved=1 "
					  ."ORDER BY last_post DESC LIMIT 0,1");
		          
			$tt = $DB->fetch_row();
			
			$db_string = $DB->compile_db_update_string( array(
															   last_title       => $tt['title']            ? $tt['title']            : "",
															   last_id          => $tt['tid']              ? $tt['tid']              : "",
															   last_post        => $tt['last_post']        ? $tt['last_post']        : "",
															   last_poster_name => $tt['last_poster_name'] ? $tt['last_poster_name'] : "",
															   last_poster_id   => $tt['last_poster_id']   ? $tt['last_poster_id']   : "",
													  )      );
			
			$DB->query("UPDATE ibf_forums SET ".$db_string.",posts=posts-1 WHERE id='".$this->forum['id']."'");
		}
		
		$this->moderate_log("Deleted a post");
	
		$print->redirect_screen( $ibforums->lang['post_deleted'], "act=ST&f=".$this->forum['id']."&t=".$this->topic['tid']."&st=".$ibforums->input['st'] );

	
	}

	function rebuild_topic() {}
	
	/*************************************************/
	// DELETE TOPIC:
	// ---------------
	//
	/*************************************************/
	
	function delete_form() {
		global $std, $ibforums, $DB, $print;
		
		$passed = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1) {
			$passed = 1;
		}
		
		else if ($this->topic['starter_id'] == $ibforums->member['id'])
		{
			if ($ibforums->member['g_delete_own_topics'] == 1)
			{
				$passed = 1;
			}
		}
		
		else if ($this->moderator['delete_topic'] == 1) {
			$passed = 1;
		}
		else {
			$passed = 0;
		}
		
		if ($passed != 1) $this->moderate_error();
		
		if (empty($this->topic['tid'])) {
			$this->moderate_error();
		}
		
		$this->output = $this->html->delete_js();
		
		$this->output .= $this->html_start_form( array( 1 => array( 'CODE', '08' ),
												        2 => array( 't', $this->topic['tid'] )
		 								       )      );
		 								
		$this->output .= $this->html->table_top( $ibforums->lang['top_delete']." ".$this->forum['name']." &gt; ".$this->topic['title'] );
		$this->output .= $this->html->mod_exp( $ibforums->lang['delete_topic'] );
		$this->output .= $this->html->end_form( $ibforums->lang['submit_delete'] );
		
		$this->page_title = $ibforums->lang['t_delete'].": ".$this->topic['title'];
		
		$this->nav = array ( "<a href='{$this->base_url}&act=SF&f={$this->forum['id']}'>{$this->forum['name']}</a>",
							 "<a href='{$this->base_url}&act=ST&f={$this->forum['id']}&t={$this->topic['tid']}'>{$this->topic['title']}</a>"
						   );
	}
	
	function delete_topic() {
		global $std, $ibforums, $DB, $print;
		
		$passed = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1) {
			$passed = 1;
		}
		
		else if ($this->topic['starter_id'] == $ibforums->member['id'])
		{
			if ($ibforums->member['g_delete_own_topics'] == 1)
			{
				$passed = 1;
			}
		}
		
		else if ($this->moderator['delete_topic'] == 1) {
			$passed = 1;
		}
		else {
			$passed = 0;
		}
		
		if ($passed != 1) $this->moderate_error();
		
		if (empty($this->topic['tid'])) {
			$this->moderate_error();
		}
		
		// Do we have a linked topic to remove?
		
		$DB->query("SELECT tid FROM ibf_topics WHERE state='link' AND moved_to='".$this->topic['tid'].'&'.$this->forum['id']."'");
		
		if ( $linked_topic = $DB->fetch_row() )
		{
			$DB->query("DELETE FROM ibf_topics WHERE tid='".$linked_topic['tid']."'");
		}
		
		
		// Remove polls assigned to this topic
		
		$DB->query("DELETE FROM ibf_polls WHERE tid='".$this->topic['tid']."'");
		
		// Remove poll voters
		
		$DB->query("DELETE FROM ibf_voters WHERE tid='".$this->topic['tid']."'");
		
		// Remove the topic itself
		
		$DB->query("DELETE FROM ibf_topics WHERE tid='".$this->topic['tid']."'");
		
		// Get the attach ID's and filenames
		
		$DB->query("SELECT attach_id, attach_hits, attach_file FROM ibf_posts WHERE attach_id <> '' AND topic_id='".$this->topic['tid']."'");
		
		// Remove the attachments
		
		if ( $DB->get_num_rows() )
		{
			while ( $r = $DB->fetch_row() )
			{
				if (is_file($this->upload_dir."/".$r['attach_id']))
				{
					@unlink ($this->upload_dir."/".$r['attach_id']);
				}
			}
		}
		
		// Remove the posts
		
		$DB->query("DELETE FROM ibf_posts WHERE topic_id='".$this->topic['tid']."'");
		
		//------------------------------------------------
		// Update the forum topic/post and stat counters.
		//
		// We also need to make sure the last forum id and
		// title is correct.
		//------------------------------------------------
		
		$DB->query("SELECT COUNT(tid) as tcount from ibf_topics WHERE approved=1");
		$topics = $DB->fetch_row();
		
		$DB->query("SELECT COUNT(pid) as pcount from ibf_posts WHERE queued <> 1");
		$posts  = $DB->fetch_row();
		
		$DB->query("SELECT COUNT(tid) as tcount from ibf_topics WHERE approved=1 and forum_id='".$this->forum['id']."'");
		$f_topics = $DB->fetch_row();
		
		$DB->query("SELECT COUNT(pid) as pcount from ibf_posts WHERE queued <> 1 and forum_id='".$this->forum['id']."'");
		$f_posts  = $DB->fetch_row();
		
		$this->forum['topics'] = $f_topics['tcount'];
		$this->forum['posts']  = $f_posts['pcount'] - $f_topics['tcount'];
		
		$DB->query("SELECT title, tid, last_post, last_poster_id, last_poster_name "
		          ."FROM ibf_topics WHERE forum_id='".$this->forum['id']."' AND approved=1 "
		          ."ORDER BY last_post DESC LIMIT 0,1");
		          
		$tt = $DB->fetch_row();
		
		$db_string = $DB->compile_db_update_string( array(
														   last_title       => $tt['title']            ? $tt['title']            : "",
														   last_id          => $tt['tid']              ? $tt['tid']              : "",
														   last_post        => $tt['last_post']        ? $tt['last_post']        : "",
														   last_poster_name => $tt['last_poster_name'] ? $tt['last_poster_name'] : "",
														   last_poster_id   => $tt['last_poster_id']   ? $tt['last_poster_id']   : "",
														   topics           => $this->forum['topics'],
														   posts            => $this->forum['posts']
												  )      );
		
		$DB->query("UPDATE ibf_forums SET $db_string WHERE id='".$this->forum['id']."'");
		
		// Update the main board stats.
		
		$posts = $posts['pcount'] - $topics['tcount'];
		
		$DB->query("UPDATE ibf_stats SET TOTAL_TOPICS='".$topics['tcount']."', TOTAL_REPLIES='".$posts."'");
		
		$this->moderate_log("Deleted a topic");
	
		$print->redirect_screen( $ibforums->lang['p_deleted'], "act=SF&f=".$this->forum['id'] );
	
	}
	
	
	/*************************************************/
	// EDIT TOPIC:
	// ---------------
	//
	/*************************************************/
	
	function edit_form() {
		global $std, $ibforums, $DB, $print;
		
		$passed = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1) {
			$passed = 1;
		}
		
		else if ($this->moderator['edit_topic'] == 1) {
			$passed = 1;
		}
		else {
			$passed = 0;
		}
		
		if ($passed != 1) $this->moderate_error();
		
		if (empty($this->topic['tid']))
		{
			$this->moderate_error();
		}
		
		$this->output = $this->html_start_form( array( 1 => array( 'CODE', '12' ),
												       2 => array( 't', $this->topic['tid'] )
		 								      )      );
		 								
		$this->output .= $this->html->table_top( $ibforums->lang['top_edit']." ".$this->forum['name']." &gt; ".$this->topic['title'] );
		$this->output .= $this->html->mod_exp( $ibforums->lang['edit_topic'] );
		$this->output .= $this->html->topictitle_fields( $this->topic['title'], $this->topic['description'] );
		$this->output .= $this->html->end_form( $ibforums->lang['submit_edit'] );
		
		$this->page_title = $ibforums->lang['t_edit'].": ".$this->topic['title'];
		
		$this->nav = array ( "<a href='{$this->base_url}&act=SF&f={$this->forum['id']}'>{$this->forum['name']}</a>",
							 "<a href='{$this->base_url}&act=ST&f={$this->forum['id']}&t={$this->topic['tid']}'>{$this->topic['title']}</a>"
						   );
	}
	
	function do_edit() {
		global $std, $ibforums, $DB, $print;
		
		$passed = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1) {
			$passed = 1;
		}
		
		else if ($this->moderator['edit_topic'] == 1) {
			$passed = 1;
		}
		else {
			$passed = 0;
		}
		
		if ($passed != 1) $this->moderate_error();
		
		if (empty($this->topic['tid'])) {
			$this->moderate_error();
		}
		
		if ($ibforums->input['TopicTitle'] == "") {
			$std->Error( array( 'LEVEL' => 2, 'MSG' => 'no_topic_title' ) );
		}
		
		$topic_title = preg_replace( "/'/", "/\\'/", $ibforums->input['TopicTitle'] );
		$topic_desc  = preg_replace( "/'/", "/\\'/", $ibforums->input['TopicDesc']  );
		
		$DB->query("UPDATE ibf_topics SET title='$topic_title', description='$topic_desc' WHERE tid='".$this->topic['tid']."'");
		
		if ($this->topic['tid'] == $this->forum['last_id'])
		{
			$DB->query("UPDATE ibf_forums SET last_title='$topic_title' WHERE id='".$this->forum['id']."'");
		}
		
		$this->moderate_log("Edited a topic title");
	
		$print->redirect_screen( $ibforums->lang['p_edited'], "act=SF&f=".$this->forum['id'] );
		
		
	}
		
		
	/*************************************************/
	// OPEN TOPIC:
	// ---------------
	//
	/*************************************************/
	
	function open_topic() {
		global $std, $ibforums, $DB, $print;
		
		if ($this->topic['state'] == 'open') {
			$this->moderate_error();
		}
		
		$passed = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1) {
			$passed = 1;
		}
		
		else if ($this->topic['starter_id'] == $ibforums->member['id']) {
			if ($ibforums->member['g_open_close_topics'] == 1) {
				$passed = 1;
			}
		}
		else
		{
			$passed = 0;
		}
		
		if ($this->moderator['open_topic'] == 1) {
			$passed = 1;
		}
		
		
		if ($passed != 1) $this->moderate_error();
		
		$this->alter_topic( array( 'TOPIC' => $this->topic['tid'], 'FIELD' => 'state', 'VALUE' => 'open' ) );
		
		$this->moderate_log("Opened Topic");
	
		$print->redirect_screen( $ibforums->lang['p_opened'], "act=ST&f=".$this->forum['id']."&t=".$this->topic['tid']."&st=".$ibforums->input['st'] );
		
	}
	


	/*************************************************/
	// CLOSE TOPIC:
	// ---------------
	//
	/*************************************************/
	
	function close_topic() {
		global $std, $ibforums, $DB, $print;
		
		$passed = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1) {
			$passed = 1;
		}
		
		else if ($this->topic['starter_id'] == $ibforums->member['id']) {
			if ($ibforums->member['g_open_close_topics'] == 1) {
				$passed = 1;
			}
		}
		else
		{
			$passed = 0;
		}
		
		if ($this->moderator['close_topic'] == 1) {
			$passed = 1;
		}
		
		
		if ($passed != 1) $this->moderate_error();
		
		$this->alter_topic( array( 'TOPIC' => $this->topic['tid'], 'FIELD' => 'state', 'VALUE' => 'closed' ) );
		
		$this->moderate_log("Locked Topic");
	
		$print->redirect_screen( $ibforums->lang['p_closed'], "act=SF&f=".$this->forum['id'] );
		
	}


	/*************************************************/
	// PIN TOPIC:
	// ---------------
	//
	/*************************************************/
	
	function pin_topic() {
		global $std, $ibforums, $DB, $print;
		
		if ($this->topic['PIN_STATE'] == 1) {
			$this->moderate_error();
		}
		
		$passed = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1) {
			$passed = 1;
		}
		
		else if ($this->moderator['pin_topic'] == 1) {
			$passed = 1;
		}
		else {
			$passed = 0;
		}
		
		if ($passed != 1) $this->moderate_error();
		
		$this->alter_topic( array( 'TOPIC' => $this->topic['tid'], 'FIELD' => 'pinned', 'VALUE' => '1' ) );
		
		$this->moderate_log("Pinned Topic");
	
		$print->redirect_screen( $ibforums->lang['p_pinned'], "act=ST&f=".$this->forum['id']."&t=".$this->topic['tid']."&st=".$ibforums->input['st'] );
		
	}
	
	/*************************************************/
	// UNPIN TOPIC:
	// ---------------
	//
	/*************************************************/
	
	function unpin_topic() {
		global $std, $ibforums, $DB, $print;
		
		if ($this->topic['pinned'] == 0) {
			$this->moderate_error();
		}
		
		$passed = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1) {
			$passed = 1;
		}
		
		else if ($this->moderator['unpin_topic'] == 1) {
			$passed = 1;
		}
		else {
			$passed = 0;
		}
		
		if ($passed != 1) $this->moderate_error();
		
		$this->alter_topic( array( 'TOPIC' => $this->topic['tid'], 'FIELD' => 'pinned', 'VALUE' => '0' ) );
		
		$this->moderate_log("Unpinned Topic");
	
		$print->redirect_screen( $ibforums->lang['p_unpinned'], "act=ST&f=".$this->forum['id']."&t=".$this->topic['tid']."&st=".$ibforums->input['st'] );
		
	}
	
//+---------------------------------------------------------------------------------------------
	
			
	/*************************************************/
	// MODERATE ERROR:
	// ---------------
	//
	// Function for error messages in this script
	//
	/*************************************************/
	
	function moderate_error($msg = 'moderate_no_permission') {
		global $std;
		
		$std->Error( array( 'LEVEL' => 2, 'MSG' => $msg ) );
		
		// Make sure we exit..
		
		exit();
	}
	
	/*************************************************/
	// MODERATE LOG:
	// ---------------
	//
	// Function for adding the mod action to the DB
	//
	/*************************************************/
	
	function moderate_log($title = 'unknown') {
		global $std, $ibforums, $DB, $HTTP_REFERER, $QUERY_STRING;
		
		$db_string = $std->compile_db_string( array (
														'forum_id'    => $ibforums->input['f'],
														'topic_id'    => $ibforums->input['t'],
														'post_id'     => $ibforums->input['p'],
														'member_id'   => $ibforums->member['id'],
														'member_name' => $ibforums->member['name'],
														'ip_address'  => $ibforums->input['IP_ADDRESS'],
														'http_referer'=> $HTTP_REFERER,
														'ctime'       => time(),
														'topic_title' => $this->topic['title'],
														'action'      => $title,
														'query_string'=> $QUERY_STRING,
													)
										    );
		
		$DB->query("INSERT INTO ibf_moderator_logs (" .$db_string['FIELD_NAMES']. ") VALUES (". $db_string['FIELD_VALUES'] .")");
		
	}
	
	
	/*************************************************/
	// Re Count topics for the forums:
	// ---------------
	//
	// Handles simple moderation functions, saves on
	// writing the same code over and over.
	// ASS_U_ME's that the requesting user has been
	// authenticated by this stage.
	//
	/*************************************************/
	
	function recount($fid="") {
		global $ibforums, $root_path, $DB, $std;
		
		if ($fid == "")
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'move_no_source' ) );
		}
		
		// Get the topics..
		
		$DB->query("SELECT COUNT(tid) as count FROM ibf_topics WHERE approved=1 and forum_id='".$fid."'");
		$topics = $DB->fetch_row();
		
		// Get the posts..
		
		$DB->query("SELECT COUNT(pid) as count FROM ibf_posts WHERE queued <> 1 and forum_id='".$fid."'");
		$posts = $DB->fetch_row();
		
		// Get the forum last poster..
		
		$DB->query("SELECT tid, title, last_poster_id, last_poster_name, last_post FROM ibf_topics WHERE approved=1 and forum_id='".$fid."' ORDER BY last_post DESC LIMIT 0,1");
		$last_post = $DB->fetch_row();
		
		// Reset this forums stats
		
		$db_string = $DB->compile_db_update_string( array (
															 'last_poster_id'   => $last_post['last_poster_id'],
															 'last_poster_name' => $last_post['last_poster_name'],
															 'last_post'        => $last_post['last_post'],
															 'last_title'       => $last_post['title'],
															 'last_id'          => $last_post['tid'],
															 'topics'           => $topics['count'],
															 'posts'            => $posts['count']
												 )        );
												 
		$DB->query("UPDATE ibf_forums SET $db_string WHERE id='".$fid."'");
		
	}
	
	
	/*************************************************/
	// ALTER TOPIC:
	// ---------------
	//
	// Handles simple moderation functions, saves on
	// writing the same code over and over.
	// ASS_U_ME's that the requesting user has been
	// authenticated by this stage.
	//
	/*************************************************/
	
	function alter_topic( $data = array() ) {
		global $ibforums, $DB;
		
		if ( $data['FIELD'] == "" ) return -1;
		if ( $data['VALUE'] == "" ) return -1;
		if ( $data['TOPIC'] == "" ) return -1;
		
		if ($data['TOPIC'] < 1) return -1;
		
		$data['VALUE'] = preg_replace( "/'/", "\\'", $data['VALUE'] );
		
		$DB->query("UPDATE ibf_topics SET ".$data['FIELD']."='".$data['VALUE']."' WHERE tid='".$data['TOPIC']."'");
	}
		
	/*****************************************************/
	// HTML: start form.
	// ------------------
	// Returns the HTML for the <FORM> opening tag
	/*****************************************************/
	
	function html_start_form($additional_tags=array()) {
		global $ibforums;
		
		$form = "<form action='{$this->base_url}' method='POST' name='REPLIER'>".
				"<input type='hidden' name='st' value='".$ibforums->input[st]."'>".
				"<input type='hidden' name='act' value='Mod'>".
				"<input type='hidden' name='s' value='".$ibforums->session_id."'>".
				"<input type='hidden' name='f' value='".$this->forum['id']."'>";
				
		// Any other tags to add?
		
		if (isset($additional_tags)) {
			foreach($additional_tags as $k => $v) {
				$form .= "\n<input type='hidden' name='{$v[0]}' value='{$v[1]}'>";
			}
		}
		
		return $form;
    }
    
}

?>
