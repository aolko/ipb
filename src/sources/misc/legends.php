<?php

/*
+--------------------------------------------------------------------------
|   IBFORUMS v1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001,2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > Show all emo's / BB Tags module
|   > Module written by Matt Mecham
|   > Date started: 18th April 2002
|
|	> Module Version Number: 1.0.0
+--------------------------------------------------------------------------
*/


$idx = new legends;

class legends {

    var $output    = "";
    var $base_url  = "";
    var $html      = "";

    function legends() {
    
    	//------------------------------------------------------
    	// $is_sub is a boolean operator.
    	// If set to 1, we don't show the "topic subscribed" page
    	// we simply end the subroutine and let the caller finish
    	// up for us.
    	//------------------------------------------------------
    
        global $ibforums, $DB, $std, $print, $skin_universal;
        
        $ibforums->lang    = $std->load_words($ibforums->lang, 'lang_legends', $ibforums->lang_id );

        require "./Skin/".$ibforums->skin_id."/skin_legends.php";
    	$this->html = new skin_legends();
    	
    	$this->base_url        = "{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}";
    	
    	
    	
    	//--------------------------------------------
    	// What to do?
    	//--------------------------------------------
    	
    	switch($ibforums->input['CODE'])
    	{
    		case 'emoticons':
    			$this->show_emoticons();
    			break;
    		
    		default:
    			$this->show_emoticons();
    			break;
    	}
    	
    	// If we have any HTML to print, do so...
    	
        $print->pop_up_window( $this->page_title, $this->output );
    		
 	}
 	
 	function show_emoticons()
 	{
 		global $ibforums, $DB, $std;
 		
 		$this->page_title = $ibforums->lang['emo_title'];
 		
 		$this->output .= $this->html->emoticon_javascript();
 		
 		$this->output .= $this->html->page_header( $ibforums->lang['emo_title'], $ibforums->lang['emo_type'], $ibforums->lang['emo_img'] );
 		
 		$DB->query("SELECT typed, image from ibf_emoticons");
			
		if ( $DB->get_num_rows() )
		{
			while ( $r = $DB->fetch_row() )
			{
				$this->output .= $this->html->emoticons_row( stripslashes($r['typed']), stripslashes($r['image']) );
											
			}
		}
		
		$this->output .= $this->html->page_footer();
    	
 	}
 	
 	
        
}

?>





