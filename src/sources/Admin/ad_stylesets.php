<?php

/*
+--------------------------------------------------------------------------
|   IBFORUMS v1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001,2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > Help Control functions
|   > Module written by Matt Mecham
|   > Date started: 2nd April 2002
|
|	> Module Version Number: 1.0.0
+--------------------------------------------------------------------------
*/



$idx = new ad_settings();


class ad_settings {

	var $base_url;

	function ad_settings() {
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;

		switch($IN['code'])
		{
			case 'wrapper':
				$this->list_wrappers();
				break;
				
			case 'add':
				$this->do_form('add');
				break;
				
			case 'edit':
				$this->do_form('edit');
				break;
				
			case 'doadd':
				$this->save_skin('add');
				break;
				
			case 'doedit':
				$this->save_skin('edit');
				break;
				
			case 'remove':
				$this->remove();
				break;
			
			//-------------------------
			case 'memskins':
				$this->mem_skins();
				break;
				
			case 'export':
				$this->export();
				break;
				
			default:
				$this->list_sets();
				break;
		}
		
	}
	
	//----------------------------------------------------
	
	function export()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing skin set ID, go back and try again");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT * from ibf_skins WHERE uid='".$IN['id']."'");
		
		if ( ! $row = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the information from the database");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT * from ibf_images WHERE imid='".$row['img_id']."'");
		
		if ( ! $img = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the information from the database");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT * from ibf_css WHERE cssid='".$row['css_id']."'");
		
		if ( ! $css = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the information from the database");
		}
		
		
		//+-------------------------------
		
		$DB->query("SELECT * from ibf_tmpl_names WHERE skid='".$row['set_id']."'");
		
		if ( ! $tmpl = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the information from the database");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT * from ibf_templates WHERE tmid='".$row['tmpl_id']."'");
		
		if ( ! $wrap = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the information from the database");
		}
		
		
		//+-------------------------------
		
		if ($INFO['base_dir'] == './')
		{
			$INFO['base_dir'] = str_replace( '\\', '/', getcwd() ) . '/';
		}
		
		$archive_dir   = $INFO['base_dir']."archive_out";
		$images_dir    = $INFO['base_dir']."style_images/".$img['imid'];
		$css_file      = $INFO['base_dir']."style_sheets/stylesheet_{$css['cssid']}.css";
		$templates_dir = $INFO['base_dir']."style_templates/".$tmpl['skid'];
		
		require $root_dir."sources/lib/tar.php";
		
		if (!is_dir($archive_dir))
		{
			$ADMIN->error("Could not locate $archive_dir, is the directory there?");
		}
		
		if (!is_writeable($archive_dir))
		{
			$ADMIN->error("Cannot write in $archive_dir, CHMOD via FTP to 0755 or 0777 to enable this script to write into it. IBF cannot do this for you");
		}
		
		if (!file_exists($css_file))
		{
			$ADMIN->error("Could not locate $css_file, is the directory there?");
		}
		
		if (!is_dir($templates_dir))
		{
			$ADMIN->error("Could not locate $templates_dir, is the directory there?");
		}
		
		if (!is_dir($images_dir))
		{
			$ADMIN->error("Could not locate $images_dir, is the directory there?");
		}
		
		//+-------------------------------
		// Set up the dir structure
		//+-------------------------------
		
		$css_name  = "stylesheet.css";
		$wrap_name = "wrapper.html";
		
		$img_dir   = 'images';
		$templ_dir = 'templates';
		
		$pack_name = preg_replace( "/\s{1,}/", "_", $row['sname'] );
		
		$new_dir = "set-".$pack_name;
		
		//+-------------------------------
		
		if ( ! mkdir($archive_dir."/".$new_dir, 0777) )
		{
			$ADMIN->error("Directory creation failed, cannot export skin set. Please check the permission in 'archive_out'");
		}
		
		//+-------------------------------
		
		if ( ! mkdir($archive_dir."/".$new_dir."/".$img_dir, 0777) )
		{
			$ADMIN->error("Directory creation failed, cannot export skin set. Please check the permission in 'archive_out'");
		}
		
		//+-------------------------------
		
		if ( ! mkdir($archive_dir."/".$new_dir."/".$templ_dir, 0777) )
		{
			$ADMIN->error("Directory creation failed, cannot export skin set. Please check the permission in 'archive_out'");
		}
		
		//+-------------------------------
		// Make the wrapper file...
		//+-------------------------------
		
		$wrap['template'] = preg_replace("/\r/", "\n", $wrap['template'] );
		
		$FH = fopen($archive_dir."/".$new_dir."/".$wrap_name, 'w');
		fwrite($FH, $wrap['template'], strlen($wrap['template']));
		fclose($FH);
		
		@chmod($archive_dir."/".$new_dir."/".$wrap_name, 0777);
		
		//+-------------------------------
		// Make the css file...
		//+-------------------------------
		
		if ( ! copy($css_file, $archive_dir."/".$new_dir."/".$css_name) )
		{
			$ADMIN->error("COPY FAILED ON STYLE SHEET $css_name, maybe the script has insufficient permissions?");
		}
		
		@chmod($archive_dir."/".$new_dir."/".$css_name, 0777);
		
		//+-------------------------------
		// Copy over the images...
		//+-------------------------------
		
		if ( ! $ADMIN->copy_dir($images_dir, $archive_dir."/".$new_dir."/".$img_dir) )
		{
			$ADMIN->error( $ADMIN->errors );
		}
		
		// Generate the config file..
		
		$file_content = "<?php\n\n";
		
		foreach($img as $k => $v)
		{
		
			$v = preg_replace( "#style_images/".$img['imid']."/#", "style_images/%img_dir%/", $v );
		
			$file_content .= "\n\$config['$k'] = \"".addslashes($v)."\";";
		}
		
		$file_content .= "\n\n?".">";
		
		$FH = fopen($archive_dir."/".$new_dir."/"."img_conf.inc", 'w');
		fwrite($FH, $file_content, strlen($file_content));
		fclose($FH);
		
		//+-------------------------------
		// Copy over the skin templates...
		//+-------------------------------
		
		if ( ! $ADMIN->copy_dir($templates_dir, $archive_dir."/".$new_dir."/".$templ_dir) )
		{
			$ADMIN->error( $ADMIN->errors );
		}
		
		// Generate the config file..
		
		$file_content = "<?php\n\n".
					    "\$config=array('author' => \"".addslashes($tmpl['author'])."\", ".
					    "'email'=>\"".addslashes($tmpl['email'])."\", ".
					    "'tbl_border'=>\"".addslashes($row['tbl_border'])."\", ".
					    "'tbl_width'=>\"".addslashes($row['tbl_width'])."\", ".
					    "'url'=>\"".addslashes($tmpl['url'])."\")\n\n?".">";
		
		$FH = fopen($archive_dir."/".$new_dir."/"."templates_conf.inc", 'w');
		fwrite($FH, $file_content, strlen($file_content));
		fclose($FH);
		
		//+-------------------------------
		// Add files and write tarball
		//+-------------------------------
		
		$tar = new tar();
		
		$tar->new_tar( $archive_dir, $new_dir.".tar" );
		$tar->add_directory( $archive_dir."/".$new_dir );
		$tar->write_tar();
		
		// Check for errors.
		
		if ($tar->error != "")
		{
			$ADMIN->error($tar->error);
		}
		
		// remove original unarchived directory
		
		$ADMIN->rm_dir($archive_dir."/".$new_dir);
		
		$ADMIN->done_screen("Skin Pack Export Created<br><br>You can download the tar-chive <a href='archive_out/{$new_dir}.tar' target='_blank'>here</a>", "Manage Skin Sets", "act=sets" );
		
		
	}
	
	//----------------------------------------------
	
	function mem_skins()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		$DB->query("SELECT sid FROM ibf_skins WHERE uid='".$IN['oid']."'");
		$old = $DB->fetch_row();
		
		if ($IN['nid'] == 'n')
		{
			$DB->query("UPDATE ibf_members SET skin=NULL WHERE skin='".$old['sid']."'");
		}
		else
		{
			$DB->query("SELECT sid FROM ibf_skins WHERE uid='".$IN['nid']."'");
			$new = $DB->fetch_row();
			$DB->query("UPDATE ibf_members SET skin='".$new['sid']."' WHERE skin='".$old['sid']."'");
		}
		
		$ADMIN->done_screen("Member Skin Choices Updated", "Manage Skin Sets", "act=sets" );
		
	}
	
	
	//-------------------------------------------------------------
	// REMOVE WRAPPERS
	//-------------------------------------------------------------
	
	function remove()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		//+-------------------------------
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing skin set ID, go back and try again");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT * FROM ibf_skins WHERE uid='".$IN['id']."'");
		
		if (! $skin = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query that skin set information from the DB");
		}
		
		//+-------------------------------
		
		if ($skin['default_set'] == 1)
		{
			$ADMIN->error("You can not remove this skin set as it is set as the default. Set another skin as default and try again");
		}
		
		//+-------------------------------
		
		// Update the members skins..
		
		$DB->query("UPDATE ibf_members SET skin='' WHERE skin='".$skin['sid']."'");
		
		// Update the forums DB
		
		$DB->query("SELECT id FROM ibf_forums WHERE skin_id='".$skin['sid']."'");
		
		if ( $DB->get_num_rows() )
		{
			$arr = array();
			
			while ( $i = $DB->fetch_row() )
			{
				$arr['forum_skin_'.$i['id']] = '';
			}
		
			$DB->query("UPDATE ibf_forums SET skin_id='' WHERE skin_id='".$skin['sid']."'");
			
			// Remove it from the config file..
			
			$ADMIN->rebuild_config($arr);
			
		}
		
		// Remove skin from the DB
		
		$DB->query("DELETE FROM ibf_skins WHERE uid='".$IN['id']."'");
		
		$std->boink_it($SKIN->base_url."&act=sets");
			
		exit();
		
		
	}
	
	
	
	//-------------------------------------------------------------
	// ADD / EDIT SKIN SETS
	//-------------------------------------------------------------
	
	function save_skin( $type='add' )
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		//+-------------------------------
		
		if ($type == 'edit')
		{
			if ($IN['id'] == "")
			{
				$ADMIN->error("You must specify an existing skin set ID, go back and try again");
			}
		}
		
		if ($IN['sname'] == "")
		{
			$ADMIN->error("You must specify a name for this skin pack ID");
		}
		
		if ($IN['tbl_width'] == "")
		{
			$ADMIN->error("You must specify a table width");
		}
		
		$barney = array( 'sname'      => stripslashes($HTTP_POST_VARS['sname']),
						 'set_id'     => $IN['template'],
						 'tmpl_id'    => $IN['wrapper'],
						 'img_id'     => $IN['image'],
						 'css_id'     => $IN['css'],
						 'tbl_border' => $IN['tbl_border'],
						 'tbl_width'  => $IN['tbl_width'],
						 'hidden'     => $IN['hidden'],
						 'default_set'=> $IN['default_set'],
					   );
					   
		if ($type == 'add')
		{
		
			$DB->query("SELECT MAX(sid) as new_id FROM ibf_skins");
			
			$row = $DB->fetch_row();
			
			$barney['sid'] = $row['new_id'] + 1;
		
			$db_string = $DB->compile_db_insert_string( $barney );
			
			$DB->query("INSERT INTO ibf_skins (".$db_string['FIELD_NAMES'].") VALUES(".$db_string['FIELD_VALUES'].")");
			
			if ($IN['default_set'] == 1)
			{
				$DB->query("UPDATE ibf_skins SET default_set=0 WHERE sid <> '".$barney['sid']."'");
			}
			
			$std->boink_it($SKIN->base_url."&act=sets");
			
			exit();
			
		}
		else
		{
			$db_string = $DB->compile_db_update_string( $barney );
			
			$DB->query("UPDATE ibf_skins SET $db_string WHERE uid='".$IN['id']."'");
			
			if ($IN['default_set'] == 1)
			{
				$DB->query("UPDATE ibf_skins SET default_set=0 WHERE uid <> '".$IN['id']."'");
			}
			
			$ADMIN->done_screen("Skin Set Updated", "Manage Skin Sets", "act=sets" );
		}
		
		
	}
	
	//-------------------------------------------------------------
	// ADD / EDIT SETS
	//-------------------------------------------------------------
	
	function do_form( $type='add' )
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		//+-------------------------------
		
		$css       = array();
		$wrappers  = array();
		$templates = array();
		$images    = array();
		
		//+-------------------------------
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing wrapper ID, go back and try again");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT * from ibf_skins WHERE uid='".$IN['id']."'");
		
		if ( ! $row = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the information from the database");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT imid, setname FROM ibf_images");
		
		while( $img = $DB->fetch_row() )
		{
			$images[] = array( $img['imid'], $img['setname'] );
		}
		
		//+-------------------------------
		
		$DB->query("SELECT cssid, css_name FROM ibf_css");
		
		while ( $c = $DB->fetch_row() )
		{
			$css[] = array( $c['cssid'], $c['css_name'] );
		}
		
		//+-------------------------------
		
		$DB->query("SELECT tmid, name FROM ibf_templates");
		
		while ( $t = $DB->fetch_row() )
		{
			$wrappers[] = array( $t['tmid'], $t['name'] );
		}
		
		//+-------------------------------
		
		$DB->query("SELECT skid, skname FROM ibf_tmpl_names");
		
		while ( $s = $DB->fetch_row() )
		{
			$templates[] = array( $s['skid'], $s['skname'] );
		}
		
		
		//+-------------------------------
		
		if ($type == 'add')
		{
			$code = 'doadd';
			$button = 'Create Skin Set';
			$row['sname'] = $row['sname'].".2";
			$row['default_set'] = 0;
		}
		else
		{
			$code = 'doedit';
			$button = 'Edit Skin Set';
		}
		
		//+-------------------------------
	
		$ADMIN->page_detail = "You may mix n' match which skin resources you wish to apply to this skin set.";
		$ADMIN->page_title  = "Manage Skin Sets";
		
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->start_form( array( 1 => array( 'code'  , $code      ),
												  2 => array( 'act'   , 'sets'      ),
												  3 => array( 'id'    , $IN['id']   ),
									     ), "theAdminForm", "onSubmit=\"return no_specialchars('sets')\""    );
									     
		//+-------------------------------
		
		$SKIN->td_header[] = array( "&nbsp;"  , "40%" );
		$SKIN->td_header[] = array( "&nbsp;"  , "60%" );

		//+-------------------------------
		
		$ADMIN->html .= $SKIN->start_table( $button );
		
		$ADMIN->html .= $SKIN->js_no_specialchars();
		
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"<b>Skin Set Title</b>",
													$SKIN->form_input('sname', $row['sname']),
									     )      );
									     
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"<b>Use Templates:</b>",
													$SKIN->form_dropdown('template', $templates, $row['set_id']),
									     )      );
									     
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"<b>Use Image & Macro Set:</b>",
													$SKIN->form_dropdown('image', $images, $row['img_id']),
									     )      );
									     
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"<b>Use Stylesheet:</b>",
													$SKIN->form_dropdown('css', $css, $row['css_id']),
									     )      );
									     
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"<b>Use Wrapper:</b>",
													$SKIN->form_dropdown('wrapper', $wrappers, $row['tmpl_id']),
									     )      );
									     
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"<b>Basic Table Width:</b>",
													$SKIN->form_input('tbl_width', $row['tbl_width']),
									     )      );
									     
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"<b>Basic Table Border Colour:</b>",
													$SKIN->form_input('tbl_border', $row['tbl_border']),
									     )      );
									     
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"<b>Hide from Members?</b><br>Useful if you want to make a forum only skin",
													$SKIN->form_yes_no('hidden', $row['hidden']),
									     )      );
									     
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"<b>Set as default skin set?</b><br>Used for unallocted forum and member skins",
													$SKIN->form_yes_no('default_set', $row['default_set']),
									     )      );
												 
		$ADMIN->html .= $SKIN->end_form($button);
										 
		$ADMIN->html .= $SKIN->end_table();
		
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->output();
		
		
	}
	
	//-------------------------------------------------------------
	// SHOW WRAPPERS
	//-------------------------------------------------------------
	
	function list_sets()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		$form_array = array();
		
		$forums      = array();
		$forum_skins = array();
	
		$ADMIN->page_detail = "You may add/edit and remove skin sets.<br><br>Skin sets are groups of style resources. You can mix n' match board wrappers, image and macro sets, stylesheets and skin templates. If you wish to edit any of the resources, please choose the resource you wish to manage from the menu bar on the left.";
		$ADMIN->page_title  = "Manage Skin Sets";
		
		//+-------------------------------
		
		$DB->query("SELECT id, name FROM ibf_forums");
		
		while ( $f = $DB->fetch_row() )
		{
			$forums[ $f['id'] ] = $f['name'];
			
			if ($INFO['forum_skin_'.$f['id']] != "")
			{
				$forum_skins[ $INFO['forum_skin_'.$f['id']] ][] = $f['name'];
			}
		}
		
		
		$SKIN->td_header[] = array( "Title"       , "40%" );
		$SKIN->td_header[] = array( "No. Members" , "20%" );
		$SKIN->td_header[] = array( "Export"      , "10%" );
		$SKIN->td_header[] = array( "Edit"        , "10%" );
		$SKIN->td_header[] = array( "Remove"      , "10%" );
		$SKIN->td_header[] = array( "Hidden"      , "5%" );
		$SKIN->td_header[] = array( "Default"     , "5%" );
		
		//+-------------------------------
		
		$DB->query("select ibf_skins.*, count(ibf_members.id) as mcount from ibf_skins left join ibf_members on(ibf_members.skin=ibf_skins.sid) where (ibf_members.skin is not null or ibf_skins.default_set = 1) group by ibf_skins.sid order by ibf_skins.sname");
		
		$used_ids = array();
		
		if ( $DB->get_num_rows() )
		{
		
			$ADMIN->html .= $SKIN->start_table( "Current Skins Used by Members" );
			
			while ( $r = $DB->fetch_row() )
			{
				$extra = "";
				
				if ( is_array($forum_skins[ $r['sid'] ]) )
				{
					if ( count($forum_skins[ $r['sid'] ]) > 0 )
					{
						$extra = "<br>(Used in forums: ".implode( ",", $forum_skins[ $r['sid'] ] )." )";
					}
				}
				
				$default = '&nbsp;';
				$hidden  = '&nbsp;';
				
				if ($r['hidden'] == 1)
				{
					$hidden = "<span style='color:red;font-weight:bold'>X</span>";
				}
				
				if ($r['default_set'] == 1)
				{
					$default = "<span style='color:red;font-weight:bold'>X</span>";
				}
				
				$ADMIN->html .= $SKIN->add_td_row( array( "<b>".stripslashes($r['sname'])."</b>$extra",
														  "<center>".$r['mcount']."</center>",
														  "<center><a href='".$SKIN->base_url."&act=sets&code=export&id={$r['uid']}'>Export</a></center>",
														  "<center><a href='".$SKIN->base_url."&act=sets&code=edit&id={$r['uid']}'>Edit</a></center>",
														  "<center><a href='".$SKIN->base_url."&act=sets&code=remove&id={$r['uid']}'>Remove</a></center>",
														  "<center>$hidden</center>",
														  "<center>$default</center>",
												 )      );
												   
				$used_ids[] = $r['uid'];
				
				$form_array[] = array( $r['uid'], $r['sname'] );
				
			}
			
			$ADMIN->html .= $SKIN->end_table();
		}
		
		if ( count($used_ids) > 0 )
		{
		
			$DB->query("SELECT * FROM ibf_skins WHERE uid NOT IN(".implode(",",$used_ids).")");
		
			if ( $DB->get_num_rows() )
			{
			
				$SKIN->td_header[] = array( "Title"  , "60%" );
				$SKIN->td_header[] = array( "Export" , "10%" );
				$SKIN->td_header[] = array( "Edit"   , "10%" );
				$SKIN->td_header[] = array( "Remove" , "10%" );
				$SKIN->td_header[] = array( "Hidden" , "5%" );
				$SKIN->td_header[] = array( "Default", "5%" );
			
				$ADMIN->html .= $SKIN->start_table( "Skin Sets not used by Members" );
				
				while ( $r = $DB->fetch_row() )
				{
					
					$extra = "";
				
				if ( is_array($forum_skins[ $r['sid'] ]) )
				{
					if ( count($forum_skins[ $r['sid'] ]) > 0 )
					{
						$extra = "<br>(Used in forums: ".implode( ",", $forum_skins[ $r['sid'] ] )." )";
					}
				}
				
				$default = '&nbsp;';
				$hidden  = '&nbsp;';
				
				if ($r['hidden'] == 1)
				{
					$hidden = "<span style='color:red;font-weight:bold'>X</span>";
				}
				
				if ($r['default_set'] == 1)
				{
					$default = "<span style='color:red;font-weight:bold'>X</span>";
				}
				
				$ADMIN->html .= $SKIN->js_checkdelete();
				
			
				$ADMIN->html .= $SKIN->add_td_row( array( "<b>".stripslashes($r['sname'])."</b>$extra",
														  "<center><a href='".$SKIN->base_url."&act=sets&code=export&id={$r['uid']}'>Export</a></center>",
														  "<center><a href='".$SKIN->base_url."&act=sets&code=edit&id={$r['uid']}'>Edit</a></center>",
														  "<center><a href='javascript:checkdelete(\"act=sets&code=remove&id={$r['uid']}\")'>Remove</a></center>",
														  "<center>$hidden</center>",
														  "<center>$default</center>",
												 )      );
													 
					$form_array[] = array( $r['uid'], $r['sname'] );
													   
				}
				
				$ADMIN->html .= $SKIN->end_table();
			}
		}
		
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->start_form( array( 1 => array( 'code'  , 'add'     ),
												  2 => array( 'act'   , 'sets'    ),
									     )      );
		
		$SKIN->td_header[] = array( "&nbsp;"  , "40%" );
		$SKIN->td_header[] = array( "&nbsp;"  , "60%" );
		
		$ADMIN->html .= $SKIN->start_table( "Create New Skin Set" );
			
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->add_td_row( array( "<b>Base new skin set on...</b>" ,
										  		  $SKIN->form_dropdown( "id", $form_array)
								 )      );
		
		$ADMIN->html .= $SKIN->end_form("Create new Skin Set");
										 
		$ADMIN->html .= $SKIN->end_table();
		
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->start_form( array( 1 => array( 'code'  , 'memskins'     ),
												  2 => array( 'act'   , 'sets'         ),
									     )      );
		
		$SKIN->td_header[] = array( "&nbsp;"  , "40%" );
		$SKIN->td_header[] = array( "&nbsp;"  , "60%" );
		
		$ADMIN->html .= $SKIN->start_table( "Swop members skin choice" );
			
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->add_td_row( array( "<b>Where members use skin...</b>",
										  		  $SKIN->form_dropdown( "oid", $form_array)
								 )      );
								 
		$form_array[] = array( 'n', 'None (Will use whatever is set as default)' );
								 
		$ADMIN->html .= $SKIN->add_td_row( array( "<b>update to use skin...</b>",
										  		  $SKIN->form_dropdown( "nid", $form_array)
								 )      );
		
		$ADMIN->html .= $SKIN->end_form("Update members skin choice");
										 
		$ADMIN->html .= $SKIN->end_table();
		
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->output();
	
	}
	
	
}


?>