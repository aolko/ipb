<?php

class skin_msg {


function preview($data) {
global $ibforums;
return <<<EOF

	<td colspan='2' id='row1'>
     <table cellpadding='0' cellspacing='1' border='0' width='100%' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
        <tr>
            <td>
                <table cellpadding='5' cellspacing='1' border='0' width='100%'>
                <tr>
                <td id='category' valign='top' align='left'><b>{$ibforums->lang['pm_preview']}</b></td>
                </tr>
                <tr>
                <td id='row1' valign='top' align='left'>$data</td>
                </tr>
                </table>
            </td>
        </tr>
    </table>
    </td>
    </tr>
    <tr>

EOF;
}



function pm_popup($text) {
global $ibforums;
return <<<EOF
<script language='javascript'>
<!--
 function goto_inbox() {
 	opener.document.location.href = '{$ibforums->base_url}&act=Msg&CODE=01';
 	window.close();
 }
 
 function goto_this_inbox() {
 	window.resizeTo('650','500');
 	document.location.href = '{$ibforums->base_url}&act=Msg&CODE=01';
 }
//-->
</script>

<table cellspacing='1' cellpadding='10' width='100%' height='100%' align='center' id='row1'>
<tr>
   <td id='pagetitle' align='center'>{$ibforums->lang['pmp_title']}</td>
</tr>
<tr>
   <td align='center'>$text</td>
</tr>
<tr>
   <td align='center' style='font-size:12px;font-weight:bold'><a href='javascript:goto_inbox();'>{$ibforums->lang['pmp_go_inbox']}</a> ( <a href='javascript:goto_this_inbox();'>{$ibforums->lang['pmp_thiswindow']}</a> )<br><br><a href='javascript:window.close();'>{$ibforums->lang['pmp_ignore']}</a></td>
</tr>
</table>
EOF;
}

function archive_html_header() {
global $ibforums;
return <<<EOF
<html>
 <head>
  <title>Private Message Archive</title>
 </head>
 <body bgcolor='#FFFFFF'>
EOF;
}

function archive_html_entry($info) {
global $ibforums;
return <<<EOF
<table cellspacing='1' cellpadding='0' width='80%' bgcolor='#000000' align='center'>
 <tr>
  <td>
   <table cellspacing='1' cellpadding='4' width='100%' align='center'>
    <tr>
     <td valign='top' bgcolor='#FEFEFE'><font face='verdana' size='3'><b>{$info['msg_title']}</b></td>
    </tr>
    <tr>
    <td bgcolor='#FFFFFF'><font face='verdana size='2'>{$info['msg_content']}</td>
    </tr>
    <tr>
    <td valign='top' bgcolor='#FEFEFE'><font face='verdana' size='3'>Sent by <b>{$info['msg_sender']}</b> on {$info['msg_date']}</td>
    </tr>
   </table>
  </td>
 </tr>
</table>
<br>
EOF;
}

function archive_html_footer() {
global $ibforums;
return <<<EOF
 </body>
</html>
EOF;
}

function archive_complete() {
global $ibforums;
return <<<EOF
                   <td id='row1 colspan='2'><span id='pagetitle'>{$ibforums->lang['arc_comp_title']}</span><br>{$ibforums->lang['arc_complete']}</td>
                 </tr>
EOF;
}

function archive_form($jump_html="") {
global $ibforums;
return <<<EOF
                   <td id='row1 colspan='2'><span id='pagetitle'>{$ibforums->lang['archive_title']}</span><br>{$ibforums->lang['archive_text']}</td>
                 </tr>
                <tr>
                   <td id='row1 colspan='2'>
                    <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post">
                    <input type='hidden' name='act' value='Msg'>
                    <input type='hidden' name='CODE' value='15'>
                    <input type='hidden' name='s' value='{$ibforums->session_id}'>
                   </td>
                 </tr>
                 <tr>
                 	<td id='row1'><b>{$ibforums->lang['arc_folders']}</b></td>
                 	<td id='row1'>$jump_html</td>
                 </tr>
                 <tr>
					<td id='row2'><b>{$ibforums->lang['arc_dateline']}</b></td>
					<td id='row2' valign='middle'><select name='dateline' class='forminput'><option value='1'>1</option><option value='7'>7</option><option value='30' selected>30</option><option value='90'>90</option><option value='365'>365</option><option value='all'>{$ibforums->lang['arc_alldays']}</option></select>&nbsp;&nbsp;{$ibforums->lang['arc_days']}</td>
				 </tr>
				 <tr>
					<td id='row1'><b>{$ibforums->lang['arc_max']}</b></td>
					<td id='row1' valign='middle'><select name='number' class='forminput'><option value='5'>5</option><option value='10'>10</option><option value='20' selected>20</option><option value='30'>30</option><option value='40'>40</option><option value='50'>50</option></select></td>
				 </tr>
				 <tr>
					<td id='row2'><b>{$ibforums->lang['arc_delete']}</b></td>
					<td id='row2' valign='middle'><select name='delete' class='forminput'><option value='yes' selected>{$ibforums->lang['arc_yes']}</option><option value='no'>{$ibforums->lang['arc_no']}</option></select></td>
				 </tr>
				 <tr>
					<td id='row1'><b>{$ibforums->lang['arc_type']}</b></td>
					<td id='row1' valign='middle'><select name='type' class='forminput'><option value='xls' selected>{$ibforums->lang['arc_xls']}</option><option value='html'>{$ibforums->lang['arc_html']}</option></select></td>
				 </tr>
				 <tr>
				  <td colspan='2' align='center' id='row2'><input type="submit" value="{$ibforums->lang['arc_submit']}" class='forminput'></td>
				 </tr>
				 </form>
EOF;
}

function No_msg_inbox() {
global $ibforums;
return <<<EOF
      <tr>
      <td id='row1' colspan='5' align='center'><b>{$ibforums->lang['inbox_no_msg']}</b></td>                
      </tr>
EOF;
}

function prefs_add_dirs() {
global $ibforums;
return <<<EOF
                <tr>
                   <td colspan='2' id='category'><b>{$ibforums->lang['prefs_new']}</b></td>
                 </tr>
                <tr>
                   <td id='row1 colspan='2'>{$ibforums->lang['prefs_text_b']}</td>
                 </tr>
EOF;
}

function end_address_table() {
global $ibforums;
return <<<EOF
</table></td></tr>
EOF;
}

function Address_header() {
global $ibforums;
return <<<EOF
                   <td colspan='2' id='category'><b>{$ibforums->lang['address_current']}</b></td>
                 </tr>
EOF;
}

function Address_none() {
global $ibforums;
return <<<EOF
      <tr>
      <td id='row1 colspan='2' align='center'><b>{$ibforums->lang['address_none']}</b></td>                
      </tr>
EOF;
}

function address_edit($data) {
global $ibforums;
return <<<EOF
                  <td colspan='2' id='category'><b>{$ibforums->lang['member_edit']}</b></font></td>
                  </tr>
                  <tr>
                  <td id='row1 align='left' valign='middle' colspan='2'>
                    <table cellspacing='1' cellpadding='2' width='100%' border='0'>
                    <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post">
                    <input type='hidden' name='act' value='Msg'>
                    <input type='hidden' name='CODE' value='12'>
                    <input type='hidden' name='s' value='{$ibforums->session_id}'>
                    <input type='hidden' name='MID' value='{$data[MEMBER]['contact_id']}'>
                    <tr>
                    <td valign='middle' align='left'><b>{$data[MEMBER]['contact_name']}</b></td>
                    <td valign='middle' align='left'>{$ibforums->lang['enter_desc']}<br><input type='text' name='mem_desc' size='30' maxlength='60' value='{$data[MEMBER]['contact_desc']}' class='forminput'></td>
                    <td valign='middle' align='left'>{$ibforums->lang['allow_msg']}<br>{$data[SELECT]}</td>
                    </tr>
                    <tr>
                    <td colspan='3' align='center'><input type="submit" value="{$ibforums->lang['submit_address_edit']}" class='forminput'></td>
                    </tr>
                    </form>
                    </table>
                    </td>
                    </tr>
EOF;
}

function render_address_row($entry) {
global $ibforums;
return <<<EOF
                 <tr>
                   <td id='row1 align='left' valign='middle'><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Profile&CODE=03&MID={$entry['contact_id']}'><b>{$entry['contact_name']}</b></a> &nbsp; &nbsp;[ {$entry['contact_desc']} ]</td>
                   <td id='row1 align='left' valign='middle'>
                        [ <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Msg&CODE=11&MID={$entry['contact_id']}' class='misc'>{$ibforums->lang['edit']}</a> ] :: [ <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Msg&CODE=10&MID={$entry['contact_id']}'>{$ibforums->lang['delete']}</a> ]
                         &nbsp;&nbsp;( {$entry['text']} )
                   </td>
                 </tr>
EOF;
}

function address_add($mem_to_add) {
global $ibforums;
return <<<EOF
                  <tr>
                  <td colspan='2' id='category'><b>{$ibforums->lang['member_add']}</b></font></td>
                  </tr>
                  <tr>
                  <td id='row1 align='left' valign='middle' colspan='2'>
                    <table cellspacing='1' cellpadding='2' width='100%' border='0'>
                    <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post">
                    <input type='hidden' name='act' value='Msg'>
                    <input type='hidden' name='CODE' value='09'>
                    <input type='hidden' name='s' value='{$ibforums->session_id}'>
                    <tr>
                    <td valign='middle' align='left'>{$ibforums->lang['enter_a_name']}<br><input type='text' name='mem_name' size='20' maxlength='40' value='$mem_to_add' class='forminput'></td>
                    <td valign='middle' align='left'>{$ibforums->lang['enter_desc']}<br><input type='text' name='mem_desc' size='30' maxlength='60' value='' class='forminput'></td>
                    <td valign='middle' align='left'>{$ibforums->lang['allow_msg']}<br><select name='allow_msg' class='forminput'><option value='yes' selected>{$ibforums->lang['yes']}<option value='no'>{$ibforums->lang['no']}</select></td>
                    </tr>
                    <tr>
                    <td id='row2' colspan='3' align='center'><input type="submit" value="{$ibforums->lang['submit_address']}" class='forminput'></td>
                    </tr>
                    </form>
                    </table>
                    </td>
                    </tr>
EOF;
}

function prefs_footer() {
global $ibforums;
return <<<EOF
                 <tr>
                   <td id='row2 colspan='2' align='center'><input type='submit' value='{$ibforums->lang['prefs_submit']}' class='forminput'></td>
                 </tr></form>
EOF;
}

function prefs_row($data) {
global $ibforums;
return <<<EOF
                 <tr>
                   <td id='row1 colspan='2'><input type='text' name='{$data[ID]}' value='{$data[REAL]}' class='forminput'>{$data[EXTRA]}</td>
                 </tr>
EOF;
}

function Address_table_header() {
global $ibforums;
return <<<EOF
                 <tr>
                 <td valign='top' id='row1 colspan='2'>
                 <table cellpadding='4' cellspacing='0' align='center' width='100%' style='border:1px solid {$this->class['SKIN']['TABLE_BORDER_COL']}'>
                 <tr>
                   <td id='row2 align='left' width='60%' id='titlemedium'><b>{$ibforums->lang['member_name']}</b></td>
                   <td id='row2 align='left' width='40%' id='titlemedium'><b>{$ibforums->lang['enter_block']}</b></td>
                 </tr>
EOF;
}

function Render_msg($data) {
global $ibforums;
return <<<EOF

			<td id='row1 align='left'><span  id='pagetitle'>{$data['msg']['title']}</span><br>{$data['msg']['msg_date']}</td>
            <td id='row1 align='right'>
            <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&CODE=04&act=Msg&MID={$data['member']['id']}&MSID={$data['msg']['msg_id']}'>{$ibforums->skin[M_REPLY]}</a>
            &nbsp;
            <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&CODE=02&act=Msg&MID={$data['member']['id']}'>{$ibforums->skin[M_ADDMEM]}</a>
            &nbsp;
            <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&CODE=05&act=Msg&MSID={$data['msg']['msg_id']}&VID={$data['member']['VID']}'>{$ibforums->skin[M_DELETE]}</a>
            </td>
            </tr>
           
            <tr>
             <td colspan='2'>
			  <table width='100%' cellpadding='4' cellspacing='1' bgcolor='{$ibforums->skin['tbl_border']}'>
                <tr>
                	<td valign='middle' id='titlemedium' align='center' width='25%'>{$ibforums->lang['author']}</td>
                	<td valign='middle' id='titlemedium' align='center' width='75%'>{$ibforums->lang['m_pmessage']}</td>
                </tr>
                <tr>
        		    <td valign='top' id='row1'><span id='normalname'>{$data['member']['name']}</span><br>{$data['member']['avatar']}<br><span id="membertitle">{$data['member']['title']}</span><span id='postdetails'><br>{$data['member']['MEMBER_GROUP']}<br>{$data['member']['MEMBER_POSTS']}<br>{$data['member']['MEMBER_JOINED']}</span></td>
                    <td valign='top' height='100%' id='row1'>
            		    <span id='postcolor'>
           			     {$data['msg']['message']}
                         {$data['member']['signature']}
                        </span>
                    </td>
                 </tr>
                 <tr>
                   <td colspan='2' id='titlemedium' align='center'>{$data['member']['MESSAGE_ICON']}{$data['member']['EMAIL_ICON']}{$data['member']['WEBSITE_ICON']}{$data['member']['ICQ_ICON']}{$data['member']['AOL_ICON']}{$data['member']['YAHOO_ICON']}</td>
                 </tr>
			  </table>
			</td>
		</tr>
        <tr>
        <td id='row1 align='right' colspan='2'>
                     <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" name='jump' method="post">
                     <input type='hidden' name='act' value='Msg'>
                     <input type='hidden' name='CODE' value='01'>
                     <input type='hidden' name='s'    value='{$ibforums->session_id}'>
                     <font class='misc'><b>{$ibforums->lang[goto_folder]}:</b>&nbsp; {$data['jump']}
                     <input type='submit' name='submit' value='{$ibforums->lang[goto_submit]}' class='forminput'>
                     </form>
            </td>
        </tr>


EOF;
}

function inbox_table_header($dirname, $info) {
global $ibforums;
return <<<EOF
                 <!-- inbox folder -->
                     <script language='JavaScript'>
                     <!--
                     function CheckAll(cb) {
                         var fmobj = document.mutliact;
                         for (var i=0;i<fmobj.elements.length;i++) {
                             var e = fmobj.elements[i];
                             if ((e.name != 'allbox') && (e.type=='checkbox') && (!e.disabled)) {
                                 e.checked = fmobj.allbox.checked;
                             }
                         }
                     }
                     function CheckCheckAll(cb) {	
                         var fmobj = document.mutliact;
                         var TotalBoxes = 0;
                         var TotalOn = 0;
                         for (var i=0;i<fmobj.elements.length;i++) {
                             var e = fmobj.elements[i];
                             if ((e.name != 'allbox') && (e.type=='checkbox')) {
                                 TotalBoxes++;
                                 if (e.checked) {
                                     TotalOn++;
                                 }
                             }
                         }
                         if (TotalBoxes==TotalOn) {fmobj.allbox.checked=true;}
                         else {fmobj.allbox.checked=false;}
                     }
                     //-->
                     </script>
                 
                 <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id};CODE=06;act=Msg" name='mutliact' method="post">
                 <input type='hidden' name='act' value='Msg'>
                 <input type='hidden' name='CODE' value='06'>
                 <input type='hidden' name='s'    value='{$ibforums->session_id}'>
                 <tr>
                 <td valign='top' colspan='2'><span id='pagetitle'>$dirname</span></td>
                 </tr>
                 <tr>
                   <td colspan='2'><b>{$ibforums->lang['total_messages']}:</b> {$info['total_messages']} {$info['full_percent']}<br><b>{$ibforums->lang['messages_left']}</b> {$info['space_free']} {$info['full_messenger']}</td>
                 </tr>
                 <tr>
                 <td valign='top' colspan='2'>
                 <table cellpadding='4' cellspacing='1' align='center' width='100%' style='border:1px solid {$ibforums->skin['tbl_border']}'>
                 <tr>
                   <td  align='left' width='5%' id='titlemedium'>&nbsp;</td>
                   <td  align='left' width='40%' id='titlemedium'><b>{$ibforums->lang['message_title']}</b></td>
                   <td  align='left' width='30%' id='titlemedium'><b>{$ibforums->lang['message_from']}</b></td>
                   <td  align='left' width='20%' id='titlemedium'><b>{$ibforums->lang['message_date']}</b></td>
                   <td  align='left' width='5%' id='titlemedium'><input name="allbox" type="checkbox" value="Check All" onClick="CheckAll();"></td>
                 </tr>
EOF;
}

function inbox_row($data) {
global $ibforums;
return <<<EOF
                 <tr>
                   <td id='row2 align='left' valign='middle'>{$data['msg']['icon']}</td>
                   <td id='row2 align='left'><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Msg&CODE=03&VID={$data['stat']['current_id']}&MSID={$data['msg']['msg_id']}'>{$data['msg']['title']}</a></td>
                   <td id='row2 align='left'><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Profile&MID={$data['msg']['from_id']}'>{$data['msg']['from_name']}</a> {$data['msg']['add_to_contacts']}</td>
                   <td id='row2 align='left'>{$data['msg']['date']}</td>
                   <td id='row2 align='left'><input type='checkbox' name='msgid-{$data['msg']['msg_id']}' value='yes' class='forminput'></td>
                 </tr>
EOF;
}

function end_inbox($vdi_html) {
global $ibforums;
return <<<EOF
				  <tr>
				   <td align='right' nowrap id='titlemedium' colspan='5'>
                     <input type='submit' name='move' value='{$ibforums->lang['move_button']}' class='forminput'> $vdi_html {$ibforums->lang['move_or_delete']} <input type='submit' name='delete' value='{$ibforums->lang['delete_button']}' class='forminput'> {$ibforums->lang['selected_msg']}
                  </td>
                  </tr>
                  </table>
                  </form>
                  </td></tr>
                  <tr>
                  <td id='row1 align='left' valign='middle'>
                    {$ibforums->skin['M_READ']}&nbsp;{$ibforums->lang['icon_read']}
                    &nbsp;{$ibforums->skin['M_UNREAD']}&nbsp;{$ibforums->lang['icon_unread']}
                  </td>
                  <td id='row1 align='right' valign='middle'>
                     <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id};CODE=01;act=Msg" name='jump' method="post">
                     <input type='hidden' name='act' value='Msg'>
                     <input type='hidden' name='CODE' value='01'>
                     <input type='hidden' name='s'    value='{$ibforums->session_id}'>
                     <b>{$ibforums->lang['goto_folder']}: </b>&nbsp; $vdi_html
                     <input type='submit' name='submit' value='{$ibforums->lang['goto_submit']}' class='forminput'>
                     </form>
                 </td>
                  </tr>
EOF;
}

function send_form_footer() {
global $ibforums;
return <<<EOF
				<tr>
				 <td colspan='2' id='category'><b>{$ibforums->lang['msg_options']}</b></td>
                </tr>
			    <tr>
			     <td id='row1' align='left' width='40%'>&nbsp;</td>
				 <td id='row1' align='left' width='60%'>
				    <input type='checkbox' name='add_sent' value='yes' checked>&nbsp;<b>{$ibforums->lang['auto_sent_add']}</b>
				    <br><input type='checkbox' name='add_tracking' value='1'>&nbsp;<b>{$ibforums->lang['read_rec_notify']}</b>
				 </td>
				</tr>
                <tr>
                <td id='row2' align='center' colspan='2'>
                <input type="submit" value="{$ibforums->lang['submit_send']}" class='forminput' name='submit'>
                <input type="submit" value="{$ibforums->lang['pm_pre_button']}" class='forminput' name='preview'>
                </td>
                </tr>
                </form>
EOF;
}

function Send_form($data) {
global $ibforums;
return <<<EOF
     <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post" name='REPLIER' onSubmit='return ValidateForm(1)'>
     <input type='hidden' name='act' value='Msg'>
     <input type='hidden' name='CODE' value='04'>
     <input type='hidden' name='MODE' value='01'>
     <input type='hidden' name='s' value='{$ibforums->session_id}'>
                 
                   <td colspan='2' id='category'><b>{$ibforums->lang['to_whom']}</b></td>
                 </tr>
                <tr>
                <td id='row1' align='left' width='40%'>{$ibforums->lang['address_list']}</td>
                <td id='row1' align='left' width='60%' valign='top'>{$data[CONTACTS]}</td>
                </tr>  
                <tr>
                <td id='row1' align='left' width='40%'>{$ibforums->lang['enter_name']}</td>
                <td id='row1' align='left' width='60%' valign='top'><input type='text' name='entered_name' value='{$data[N_ENTER]}' class='forminput'></td>
                </tr>
                <tr>
                <td colspan='2' id='category'><b>{$ibforums->lang['enter_message']}</b></td>
                </tr>
                <tr>
                <td id='row1' align='left'  width='40%'>{$ibforums->lang['msg_title']}</td>
                <td id='row1' align='left' width='60%' valign='top'><input type='text' name='msg_title' size='40' maxlength='40' value='{$data[O_TITLE]}' class='forminput'></td>
                </tr>
EOF;
}

function prefs_header() {
global $ibforums;
return <<<EOF
                   <td colspan='2' id='category'><b>{$ibforums->lang['prefs_current']}</b></td>
                 </tr>
                <tr>
                   <td id='row1 colspan='2'>{$ibforums->lang['prefs_text_a']}</td>
                 </tr>
                    <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post">
                    <input type='hidden' name='act' value='Msg'>
                    <input type='hidden' name='CODE' value='08'>
                    <input type='hidden' name='s' value='{$ibforums->session_id}'>
EOF;
}


}
?>