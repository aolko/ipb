<?php

/*
+--------------------------------------------------------------------------
|   IBFORUMS v1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001,2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > Admin wrapper script
|   > Script written by Matt Mecham
|   > Date started: 1st March 2002
|
+--------------------------------------------------------------------------
*/

/*-----------------------------------------------
  USER CONFIGURABLE ELEMENTS
 ------------------------------------------------*/
 
// Root path

$root_path = "./";

// Check IP address to see if they match?
// this may cause problems for users on proxies
// where the IP address changes during a session

$check_ip = 1;

// Use GZIP content encoding for fast page generation
// in the admin center?

$use_gzip = 1;
 
 
/*-----------------------------------------------
  NO USER EDITABLE SECTIONS BELOW
 ------------------------------------------------*/
 
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
set_magic_quotes_runtime(0);

if (function_exists('ini_get'))
{
	$safe_switch = ini_get("safe_mode") ? 1 : 0;
}
else
{
	$safe_switch = 1;
}

define( 'SAFE_MODE_ON', $safe_switch );

if (function_exists("set_time_limit") == 1 and SAFE_MODE_ON == 0)
{
  @set_time_limit(0);
}


class Debug {
    function startTimer() {
        global $starttime;
        $mtime = microtime ();
        $mtime = explode (' ', $mtime);
        $mtime = $mtime[1] + $mtime[0];
        $starttime = $mtime;
    }
    function endTimer() {
        global $starttime;
        $mtime = microtime ();
        $mtime = explode (' ', $mtime);
        $mtime = $mtime[1] + $mtime[0];
        $endtime = $mtime;
        $totaltime = round (($endtime - $starttime), 5);
        return $totaltime;
    }
}

class info {

	var $vars    = "";
	var $version = '1.0';
	
	
	function info($INFO)
	{
		//global $INFO;
		$this->vars = $INFO;
	}
}
		


/*-----------------------------------------------
  Import $INFO
 ------------------------------------------------*/
 
require $root_path."conf_global.php";

$ibforums = new info($INFO);

$Debug = new Debug;
$Debug->startTimer();

/*-----------------------------------------------
  Make sure our data is reset on each invocation
 ------------------------------------------------*/
$MEMBER          = array();
$SESSION_ID      = "";
$SKIN            = "";

// Put an end to insane thoughs before they begin
$MEMBER_NAME     = "";
$MEMBER_PASSWORD = "";
$MEMBER_EMAIL    = "";
$UserName        = "";
$PassWord        = "";


/*-----------------------------------------------
  Load up our classes (compiled into one package)
 ------------------------------------------------*/
 
require $root_path."sources/functions.php";

$std     = new FUNC;

/*-----------------------------------------------
  Steralize our FORM and GET input
 ------------------------------------------------*/

$IN = $std->parse_incoming();

$IN['AD_SESS'] = $HTTP_POST_VARS['adsess'] ? $HTTP_POST_VARS['adsess'] : $HTTP_GET_VARS['adsess'];

/*-----------------------------------------------
  Import $PAGES and $CATS
 ------------------------------------------------*/
 
require $root_path."sources/Admin/admin_pages.php";


/*-----------------------------------------------
  Import Skinable elements
 ------------------------------------------------*/
 
require $root_path."sources/Admin/admin_skin.php";

$SKIN = new admin_skin();

/*-----------------------------------------------
  Import Admin Functions
 ------------------------------------------------*/
 
require $root_path."sources/Admin/admin_functions.php";

$ADMIN = new admin_functions();

/*-----------------------------------------------
  Load up our database library
 ------------------------------------------------*/
 
$INFO['sql_driver'] = !$INFO['sql_driver'] ? 'mySQL' : $INFO['sql_driver'];

$to_require = $root_path."sources/Drivers/".$INFO['sql_driver'].".php";
require ($to_require);

$DB = new db_driver;

$DB->obj['sql_database']     = $INFO['sql_database'];
$DB->obj['sql_user']         = $INFO['sql_user'];
$DB->obj['sql_pass']         = $INFO['sql_pass'];
$DB->obj['sql_host']         = $INFO['sql_host'];
$DB->obj['sql_tbl_prefix']   = $INFO['sql_tbl_prefix'];

// Get a DB connection
$DB->connect();

//------------------------------------------------
// Fix up the "show" ID's for the menu tree...
//
// show=1,4,5 holds the current ID's, clicking on a
// collapse link creates out=4 - "4" is then removed
// from the show link.
//
// Good eh?
//------------------------------------------------

if ($IN['show'] == 'none')
{
	$IN['show'] = "";
}
else if ($IN['show'] == 'all')
{
	$IN['show']     = "";
	
	foreach($CATS as $cid => $name)
	{
		$IN['show'] .= $cid.',';
	}
}
else
{
	$IN['show'] = preg_replace( "/(?:^|,)".$IN['out']."(?:,|$)/", ",", $IN['show'] );
	$IN['show'] = preg_replace( "/,,/" , "" , $IN['show'] );
	$IN['show'] = preg_replace( "/,$/" , "" , $IN['show'] );
	$IN['show'] = preg_replace( "/^,/" , "" , $IN['show'] );
}


//------------------------------------------------
// Admin.php Rules:
//
// No adsess number?
// -----------------
//
// Then we log into the admin CP
//
// Got adsess number?
// ------------------
//
// Then we check the cookie "ad_login" for a session key.
//
// If this session key matches the one stored in the admin_sessions
// table, then we check the data against the data stored in the 
// profiles table.
//
// The session key and ad_sess keys are generated each time we log in.
//
// If we don't have a valid adsess in the URL, then we ask for a log in.
//
//------------------------------------------------

$session_validated = 0;
$this_session      = array();

$validate_login = 0;

if ($IN['login'] != 'yes') {

	if ( (!$IN['adsess']) or (empty($IN['adsess'])) or (!isset($IN['adsess'])) or ($IN['adsess'] == "") )
	{
		//----------------------------------
		// No URL adsess found, lets log in.
		//----------------------------------
		
		do_login("No administration session found");
	}
	else
	{
		//----------------------------------
		// We have a URL adsess, lets verify...
		//----------------------------------
		
		$DB->query("SELECT * FROM ibf_admin_sessions WHERE ID='".$IN['adsess']."'");
		$row = $DB->fetch_row();
		
		if ($row['ID'] == "")
		{
			//----------------------------------
			// Fail-safe, no DB record found, lets log in..
			//----------------------------------
			
			do_login("Could not retrieve session record");
		}
		else if ($row['MEMBER_ID'] == "")
		{
		
			//----------------------------------
			// No member ID is stored, log in!
			//----------------------------------
			
			do_login("Could not retrieve a valid member id");
			
		}
		else
		{
			//----------------------------------
			// Key is good, check the member details
			//----------------------------------
			
			$DB->query("SELECT * FROM ibf_members WHERE id='".$row['MEMBER_ID']."'");
			$MEMBER = $DB->fetch_row();
			
			if ($MEMBER['id'] == "")
			{
			
				//----------------------------------
				// Ut-oh, no such member, log in!
				//----------------------------------
				
				do_login("Member ID invalid");
				
			}
			else
			{
				//----------------------------------
				// Member found, check passy
				//----------------------------------
				
				if ($row['SESSION_KEY'] != $MEMBER['password'])
				{
					//----------------------------------
					// Passys don't match..
					//----------------------------------
					
					do_login("Session member password mismatch");
					
				}
				else
				{
					//----------------------------------
					// Do we have admin access?
					//----------------------------------
					
					$DB->query("SELECT * FROM ibf_groups WHERE g_id='".$MEMBER['mgroup']."'");
					
					$GROUP = $DB->fetch_row();
					
					if ($GROUP['g_access_cp'] != 1)
					{
						do_login("You do not have access to the administrative CP");
					}
					else
					{
						$session_validated = 1;
						$this_session      = $row;
					}
				}
			}
		}
	}
}
else 
{
	//----------------------------------
	// We must have submitted the form
	// time to check some details.
	//----------------------------------
	
	if ( empty($IN['username']) )
	{
		do_login("You must enter a username before proceeding");
	}
	
	if ( empty($IN['password']) )
	{
		do_login("You must enter a password before proceeding");
	}
	
	//----------------------------------
	// Attempt to get the details from the
	// DB
	//----------------------------------
	
	$DB->query("SELECT name, password, id, mgroup FROM ibf_members WHERE LOWER(name)='".strtolower($IN['username'])."'");
	$mem = $DB->fetch_row();
	
	if ( empty($mem['id']) )
	{
		do_login("Could not find a record matching that username, please check the spelling");
	}
	
	$pass    = md5( $IN['password'] );
	
	if ($pass != $mem['password'])
	{
		do_login("The password entered did not match the one in our records");
	}
	else
	{
		$DB->query("SELECT * FROM ibf_groups WHERE g_id='".$mem['mgroup']."'");
					
		$GROUP = $DB->fetch_row();
		
		if ($GROUP['g_access_cp'] != 1)
		{
			do_login("You do not have access to the administrative CP");
		}
		else
		{
		
			//----------------------------------
			// All is good, rejoice as we set a
			// session for this user
			//----------------------------------
			
			$sess_id = md5( uniqid( microtime() ) );
			
			$db_string = $DB->compile_db_insert_string( array (
																'ID'           => $sess_id,
																'IP_ADDRESS'   => $IN['IP_ADDRESS'],
																'MEMBER_NAME'  => $mem['name'],
																'MEMBER_ID'    => $mem['id'],
																'SESSION_KEY'  => $pass,
																'LOCATION'     => 'index',
																'LOG_IN_TIME'  => time(),
																'RUNNING_TIME' => time(),
													  )        );
													  
			$DB->query("INSERT INTO ibf_admin_sessions (".$db_string['FIELD_NAMES'].") VALUES (".$db_string['FIELD_VALUES'].")");
		
			$IN['AD_SESS'] = $sess_id;
			
			// Print the "well done page"
			
			$ADMIN->page_title = "Log in successful";
			
			$ADMIN->page_detail = "You may now proceed to the administrative control panel";
			
			$ADMIN->html .= $SKIN->start_table("Proceed");
			
			$ADMIN->html .= "<tr><td id='tdrow1'><a href='".$INFO['board_url']."/admin.".$INFO['php_ext']."?adsess=".$IN['AD_SESS']."'>Take me to administrative control panel</a></td></tr>";
			
			$ADMIN->html .= $SKIN->end_table();
			
			$ADMIN->output();
		
		}
		
	}
		
}


//----------------------------------
// Ok, so far so good. If we have a 
// validate session, check the running
// time. if it's older than 2 hours,
// ask for a log in
//----------------------------------


if ($session_validated == 1)
{
	if ($this_session['RUNNING_TIME'] < ( time() - 60*60*2) )
	{
		$session_validated = 0;
		do_login("This administration session has expired");
	}
	
	//------------------------------
	// Are we checking IP's?
	//------------------------------
	
	else if ($check_ip == 1)
	{
		if ($this_session['IP_ADDRESS'] != $IN['IP_ADDRESS'])
		{
			$session_validated = 0;
			do_login("Your current IP address does not match the one in our records");
		}
	}
}

if ($session_validated == 1 )
{
	//------------------------------
	// If we get this far, we're good to go..
	//------------------------------
	
	$IN['AD_SESS'] = $IN['adsess'];
	
	//------------------------------
	// Lets update the sessions table:
	//------------------------------
	
	$DB->query("UPDATE ibf_admin_sessions SET RUNNING_TIME='".time()."', LOCATION='".$IN['act']."' WHERE MEMBER_ID='".$MEMBER['id']."' AND ID='".$IN['AD_SESS']."'");
	
	do_admin_stuff();
	
}
else
{
	//------------------------------
	// Session is not validated...
	//------------------------------
	
	do_login("Session not validated - please attempt to log in again");
	
}



function do_login($message="") {
	global $IN, $DB, $ADMIN, $SKIN;
	
	//-------------------------------------------------------
	// Remove all out of date sessions, like a good boy. Woof.
	//-------------------------------------------------------
	
	$cut_off_stamp = time() - 60*60*2;
	
	$DB->query("DELETE FROM ibf_admin_sessions WHERE RUNNING_TIME < $cut_off_stamp");
	
	//+------------------------------------------------------
	
	$ADMIN->page_detail = "You must have administrative access to successfully log into the Invision Board Admin CP.<br>Please enter your forums username and password below";
	
	if ($message != "")
	{
		$ADMIN->page_detail .= "<br><br><span style='color:red;font-weight:bold'>$message</span>";
	}
	
	$ADMIN->html .= "<script language='javascript'>
					  <!--
					  	if (top.location != self.location) { top.location = self.location }
					  //-->
					 </script>
					 ";
	
	$ADMIN->html .= $SKIN->start_form( array( 1 => array('login', 'yes') ) );
	
	$SKIN->td_header[] = array( "&nbsp;"  , "40%" );
	$SKIN->td_header[] = array( "&nbsp;"  , "60%" );
	
	$ADMIN->html .= $SKIN->start_table( "Verification Required" );
	
	$ADMIN->html .= $SKIN->add_td_row( array( "Your Forums Username:",
											  "<input type='text' style='width:100%' name='username' value=''>",
									 )      );
		
	$ADMIN->html .= $SKIN->add_td_row( array( "Your Forums Password:",
											  "<input type='password' style='width:100%' name='password' value=''>",
									 )      );
									 
	$ADMIN->html .= $SKIN->end_form("Log in");
	
	$ADMIN->html .= $SKIN->end_table();
		
	$ADMIN->output();

}





function do_admin_stuff() {
	global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $ibforums;


	/*----------------------------------
	  What do you want to require today?
	------------------------------------*/
	
	$choice = array(
					 "idx"      => "doframes",
					 "menu"     => "menu",
					 "index"    => "index",
					 "cat"      => "categories",
					 "forum"    => "forums",
					 "mem"      => "member",
					 'group'    => "groups",
					 'mod'      => 'moderator',
					 'op'       => 'settings',
					 'help'     => 'help',
					 'skin'     => 'skins',
					 'wrap'     => 'wrappers',
					 'style'    => 'stylesheets',
					 'image'    => 'imagemacros',
					 'sets'     => 'stylesets',
					 'templ'    => 'templates',
					 'lang'     => 'languages',
					 'import'   => 'skin_import',
					 'modlog'   => 'modlogs',
				   );
	
					
	/***************************************************/
	
	$IN['act'] = $IN['act'] == '' ? "idx" : $IN['act'];
	
	// Check to make sure the array key exits..
	if (! isset($choice[$IN['act']]) ) {
		$IN['act'] = 'idx';
	}
	
	// Require and run
	
	if ($IN['act'] == 'idx')
	{
		print $SKIN->frame_set();
		exit;
	}
	else if ($IN['act'] == 'menu')
	{
		$ADMIN->menu();
	}
	else
	{
		require $root_path."sources/Admin/ad_".$choice[$IN['act']].".php";
	}
	
}



//+-------------------------------------------------
// GLOBAL ROUTINES
//+-------------------------------------------------

function fatal_error($message="", $help="") {
	echo("$message<br><br>$help");
	exit;
}
?>
