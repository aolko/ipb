<?php

/*
+--------------------------------------------------------------------------
|   IBFORUMS v1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001,2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > Board index module
|   > Module written by Matt Mecham
|   > Date started: 17th February 2002
|
|	> Module Version Number: 1.0.0
+--------------------------------------------------------------------------
*/


$idx = new Boards;

class Boards {

    var $output   = "";
    var $base_url = "";
    var $html     = "";
    var $output   = "";
    var $forums   = array();
    var $mods     = array();
    var $cats     = array();
    var $children = array();
    var $nav;
    
    var $news_topic_id = "";
    var $news_forum_id = "";
    var $news_title    = "";
    
    function Boards() {
        global $ibforums, $DB, $std, $print, $skin_universal;
        
        require "./Skin/".$ibforums->skin_id."/skin_boards.php";
        
        $this->base_url = "{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}";

        // Get more words for this invocation!
        $ibforums->lang = $std->load_words($ibforums->lang, 'lang_boards', $ibforums->lang_id);

        $this->html     = new skin_boards();
        
        if (! $ibforums->member['id'] )
        {
        	$ibforums->input['last_visit'] = time();
        }
        
        $this->output .= $this->html->PageTop( $std->get_date( $ibforums->input['last_visit'], 'LONG' ) );
        
        // Get the forums and category info from the DB
        
        $last_c_id = -1;
        
        $DB->query("SELECT f.*, c.id as cat_id, c.position as cat_position, c.state as cat_state, c.name as cat_name, c.description as cat_desc, ".
        		   "c.image, c.url from ibf_forums f, ibf_categories c order by c.position, f.position");
        		   
        		   
        while ( $r = $DB->fetch_row() )
        {
        	if ($last_c_id != $r['cat_id'])
        	{
        		$this->cats[ $r['cat_id'] ] = array( 'id'          => $r['cat_id'],
        											 'position'    => $r['cat_position'],
        											 'state'       => $r['cat_state'],
        											 'name'        => $r['cat_name'],
        											 'description' => $r['cat_desc'],
        											 'image'       => $r['image'],
        											 'url'         => $r['url'],
        										   );
        										   
        		$last_c_id = $r['cat_id'];
        	}
        	
        	if ($r['parent_id'] > 0)
			{
				$this->children[ $r['parent_id'] ][$r['id']] = $r;
			}
			else
			{
				$this->forums[ $r['id'] ] = $r;
			}
        }
        
        $DB->query("SELECT forum_id, member_name, member_id FROM ibf_moderators");
        
        while ( $mod = $DB->fetch_row() )
        {
        	$this->mods[ $mod['forum_id'] ][] = array( 'name' => $mod['member_name'],
        											   'id'   => $mod['member_id'],
        											 );
        }
        
        //-----------------------------------
        // What are we doing?
        //-----------------------------------
        
        if ($ibforums->input['c'] != "")
        {
        	$this->show_single_cat();
        	$this->nav[] = $this->cats[ $ibforums->input['c'] ]['name'];
        }
        else if ($ibforums->input['sub'] == 1)
        {
        	$this->show_subforum();
        	$this->nav[] = $this->forums[ $ibforums->input['f'] ]['name'];
        }
        else
        {
        	$this->process_all_cats();
        }
        
        //*********************************************/
		// Add in show online users
		//*********************************************/
		
		$active = array( 'TOTAL'   => 0 ,
						 'NAMES'   => "",
						 'GUESTS'  => 0 ,
						 'MEMBERS' => 0 ,
						 'ANON'    => 0 ,
					   );
					   
		$stats_html = "";
		
		if ($ibforums->vars['show_active'])
		{
		
			// Get the users from the DB
			
			$time = time() - 900;
			
			$DB->query("SELECT s.member_id, s.member_name, s.login_type, g.suffix, g.prefix FROM ibf_sessions s, ibf_groups g WHERE running_time > '$time' AND g.g_id=s.member_group ORDER BY running_time DESC");
			
			// cache all printed members so we don't double print them
			$cached = array();
			while ($result = $DB->fetch_row() )
			{
				if ($result['member_id'] == 0)
				{
					$active['GUESTS']++;
				}
				else
				{
					if (empty( $cached[ $result['member_id'] ] ) )
					{
						$cached[ $result['member_id'] ] = 1;
						if ($result['login_type'] == 1)
						{
							$active['ANON']++;
						}
						else
						{
							$active['MEMBERS']++;
							$active['NAMES'] .= " <span id='highlight'>&gt;</span><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Profile&MID={$result['member_id']}'>{$result['prefix']}{$result['member_name']}{$result['suffix']}</a>";
						}
					}
					
				}
			}
			
			$active['TOTAL'] = $active['MEMBERS'] + $active['GUESTS'] + $active['ANON'];
			
			// Show a link?
			
			if ($ibforums->vars['allow_online_list'])
			{
				$active['LINK'] = '[ '."<a href='".$this->base_url."&act=Online&CODE=listall'>".$ibforums->lang['browser_user_list']."</a>".' ]';
			}
			
			$stats_html .= $this->html->ActiveUsers($active);
			
			
			//-----------------------------------------------
			// Are we viewing the calendar?
			//-----------------------------------------------
			
			if ($ibforums->vars['show_birthdays'])
			{
			
				$user_time = time() + ($ibforums->vars['TIME_ZONE'] * 3600) + ($ibforums->member['TIME_ADJUST'] * 3600 );
				
				$date = getdate($user_time);
				
				$day   = $date['mday'];
				$month = $date['mon'];
				$year  = $date['year'];
				
				$birthstring = "";
				$count       = 0;
				
				$DB->query("SELECT id, name, bday_day as DAY, bday_month as MONTH, bday_year as YEAR from ibf_members WHERE bday_day='$day' and bday_month='$month'");
				
				while ($user = $DB->fetch_row())
				{
					$birthstring .= "<span id='highlight'>&gt;</span><a href='{$this->base_url}&act=Profile&CODE=03&MID={$user['id']}'>{$user['name']}</a> ";
					if ($user['YEAR'])
					{
						$pyear = $year - $user['YEAR'];  // $year = 2002 and $user['YEAR'] = 1976
						$birthstring .= "(<b>$pyear</b>) ";
					}
					$count++;
				}
				
				$lang = $ibforums->lang['no_birth_users'];
				
				if ($count > 0) {
					$lang = ($count > 1) ? $ibforums->lang['birth_users'] : $ibforums->lang['birth_user'];
				}
				else
				{
					$count = "";
				}
				
				$stats_html .= $this->html->birthdays( $birthstring, $count, $lang  );
			}
				
		}
		
		//*********************************************/
		// Add in show stats
		//*********************************************/
		
		
		if ($ibforums->vars['show_totals'])
		{
		
			$DB->query("SELECT * FROM ibf_stats");
			$stats = $DB->fetch_row();
			
			// Update the most active count if needed
			
			if ($active['TOTAL'] > $stats['MOST_COUNT'])
			{
				$DB->query("UPDATE ibf_stats SET MOST_DATE='".time()."', MOST_COUNT='".$active[TOTAL]."'");
				$stats['MOST_COUNT'] = $active[TOTAL];
				$stats['MOST_DATE']  = time();
			}
			
			$most_time = $std->get_date( $stats['MOST_DATE'], 'LONG' );
			
			$ibforums->lang['most_online'] = preg_replace( "/<#NUM#>/" ,   $stats['MOST_COUNT']  , $ibforums->lang['most_online'] );
			$ibforums->lang['most_online'] = preg_replace( "/<#DATE#>/",   $most_time            , $ibforums->lang['most_online'] );
			
			$total_posts = $stats['TOTAL_REPLIES']+$stats['TOTAL_TOPICS'];
			
			$ibforums->lang['total_word_string'] = preg_replace( "/<#posts#>/" , "$total_posts"          , $ibforums->lang['total_word_string'] );
			$ibforums->lang['total_word_string'] = preg_replace( "/<#reg#>/"   , $stats['MEM_COUNT']     , $ibforums->lang['total_word_string'] );
			$ibforums->lang['total_word_string'] = preg_replace( "/<#mem#>/"   , $stats['LAST_MEM_NAME'] , $ibforums->lang['total_word_string'] );
			
			$stats_html .= $this->html->ShowStats($ibforums->lang['total_word_string']);
			
		}
		
		if ($stats_html != "")
		{
			$this->output .= $this->html->stats_header();
			$this->output .= $stats_html;
			$this->output .= $this->html->stats_footer();
		}
		
		
		// Add in board info footer
		$this->output .= $this->html->BoardInformation();
		
		// Check for news forum.
		
		if ($this->news_title and $this->news_topic_id and $this->news_forum_id)
		{
			$t_html = $this->html->newslink( $this->news_forum_id, stripslashes($this->news_title) , $this->news_topic_id);
			$this->output = preg_replace( "/<!-- IBF\.NEWSLINK -->/" , "$t_html" , $this->output );
		}

        $print->add_output("$this->output");
        $print->do_output( array( 'TITLE' => $ibforums->vars['board_name']." (Powered by Invision Board)", 'JS' => 0, 'NAV' => $this->nav ) );
        
	}
	
	//*********************************************/
	//
	// SHOW A SUB FORUM
	//
	//*********************************************/   
    
	function show_subforum() {
	
		global $std, $DB, $ibforums;    
        
        $fid = $ibforums->input['f'];
        
        if (!is_array( $this->forums[ $fid ] ) )
        {
        	$std->Error( array( 'LEVEL' => 1, 'MSG' => 'missing_files' ) );
        }
        
        if ( (isset($this->children[ $fid ])) and (count($this->children[ $fid ]) > 0 ) )
    	{
    		foreach ($this->children[ $fid ] as $idx => $data)
    		{
    			$temp_html .= $this->process_forum($data['id'], $data);
    		}
    	}
    	else
    	{
    		$std->Error( array( 'LEVEL' => 1, 'MSG' => 'missing_files' ) );
    	}

		if ($temp_html != "")
		{
			$this->output .= $this->html->subheader();
			$this->output .= $temp_html;
			$this->output .= $this->html->end_this_cat();
		}
		else
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'missing_files' ) );
		}
		unset($temp_html);
	
		$this->output .= $this->html->end_all_cats();
    }
    
	
	//*********************************************/
	//
	// PROCESS ALL CATEGORIES
	//
	//*********************************************/
	
	function process_all_cats() {
	
		global $std, $DB, $ibforums;    
        
        foreach ($this->cats as $cat_id => $cat_data)
        {
        
        	//----------------------------
        	// Is this category turned on?
        	//----------------------------
        
        	if (! $cat_data['state'] )
        	{
        		continue;
        	}
        
            foreach ($this->forums as $forum_id => $forum_data)
            {
                if ($forum_data['category'] == $cat_id)
                {
                	//-----------------------------------
                    // We store the HTML in a temp var so
                    // we can make sure we have cats for
                    // this forum, or hidden forums with a 
                    // cat will show the cat strip - we don't
                    // want that, no - we don't.
                    //-----------------------------------
                    
                    $temp_html .= $this->process_forum($forum_id, $forum_data);
                }
            }
            
            if ($temp_html != "")
            {
            	$this->output .= $this->html->CatHeader_Expanded($cat_data);
            	$this->output .= $temp_html;
            	$this->output .= $this->html->end_this_cat();
            }
            
            unset($temp_html);
        }
        
        $this->output .= $this->html->end_all_cats();
        
    }
 
 
	//*********************************************/
	//
	// SHOW A SINGLE CATEGORY
	//
	//*********************************************/   
    
	function show_single_cat() {
	
		global $std, $DB, $ibforums;    
        
        $cat_id = $ibforums->input['c'];
        
        if (!is_array( $this->cats[ $cat_id ] ) )
        {
        	$std->Error( array( 'LEVEL' => 1, 'MSG' => 'missing_files' ) );
        }
        
        $cat_data = $this->cats[ $cat_id ];
        
		//----------------------------
		// Is this category turned on?
		//----------------------------
        
		if (! $cat_data['state'] )
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'missing_files' ) );
		}
	
		foreach ($this->forums as $forum_id => $forum_data)
		{
			if ($forum_data['category'] == $cat_id)
			{
				//-----------------------------------
				// We store the HTML in a temp var so
				// we can make sure we have cats for
				// this forum, or hidden forums with a 
				// cat will show the cat strip - we don't
				// want that, no - we don't.
				//-----------------------------------
				
				$temp_html .= $this->process_forum($forum_id, $forum_data);
			}
		}
		
		if ($temp_html != "")
		{
			$this->output .= $this->html->CatHeader_Expanded($cat_data);
			$this->output .= $temp_html;
			$this->output .= $this->html->end_this_cat();
		}
		else
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'missing_files' ) );
		}
		unset($temp_html);
	
		$this->output .= $this->html->end_all_cats();
    }
    
	//*********************************************/
	//
	// RENDER A FORUM
	//
	//*********************************************/   
    
    function process_forum($forum_id="", $forum_data="")
    {
    	global $std, $ibforums;
    	
    	
    	if ($forum_data['subwrap'] == 1)
    	{
    	
    		$printed_children = 0;
    		
    		//--------------------------------------
    		// This is a sub cat forum...
    		//--------------------------------------
    		
    		// Do we have any sub forums here?
    		
    		if ( (isset($this->children[ $forum_data['id'] ])) and (count($this->children[ $forum_data['id'] ]) > 0 ) )
    		{
    		
    			$newest = array();
    		
    			foreach($this->children[ $forum_data['id'] ] as $idx => $data)
				{
    				//--------------------------------------
					// Check permissions...
					//--------------------------------------
			
					if ($data['read_perms'] != '*')
					{
						if ( ! preg_match( "/(^|,)".$ibforums->member['mgroup']."(,|$)/", $data['read_perms'] ) )
						{
							continue;
						}
					}
					
					// Do the news stuff first
					
					if (isset($data['last_title']) and $data['last_id'] != "")
					{
					
						if ( ( $ibforums->vars['index_news_link'] == 1 ) and (! empty($ibforums->vars['news_forum_id']) ) and ($ibforums->vars['news_forum_id'] == $data['id']) )
						{
							
						   $this->news_topic_id = $data['last_id'];
						   $this->news_forum_id = $data['id'];
						   $this->news_title    = $data['last_title'];
							
						}
						
					}
					
					if ($data['last_post'] > $newest['last_post'])
					{
						
						$newest['last_post'] = $data['last_post'];
						$newest['fid']       = $data['id'];
						
						if (strlen($data['last_title']) > 30)
						{
							$data['last_title'] = str_replace( "&#33;" , "!", $data['last_title'] );
							$data['last_title'] = str_replace( "&quot;", "\"", $data['last_title'] );
							$data['last_title'] = substr($data['last_title'],0,27) . "...";
							$data['last_title'] = preg_replace( '/&(#(\d+;?)?)?\.\.\.$/', '...', $data['last_title'] );
						}
						if ($data['password'] != "")
						{
							$newest['last_topic'] = $ibforums->lang['f_protected'];
						}
						else
						{
							$newest['last_topic'] = "<a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ST&f={$data['id']}&t={$data['last_id']}&view=getlastpost'>{$data['last_title']}</a>";
						}
						
						if ( isset($data['last_poster_name']))
						{
							$newest['last_poster'] = $data['last_poster_id'] ? "<a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Profile&CODE=03&MID={$data['last_poster_id']}'>{$data['last_poster_name']}</a>"
																		   : $data['last_poster_name'];
						}
			
					}
					
					$newest['posts']  += $data['posts'];
					$newest['topics'] += $data['topics'];
					$newest['status'] = $data['status'];
					
					$printed_children++;
					
				}
				
				if ($printed_children < 1)
				{
					// If we don't have permission to view any forums
					// then simply return and the row won't be printed
					
					return "";
					
				}
				
				// Fix up the last of the data
				
				$newest['img_new_post'] = $this->new_posts($newest, 1);
			
				$newest['last_post'] = $std->get_date($newest['last_post'], 'LONG');
				
				$newest['last_topic']  = $newest['last_topic']  ? $newest['last_topic']  : $ibforums->lang['f_none'];
    			
    			$newest['last_poster'] = $newest['last_poster'] ? $newest['last_poster'] : $ibforums->lang['f_none'];
    			
    			foreach($newest as $k => $v)
    			{
    				$forum_data[$k] = $v;
    			}
    			
    			return $this->html->ForumRow($forum_data);
    			
    		}
    		else
    		{
    			return "";
    		}
    	
    	}
    	else
    	{
    	
			//--------------------------------------
			// Check permissions...
			//--------------------------------------
			
			if ($forum_data['read_perms'] != '*')
			{
				if ( ! preg_match( "/(^|,)".$ibforums->member['mgroup']."(,|$)/", $forum_data['read_perms'] ) )
				{
					return "";
				}
			}
			
			$forum_data['img_new_post'] = $this->new_posts($forum_data);
			
			$forum_data['last_post'] = $std->get_date($forum_data['last_post'], 'LONG');
						
			$forum_data['last_topic'] = $ibforums->lang['f_none'];
			
			if (isset($forum_data['last_title']) and $forum_data['last_id'] != "")
			{
			
				if ( ( $ibforums->vars['index_news_link'] == 1 ) and (! empty($ibforums->vars['news_forum_id']) ) and ($ibforums->vars['news_forum_id'] == $forum_data['id']) )
				{
					
				   $this->news_topic_id = $forum_data['last_id'];
				   $this->news_forum_id = $forum_data['id'];
				   $this->news_title    = $forum_data['last_title'];
					
				}
				
				$forum_data['last_title'] = str_replace( "&#33;" , "!", $forum_data['last_title'] );
				$forum_data['last_title'] = str_replace( "&quot;", "\"", $forum_data['last_title'] );
				
					
				if (strlen($forum_data['last_title']) > 30)
				{
					$forum_data['last_title'] = substr($forum_data['last_title'],0,27) . "...";
					$forum_data['last_title'] = preg_replace( '/&(#(\d+;?)?)?\.\.\.$/', '...', $forum_data['last_title'] );
				}
				
				if ($forum_data['password'] != "")
				{
					$forum_data['last_topic'] = $ibforums->lang['f_protected'];
				}
				else
				{
					$forum_data['last_topic'] = "<a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ST&f={$forum_data['id']}&t={$forum_data['last_id']}&view=getlastpost'>{$forum_data['last_title']}</a>";
				}
			}	 
							
			if ( isset($forum_data['last_poster_name']))
			{
				$forum_data['last_poster'] = $forum_data['last_poster_id'] ? "<a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Profile&CODE=03&MID={$forum_data['last_poster_id']}'>{$forum_data['last_poster_name']}</a>"
																		   : $forum_data['last_poster_name'];
			}
			else
			{
				$forum_data['last_poster'] = $ibforums->lang['f_none'];
			}
	
			//---------------------------------
			// Moderators
			//---------------------------------
			
			$forum_data['moderator'] = "";
			
			if (isset($this->mods[ $forum_data['id'] ] ) )
			{
				$forum_data['moderator'] = $ibforums->lang['forum_leader'].' ';
				
				if (is_array($this->mods[ $forum_data['id'] ]) )
				{
					foreach ($this->mods[ $forum_data['id'] ] as $moderator)
					{
						$forum_data['moderator'] .= "<a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Profile&CODE=03&MID={$moderator['id']}'>{$moderator['name']}</a>, ";
					}
					$forum_data['moderator'] = preg_replace( "!,\s+$!", "", $forum_data['moderator'] );
				}
				else
				{
					$forum_data['moderator'] .= "<a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Profile&CODE=03&MID={$this->mods[$forum_data['id']]['id']}'>{$this->mods[$forum_data['id']]['name']}</a>";
				}
			}
			
			
			return $this->html->ForumRow($forum_data);
		
		}
                    
	}
    
    
    function new_posts($forum_data, $sub=0) {
        global $ibforums, $std;
        
        $rtime = $ibforums->input['last_visit'];
        
        $fid   = $forum_data['fid'] == "" ? $forum_data['id'] : $forum_data['fid'];
        
        if ( $ftime = $std->my_getcookie('fread_'.$fid ) )
        {
        	$rtime = $ftime > $rtime ? $ftime : $rtime;
        }
        
        if ($sub == 0)
        {
			if ( ! $forum_data['status'] )
			{
				return $ibforums->skin['C_LOCKED'];
			}
			
			$sub_cat_img = '';
        }
        else
        {
        	$sub_cat_img = '_CAT';
        }
        
        if ($forum_data['password'] and $sub == 0)
        {
            return $forum_data['last_post'] > $rtime ? $ibforums->skin['C_ON_RES']
                                                     : $ibforums->skin['C_OFF_RES'];
        }
        
        return $forum_data['last_post']  > $rtime ? $ibforums->skin['C_ON'.$sub_cat_img]
                                                  : $ibforums->skin['C_OFF'.$sub_cat_img];
    }
        
}

?>
