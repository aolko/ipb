<?php

class ReportView {



function body() {
global $ibforums;
return <<<EOF
                <tr><!--test-->
                <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' valign='top'>{$ibforums->lang['message']}<br><br><span style='font-size:12px;color:red;font-weight:bold'>{$ibforums->lang['warn']}</span></td> 
                <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><textarea cols='60' rows='12' wrap='soft' name='Post' tabindex='3' class='textinput'></textarea></td>
                </tr>
EOF;
}

function foot() {
global $ibforums;
return <<<EOF
                <tr>
                <td bgcolor='{$ibforums->skin['MISCBACK_TWO']}' align='center' colspan='2'>
                <input type="submit" value="{$ibforums->lang['send']}" class='forminput'>
                </tr>
               </table>
            </td>
         </tr>
      </table>
      </form>
EOF;
}

function header($data) {
global $ibforums;
return <<<EOF
     <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}" method="post" name='REPLIER'>
     <input type='hidden' name='act' value='Report'>
     <input type='hidden' name='SEND' value='1'>
     <input type='hidden' name='s' value='{$ibforums->session_id}'>
     <input type='hidden' name='f' value='$iB::IN{'f'}'>
     <input type='hidden' name='t' value='$iB::IN{'t'}'>
     <input type='hidden' name='p' value='$iB::IN{'p'}'>
     <table cellpadding='0' cellspacing='1' border='0' width='{$ibforums->skin['TABLE_WIDTH']}' bgcolor='{$ibforums->skin['TABLE_BORDER_COL']}' align='center'>
        <tr>
            <td>
               <table cellpadding='4' cellspacing='1' border='0' width='100%'>
                <tr>
                <td bgcolor='{$ibforums->skin['TITLEBACK']}' align='left' colspan='2' id='titlemedium'>{$ibforums->lang['title']}</td>
                </tr>
                <tr>
                <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' align='left'  width='20%' valign='top'><b>{$ibforums->lang['in_forum']}</b></td>
                <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' width='80%'><b>{$data[FORUM]}->{'FORUM_NAME'}</b></td>
                </tr>
                <tr>
                <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' align='left'  width='20%' valign='top'><b>{$ibforums->lang['in_topic']}</b></td>
                <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' width='80%'><b>{$data[TOPIC]}->{'TOPIC_TITLE'}</b></td>
                </tr>
EOF;
}

function sent_screen($member_name) {
global $ibforums;
return <<<EOF
     <table cellpadding='0' cellspacing='1' border='0' width='{$ibforums->skin['TABLE_WIDTH']}' bgcolor='{$ibforums->skin['TABLE_BORDER_COL']}' align='center'>
        <tr>
            <td>
               <table cellpadding='4' cellspacing='0' border='0' width='100%'>
                <tr>
                   <td bgcolor='{$ibforums->skin['TITLEBACK']}' colspan='2' id='titlelarge'><b>{$ibforums->lang['email_sent']}</b></td>                
                 </tr>
                 <tr>
                   <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' colspan='2' id='usermenu'><img src='{$ibforums->vars['html_url']}/images/cp_redtri.gif' border='0' height='15' width='15' align='middle' alt=''> <b>{$ibforums->lang['email_sent']}</b></td>
                 </tr>
                 </tr>
                 <tr>
                   <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' valign='middle'><img src='{$ibforums->vars['html_url']}/images/msg_sent.gif' border='0' height='32' width='32' alt=''></td>
                   <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' valign='middle'><b>{$ibforums->lang['email_sent_txt']} $member_name</td>
                 </tr>
                </table>
            </td>
         </tr>
      </table>
EOF;
}

function guest() {
global $ibforums;
return <<<EOF
                <tr>
                <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' align='left'  width='20%' valign='top'><b>{$ibforums->lang['name']}</b></td>
                <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' width='80%'><input type='text' name='name' value='' size='50' maxlength='50' class='forminput'></td>
                </tr>
                <tr>
                <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' align='left'  width='20%' valign='top'><b>{$ibforums->lang['email']}</b></td>
                <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' width='80%'><input type='text' name='email' value='' size='50' maxlength='50' class='forminput'></td>
                </tr>
EOF;
}


}
?>