<?php

class skin_post {



function table_structure() {
global $ibforums;
return <<<EOF
<!--START TABLE-->
<!--NAME FIELDS-->
<!--TOPIC TITLE-->
<!--POLL BOX-->
<!--POST BOX-->
<!--QUOTE BOX-->
<!--POST ICONS-->
<!--UPLOAD FIELD-->
<!--MOD OPTIONS-->
<!--END TABLE-->
EOF;
}

function table_top($data) {
global $ibforums;
return <<<EOF
	<script language='Javascript' type='text/javascript'>
		<!--
		function PopUp(url, name, width,height,center,resize,scroll,posleft,postop) {
			if (posleft != 0) { x = posleft }
			if (postop  != 0) { y = postop  }
		
			if (!scroll) { scroll = 1 }
			if (!resize) { resize = 1 }
		
			if ((parseInt (navigator.appVersion) >= 4 ) && (center)) {
			  X = (screen.width  - width ) / 2;
			  Y = (screen.height - height) / 2;
			}
			if (scroll != 0) { scroll = 1 }
		
			var Win = window.open( url, name, 'width='+width+',height='+height+',top='+Y+',left='+X+',resizable='+resize+',scrollbars='+scroll+',location=no,directories=no,status=no,menubar=no,toolbar=no');
	     }
		//-->
	</script>
<table width="{$ibforums->skin['tbl_width']}" align='center' border="0" cellspacing="1" cellpadding="0" bgcolor="{$ibforums->skin['tbl_border']}">
  <tr> 
    <td id='maintitleback' background='{$ibforums->vars['img_url']}/tile_back.gif'> 
      &nbsp;
    </td>
  </tr>
  <tr> 
    <td id='mainbg'> 
      <table width="100%" border="0" cellspacing="1" cellpadding="4">
        <tr> 
          <td id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif' colspan="2">$data</td>
        </tr>
EOF;
}

function EndForm($data) {
global $ibforums;
return <<<EOF
        <tr> 
          <td id='mainfoot' align="center" colspan="2"><input type="submit" name="submit" value="$data" tabindex='4' class='forminput'>&nbsp;
                <input type="submit" name="preview" value="{$ibforums->lang['button_preview']}" tabindex='5' class='forminput'></td>
        </tr>
      </table>
    </td>
  </tr>
  </form>
</table>
EOF;
}

function smilie_table() {
global $ibforums;
return <<<EOF
    <table align="center" cellspacing='1' cellpadding='3' border='0' id='row2' style="border-width:1px; border-color:{$ibforums->skin['tbl_border']}; border-style:solid; width:95%" align='left'>
    <tr>
        <td colspan='{$ibforums->vars['emo_per_row']}' align='center'>{$ibforums->lang['click_smilie']}</td>
    </tr>
    <!--THE SMILIES-->
    <tr>
        <td colspan='{$ibforums->vars['emo_per_row']}' id='row1' align='center'><a href='javascript:emo_pop()'>{$ibforums->lang['all_emoticons']}</a></td>
    </tr>
    </table>
EOF;
}

function TopicSummary_bottom() {
global $ibforums;
return <<<EOF
           <!-- Cgi-bot TopicSummaryBottom -->
        <tr>
           <td valign='left' colspan='2' id='titlemedium'><a href="javascript:PopUp('index.{$ibforums->vars['php_ext']}?act=ST&f={$ibforums->input['f']}&t={$ibforums->input['t']}','TopicSummary',700,450,1,1)">{$ibforums->lang['review_topic']}</a></td>
        </tr>
        </table>
      </td>
     </tr>
    </table>
    <!-- Cgi-bot End TopicSummaryBottom -->
EOF;
}

function PostIcons() {
global $ibforums;
return <<<EOF
        <tr> 
          <td valign="top" id='row1'>{$ibforums->lang['post_icon']}</td>
          <td valign="top" width="100%" id='row1'>
				  <INPUT type="radio" name="iconid" value="1">&nbsp;&nbsp;<IMG SRC="{$ibforums->vars['img_url']}/icon1.gif" HEIGHT='15' WIDTH='15' ALIGN='center' alt=''>&nbsp;&nbsp;&nbsp;<INPUT type="radio" name="iconid" value="2" >&nbsp;&nbsp;<IMG SRC="{$ibforums->vars['img_url']}/icon2.gif" HEIGHT='15' WIDTH='15' ALIGN='center' alt=''>&nbsp;&nbsp;&nbsp;<INPUT type="radio" name="iconid" value="3" >&nbsp;&nbsp;<IMG SRC="{$ibforums->vars['img_url']}/icon3.gif" HEIGHT='15' WIDTH='15' ALIGN='center' alt=''>&nbsp;&nbsp;&nbsp;<INPUT type="radio" name="iconid" value="4" >&nbsp;&nbsp;<IMG SRC="{$ibforums->vars['img_url']}/icon4.gif" HEIGHT='15' WIDTH='15' ALIGN='center' alt=''>&nbsp;&nbsp;&nbsp;<INPUT type="radio" name="iconid" value="5" >&nbsp;&nbsp;<IMG SRC="{$ibforums->vars['img_url']}/icon5.gif" HEIGHT='15' WIDTH='15' ALIGN='center' alt=''>&nbsp;&nbsp;&nbsp;<INPUT type="radio" name="iconid" value="6" >&nbsp;&nbsp;<IMG SRC="{$ibforums->vars['img_url']}/icon6.gif" HEIGHT='15' WIDTH='15' ALIGN='center' alt=''>&nbsp;&nbsp;&nbsp;<INPUT type="radio" name="iconid" value="7" >&nbsp;&nbsp;<IMG SRC="{$ibforums->vars['img_url']}/icon7.gif" HEIGHT='15' WIDTH='15' ALIGN='center' alt=''>&nbsp;&nbsp;&nbsp;<br>
				  <INPUT type="radio" name="iconid" value="8">&nbsp;&nbsp;<IMG SRC="{$ibforums->vars['img_url']}/icon8.gif" HEIGHT='15' WIDTH='15' ALIGN='center' alt=''>&nbsp;&nbsp;&nbsp;<INPUT type="radio" name="iconid" value="9" >&nbsp;&nbsp;<IMG SRC="{$ibforums->vars['img_url']}/icon9.gif" HEIGHT='15' WIDTH='15' ALIGN='center' alt=''>&nbsp;&nbsp;&nbsp;<INPUT type="radio" name="iconid" value="10" >&nbsp;&nbsp;<IMG SRC="{$ibforums->vars['img_url']}/icon10.gif" HEIGHT='15' WIDTH='15' ALIGN='center' alt=''>&nbsp;&nbsp;&nbsp;<INPUT type="radio" name="iconid" value="11" >&nbsp;&nbsp;<IMG SRC="{$ibforums->vars['img_url']}/icon11.gif" HEIGHT='15' WIDTH='15' ALIGN='center' alt=''>&nbsp;&nbsp;&nbsp;<INPUT type="radio" name="iconid" value="12" >&nbsp;&nbsp;<IMG SRC="{$ibforums->vars['img_url']}/icon12.gif" HEIGHT='15' WIDTH='15' ALIGN='center' alt=''>&nbsp;&nbsp;&nbsp;<INPUT type="radio" name="iconid" value="13" >&nbsp;&nbsp;<IMG SRC="{$ibforums->vars['img_url']}/icon13.gif" HEIGHT='15' WIDTH='15' ALIGN='center' alt=''>&nbsp;&nbsp;&nbsp;<INPUT type="radio" name="iconid" value="14" >&nbsp;&nbsp;<IMG SRC="{$ibforums->vars['img_url']}/icon14.gif" HEIGHT='15' WIDTH='15' ALIGN='center' alt=''>&nbsp;&nbsp;&nbsp;
				  <BR>
				  <INPUT type="radio" name="iconid" value="0" CHECKED>&nbsp;&nbsp;[ Use None ]
				 </td>
        </tr>
EOF;
}

function errors($data) {
global $ibforums;
return <<<EOF
     <table cellpadding='0' cellspacing='1' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['TABLE_BORDER_COL']}' align='center'>
        <tr>
            <td>
                <table cellpadding='5' cellspacing='1' border='0' width='100%'>
                <tr>
                <td id='row1' valign='top' align='left'><b>{$ibforums->lang['errors_found']}</b></font><hr noshade size='1' color='{$ibforums->skin['tbl_border']}'>$data</td>
                </tr>
                </table>
            </td>
        </tr>
    </table>
    <br>
EOF;
}

function Upload_field($data) {
global $ibforums;
return <<<EOF
        <tr> 
          <td id="subtitle" colspan="2">{$ibforums->lang['upload_title']}</td>
        </tr>
        <tr> 
          <td id='row1'>{$ibforums->lang['upload_text']} $data</td>
          <td id='row1' width="100%"><input class='textinput' type='file' size='30' name='FILE_UPLOAD'></td>
        </tr>
EOF;
}

function topictitle_fields($data) {
global $ibforums;
return <<<EOF
        <tr> 
          <td id="subtitle" colspan="2">{$ibforums->lang['tt_topic_settings']}</td>
        </tr>
        <tr> 
          <td id='row1'>{$ibforums->lang['topic_title']}</td>
          <td id='row1' width="100%" valign="top"><input type='text' size='40' maxlength='50' name='TopicTitle' value='{$data[TITLE]}' tabindex='1' class='forminput'></td>
        </tr>
        <tr> 
          <td id='row1'>{$ibforums->lang['topic_desc']}</td>
          <td id='row1' width="100%" valign="top"><input type='text' size='40' maxlength='40' name='TopicDesc' value='{$data[DESC]}' tabindex='2' class='forminput'></td>
        </tr>
EOF;
}

function TopicSummary_body($data) {
global $ibforums;
return <<<EOF
             <tr id='postdetails'>
               <td id='row1' align='left' valign='top' width='20%'><b>{$data['author']}</b></td>
               <td id='row1' align='left' valign='top' width='80%'>{$ibforums->lang['posted_on']} {$data['date']}<hr noshade size='1'><span id='postcolor'>{$data['post']}</span></td>
             </tr>
EOF;
}

function preview($data) {
global $ibforums;
return <<<EOF
     <table cellpadding='0' cellspacing='1' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
        <tr>
            <td>
                <table cellpadding='5' cellspacing='1' border='0' width='100%'>
                <tr>
                <td id='row1' valign='top' align='left'><b>{$ibforums->lang['post_preview']}</b><hr noshade size='1' color='{$ibforums->skin['tbl_border']}'><span id='postcolor'>$data</span></td>
                </tr>
                </table>
            </td>
        </tr>
    </table>
    <br>
EOF;
}

function TopicSummary_top() {
global $ibforums;
return <<<EOF
    <a name="top">
    <!-- Cgi-bot TopicSummaryTop -->
    <br>
     <table cellpadding='0' cellspacing='0' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
       <tr>
         <td>
            <table cellpadding='3' cellspacing='1' border='0' width='100%'>
             <tr>
               <td valign='left' colspan='2' id='titlemedium'>{$ibforums->lang['last_posts']}</td>
             </tr>  
        <!-- Cgi-bot End TopicSummaryTop -->
EOF;
}

function nameField_reg() {
global $ibforums;
return <<<EOF
<!-- REG NAME -->
EOF;
}

function mod_options($jump) {
global $ibforums;
return <<<EOF
        <tr> 
          <td id="subtitle" colspan="2">{$ibforums->lang['tt_options']}</td>
        </tr>
        <tr> 
          <td id="row1">{$ibforums->lang['mod_options']}</td>
          <td id="row1" width="100%">$jump</td>
        </tr>
EOF;
}

function quote_box($data) {
global $ibforums;
return <<<EOF
        <tr> 
          <td id="subtitle" colspan="2">{$ibforums->lang['post_to_quote']}</td>
        </tr>
        <tr> 
          <td id='row1' valign="top">{$ibforums->lang['post_to_quote_txt']}</td>
          <td id='row1' width="100%" valign="top">
                <textarea cols='60' rows='12' wrap='soft' name='QPost' class='textinput'>{$data['post']}</textarea><input type='hidden' name='QAuthor' value='{$data['author_id']}'><input type='hidden' name='QAuthorN' value='{$data['author_name']}'><input type='hidden' name='QDate'   value='{$data['post_date']}'></td>
        </tr>
EOF;
}

function postbox_buttons($data) {
global $ibforums;
return <<<EOF
<script language="javascript1.2">
<!--

	var MessageMax  = "{$ibforums->lang['the_max_length']}";
	var Override    = "{$ibforums->lang['override']}";
function emo_pop()
{
	
  window.open('index.{$ibforums->vars['php_ext']}?act=legends&CODE=emoticons&s={$ibforums->session_id}','Legends','width=250,height=500,resizable=yes,scrollbars=yes'); 
     
}	
function CheckLength() {
	MessageLength  = document.REPLIER.Post.value.length;
	message  = "";

		if (MessageMax !=0) {
			message = "{$ibforums->lang['js_post']}:\\n{$ibforums->lang['js_max_length']} " + MessageMax + " {$ibforums->lang['js_characters']}.";
		} else {
			message = "";
		}
		alert(message + "\\n{$ibforums->lang['js_used']} " + MessageLength + " {$ibforums->lang['js_characters']}.");
}
	
	function ValidateForm(isMsg) {
		MessageLength  = document.REPLIER.Post.value.length;
		errors = "";
		
		if (isMsg == 1)
		{
			if (document.REPLIER.msg_title.value.length < 2)
			{
				errors = "{$ibforums->lang['msg_no_title']}";
			}
		}
	
		if (MessageLength < 2) {
			 errors = "{$ibforums->lang['js_no_message']}";
		}
		if (MessageMax !=0) {
			if (MessageLength > MessageMax) {
				errors = "{$ibforums->lang['js_max_length']} " + MessageMax + " {$ibforums->lang['js_characters']}. {$ibforums->lang['js_current']}: " + MessageLength;
			}
		}
		if (errors != "" && Override == "") {
			alert(errors);
			return false;
		} else {
			document.REPLIER.submit.disabled = true;
			return true;
		}
	}
	
	
	
	// IBC Code stuff
	var text_enter_url      = "{$ibforums->lang['jscode_text_enter_url']}";
	var text_enter_url_name = "{$ibforums->lang['jscode_text_enter_url_name']}";
	var text_enter_image    = "{$ibforums->lang['jscode_text_enter_image']}";
	var text_enter_email    = "{$ibforums->lang['jscode_text_enter_email']}";
	var text_enter_flash    = "{$ibforums->lang['jscode_text_enter_flash']}";
	var text_code           = "{$ibforums->lang['jscode_text_code']}";
	var text_quote          = "{$ibforums->lang['jscode_text_quote']}";
	var error_no_url        = "{$ibforums->lang['jscode_error_no_url']}";
	var error_no_title      = "{$ibforums->lang['jscode_error_no_title']}";
	var error_no_email      = "{$ibforums->lang['jscode_error_no_email']}";
	var error_no_width      = "{$ibforums->lang['jscode_error_no_width']}";
	var error_no_height     = "{$ibforums->lang['jscode_error_no_height']}";
	//-->
</script>
<script language='Javascript' src='html/ibfcode.js'></script>

        <tr> 
          <td id="subtitle" colspan="2">{$ibforums->lang['ib_code_buttons']}</td>
        </tr>
        <tr> 
          <td id='row1'>{$ibforums->lang['ibf_code_txt']}</td>
          <td id='row1' width="100%" valign="top">
<table cellpadding='2' cellspacing='2' width='100%' align='center'>
                		<tr>
                			<td nowrap width='10%'>
							  <input type='button' accesskey='b' value=' B '       onClick='simpletag("B")'       class='codebuttons' title="BOLD: [Control / Alt] + b"      name='bold'   style="font-weight:bold">
							  <input type='button' accesskey='i' value=' I '       onClick='simpletag("I")'       class='codebuttons' title="ITALIC: [Control / Alt] + i"    name='italic' style="font-style:italic">
							  <input type='button' accesskey='u' value=' U '       onClick='simpletag("U")'       class='codebuttons' title="UNDERLINE: [Control / Alt] + u" name='under' style="text-decoration:underline">
							  
							  <select name='ffont' class='codebuttons' onchange="alterfont(this.options[this.selectedIndex].value, 'FONT')">
							  <option value='0'>{$ibforums->lang['ct_font']}</option>
							  <option value='Arial' style='font-family:Arial'>{$ibforums->lang['ct_arial']}</option>
							  <option value='Times' style='font-family:Times'>{$ibforums->lang['ct_times']}</option>
							  <option value='Courier' style='font-family:Courier'>{$ibforums->lang['ct_courier']}</option>
							  <option value='Impact' style='font-family:Impact'>{$ibforums->lang['ct_impact']}</option>
							  <option value='Geneva' style='font-family:Geneva'>{$ibforums->lang['ct_geneva']}</option>
							  <option value='Optima' style='font-family:Optima'>Optima</option>
							  </select><select name='fsize' class='codebuttons' onchange="alterfont(this.options[this.selectedIndex].value, 'SIZE')">
							  <option value='0'>{$ibforums->lang['ct_size']}</option>
							  <option value='1'>{$ibforums->lang['ct_sml']}</option>
							  <option value='7'>{$ibforums->lang['ct_lrg']}</option>
							  <option value='14'>{$ibforums->lang['ct_lest']}</option>
							  </select><select name='fcolor' class='codebuttons' onchange="alterfont(this.options[this.selectedIndex].value, 'COLOR')">
							  <option value='0'>{$ibforums->lang['ct_color']}</option>
							  <option value='blue' style='color:blue'>{$ibforums->lang['ct_blue']}</option>
							  <option value='red' style='color:red'>{$ibforums->lang['ct_red']}</option>
							  <option value='purple' style='color:purple'>{$ibforums->lang['ct_purple']}</option>
							  <option value='orange' style='color:orange'>{$ibforums->lang['ct_orange']}</option>
							  <option value='yellow' style='color:yellow'>{$ibforums->lang['ct_yellow']}</option>
							  <option value='gray' style='color:gray'>{$ibforums->lang['ct_grey']}</option>
							  <option value='green' style='color:green'>{$ibforums->lang['ct_green']}</option>
							  </select>
							</td>
							<td align='left'nowrap width='10%'><input type='button' accesskey='c' value=' x '       onClick='closelast()'          class='codebuttons' title="Close Current Tag: [Control / Alt] + c"      name='bold'   style="color:red"> Close Current Tag</td>
						 </tr>
						 <tr>
						    <td align='left'>
							  <input type='button' accesskey='h' value=' http:// ' onClick='tag_url()'            class='codebuttons' title="HYPERLINK: [Control / Alt] + h" style="text-decoration:underline;color:blue">
							  <input type='button' accesskey='g' value=' IMG '     onClick='tag_image()'          class='codebuttons' title="IMG: [Control / Alt] + g"       >
							  <input type='button' accesskey='e' value='  @  '     onClick='tag_email()'          class='codebuttons' title="EMAIL: [Control / Alt] + e"     style="text-decoration:underline;color:blue">
							  <input type='button' accesskey='q' value=' Quote '   onClick='simpletag("QUOTE")'   class='codebuttons' title="QUOTE: [Control / Alt] + q" name='quote'>
							  <input type='button' accesskey='p' value=' Code '    onClick='simpletag("CODE")'    class='codebuttons' title="CODE: [Control / Alt] + p"  name='code'>
							  <input type='button' accesskey='s' value=' SQL '     onClick='simpletag("SQL")'    class='codebuttons' title="SQL: [Control / Alt] + s"   name='code'>
							  <input type='button' accesskey='t' value=' HTML '     onClick='simpletag("HTML")'    class='codebuttons' title="HTML: [Control / Alt] + t"   name='code'>
							</td>
							<td align='left'>
							  <input type='button' accesskey='x' value=' X '       onClick='closeall()'          class='codebuttons' title="Close Current Tag: [Control / Alt] + x"      name='bold'   style="color:red;font-weight:bold"> Close All Tags
							</td>
						</tr>
					</table>
          </td>
        </tr>
        <tr> 
          <td id="subtitle" colspan="2">{$ibforums->lang['post']}</td>
        </tr>
        <tr> 
          <td id='row1'>(<a href='javascript:CheckLength()'>{$ibforums->lang['check_length']}</a>)<br><br><!--SMILIE TABLE--><img src="{$ibforums->vars['img_url']}/spacer.gif" alt="" width="180" height="1"></td>
          <td id='row1' width="100%" valign="top"><textarea cols='80' rows='15' wrap='soft' name='Post' tabindex='3' class='textinput'>$data</textarea><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><input type='checkbox' name='enableemo' value='yes' checked></td>
                <td width="100%">{$ibforums->lang['enable_emo']}</td>
              </tr>
              <tr> 
                <td><input type='checkbox' name='enablesig' value='yes' checked></td>
                <td width="100%">{$ibforums->lang['enable_sig']}</td>
              </tr>
            </table></td>
        </tr>
EOF;
}

function nameField_unreg($data) {
global $ibforums;
return <<<EOF
        <tr> 
          <td id="subtitle" colspan="2">{$ibforums->lang['unreg_namestuff']}</td>
        </tr>
        <tr> 
          <td id="row1">{$ibforums->lang['guest_name']}</td>
          <td id="row1" width="100%"><input type='text' size='40' maxlength='40' name='UserName' value='$data' onMouseOver="this.focus()" onFocus="this.select()"></td>
        </tr>
EOF;
}

function poll_box($data) {
global $ibforums;
return <<<EOF
        <tr> 
          <td id="subtitle" colspan="2">{$ibforums->lang['tt_poll_settings']}</td>
        </tr>
        <tr> 
          <td id='row1'>{$ibforums->lang['poll_choices']}</td>
          <td id='row1' width="100%" valign="top"><textarea cols='60' rows='12' wrap='soft' name='PollAnswers' class='textinput'>$data</textarea>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><input type='checkbox' size='40' value='1' name='allow_disc' class='forminput'></td>
                <td width="100%">{$ibforums->lang['poll_only']}</td>
              </tr>
            </table></td>
        </tr>
EOF;
}


}
?>