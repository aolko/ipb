<?php

$lang = array (

'cp_redirect_mod_topics'  => "Topics/Posts have been approved or deleted - redirecting back to check for more...",

'cp_prune'  => "Manage Topics in forum: ",
'cp_prune_days' => "<b>(Re)move topics with no new posts over [x] days.</b><br>(x = value entered)",
'cp_prune_type' => "<b>(Re)move which type of topic?</b>",
'cp_pday_open'  => "Open Topics only",
'cp_pday_closed' => "Locked Topics only",
'cp_pday_link'   => "Move linked Topics only",
'cp_pday_all'    => "Any type of topic",
'cp_prune_replies' => "<b>..with less than [x] replies</b><br>(Leave this blank to remove topics regardless of replies)",
'cp_prune_member' => "<b>...started by member</b><br>(Leave blank to (re)move regardless of topic starter)",
'cp_prune_sub1'   => "Check Form Submission",
'cp_prune_sub2'   => "Prune this forum!",
'cp_action'       => "<b>Prune Action</b>",
'cp_ac_prune'     => "PRUNE ONLY, DON'T MOVE",
'cp_prune_text'   => "You may remove topics or move topics to another forum based on your criteria. To move topics, choose a destination forum from the drop down menu. To simply remove the topics, choose 'PRUNE ONLY' from the forums drop down menu. Moving topics will not leave moved links in the source forum",
'cp_pages'        => "Page: ",
'cp_error_no_mem' => "We could not locate a member by that name, please try again",
'cp_error_no_topics' => "We could not locate any topics to manage.<br><br>(<b>Note:</b> If you have received this notice after approving or deleting topics/posts, this notice simply means that there are no more topics/posts to approve.)",
'cp_total_topics'    => "Total topics in this forum",
'cp_total_match'     => "Total topics to (re)move",
'cp_check_result'    => "Form Check Results",
'cp_check_text'      => "Continue only if satisfied with the results, clicking the link below WILL carry out the action. If you need to make any changes, please go back and do so now.",
'cp_mod_topics_title2' => "Moderating New Topics in: ",
'cp_mod_posts_title2' => "Moderating New Posts in: ",
'cp_prune_domove'    => "Move the topics!",
'cp_prune_dorem'     => "Remove the topics!",
'cp_same_forum'      => "You cannot move into the same forum",
'cp_no_forum'        => "That is not a valid destination forum",
'cp_3_title'         => "Topic Title",
'cp_3_replies'       => "Queued Replies",
'cp_3_approveall'    => "Approve All",
'cp_3_viewall'       => "Manage Queued",
'cp_3_postno'        => "Post #",

'cp_error_no_subforum' => "You cannot choose a sub category as a destination forum",

'cp_1_approve'   => "Approve",
'cp_1_remove'    => "Delete",
'cp_1_selected'  => "selected posts",
'cp_1_go'        => "Go!",

'cp_results'        => "Result",
'cp_result_move'    => "Number of topics moved: ",
'cp_result_del'     => "Number of topics deleted: ",

'cp_edit_user' => "Edit a users profile",
'cp_find_user' => "Enter part, or all of the username you wish to edit",
'cp_find_submit' => "Find user",
'cp_find_2_user' => "Please select a member from the matches",
'cp_find_2_submit' => "Edit users profile",
'cp_no_matches'    => "There was no data returned from your search input, please try again",
'cp_admin_user'    => "You cannot edit an administrators profile from this control panel",
'cp_no_perms'      => "You do not have permission for this action.",

'cp_remove_av'     => "Remove users avatar?",
'cp_edit_website'  => "Website",
'cp_edit_location' => "Location",
'cp_edit_signature' => "Signature",
'cp_edit_interests' => "Interests",



'cp_mod_in'  => "Current Forum",
'cp_topics_wait' => "Topics Pending Moderation",
'cp_posts_wait'  => "Posts Pending Moderation",

'cp_modcp_ptitle' => "Moderators Control Panel",
'cp_modcp_home'  => "Moderators CP Home",

'cp_error'       =>  "Results...",
'cp_welcome'     =>  "Welcome!",
'cp_welcome_text' => "Welcome to your moderators control panel.<br>From here you will be able to process moderated topics and posts, mass move topics and prune posts. Simply choose an option from the left menu bar to proceed.",

'cp_mod_topics_title'  => "Moderate Postings",
'cp_mod_topics'        => "Moderate New Topics",
'cp_mod_posts'         => "Moderate New Posts",

'cp_manage_posts'      => "Manage Postings",
'cp_prune_posts'       => "Mass Move /Prune Topics",

'cp_mod_users_title'   => "Manage Users",
'cp_user_edit'         => "Edit Users Profile",










'leave_link'   => "Leave a link to the new topic in the source forum?",

'yes' => "Yes",
'no'  => "No",

'move_exp' => "Please select the destination forum and method of moving",

'pe_top' => "Editing Poll: ",
'pe_option' => "Poll Option:",

'pe_submit' => "Edit this poll",

'pe_pollonly' => "Make poll only (no new replies allowed?)",
'pe_yes'      => "Yes",
'pe_no'       => "No",
'pe_done'     => "Poll Edited",

'pd_top'   => "Deleting Poll:",

'pd_text'  => "Please confirm poll removal by clicking the button below",
'pd_submit' => "Remove this poll",
'pd_redirect' => "Poll Removed",


post_deleted               => "Post deleted",
top_open					=>	"Opening a topic in ",
top_edit					=>	"Editing Topic ",
submit_move					=>	"Move this topic",
UNPIN_TOPIC					=>	"UnPin Topics",
OPEN_TOPIC					=>	"Open this topic",
DELETE_TOPIC					=>	"Delete this topic",
top_close					=>	"Closing a topic in ",
t_open					=>	"Open Topic",
t_edit					=>	"Edit Topic",
p_moved					=>	"Topic Moved",
p_pinned					=>	"Topic Pinned",
t_delete					=>	"Delete Topic",
top_move					=>	"Move Topic ",
p_edited					=>	"Topic Edited",
MOVE_TOPIC					=>	"Move this topic",
edit_f_desc					=>	"Topic Description",
CLOSE_TOPIC					=>	"Close this topic",
t_close					=>	"Close Topic",
delete_topic					=>	"Deleting Topic, only continue if you wish to delete this topic, there will be no other confirmation screens.",
move_from					=>	"Move this topic from",
p_deleted					=>	"Topic Deleted",
t_move					=>	"Move Topic",
edit_f_title					=>	"Topic Title",
final_rebuild					=>	"<#DELETE_COUNT#> duplicate posts removed. The new reply counter has been set to <#REPLY_COUNT#>",
to					=>	"to",
close_topic					=>	"<b>NOTE:</b> You do not have to post a message - it is optional",
submit_close					=>	"Close this topic",
post_optional					=>	"<b>NOTE:</b> You do not have to post a message - it is optional",
p_closed					=>	"Topic Closed",
top_delete					=>	"Deleting Topic ",
p_opened					=>	"Topic Opened",
edit_topic					=>	"Editing Topic Details",
p_pdeleted					=>	"Post Deleted",
delete_old					=>	"Source forum option:",
PIN_TOPIC					=>	"Pin Topics",
post_icon					=>	"<b>Post Icons</b>",
DELETE_POST					=>	"Delete Posts",
submit_open					=>	"Open this topic",
submit_edit					=>	"Edit this topic",
submit_delete					=>	"Delete this topic",
p_unpinned					=>	"Topic Unpinned",
);
?>