<?php

/*
+--------------------------------------------------------------------------
|   IBFORUMS v1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001,2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > Skin -> Templates functions
|   > Module written by Matt Mecham
|   > Date started: 15th April 2002
|
|	> Module Version Number: 1.0.0
+--------------------------------------------------------------------------
*/




$idx = new ad_settings();


class ad_settings {

	var $base_url;

	function ad_settings() {
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;

		switch($IN['code'])
		{
			case 'add':
				$this->add_templates();
				break;
				
			case 'edit':
				$this->do_form('edit');
				break;
				
			case 'edit2':
				$this->show_file();
				break;
				
			case 'doedit':
				$this->do_edit();
				break;
				
			case 'remove':
				$this->remove();
				break;
				
			case 'tools':
				$this->tools();
				break;
				
			case 'editinfo':
				$this->edit_info();
				break;
				
			case 'export':
				$this->export();
				break;
			
			//-------------------------
			default:
				$this->list_current();
				break;
		}
		
	}
	
	function export()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing template set ID, go back and try again");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT * from ibf_tmpl_names WHERE skid='".$IN['id']."'");
		
		if ( ! $row = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the information from the database");
		}
		
		//+-------------------------------
		
		$archive_dir   = $INFO['base_dir']."/archive_out";
		$templates_dir = $INFO['base_dir']."/style_templates/".$row['skid'];
		
		require $root_dir."sources/lib/tar.php";
		
		if (!is_dir($archive_dir))
		{
			$ADMIN->error("Could not locate $archive_dir, is the directory there?");
		}
		
		if (!is_writeable($archive_dir))
		{
			$ADMIN->error("Cannot write in $archive_dir, CHMOD via FTP to 0755 or 0777 to enable this script to write into it. IBF cannot do this for you");
		}
		
		if (!is_dir($templates_dir))
		{
			$ADMIN->error("Could not locate $templates_dir, is the directory there?");
		}
		
		//+-------------------------------
		// Attempt to copy the files to the
		// working directory...
		//+-------------------------------
		
		$l_name = preg_replace( "/\s{1,}/", "_", $row['skname'] );
		
		$new_dir = "tmpl-".$l_name;
		
		if ( ! $ADMIN->copy_dir($templates_dir, $archive_dir."/".$new_dir) )
		{
			$ADMIN->error( $ADMIN->errors );
		}
		
		// Generate the config file..
		
		$file_content = "<?php\n\n".
					    "\$config=array('author' => \"".addslashes($row['author'])."\", ".
					    "'email'=>\"".addslashes($row['email'])."\", ".
					    "'url'=>\"".addslashes($row['url'])."\")\n\n?".">";
		
		$FH = fopen($archive_dir."/".$new_dir."/"."conf.inc", 'w');
		fwrite($FH, $file_content, strlen($file_content));
		fclose($FH);
		
		// Add files and write tarball
		
		$tar = new tar();
		
		$tar->new_tar( $archive_dir, $new_dir.".tar" );
		$tar->add_directory( $archive_dir."/".$new_dir );
		$tar->write_tar();
		
		// Check for errors.
		
		if ($tar->error != "")
		{
			$ADMIN->error($tar->error);
		}
		
		// remove original unarchived directory
		
		$ADMIN->rm_dir($archive_dir."/".$new_dir);
		
		$ADMIN->done_screen("Template Pack Export Created<br><br>You can download the tar-chive <a href='archive_out/{$new_dir}.tar' target='_blank'>here</a>", "Manage Template Sets", "act=templ" );
		
		
	}
	
	//+--------------------------------------------------------------------------------
	//+--------------------------------------------------------------------------------
	
	function tools_rebuildphp()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing template set ID, go back and try again");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT * from ibf_tmpl_names WHERE skid='".$IN['id']."'");
		
		if ( ! $row = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the information from the database");
		}
		
		$templates_dir = $root_path."style_templates/".$IN['id'];
		
		$skin_dir     = $root_path."Skin/s".$IN['id'];
		
		$errors = array();
		
		$flag = 0;
		
		if (! file_exists( $root_path."style_templates/".$IN['id']."/config.php" ) )
		{
			$ADMIN->error("Could not locate the template set configuration file, make sure 'config.php' resides in the templates directory you are editing. If
						   it is not there, you can copy one from another directory.");
		}
		
		//+-------------------------------
		
		require $root_path."style_templates/".$IN['id']."/config.php";
		
		//+-------------------------------
		
		
		if ( ! is_writeable($skin_dir) )
		{
			$ADMIN->error("Cannot write into '$skin_dir', please check the CHMOD value, and if needed, CHMOD to 0777 via FTP. IBF cannot do this for you.");
		}
		
		if ( ! is_writeable($templates_dir) )
		{
			$ADMIN->error("Cannot write into '$templates_dir', please check the CHMOD value, and if needed, CHMOD to 0777 via FTP. IBF cannot do this for you.");
		}
		
		if ( is_dir($templates_dir) )
		{
			if ( $handle = opendir($templates_dir) )
			{
			
				while (($filename = readdir($handle)) !== false)
				{
					if (($filename != ".") && ($filename != ".."))
					{
						if ( preg_match( "/^index\./", $filename ) )
						{
							continue;
						}
						
						//-------------------------------------------
					
						if ( preg_match( "/\.html$/", $filename ) )
						{
							
							$name = preg_replace( "/\.html$/", "", $filename );
							
							if ( file_exists($skin_dir."/".$name.".php") )
							{
								if (! is_writeable($skin_dir."/".$name.".php") )
								{
									$errors[] = "Cannot write to $name".".php, skipping file...";
									continue;
								}
							}
							
							if ($FHD = fopen($templates_dir."/".$filename, 'r') )
							{
								$text = fread($FHD, filesize($templates_dir."/".$filename) );
								fclose($FHD);
							}
							else
							{
								$errors[] = "Could not open $filename, skipping file...";
								continue;
							}
							
							//----------------------------------------------------
							
							$need = count($skin[$name]);
							$start = 0;
							$end   = 0;
							
							if ($need < 1)
							{
								$errors[] = "Error recalling function data for $filename, skipping...";
								continue;
							}
		
							// check to make sure the splitter tags are intact
							
							foreach($skin[$name] as $func_name => $data)
							{
								if ( preg_match("/<!--\|IBF\|$func_name\|START\|-->/", $text) )
								{
									$start++;
								}
								
								//+-------------------------------
								
								if ( preg_match("/<!--\|IBF\|$func_name\|END\|-->/", $text) )
								{
									$end++;
								}
							}
							
							if ($start != $end)
							{
								$errors[] = "Some start or end template splitter comments are missing in $filename, skipping file....";
								continue;
							}
							
							if ($start != $need)
							{
								$errors[] = "Some template splitter comments are missing in $filename, skipping file...";
								continue;
							}
							
							//+-------------------------------
							// Convert the tags back to php native
							//+-------------------------------
							
							$text = $this->unconvert_tags($text);
							
							//+-------------------------------
							// Start parsing the php skin file
							//+-------------------------------
							
							$final = "<"."?php\n\n".
									 "class $name {\n\n";
							
							foreach($skin[$name] as $func_name => $data)
							{
							
								$top = "\n\nfunction $func_name($data) {\n".
									   "global \$ibforums;\n".
									   "return <<<EOF\n";
									   
								$bum = "\nEOF;\n}\n";
							
								$text = preg_replace("/\s*<!--\|IBF\|$func_name\|START\|-->\s*\n/", "$top", $text);
								
								
								//+-------------------------------
								
								$text = preg_replace("/\s*<!--\|IBF\|$func_name\|END\|-->\s*\n/", "$bum", $text);
							}
							
							$end = "\n\n}\n?".">";
							
							$final .= $text.$end;
							
							if ($fh = fopen( $skin_dir."/".$name.".php", 'w' ) )
							{
								fwrite($fh, $final, strlen($final) );
								fclose($fh);
							}
							else
							{
								$errors[] = "Could not save information to $phpskin, please ensure that the CHMOD permissions are correct.";
							}
							
							$end   = "";
							$final = "";
							$top   = "";
							
						} // if *.php
						
					} // if not dir
					
				} // while loop
				
				closedir($handle);
				
			}
			else
			{
				$errors[] = "Could not open directory $templates_dir for reading!";
			}
		}
		else
		{
			$errors[] = "$templates_dir is not a directory, please check the \$root_path variable in admin.php";
		}
		
		if (count($errors > 0))
		{
			$ADMIN->html .= $SKIN->start_table("Errors and warnings");
		
			$ADMIN->html .= $SKIN->add_td_basic( implode("<br>", $errors) );
											 
			$ADMIN->html .= $SKIN->end_table();
		}
		
		$ADMIN->done_screen("Source PHP Skin files updated from the editable templates", "Manage Template sets", "act=templ" );
		
		
	}
	
	
	
	function edit_info()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing template set ID, go back and try again");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT * from ibf_tmpl_names WHERE skid='".$IN['id']."'");
		
		if ( ! $row = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the information from the database");
		}
		
		$final['skname'] = stripslashes($HTTP_POST_VARS['skname']);
		
		if (isset($HTTP_POST_VARS['author']))
		{
			$final['author'] = stripslashes($HTTP_POST_VARS['author']);
			$final['email']  = stripslashes($HTTP_POST_VARS['email']);
			$final['url']    = stripslashes($HTTP_POST_VARS['url']);
		}
		
		$db_string = $DB->compile_db_update_string( $final );
		
		$DB->query("UPDATE ibf_tmpl_names SET $db_string WHERE skid='".$IN['id']."'");
		
		$ADMIN->done_screen("Template information updated", "Manage Template sets", "act=templ" );
		
	}
	
	//+-------------------------------
	//+-------------------------------
	
	function do_edit()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		$text = stripslashes($HTTP_POST_VARS['template']);
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing template set ID, go back and try again");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT * from ibf_tmpl_names WHERE skid='".$IN['id']."'");
		
		if ( ! $row = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the information from the database");
		}
		
		//+-------------------------------
		
		$real_name = preg_replace( "/\.html/", "", $IN['name'] );
		
		//+-------------------------------
		
		$template = $root_path."style_templates/".$IN['id']."/".$IN['name'];
		$phpskin  = $root_path."Skin/s".$IN['id']."/".$real_name.".php";
		
		//+-------------------------------
		
		if (! file_exists( $root_path."style_templates/".$IN['id']."/config.php" ) )
		{
			$ADMIN->error("Could not locate the template set configuration file, make sure 'config.php' resides in the templates directory you are editing. If
						   it is not there, you can copy one from another directory.");
		}
		
		//+-------------------------------
		
		require $root_path."style_templates/".$IN['id']."/config.php";
		
		//+-------------------------------
		
		if ( ! is_writeable($template) )
		{
			$ADMIN->error("Cannot write into '$template', please check the CHMOD value, and if needed, CHMOD to 0777 via FTP. IBF cannot do this for you.");
		}
		
		if ( ! is_writeable($phpskin) )
		{
			$ADMIN->error("Cannot write into '$phpskin', please check the CHMOD value, and if needed, CHMOD to 0777 via FTP. IBF cannot do this for you.");
		}
		
		//+-------------------------------
		// Ok, make sure we actually have
		// some info to parse here.
		//+-------------------------------
		
		if ($text == "")
		{
			$ADMIN->error("You can't delete the template in this manner");
		}
		
		//+-------------------------------
		// Swop back < and >
		//+-------------------------------
		
		$text = preg_replace("/&#60;/", "<", $text);
		$text = preg_replace("/&#62;/", ">", $text);
		$text = preg_replace("/&#38;/", "&", $text);
		
		//+-------------------------------
		// Convert \r to nowt
		//+-------------------------------
		
		$text = preg_replace("/\r/", "", $text);
		
		//+-------------------------------
		// Make sure we have enough subs
		// left. Check to make sure we've
		// not removed and start and end tags
		//+-------------------------------
		
		$start = 0;
		$end   = 0;
		
		$need = count($skin[$real_name]);
		
		//<!--|IBF|stats_header|END|-->
		
		// check to make sure the splitter tags are intact
		
		foreach($skin[$real_name] as $func_name => $data)
		{
			if ( preg_match("/<!--\|IBF\|$func_name\|START\|-->/", $text) )
			{
				$start++;
			}
			
			//+-------------------------------
			
			if ( preg_match("/<!--\|IBF\|$func_name\|END\|-->/", $text) )
			{
				$end++;
			}
		}
		
		if ($start != $end)
		{
			$ADMIN->error("Some start or end template splitter comments are missing, please go back and try again");
		}
		
		if ($start != $need)
		{
			$ADMIN->error("Some template splitter comments are missing, please go back and try again");
		}
		
		//+-------------------------------
		// Save the template back to disk
		//+-------------------------------
		
		if ($fh = fopen( "$template", 'w' ) )
		{
			fwrite( $fh, $text, strlen($text) );
			fclose($fh);
		}
		else
		{
			$ADMIN->error("Could not save information to $template, please ensure that the CHMOD permissions are correct.");
		}
		
		//+-------------------------------
		// Convert the tags back to php native
		//+-------------------------------
		
		$text = $this->unconvert_tags($text);
		
		//+-------------------------------
		// Start parsing the php skin file
		//+-------------------------------
		
		$final = "<"."?php\n\n".
				 "class $real_name {\n\n";
		
		foreach($skin[$real_name] as $func_name => $data)
		{
		
			$top = "\n\nfunction $func_name($data) {\n".
			       "global \$ibforums;\n".
			       "return <<<EOF\n";
			       
			$bum = "\nEOF;\n}\n";
		
			$text = preg_replace("/\s*<!--\|IBF\|$func_name\|START\|-->\s*\n/", "$top", $text);
			
			
			//+-------------------------------
			
			$text = preg_replace("/\s*<!--\|IBF\|$func_name\|END\|-->\s*\n/", "$bum", $text);
		}
		
		$end = "\n\n}\n?".">";
		
		$final .= $text.$end;
		
		if ($fh = fopen( "$phpskin", 'w' ) )
		{
			fwrite($fh, $final, strlen($final) );
			fclose($fh);
		}
		else
		{
			$ADMIN->error("Could not save information to $phpskin, please ensure that the CHMOD permissions are correct.");
		}
		
		$ADMIN->done_screen("Template file updated", "Manage Templates in template set: {$row['skname']}", "act=templ&code=edit&id={$IN['id']}" );
		
	}
	
	//------------------------------------------------------------------------------------
	//------------------------------------------------------------------------------------
	
	function tools()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must choose a valid skin file to perform this operation on");
		}
		
		if ($IN['tool'] == 'conf')
		{
			$this->tools_conf();
		}
		else if ($IN['tool'] == 'tmpl')
		{
			$this->tools_build_tmpl();
		}
		else
		{
			$this->tools_rebuildphp();
		}
		
	}
	
	//------------------------------------------------------------------------------------
	
	function tools_build_tmpl()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		// Rebuilds the skin.html editable files from the PHP source files
		
		$templates_dir = $root_path."style_templates/".$IN['id'];
		
		$skin_dir     = $root_path."Skin/s".$IN['id'];
		
		$errors = array();
		
		$flag = 0;
		
		
		if ( ! is_writeable($skin_dir) )
		{
			$ADMIN->error("Cannot write into '$skin_dir', please check the CHMOD value, and if needed, CHMOD to 0777 via FTP. IBF cannot do this for you.");
		}
		
		if ( ! is_writeable($templates_dir) )
		{
			$ADMIN->error("Cannot write into '$templates_dir', please check the CHMOD value, and if needed, CHMOD to 0777 via FTP. IBF cannot do this for you.");
		}
		
		if ( is_dir($skin_dir) )
		{
			if ( $handle = opendir($skin_dir) )
			{
			
				while (($filename = readdir($handle)) !== false)
				{
					if (($filename != ".") && ($filename != ".."))
					{
					
						if ( preg_match( "/\.php$/", $filename ) )
						{
							
							$name = preg_replace( "/^(\S+)\.(\S+)$/", "\\1", $filename );
							
							if ( file_exists($templates_dir."/".$name.".html") )
							{
								if (! is_writeable($templates_dir."/".$name.".html") )
								{
									$errors[] = "Cannot write to $name".".html, skipping file...";
								}
							}
							
							$farray = file($skin_dir."/".$filename) or die("Could not open $skin_dir/$filename for reading");
							
							//$tfh = fopen($skin_dir."/".$filename , 'r')
							
							//----------------------------------------------------
							
							$functions = array();
							
							foreach($farray as $f)
							{
								
								// Skip javascript functions...
								
								if ( preg_match( "/<script/i", $f ) )
								{
									$script_token = 1;
								}
								
								if ( preg_match( "/<\/script>/i", $f ) )
								{
									$script_token = 0;
								}
								
								//-------------------------------
								
								if ($script_token == 0)
								{
									if ( preg_match( "/^function\s*([\w\_]+)\s*\((.*)\)/i", $f, $matches ) )
									{
										$functions[$matches[1]] = '';
										$flag = $matches[1];
										continue;
									}
								}
									
								if ($flag)
								{
									$functions[$flag] .= $f;
									continue;
								}
								 
							}
							
							//----------------------------------------------------
							
							$final = "";
							$flag  = 0;
							
							foreach($functions as $fname => $ftext)
							{
								$final .= "<!--|IBF|$fname|START|-->\n\n";
								
								preg_match( "/return <<<EOF(.+?)EOF;/s", $ftext, $matches );
								
								$final .= $this->convert_tags($matches[1]);
								
								$final .= "\n\n<!--|IBF|$fname|END|-->\n\n";
							}
							
							$fh = fopen($templates_dir."/".$name.".html", 'w');
							fwrite( $fh, $final, strlen($final) );
							fclose($fh);
							
							$functions = array();
							
							//----------------------------------------------------
							
						} // if *.php
						
					} // if not dir
					
				} // while loop
				
				closedir($handle);
				
			}
			else
			{
				$ADMIN->error("Could not open directory $skin_dir for reading!");
			}
		}
		else
		{
			$ADMIN->error("$skin_dir is not a directory, please check the \$root_path variable in admin.php");
		}
		
		$ADMIN->done_screen("Editable templates updated from source PHP skin files", "Manage Template sets", "act=templ" );
		
		if (count($errors > 0))
		{
			$this->html .= $SKIN->start_table("Errors and warnings");
		
			$this->html .= $SKIN->add_td_basic( implode("<br>", $errors) );
											 
			$this->html .= $SKIN->end_table();
		}
		
		
	}
	
	//----------------------------------------------------
	
	
	function tools_conf()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		// Rebuilds the config.php file (remaps function names and ($data) stuff
		
		$final = "<?"."php\n\n";
		
		$config_file = $root_path."style_templates/".$IN['id']."/config.php";
		
		$skin_dir    = $root_path."Skin/s".$IN['id'];
		
		if ( ! file_exists($config_file) )
		{
			$ADMIN->error("Could not locate the master config file in $config_file, is it there?");
		}
		
		if ( ! is_writeable($config_file) )
		{
			$ADMIN->error("Cannot rewrite the master config file in $config_file, please check the CHMOD value - and if needed, set to 0777 via FTP");
		}
		
		$script_token = 0;
		
		if ( is_dir($skin_dir) )
		{
			if ( $handle = opendir($skin_dir) )
			{
			
				while (($filename = readdir($handle)) !== false)
				{
					if (($filename != ".") && ($filename != ".."))
					{
					
						if ( preg_match( "/\.php$/", $filename ) )
						{
							
							$name = preg_replace( "/^(\S+)\.(\S+)$/", "\\1", $filename );
							
							$astart = "\$skin["."'$name'"."]";
							
							$farray = file($skin_dir."/".$filename) or die("Could not open $skin_dir/$filename for reading");
							
							foreach($farray as $f)
							{
								
								// Skip javascript functions...
								
								if ( preg_match( "/<script/i", $f ) )
								{
									$script_token = 1;
								}
								
								if ( preg_match( "/<\/script>/i", $f ) )
								{
									$script_token = 0;
								}
								
								//-------------------------------
								
								if ($script_token == 0)
								{
									if ( preg_match( "/^function\s*([\w\_]+)\s*\((.*)\)/i", $f, $matches ) )
									{
										$matches[2] = preg_replace( "/\'/", "\"", $matches[2] );
									
										$final .= $astart."['".$matches[1]."']"."   =   '".$matches[2]."';\n";
									}
								}
							}
						}
					}
				}
				
				closedir($handle);
				
			}
			else
			{
				$ADMIN->error("Could not open directory $skin_dir for reading!");
			}
		}
		else
		{
			$ADMIN->error("$skin_dir is not a directory, please check the \$root_path variable in admin.php");
		}
		
		$final .= "?".">";
		
		$fh = fopen($config_file, 'w');
		fwrite( $fh, $final, strlen($final) );
		fclose($fh);
		
		$ADMIN->done_screen("Master configuration template file updated", "Manage Template sets", "act=templ" );
		
	}
		
	
	//-------------------------------------------------------------
	// Add images..
	//-------------------------------------------------------------
	
	
	function add_templates()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing template set ID, go back and try again");
		}
		
		//-------------------------------------
		
		if ( ! is_writeable($root_path.'style_templates') )
		{
			$ADMIN->error("The directory 'style_templates' is not writeable by this script. Please check the permissions on that directory. CHMOD to 0777 if in doubt and try again");
		}
		
		if ( ! is_writeable($root_path.'Skin') )
		{
			$ADMIN->error("The directory 'Skin' is not writeable by this script. Please check the permissions on that directory. CHMOD to 0777 if in doubt and try again");
		}
		
		//-------------------------------------
		
		if ( ! is_dir($root_path.'style_templates/'.$IN['id']) )
		{
			$ADMIN->error("Could not locate the original template set to copy, please check and try again");
		}
		
		if ( ! is_dir($root_path.'Skin/s'.$IN['id']) )
		{
			$ADMIN->error("Could not locate the original template set to copy, please check and try again");
		}
		
		//-------------------------------------
		
		$DB->query("SELECT * FROM ibf_tmpl_names WHERE skid='".$IN['id']."'");
		
		//-------------------------------------
		
		if ( ! $row = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query that template set from the DB, so there");
		}
		
		//-------------------------------------
		
		$row['skname'] = $row['skname'].".2";
		
		// Insert a new row into the DB...
		
		$final = array();
		
		foreach($row as $k => $v)
		{
			if ($k == 'skid')
			{
				continue;
			}
			else
			{
				$final[ $k ] = $v;
			}
		}
		
		$db_string = $DB->compile_db_insert_string( $final );
		
		$DB->query("INSERT INTO ibf_tmpl_names (".$db_string['FIELD_NAMES'].") VALUES(".$db_string['FIELD_VALUES'].")");
		
		$new_id = $DB->get_insert_id();
		
		//-------------------------------------
		
		if ( ! $ADMIN->copy_dir( $INFO['base_dir'].'style_templates/'.$IN['id'] , $INFO['base_dir'].'style_templates/'.$new_id ) )
		{
			$DB->query("DELETE FROM ibf_tmpl_names WHERE skid='$new_id'");
			
			$ADMIN->error( $ADMIN->errors );
		}
		
		//-------------------------------------
		
		if ( ! $ADMIN->copy_dir( $INFO['base_dir'].'Skin/s'.$IN['id'] , $INFO['base_dir'].'Skin/s'.$new_id ) )
		{
			$DB->query("DELETE FROM ibf_tmpl_names WHERE skid='$new_id'");
			
			$ADMIN->error( $ADMIN->errors );
		}
		
		//-------------------------------------
		// Pass to edit / add form...
		//-------------------------------------
		
		$this->do_form('add', $new_id);
	
	}
	
	//-------------------------------------------------------------
	// REMOVE WRAPPERS
	//-------------------------------------------------------------
	
	function remove()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		//+-------------------------------
		
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing template set ID, go back and try again");
		}
		if ( SAFE_MODE_ON == 0 )
		{
			
			if ( ! $ADMIN->rm_dir( $INFO['base_dir']."style_templates/".$IN['id'] ) )
			{
				$ADMIN->error("Could not remove the template files, please check the CHMOD permissions to ensure that this script has the correct permissions to allow this");
			}
			
			if ( ! $ADMIN->rm_dir( $INFO['base_dir']."Skin/s".$IN['id'] ) )
			{
				$ADMIN->error("Could not remove the template files, please check the CHMOD permissions to ensure that this script has the correct permissions to allow this");
			}
			
			$DB->query("DELETE FROM ibf_tmpl_names WHERE skid='".$IN['id']."'");
			
			$std->boink_it($SKIN->base_url."&act=templ");
			exit();
		}
		else
		{
			$DB->query("DELETE FROM ibf_tmpl_names WHERE skid='".$IN['id']."'");
			$ADMIN->info_screen("Safe mode restriction detected.<br>You will need to manually remove the directories 'Skin/s{$IN['id']}' and 'style_templates/{$IN['id']}' via FTP. The information has been removed from the database.");
		}
		
	}
	
	//-------------------------------------------------------------
	// EDIT TEMPLATES, STEP TWO
	//-------------------------------------------------------------
	
	function show_file()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		$wrap = 'soft';
		
		if ($IN['rows'] != "")
		{
			$rows = $IN['rows'];
		}
		
		if ($IN['cols'] != "")
		{
			$cols = $IN['cols'];
		}
		
		if ($IN['wrap'] != "")
		{
			$wrap = $IN['wrap'];
		}
		
		$std->my_setcookie('ad_tempform', $rows."-".$cols."-".$wrap );
		
		//+-------------------------------
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing template set ID, go back and try again");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT * from ibf_tmpl_names WHERE skid='".$IN['id']."'");
		
		if ( ! $row = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the information from the database");
		}
		
		$template = $root_path."style_templates/".$IN['id']."/".$IN['template_name'];
		
		
		if ( ! file_exists($template) )
		{
			$ADMIN->error("Cannot locate '$template', please check the paths and ensure the file and directories are correct.");
		}
		
		//+-------------------------------
		
		if ( ! is_writeable($template) )
		{
			$ADMIN->error("Cannot write into '$template', please check the CHMOD value, and if needed, CHMOD to 0777 via FTP. IBF cannot do this for you.");
		}
		
		//+-------------------------------
		
		if ($fh = @fopen( $template, 'r' ) )
		{
			$templ = fread( $fh, filesize($template) );
			fclose($fh);
		}
		else
		{
			if ( SAFE_MODE_ON == 0 )
			{
				$ADMIN->error("Could not load $template, please ensure that the file is there and the CHMOD permissions are correct.");
			}
			else
			{
				$ADMIN->info_screen("<b>Safe mode restriction detected</b><br>Due to the nature of your PHP installation, you may not be able to edit templates using the online editor");
			}
		}
		
		//+-------------------------------
		// Swop < and > into ascii entities
		// to prevent textarea breaking html
		//+-------------------------------
		
		$templ = preg_replace("/&/", "&#38;", $templ );
		$templ = preg_replace("/</", "&#60;", $templ );
		$templ = preg_replace("/>/", "&#62;", $templ );
		
		
		
		//+-------------------------------
	
		$ADMIN->page_detail = "You may edit the HTML of this template.<br>Please DO NOT REMOVE OR EDIT the Invision board comment tags (example: &lt!--|IBF|table_structure|START|--&gt). Doing so may break the template file and your board. If in doubt, create a new skin and test the edits on that. These tags must also be on their own line, they cannot be embedded into the HTML";
		$ADMIN->page_title  = "Edit Template Set File: ".$IN['template_name']." in set: ".$row['skname'];
		
									     
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->start_form( array( 1 => array( 'code'  , 'doedit'    ),
												  2 => array( 'act'   , 'templ'     ),
												  3 => array( 'id'    , $IN['id']   ),
												  4 => array( 'name'  , $IN['template_name'] ),
									     )      );
									     
		//+-------------------------------
		
		$SKIN->td_header[] = array( "&nbsp;"   , "100%" );

		//+-------------------------------
		
		$ADMIN->html .= $SKIN->start_table( "The template" );
		
		$ADMIN->html .= $SKIN->add_td_basic( "&lt;-- <a href='".$ADMIN->base_url."&act=templ&code=edit&id={$IN['id']}'>Go back to choose another template to edit in set '{$row['skname']}'</a>", "center", "catrow");
		
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"<center>".$SKIN->form_textarea('template', $templ, $cols, $rows, $wrap)."</center>",
									     )      );
									     
		
									     
		$ADMIN->html .= $SKIN->end_form("Update this file");
									     
		$ADMIN->html .= $SKIN->end_table();
		
		$ADMIN->output();
		
		
	}
	
	//-------------------------------------------------------------
	// EDIT TEMPLATES, STEP ONE
	//-------------------------------------------------------------
	
	function do_form($method='edit', $new_id="")
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		if ($new_id != "")
		{
			$IN['id'] = $new_id;
		}
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing template set ID, go back and try again");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT * from ibf_tmpl_names WHERE skid='".$IN['id']."'");
		
		if ( ! $row = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the information from the database");
		}
		
		$templates_dir = $INFO['base_dir']."style_templates/".$IN['id'];
		
		$form_array = array();
		
		//+-------------------------------
		
		if ($method != 'add')
		{
			if ( ! is_writeable($templates_dir) )
			{
				$ADMIN->error("Cannot write into '$templates_dir', please check the CHMOD value, and if needed, CHMOD to 0777 via FTP. IBF cannot do this for you.");
			}
		}
		
		//+-------------------------------
		
		if ( is_dir($templates_dir) )
		{
			$handle = opendir($templates_dir);
			
			while (($filename = readdir($handle)) !== false)
			{
				if (($filename != ".") && ($filename != ".."))
				{
					if (preg_match("/^index/", $filename))
					{
						continue;
					}
					
					if (preg_match("/\.html$/", $filename))
					{
						$form_array[] = array( $filename, $filename );
					}
				}
			}
				
			closedir($handle);
		}
		
		if ($row['author'] and $row['email'])
		{
			$author = "<br><br>This template set <b>'{$row['skname']}'</b> was created by <a href='mailto:{$row['email']}' target='_blank'>{$row['author']}</a>";
		}
		else if ($row['author'])
		{
			$author = "<br><br>This template set <b>'{$row['skname']}'</b> was created by {$row['author']}";
		}
		
		if ($row['url'])
		{
			$url = "<br><br>Authors website: <a href='{$row['url']}' target='_blank'>{$row['url']}</a>";
		}
		
		//+-------------------------------
	
		$ADMIN->page_detail = "Please choose which section you wish to edit below.$author $url";
		$ADMIN->page_title  = "Edit Template sets";
		
		//+-------------------------------
		
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->js_no_specialchars();
		
		
		$ADMIN->html .= $SKIN->start_form( array( 1 => array( 'code'  , 'editinfo'    ),
												  2 => array( 'act'   , 'templ'       ),
												  3 => array( 'id'    , $IN['id']     ),
									     ), "theAdminForm", "onSubmit=\"return no_specialchars('templates')\""      );
									     
		//+-------------------------------
		
		$SKIN->td_header[] = array( "&nbsp;"   , "60%" );
		$SKIN->td_header[] = array( "&nbsp;"   , "40%" );

		//+-------------------------------
		
		$ADMIN->html .= $SKIN->start_table( "Edit template information" );
		
									     
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"<b>Template Set Name</b>",
													$SKIN->form_input('skname', $row['skname']),
									     )      );
									     
		if ($method == 'add')
		{
									     
			$ADMIN->html .= $SKIN->add_td_row( array( 
														"<b>Template set author name:</b>",
														$SKIN->form_input('author', $row['author']),
											 )      );
											 
			$ADMIN->html .= $SKIN->add_td_row( array( 
														"<b>Template set author email:</b>",
														$SKIN->form_input('email', $row['email']),
											 )      );
											 
			$ADMIN->html .= $SKIN->add_td_row( array( 
														"<b>Template set author webpage:</b>",
														$SKIN->form_input('url', $row['url']),
											 )      );
									     
		}
									     
		$ADMIN->html .= $SKIN->end_form("Edit template set details");
									     
		$ADMIN->html .= $SKIN->end_table();
									     
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->start_form( array( 1 => array( 'code'  , 'edit2'    ),
												  2 => array( 'act'   , 'templ'     ),
												  3 => array( 'id'    , $IN['id']   ),
									     ), 'secondform'     );
									     
		//+-------------------------------
		
		$SKIN->td_header[] = array( "&nbsp;"   , "60%" );
		$SKIN->td_header[] = array( "&nbsp;"   , "40%" );

		//+-------------------------------
		
		if ( $cookie = $std->my_getcookie('ad_tempform') )
		{
			list($rows, $cols, $wrap) = explode( '-', $cookie );
		}
		
		$cols = $cols ? $cols : 80;
		$rows = $rows ? $rows : 40;
		$wrap = $wrap ? $wrap : 'soft';
		
		$ADMIN->html .= $SKIN->start_table( "Edit templates in set '".$row['skname']."'" );
		
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"<b>Please select a template file to edit</b>",
													$SKIN->form_dropdown('template_name', $form_array),
									     )      );
									     
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"<b>Text area rows:</b>",
													$SKIN->form_input('rows', $rows),
									     )      );
									     
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"<b>Text area columns:</b>",
													$SKIN->form_input('cols', $cols),
									     )      );
									     
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"<b>Text area wrapping:</b>",
													$SKIN->form_dropdown('wrap', 
													array( 0 => array( 'soft', 'soft'),
														   1 => array( 'virtual', 'virtual'),
														   2 => array( 'off', 'off'),
														 ),
													$wrap),
									     )      );
									     
		$ADMIN->html .= $SKIN->end_form("Edit this template file");
									     
		$ADMIN->html .= $SKIN->end_table();
									     
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->output();
		
		
	}
	
	//-------------------------------------------------------------
	// SHOW CURRENT TEMPLATE PACKS
	//-------------------------------------------------------------
	
	function list_current()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		$form_array = array();
	
		$ADMIN->page_detail = "The skin templates contain the all the board HTML. You can edit each section individually via the online editor, or download the template files to edit in another editor.";
		$ADMIN->page_title  = "Manage Template Sets";
		
		//+-------------------------------
		
		$SKIN->td_header[] = array( "Title"        , "40%" );
		$SKIN->td_header[] = array( "Allocation"   , "30%" );
		$SKIN->td_header[] = array( "Export"       , "10%" );
		$SKIN->td_header[] = array( "Edit"         , "10%" );
		$SKIN->td_header[] = array( "Remove"       , "10%" );
		
		//+-------------------------------
		
		$DB->query("SELECT DISTINCT(s.set_id), t.skid, t.skname from ibf_tmpl_names t, ibf_skins s WHERE s.set_id=t.skid ORDER BY t.skname ASC");
		
		$used_ids = array();
		$show_array = array();
		
		if ( $DB->get_num_rows() )
		{
		
			$ADMIN->html .= $SKIN->start_table( "Current Template sets In Use" );
			
			while ( $r = $DB->fetch_row() )
			{
			
				$show_array[ $r['skid'] ] .= stripslashes($r['skname'])."<br>";
			
				if ( in_array( $r['skid'], $used_ids ) )
				{
					continue;
				}
				
				$ADMIN->html .= $SKIN->add_td_row( array( "<b>".stripslashes($r['skname'])."</b>",
														  "<#X-{$r['skid']}#>",
														  "<center><a href='".$SKIN->base_url."&act=templ&code=export&id={$r['skid']}'>Export</a></center>",
														  "<center><a href='".$SKIN->base_url."&act=templ&code=edit&id={$r['skid']}'>Edit</a></center>",
														  "<i>Deallocate before removing</i>",
												 )      );
												   
				$used_ids[] = $r['skid'];
				
				$form_array[] = array( $r['skid'], $r['skname'] );
				
			}
			
			foreach( $show_array as $idx => $string )
			{
				$string = preg_replace( "/<br>$/", "", $string );
				
				$ADMIN->html = preg_replace( "/<#X-$idx#>/", "$string", $ADMIN->html );
			}
			
			$ADMIN->html .= $SKIN->end_table();
		}
		
		if ( count($used_ids) > 0 )
		{
		
			$DB->query("SELECT skid, skname FROM ibf_tmpl_names WHERE skid NOT IN(".implode(",",$used_ids).")");
		
			if ( $DB->get_num_rows() )
			{
			
				$SKIN->td_header[] = array( "Title"  , "70%" );
				$SKIN->td_header[] = array( "Export"   , "10%" );
				$SKIN->td_header[] = array( "Edit"   , "10%" );
				$SKIN->td_header[] = array( "Remove" , "10%" );
			
				$ADMIN->html .= $SKIN->start_table( "Current Unallocated Template sets" );
				
				$ADMIN->html .= $SKIN->js_checkdelete();
				
				while ( $r = $DB->fetch_row() )
				{
					
					$ADMIN->html .= $SKIN->add_td_row( array( "<b>".stripslashes($r['skname'])."</b>",
															  "<center><a href='".$SKIN->base_url."&act=templ&code=export&id={$r['skid']}'>Export</a></center>",
															  "<center><a href='".$SKIN->base_url."&act=templ&code=edit&id={$r['skid']}'>Edit</a></center>",
															  "<center><a href='javascript:checkdelete(\"act=templ&code=remove&id={$r['skid']}\")'>Remove</a></center>",
													 )      );
													 
					$form_array[] = array( $r['skid'], $r['skname'] );
													   
				}
				
				$ADMIN->html .= $SKIN->end_table();
			}
		}
		
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->start_form( array( 1 => array( 'code'  , 'add'     ),
												  2 => array( 'act'   , 'templ'    ),
									     )      );
		
		$SKIN->td_header[] = array( "&nbsp;"  , "40%" );
		$SKIN->td_header[] = array( "&nbsp;"  , "60%" );
		
		$ADMIN->html .= $SKIN->start_table( "Create New Template Set" );
			
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->add_td_row( array( "<b>Base new Template set on...</b>" ,
										  		  $SKIN->form_dropdown( "id", $form_array)
								 )      );
		
		$ADMIN->html .= $SKIN->end_form("Create new Template set");
										 
		$ADMIN->html .= $SKIN->end_table();
		
		//+-------------------------------
		//+-------------------------------
		
		
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->start_form( array( 1 => array( 'code'  , 'tools'     ),
												  2 => array( 'act'   , 'templ'    ),
									     )      );
		
		$SKIN->td_header[] = array( "Tool"  , "50%" );
		$SKIN->td_header[] = array( "run on template set"  , "50%" );
		
		$extra = "";
		
		if ( SAFE_MODE_ON == 1)
		{
			$extra = "<br><span id='detail'>WARNING: Safe mode restrictions detected, some of these tools will not work</span>";
		}
		
		$ADMIN->html .= $SKIN->start_table( "Template Tools".$extra );
			
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->add_td_row( array( $SKIN->form_dropdown( "tool",
																		array(
																				0 => array( 'conf', 'Rebuild Master Config (remap function call data)' ),
																				1 => array( 'tmpl', 'Rebuild editable templates from php skin files'   ),
																				2 => array( 'skin', 'Rebuild php skin files from editable templates'   ),
																			 )
												                      ) ,
										  		  $SKIN->form_dropdown( "id", $form_array)
								 )      );
		
		$ADMIN->html .= $SKIN->end_form("Run Tool");
										 
		$ADMIN->html .= $SKIN->end_table();
		
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->output();
	
	}
	
	function convert_tags($t="")
	{
		if ($t == "")
		{
			return "";
		}
		
		$t = preg_replace( "/{?\\\$ibforums->base_url}?/"            , "{ibf.script_url}"   , $t );
		$t = preg_replace( "/{?\\\$ibforums->session_id}?/"          , "{ibf.session_id}"   , $t );
		$t = preg_replace( "/{?\\\$ibforums->skin\['?(\w+)'?\]}?/"   , "{ibf.skin.\\1}"      , $t );
		$t = preg_replace( "/{?\\\$ibforums->lang\['?(\w+)'?\]}?/"   , "{ibf.lang.\\1}"      , $t );
		$t = preg_replace( "/{?\\\$ibforums->vars\['?(\w+)'?\]}?/"   , "{ibf.vars.\\1}"      , $t );
		$t = preg_replace( "/{?\\\$ibforums->member\['?(\w+)'?\]}?/" , "{ibf.member.\\1}"    , $t );
		
		return $t;
		
	}
	
	function unconvert_tags($t="")
	{
		if ($t == "")
		{
			return "";
		}
		
		$t = preg_replace( "/{ibf\.script_url}/i"   , '{$ibforums->base_url}'         , $t);
		$t = preg_replace( "/{ibf\.session_id}/i"   , '{$ibforums->session_id}'       , $t);
		$t = preg_replace( "/{ibf\.skin\.(\w+)}/"   , '{$ibforums->skin[\''."\\1".'\']}'   , $t);
		$t = preg_replace( "/{ibf\.lang\.(\w+)}/"   , '{$ibforums->lang[\''."\\1".'\']}'   , $t);
		$t = preg_replace( "/{ibf\.vars\.(\w+)}/"   , '{$ibforums->vars[\''."\\1".'\']}'   , $t);
		$t = preg_replace( "/{ibf\.member\.(\w+)}/" , '{$ibforums->member[\''."\\1".'\']}' , $t);
		
		return $t;
		
	}
	
	
}


?>