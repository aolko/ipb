<?php

class skin_search {


//    
function RenderRow($Data) {
global $ibforums;
return <<<EOF
    <!-- Begin Topic Entry {$Data['tid']} -->
    <tr> 
	  <td align='center' id='forum2'>{$Data['folder_img']}</td>
      <td align='center' width='3%' id='forum1'>{$Data['topic_icon']}</td>
      <td id='forum2'>
	  <table width='100%' border='0' cellspacing='0' cellpadding='0'>
		  <tr> 
			<td valign='middle'>{$Data['go_new_post']}</td>
            <td width='100%'><span id='linkthru'>{$Data['prefix']} <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?act=ST&f={$Data['forum_id']}&t={$Data['tid']}&hl={$Data['keywords']}&s={$ibforums->session_id}' id='linkthru'>{$Data['title']}</a></span>  {$Data[PAGES]}</td>
          </tr>
        </table>
        <span id='desc'>{$Data['description']}</span></td>
      <td id='forum2' width='20%' align='center'><span id="linkthru"><a href="{$ibforums->base_url}&act=SF&f={$Data['forum_id']}">{$Data['forum_name']}</a></span></td>
      <td align='center' id='forum1'>{$Data['starter']}</td>
      <td align='center' id='forum2'>{$Data['posts']}</td>
      <td align='center' id='forum1'>{$Data['views']}</td>
      <td id='forum1'>{$Data['last_post']}<br><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ST&f={$Data['forum_id']}&t={$Data['tid']}&view=getlastpost'>{$Data['last_text']}</a> <b>{$Data['last_poster']}</b></td>
    </tr>
    <!-- End Topic Entry {$Data['tid']} -->
EOF;
}

function end($Data) {
global $ibforums;
return <<<EOF
  <tr><td colspan='8' id='titlemedium'>&nbsp;</td></tr>
  </table>
  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  <table cellpadding='0 'cellspacing='4' border='0' width='{$ibforums->skin['tbl_width']}' align='center'>
      <tr>
         <td valign='middle' width='50%' nowrap align='left'>{$Data[SHOW_PAGES]}</td>
      </tr>
     </table>
   <table cellpadding='0' cellspacing='4' border='0' width='50%' align='center'>
     <tr>
        <td valign='middle' nowrap>{$ibforums->skin['B_NEW']}&nbsp;{$ibforums->lang['pm_open_new']}</td>
        <td valign='middle' nowrap>{$ibforums->skin['B_HOT']}&nbsp;{$ibforums->lang['pm_hot_new']}</td>
        <td valign='middle' nowrap>{$ibforums->skin['B_POLL']}&nbsp;{$ibforums->lang['pm_poll']}</td>
        <td valign='middle' nowrap>{$ibforums->skin['B_LOCKED']}&nbsp;{$ibforums->lang['pm_locked']}</td>
     </tr>
     <tr>
        <td valign='middle' nowrap>{$ibforums->skin['B_NORM']}&nbsp;{$ibforums->lang['pm_open_no']}</td>
        <td valign='middle' nowrap>{$ibforums->skin['B_HOT_NN']}&nbsp;{$ibforums->lang['pm_hot_no']}</td>
        <td valign='middle' nowrap>{$ibforums->skin['B_POLL_NN']}&nbsp;{$ibforums->lang['pm_poll_no']}</td>
        <td valign='middle' nowrap>{$ibforums->skin['B_MOVED']}&nbsp;{$ibforums->lang['pm_moved']}</td>
      </tr>
   </table>
EOF;
}

function Form($forums, $cats) {
global $ibforums;
return <<<EOF
    <!-- Search Form -->
    <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post" name='sForm'>
    <input type='hidden' name='act' value='Search'>
    <input type='hidden' name='s' value='{$ibforums->session_id}'>
    <input type='hidden' name='CODE' value='01'>
    $hidden_fields
    <br>
    <table cellpadding='0' cellspacing='1' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
        <tr>
            <td>
                <table cellpadding='4' cellspacing='1' border='0' width='100%'>
                    <tr>
                        <td colspan='2' id="titlemedium" background='{$ibforums->vars['img_url']}/tile_sub.gif' align='center'>{$ibforums->lang['keywords_title']}</td>
                    </tr>
                    <tr>
                        <td id='title' width='50%'><b>{$ibforums->lang['key_search']}</b></td>
                        <td id='title' width='50%'><b>{$ibforums->lang['mem_search']}</b></td>
                    </tr>
                    <tr>
                        <td id='row1' valign='top'><input type='text' maxlength='100' size='40' name='keywords' class='forminput'>&nbsp;<select name='smethod' class='forminput'><option value='simple'>{$ibforums->lang['se_simple']}</option><option value='advanced'>{$ibforums->lang['se_advanced']}</option></select><br><br>{$ibforums->lang['keysearch_text']}</td>
                        <td id='row1' valign='top'><input type='text' maxlength='100' size='50' name='namesearch' class='forminput'><br><br><input name='exactname' type='radio' value='1' checked>&nbsp;{$ibforums->lang['match_name_ex']}<br><input name='exactname' type='radio' value='1'>&nbsp;{$ibforums->lang['match_name_pa']}</td>
                    </tr>
                </table>
             </td>
           </tr>
         </table>
         
         
         <br>
         
         
   		<table cellpadding='0' cellspacing='1' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
          <tr>
              <td>
                <table cellpadding='4' cellspacing='1' border='0' width='100%'>         
                    
                    <tr>
                        <td colspan='2' id="titlemedium" background='{$ibforums->vars['img_url']}/tile_sub.gif' align='center'>{$ibforums->lang['search_options']}</td>
                    </tr>
                    
                    <tr>
                        <td id='title' width='50%' valign='top'><b>{$ibforums->lang['search_where']}</b></td>
                        <td id='title' width='50%' valign='top'><b>{$ibforums->lang['search_refine']}</b></td>
                    </tr>
                    
                    <tr>
                        <td id='row1' valign='middle'>
                        <table cellspacing='4' cellpadding='0' width='100%' align='center' border='0'>
                        	<tr>
                        	 <td valign='top' width='40%' nowrap><input type='radio' name='cat_forum' value='cat'>&nbsp;<b>{$ibforums->lang['search_cats']}</b></td>
                        	 <td valign='top' width='60%'>$cats</td>
                        	</tr>
                        	<tr>
                        	<td valign='top' nowrap><input type='radio' name='cat_forum' value='forum' checked>&nbsp;<b>{$ibforums->lang['search_forums']}</b></td>
                        	<td valign='top'>$forums</td>
                        	</tr>
                        	<tr>
                        	 <td><input type='radio' name='search_in' value='posts' checked>&nbsp;{$ibforums->lang['in_posts']}</td>
                        	 <td><input type='radio' name='search_in' value='titles'>&nbsp;{$ibforums->lang['in_topics']}</td>
                        	</tr>
                          </table>
                        </td>
                        <td id='row1' valign='top'>
                        	<table cellspacing='4' cellpadding='0' width='100%' align='center' border='0'>
                        	<tr>
                        	 <td valign='top'>
                        		<b>{$ibforums->lang['search_from']}</b>
                       			<br>
								<select name='prune' class='forminput'>
								<option value='1'>{$ibforums->lang['today']}
								<option value='7'>{$ibforums->lang['this_week']}
								<option value='30' selected>{$ibforums->lang['this_month']}
								<option value='365'>{$ibforums->lang['this_year']}
								<option value='0'>{$ibforums->lang['ever']}
								</select>
                             	<br>{$ibforums->lang['and']}&nbsp;<input type='radio' name='prune_type' value='older' class='forminput' style='background-color:{$ibforums->skin['MISCBACK_ONE']}'>&nbsp;{$ibforums->lang['older']}&nbsp;<input type='radio' name='prune_type' value='newer' class='forminput' style='background-color:{$ibforums->skin['MISCBACK_ONE']}' checked>&nbsp;{$ibforums->lang['newer']}
                            </td>
                            <td valign='top'>
								<b>{$ibforums->lang['sort_results']}</b><br>
								<select name='sort_key' class='forminput'>
								<option value='last_post'>{$ibforums->lang['last_date']}</option>
								<option value='posts'>{$ibforums->lang['number_topics']}</option>
								<option value='starter_name'>{$ibforums->lang['poster_name']}</option>
								<option value='forum_id'>{$ibforums->lang['forum_name']}</option>
								</select>
								<br><input type='radio' name='sort_order' value='desc' checked>{$ibforums->lang['descending']}
								<br><input type='radio' name='sort_order' value='asc''>{$ibforums->lang['ascending']}
                            </td>
                            </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td id='row1' colspan='2' align='center'><input type='submit' value='{$ibforums->lang['do_search']}' class='forminput'></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <SCRIPT LANGUAGE="JavaScript1.1">
        function chooseForum() {
        	document.sForm.cat_forum[1].checked = true;
        }
        function chooseCat() {
        	document.sForm.cat_forum[0].checked = true;
        }
        </SCRIPT>
EOF;
}

function active_start($Data) {
global $ibforums;
return <<<EOF
   <script language='Javascript'>
   <!--
    function checkvalues() {
    	f = document.dateline;
    	if (f.st_day.value < f.end_day.value) {
    		alert("{$ibforums->lang['active_js_error']}");
    		return false;
    	}
    	if (f.st_day.value == f.end_day.value) {
    		alert("{$ibforums->lang['active_js_error']}");
    		return false;
    	}
    }
    -->
    </script>
    <br>
   <table cellpadding='0 'cellspacing='4' border='0' width='{$ibforums->skin['tbl_width']}' align='center'>
      <tr>
         <td valign='middle' nowrap align='left'><span id='pagetitle'>{$ibforums->lang['active_topics']}</span><br>{$Data[SHOW_PAGES]}</td>
         <td valign='middle' align='right'>
          <form action='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}' method='POST' name='dateline' onSubmit='return checkvalues();'>
          <input type='hidden' name='act' value='Search'>
          <input type='hidden' name='CODE' value='getactive'>
          <input type='hidden' name='s' value='{$ibforums->session_id}'>
         </td>
        </tr>
     </table>
     <table cellpadding='2' cellspacing='0' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
       	<tr> 
    		<td id='maintitle' background='{$ibforums->vars['img_url']}/tile_back.gif' align='center'> 
      			{$ibforums->lang['active_st_text']}
				<select name='st_day' class='forminput'>
				 <option value='s1'>{$ibforums->lang['active_yesterday']}</option>
				 <option value='s2'>2 {$ibforums->lang['active_days']}</option>
				 <option value='s3'>3 {$ibforums->lang['active_days']}</option>
				 <option value='s4'>4 {$ibforums->lang['active_days']}</option>
				 <option value='s5'>5 {$ibforums->lang['active_days']}</option>
				 <option value='s6'>6 {$ibforums->lang['active_days']}</option>
				 <option value='s7'>{$ibforums->lang['active_week']}</option>
				 <option value='s30'>{$ibforums->lang['active_month']}</option>
				</select>
				&nbsp;&nbsp;{$ibforums->lang['active_end_text']}&nbsp;&nbsp;
				<select name='end_day' class='forminput'>
				 <option value='e0'>{$ibforums->lang['active_today']}</option>
				 <option value='e1'>{$ibforums->lang['active_yesterday']}</option>
				 <option value='e2'>2 {$ibforums->lang['active_days']}</option>
				 <option value='e3'>3 {$ibforums->lang['active_days']}</option>
				 <option value='e4'>4 {$ibforums->lang['active_days']}</option>
				 <option value='e5'>5 {$ibforums->lang['active_days']}</option>
				 <option value='e6'>6 {$ibforums->lang['active_days']}</option>
				 <option value='e7'>{$ibforums->lang['active_week']}</option>
				</select>
				&nbsp;&nbsp;<input type='submit' value='&gt;&gt;' class='forminput'></form>
    		</td>
  		</tr>
        <tr>
            <td>
            	<table cellpadding='0' cellspacing='0' border='0' width='100%' align='center'>
        		 <tr>
            		<td id='mainbg'>
					 <table cellpadding='4' cellspacing='1' border='0' width='100%'>
					   <tr>
						  <td id='titlemedium' colspan='2' background='{$ibforums->vars['img_url']}/tile_sub.gif'>&nbsp;</td>
						  <td align='left' id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['h_topic_title']}</td>
						  <td align='center' id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['h_forum_name']}</td>
						  <td align='center' id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['h_topic_starter']}</td>
						  <td align='center' id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['h_replies']}</td>
						  <td align='center' id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['h_hits']}</td>
						  <td id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['h_last_action']}</td>
					   </tr>
EOF;
}

function start($Data) {
global $ibforums;
return <<<EOF
   <!-- Cgi-bot Start Forum page unique top -->
   <table cellpadding='0 'cellspacing='4' border='0' width='{$ibforums->skin['tbl_width']}' align='center'>
      <tr>
         <td valign='middle' width='50%' nowrap align='left'>{$Data[SHOW_PAGES]}</td>
      </tr>
     </table>
       <table cellpadding='1' cellspacing='0' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
        <tr>
            <td>
            	<table cellpadding='0' cellspacing='0' border='0' width='100%' align='center'>
        		 <tr>
            		<td id='mainbg'>
					 <table cellpadding='2' cellspacing='1' border='0' width='100%'>
					   <tr>
						  <td id='titlemedium' colspan='2' background='{$ibforums->vars['img_url']}/tile_sub.gif'>&nbsp;</td>
						  <td align='left' id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['h_topic_title']}</td>
						  <td align='center' id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['h_forum_name']}</td>
						  <td align='center' id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['h_topic_starter']}</td>
						  <td align='center' id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['h_replies']}</td>
						  <td align='center' id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['h_hits']}</td>
						  <td id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['h_last_action']}</td>
					   </tr>
EOF;
}

function active_none() {
global $ibforums;
return <<<EOF
<tr><td colspan='8' id='row1' align='center'><b>{$ibforums->lang['active_no_topics']}</b></td></tr>
EOF;
}


}
?>