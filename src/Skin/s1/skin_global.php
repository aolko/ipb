<?php

class skin_global {



function Member_bar($msg) {
global $ibforums;
return <<<EOF
&nbsp;{$ibforums->lang['logged_in_as']} <b>{$ibforums->member['name']}</b> ( <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=UserCP&CODE=00'>{$ibforums->lang['your_cp']}</a> | <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Login&CODE=03'>{$ibforums->lang['log_out']}</a> | <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Msg&CODE=01'>{$msg[TEXT]}</a> )
EOF;
}

function BoardHeader($time="") {
global $ibforums;
return <<<EOF

<table width='{$ibforums->skin['tbl_width']}' border='0' align='center' cellpadding='0' cellspacing='1' bgcolor='{$ibforums->skin['tbl_border']}'>
  <tr> 
    <td background='{$ibforums->vars['img_url']}/header_tile.gif'><img src='{$ibforums->vars['img_url']}/logo.jpg' alt='Powered by IBForums' border='0'></td>
  </tr>
  <tr> 
    <td id='alt1'>
      <table width='100%' border='0' cellspacing='0' cellpadding='2'>
        <tr>
          <td width='100%'> 
            <% MEMBER BAR %>
          </td>
          <td nowrap valign='middle'><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Search&f={$ibforums->input['f']}' style='text-decoration:none'><img src='{$ibforums->vars['img_url']}/icon_search.gif' title='Search' name='search' border='0' align='absmiddle'> Search</a> | <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Members' style='text-decoration:none'><img src='{$ibforums->vars['img_url']}/icon_members.gif' title='Members List' name='members' align='absmiddle' border='0'> Member List</a> | <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Help' style='text-decoration:none'><img src='{$ibforums->vars['img_url']}/icon_help.gif' title='Help Files' name='help' border='0' align='absmiddle'> Help</a></td>
        </tr>
      </table>
    </td>
  </tr>
</td>
</table>
<br>
EOF;
}

function start_nav() {
global $ibforums;
return <<<EOF
<table width='{$ibforums->skin['tbl_width']}' align='center' border="0" cellspacing="0" cellpadding="1">
<tr> 
    <td>{$ibforums->skin['F_NAV']}</td>
    <td width="100%" id="nav">
EOF;
}

function end_nav() {
global $ibforums;
return <<<EOF
		</td>
  </tr>
</table>
EOF;
}

function Redirect($Text, $Url) {
global $ibforums;
return <<<EOF
<html>
<head>
<title>{$ibforums->lang['stand_by']}</title>
<meta http-equiv='refresh' content='2; url={$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}$Url'>
<link type='text/css' href='style_sheets/stylesheet_{$ibforums->skin['css_id']}.css' rel='stylesheet'>
</head>

<body id='mainbg'>
<table width='{$ibforums->skin['tbl_width']}' height='85%' align='center'>
<tr>
	<td valign='middle'>
		<table align='center' border="0" cellspacing="1" cellpadding="0" bgcolor="{$ibforums->skin['tbl_border']}">
		<tr> 
			<td id='mainbg'>
				<table width="100%" border="0" cellspacing="1" cellpadding="12">
					<tr> 
						<td width="100%" align="center" id='alt1'>
							{$ibforums->lang['thanks']}, 
							$Text<br><br>
							{$ibforums->lang['transfer_you']}<br><br>
							(<a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}$Url'>{$ibforums->lang['dont_wait']}</a>)</td>
					</tr>
				</table>
			</td>
		</tr>
	  </table>
	</td>
</tr>
</table>
</body>
</html>
EOF;
}

function PM_popup() {
global $ibforums;
return <<<EOF
     <script language='JavaScript'>
     <!--
       window.open('index.{$ibforums->vars['php_ext']}?act=Msg&CODE=99&s={$ibforums->session_id}','NewPM','width=500,height=250,resizable=yes,scrollbars=1'); 
     //-->
     </script>
EOF;
}

function Guest_bar() {
global $ibforums;
return <<<EOF
&nbsp;{$ibforums->lang['guest_stuff']} ( <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Login&CODE=00'>{$ibforums->lang['log_in']}</a> | <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Reg&CODE=00'>{$ibforums->lang['register']}</a> )
EOF;
}

function admin_link() {
global $ibforums;
return <<<EOF
( <b><a href='{$ibforums->vars['board_url']}/admin.{$ibforums->vars['php_ext']}' target='_blank'>{$ibforums->lang['admin_cp']}</a></b> )
EOF;
}

function error_log_in($q_string) {
global $ibforums;
return <<<EOF
<form action='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}' method='post'>
     <input type='hidden' name='act' value='Login'>
     <input type='hidden' name='CODE' value='01'>
     <input type='hidden' name='s' value='{$ibforums->session_id}'>
     <input type='hidden' name='referer' value='$q_string'>
     <input type='hidden' name='CookieDate' value='1'>
     <table cellpadding='0' cellspacing='0' border='0' width='80%' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
        <tr>
            <td>
                <table cellpadding='3' cellspacing='1' border='0' width='100%'>
                <tr>
                <td align='left' colspan='2' id='titlemedium'>{$ibforums->lang['er_log_in_title']}</td>
                </tr>
                <tr>
                <td id='row1' width='40%'>{$ibforums->lang['erl_enter_name']}</td>
                <td id='row1'><input type='text' size='20' maxlength='64' name='UserName' class='forminput'></td>
                </tr>
                <tr>
                <td id='row1' width='40%'>{$ibforums->lang['erl_enter_pass']}</td>
                <td id='row1'><input type='password' size='20' name='PassWord' class='forminput'></td>
                </tr>
                <tr>
                <td id='row2' align='center' colspan='2'>
                <input type='submit' name='submit' value='{$ibforums->lang['erl_log_in_submit']}' class='forminput'>
                </td>
                </table>
             </td>
         </tr>
     </table>
   </form>
EOF;
}

function board_offline($message = "") {
global $ibforums;
return <<<EOF
<table width='{$ibforums->skin['tbl_width']}' border='0' align='center' cellpadding='0' cellspacing='1' bgcolor='{$ibforums->skin['tbl_border']}'>
  <tr> 
    <td background='{$ibforums->vars['img_url']}/tile_back.gif'>
			<table width='100%' border='0' cellspacing='0' cellpadding='3'>
        <tr> 
          <td><img src='{$ibforums->vars['img_url']}/nav_m.gif' alt='' width='8' height='8'></td>
          <td width='100%' id='titlemedium'>{$ibforums->lang['offline_title']}</td>
        </tr>
      </table>
		</td>
  </tr>
  <tr> 
    <td id='mainbg'>
			<table width='100%' border='0' cellspacing='1' cellpadding='4'>
				<tr> 
          <td colspan='2' valign='top' id='post1'> <p>$message</p></td>
        </tr>
        <tr> 
          <td colspan='2' valign='top' id='posthead'>{$ibforums->lang['offline_login']}</td>
        </tr>
        <tr>
					<form action='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}' method='post'>
					<input type='hidden' name='act' value='Login'>
					<input type='hidden' name='CODE' value='01'>
					<input type='hidden' name='s' value='{$ibforums->session_id}'>
					<input type='hidden' name='referer' value=''>
					<input type='hidden' name='CookieDate' value='1'>
          <td id='alt1'>{$ibforums->lang['erl_enter_name']}<br><img src='{$ibforums->vars['img_url']}/spacer.gif' alt='' width='180' height='1'></td>
          <td width='100%' id='alt1'><input type='text' size='20' maxlength='64' name='UserName' class='forminput'></td>
        </tr>
        <tr> 
          <td id='alt1'>{$ibforums->lang['erl_enter_pass']}</td>
          <td width='100%' id='alt1'><input type='password' size='20' name='PassWord' class='forminput'></td>
        </tr>
        <tr> 
          <td colspan='2' align='center' id='titlefoot'><input type='submit' name='submit' value='{$ibforums->lang['erl_log_in_submit']}' class='forminput'></td>
					</form>
        </tr>
      </table></td>
  </tr>
</table>
EOF;
}

function Error($message, $ad_email_one="", $ad_email_two="") {
global $ibforums;
return <<<EOF
	<script language='javascript'>
	<!--
	  function contact_admin() {
	  
	  	// Very basic spam bot stopper
	  		
	  	admin_email_one = '$ad_email_one';
	  	admin_email_two = '$ad_email_two';
	  	
	  	window.location = 'mailto:'+admin_email_one+'@'+admin_email_two+'?subject=Error on the forums';
	  	
	  }
	  
	  //-->
	  </script>

<table width='{$ibforums->skin['tbl_width']}' border='0' cellspacing='1' align='center' cellpadding='0' bgcolor='{$ibforums->skin['tbl_border']}'>
  <tr> 
    <td id='maintitleback' background='{$ibforums->vars['img_url']}/tile_back.gif'> 
      <table width='100%' border='0' cellspacing='0' cellpadding='3'>
        <tr> 
          <td><img src='{$ibforums->vars['img_url']}/nav_m.gif' alt='' width='8' height='8'></td>
          <td width='100%' id='maintitle'><b>{$ibforums->lang['error_title']}</b></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td id='mainbg'> 
      <table width='100%' border='0' cellspacing='1' cellpadding='4'>
        <tr> 
          <td id='alt1' valign='top'>
							{$ibforums->lang['exp_text']}<br><br>
						  <b>{$ibforums->lang['msg_head']}</b>
							<br><br>
							<span id='alert'>$message</span>
							<br><br>
							<!-- IBF.LOG_IN_TABLE -->
							<br><br>
							<b>Useful Links:</b>
							<br><br>
			 &#149; <a href='{$ibforums->base_url}&act=Reg&CODE=10'>{$ibforums->lang['er_lost_pass']}</a><br>
              &#149; <a href='{$ibforums->base_url}&act=Reg&CODE=00'>{$ibforums->lang['er_register']}</a><br>
              &#149; <a href='{$ibforums->base_url}&act=Help&CODE=00'>{$ibforums->lang['er_help_files']}</a><br>
              &#149; <a href='javascript:contact_admin();'>{$ibforums->lang['er_contact_admin']}</a></p>
          </td>
        </tr>
        <tr> 
          <td id='titlefoot' align='center'>&lt; <a href='javascript:history.go(-1)'>{$ibforums->lang['error_back']}</a></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
EOF;
}

function Member_no_usepm_bar() {
global $ibforums;
return <<<EOF
&nbsp;{$ibforums->lang['logged_in_as']} <b>{$ibforums->member['name']}</b> ( <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=UserCP&CODE=00'>{$ibforums->lang['your_cp']}</a> | <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Login&CODE=03'>{$ibforums->lang['log_out']}</a> )
EOF;
}


}
?>