<?php

/*
+--------------------------------------------------------------------------
|   IBFORUMS v1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001,2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > Moderation Control Panel module
|   > Module written by Matt Mecham
|   > Date started: 19th February 2002
|
|   > Module Version 1.0.0
+--------------------------------------------------------------------------
*/


$idx = new Moderate;

class Moderate {

    var $output    = "";
    var $base_url  = "";
    var $html      = "";

    var $moderator = array();
    var $forum     = array();
    var $topic     = array();
    
    var $upload_dir = "";

    
    /***********************************************************************************/
	//
	// Our constructor, load words, load skin, print the topic listing
	//
	/***********************************************************************************/
    
    function Moderate() {
    
        global $ibforums, $DB, $std, $print, $skin_universal;
        
        // Make sure this is a POST request, not a naughty IMG redirect
        
        require "./Skin/".$ibforums->skin_id."/skin_mod.php";
        
        //-------------------------------------
		// Compile the language file
		//-------------------------------------
		
        $ibforums->lang  = $std->load_words($ibforums->lang, 'lang_mod'  , $ibforums->lang_id);
        $ibforums->lang  = $std->load_words($ibforums->lang, 'lang_topic', $ibforums->lang_id);

        $this->html      = new skin_mod();
        
        //-------------------------------------
        // Check the input
        //-------------------------------------
        
        
        
        $ibforums->input['f'] = $std->is_number($ibforums->input['f']);
        if (! $ibforums->input['f'] )
        {
        	$std->Error( array( 'LEVEL' => 1,'MSG' => 'missing_files') );
        }
        
        $ibforums->input['st'] = $ibforums->input['st'] ? $std->is_number($ibforums->input['st']) : 0;
        
        
        //-------------------------------------
        // Get the forum info based on the forum ID, get the category name, ID, and get the topic details
        //-------------------------------------
        
        $DB->query("SELECT f.*, c.name as cat_name, c.id as cat_id from ibf_forums f, ibf_categories c WHERE f.id=".$ibforums->input['f']." and c.id=f.category");
        
        $this->forum = $DB->fetch_row();
        
        
        //-------------------------------------
        // Error out if we can not find the forum
        //-------------------------------------
        
        if (!$this->forum['id'])
        {
        	$std->Error( array( LEVEL => 1, MSG => 'missing_files') );
        }
        
        $pass = 0;
        
        $this->base_url = "{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}";
        
        if ($ibforums->member['id'])
        {
        	if ($ibforums->member['g_is_supmod'] == 1)
        	{
        		$pass = 1;
        	}
        	else
        	{
        		$DB->query("SELECT * FROM ibf_moderators WHERE forum_id='".$this->forum['id']."' AND member_id='".$ibforums->member['id']."'");
        		if ( $this->moderator = $DB->fetch_row() )
        		{
        			$pass = 1;
        		}
        	}
        }
        
        if ($pass == 0)
        {
        	$std->Error( array( LEVEL => 1, MSG => 'no_permission') );
        }
        
        $this->upload_dir = $ibforums->vars['upload_dir'];
        
        $this->upload_dir = preg_replace( "!/$!", "", $this->upload_dir );
        
        //-------------------------------------
        
        $print->add_output( $this->html->Menu_bar($this->base_url, $this->forum['id']) );
        
        //-------------------------------------
        // Convert the code ID's into something
        // use mere mortals can understand....
        //-------------------------------------
        
        switch ($ibforums->input['CODE']) {
        
        	case 'edituser':
        		$this->find_user_one();
        		break;
        	case 'dofinduser':
        		$this->find_user_two();
        		break;
        	case 'doedituser':
        		$this->edit_user();
        		break;
        	case 'compedit':
        		$this->complete_user_edit();
        		break;
        	
        	//-------------------------
        	
        	case 'prune':
        		$this->prune_juice();
        		break;
        	case 'doprune':
        		$this->drink_prune_juice();  // eew!
        		break;
        	case 'domove':
        		$this->do_move();
        		break;
        		
        	//-------------------------
        	
        	case 'modtopics':
        		$this->mod_topics();
        		break;
        	case 'domodtopics':
        		$this->domod_topics();
        		break;
        		
        	case 'modposts':
        		$this->mod_posts();
        		break;
        	case 'modtopicview':
        		$this->mod_topicview();
        		break;
        		
        	case 'domodposts':
        		$this->mod_domodposts();
        		break;
        		
        	case 'modtopicapprove':
        		$this->approve_all();
        		break;
        	
        	default:
        		$this->splash();
        		break;
        }
        
        // If we have any HTML to print, do so...
    	
    	$fj = $std->build_forum_jump();
		$fj = preg_replace( "!#Forum Jump#!", $ibforums->lang['forum_jump'], $fj);
		
		$this->output .= $this->html->CP_end();
		
		$this->output .= $this->html->forum_jump($fj, $links);
		
		if ( count($this->nav) < 1 )
		{
			$this->nav[] = "<a href='{$this->base_url}&act=SF&f={$this->forum['id']}'>{$this->forum['name']}</a>";
			$this->nav[] = "<a href='{$this->base_url}&act=ModCP&f={$this->forum['id']}'>{$ibforums->lang['cp_modcp_home']}</a>";
		}
		
		if (! $this->page_title )
		{
			$this->page_title = $ibforums->lang['cp_modcp_ptitle'];
		}
    	
    	$print->add_output("$this->output");
        $print->do_output( array( 'TITLE' => $this->page_title, 'JS' => 1, NAV => $this->nav ) );
      
	}
	
	
	//-------------------------------------------------
	// MODERATE NEW POSTS AND STUFF
	//--------------------------------------------------
	
	function approve_all()
	{
		global $std, $ibforums, $DB, $print;
		
		
		// Sort out the approved bit
		
		$DB->query("UPDATE ibf_posts SET queued=0 WHERE topic_id='".$ibforums->input['tid']."'");
		
		$DB->query("SELECT COUNT(pid) as posts FROM ibf_posts WHERE new_topic <> 1 and topic_id='".$ibforums->input['tid']."'");
		$count = $DB->fetch_row();
		
		$DB->query("UPDATE ibf_topics SET posts=".$count['posts']." WHERE tid='".$ibforums->input['tid']."'");
		
		// Update the posters ..er.. post count.
			
		$DB->query("SELECT starter_id FROM ibf_topics WHERE tid='".$ibforums->input['tid']."'");
		
		$mems = array();
		
		while ( $r = $DB->fetch_row() )
		{
			if ($r['starter_id'] > 0)
			{
				$mems[] = $r['starter_id'];
			}
		}
		
		if ( count($mems) > 0 )
		{
			$mstring = implode( ",", $mems );
			
			$DB->query("UPDATE ibf_members SET posts=posts+1 WHERE id IN ($mstring)");
		}
		
		// Update the last topic poster, time and number of posts.
			
		$DB->query("SELECT author_id, author_name, post_date FROM ibf_posts WHERE topic_id='".$ibforums->input['tid']."' AND queued <> 1 ORDER BY pid DESC LIMIT 0,1");
		
		if ($last = $DB->fetch_row())
		{
			$db_string = $DB->compile_db_update_string( array (
																 'last_post'        => $last['post_date'],
																 'last_poster_id'   => $last['author_id'],
																 'last_poster_name' => $last['author_name'],
													  )       );
													  
			$DB->query("UPDATE ibf_topics SET $db_string WHERE tid='".$ibforums->input['tid']."'");
		}
		
		// recount...
		
		$this->recount();
		
		
		// Boink
		
		$print->redirect_screen( $ibforums->lang['cp_redirect_mod_topics'], "act=ModCP&CODE=modposts&f=".$this->forum['id'] );
		
	}
	
	//--------------------------------------------------
	
	function mod_domodposts()
	{
		global $std, $ibforums, $DB, $print;
		
		//--------------------------------------------------
		// Which TID's are we playing with?
		//--------------------------------------------------
		
		$ids = array();
		
		foreach ($ibforums->input as $key => $value)
 		{
 			if ( preg_match( "/^PID_(\d+)$/", $key, $match ) )
 			{
 				if ($ibforums->input[$match[0]])
 				{
 					$ids[] = $match[1];
 				}
 			}
 		}
 		
 		//--------------------------------------------------
		// Did we actually select anyfink?
		//--------------------------------------------------
		
		if ( count($ids) < 1 )
		{
			$this->mod_error('cp_error_no_topics');
			return;
		}
		
		$pid_count = count($ids);
		
		$pids = implode( ",", $ids );
		
		//--------------------------------------------------
		// What did we do?
		//--------------------------------------------------
		
		if ($ibforums->input['type'] == 'approve')
		{
			// Sort out the approved bit
			
			$DB->query("UPDATE ibf_topics SET posts=posts+$pid_count WHERE tid='".$ibforums->input['tid']."'");
			
			$DB->query("UPDATE ibf_posts SET queued=0 WHERE pid IN ($pids)");
			
			// Update the posters ..er.. post count.
			
			$DB->query("SELECT author_id FROM ibf_posts WHERE queued <> 1 and pid IN ($pids)");
			
			$mems = array();
			
			while ( $r = $DB->fetch_row() )
			{
				if ($r['author_id'] > 0)
				{
					$mems[] = $r['author_id'];
				}
			}
			
			if ( count($mems) > 0 )
			{
				$mstring = implode( ",", $mems );
				
				$DB->query("UPDATE ibf_members SET posts=posts+1 WHERE id IN ($mstring)");
			}
			
			// Update the last topic poster, time and number of posts.
			
			$DB->query("SELECT author_id, author_name, post_date FROM ibf_posts WHERE topic_id='".$ibforums->input['tid']."' AND queued <> 1 ORDER BY pid DESC LIMIT 0,1");
			
			if ($last = $DB->fetch_row())
			{
				$db_string = $DB->compile_db_update_string( array (
																	 'last_post'        => $last['post_date'],
																	 'last_poster_id'   => $last['author_id'],
																     'last_poster_name' => $last['author_name'],
														  )       );
														  
				$DB->query("UPDATE ibf_topics SET $db_string WHERE tid='".$ibforums->input['tid']."'");
			}
			
			
			
			// recount...
			
			$this->recount();
		}
		else
		{
			// Delete 'dem postings
			
			$DB->query("DELETE FROM ibf_posts WHERE pid IN ($pids)");
			
			// Recount..
			
			$this->recount();
		}
		
		// Boink
		
		$print->redirect_screen( $ibforums->lang['cp_redirect_mod_topics'], "act=ModCP&CODE=modposts&f=".$this->forum['id'] );

	}
	
	//**-------------------------------------------------------
	
	function mod_topicview()
	{
		global $std, $ibforums, $DB, $print;
		
		$DB->query("SELECT tid, title FROM ibf_topics WHERE tid='".$ibforums->input['tid']."'");
		
		if ( ! $DB->get_num_rows() )
		{
			$this->mod_error('cp_error_no_topics');
			return;
		}
		
		$topic = $DB->fetch_row();
		
		$this->output .= $this->html->modtopicview_start($ibforums->input['tid'], $this->forum['name'], $this->forum['id'], $topic['title']);
		
		
		//+----------------------------------------------------------------
		// Get the topics to work on
		//+----------------------------------------------------------------
		
		$DB->query( "SELECT p.*, ".
				    "m.id,m.name,m.mgroup,m.email,m.joined,m.avatar,m.avatar_size,m.posts as member_posts,m.aim_name,m.icq_number,m.signature, m.website,m.yahoo,m.title,m.hide_email,m.msnname, ".
				    "g.g_id, g.g_title, g.g_icon, t.* ".
				    "FROM ibf_posts p, ibf_members m, ibf_groups g, ibf_topics t ".
				    "WHERE t.tid='".$ibforums->input['tid']."' AND t.approved=1 AND p.topic_id=t.tid AND p.queued=1 AND p.author_id=m.id AND g.g_id=m.mgroup ".
				    "ORDER BY p.pid ASC");
		
		while( $r = $DB->fetch_row() )
		{
			$member = $this->parse_member($r);
			
			$r['post_date'] = $std->get_date( $r['post_date'], 'LONG' );
			
			$this->output .= $this->html->mod_postentry_checkbox($r['pid']);
			$this->output .= $this->html->mod_postentry( array( 'msg' => $r, 'member' => $member ) );
			
		}
		$this->output .= $this->html->mod_topic_spacer();
		$this->output .= $this->html->modtopics_end();
		
	}
	
	//+----------------------------------------------------------------
	//+----------------------------------------------------------------
	
	function mod_posts()
	{
		global $std, $ibforums, $DB, $print;
		
		
		$DB->query("SELECT t.*, COUNT(p.pid) as reply_count FROM ibf_topics t, ibf_posts p WHERE p.queued=1 AND p.new_topic <> 1 AND t.tid=p.topic_id AND t.forum_id='".$this->forum['id']."' GROUP BY t.tid ORDER BY p.post_date ASC");
		
		if ( ! $DB->get_num_rows() )
		{
			$this->mod_error('cp_error_no_topics');
			return;
		}
		
		$this->output .= $this->html->modpost_topicstart($this->forum['name'], $this->forum['id']);
		
		//+----------------------------------------------------------------
		// Get the topics to work on
		//+----------------------------------------------------------------
		
		while( $r = $DB->fetch_row() )
		{
			$this->output .= $this->html->modpost_topicentry($r['title'], $r['tid'], $r['reply_count'], $this->forum['id']);
		}
		
		$this->output .= $this->html->modpost_topicend();
		
	}
	
	//--------------------------------------------------
	// MODERATE NEW TOPICS AND STUFF
	//--------------------------------------------------
	
	function domod_topics()
	{
		global $std, $ibforums, $DB, $print;
		
		//--------------------------------------------------
		// Which TID's are we playing with?
		//--------------------------------------------------
		
		$ids = array();
		
		foreach ($ibforums->input as $key => $value)
 		{
 			if ( preg_match( "/^TID_(\d+)$/", $key, $match ) )
 			{
 				if ($ibforums->input[$match[0]])
 				{
 					$ids[] = $match[1];
 				}
 			}
 		}
 		
 		//--------------------------------------------------
		// Did we actually select anyfink?
		//--------------------------------------------------
		
		if ( count($ids) < 1 )
		{
			$this->mod_error('cp_error_no_topics');
			return;
		}
		
		$tids = implode( ",", $ids );
		
		//--------------------------------------------------
		// What did we do?
		//--------------------------------------------------
		
		if ($ibforums->input['type'] == 'approve')
		{
			// Sort out the approved bit
			
			$DB->query("UPDATE ibf_topics SET approved=1 WHERE tid IN ($tids)");
			
			$DB->query("UPDATE ibf_posts SET queued=0 WHERE topic_id IN ($tids)");
			
			// Update the posters ..er.. post count.
			
			$DB->query("SELECT starter_id FROM ibf_topics WHERE tid IN ($tids)");
			
			$mems = array();
			
			while ( $r = $DB->fetch_row() )
			{
				if ($r['starter_id'] > 0)
				{
					$mems[] = $r['starter_id'];
				}
			}
			
			if ( count($mems) > 0 )
			{
				$mstring = implode( ",", $mems );
				
				$DB->query("UPDATE ibf_members SET posts=posts+1 WHERE id IN ($mstring)");
			}
			
			
			
			
			// recount...
			
			$this->recount();
		}
		else
		{
			// Delete 'dem postings
			
			$DB->query("DELETE FROM ibf_topics WHERE tid IN ($tids)");
			
			$DB->query("DELETE FROM ibf_posts WHERE topic_id IN ($tids)");
			
			// Recount..
			
			$this->recount();
		}
		
		// Boink
		
		$print->redirect_screen( $ibforums->lang['cp_redirect_mod_topics'], "act=ModCP&CODE=modtopics&f=".$this->forum['id'] );
		
	}
	
	//**-------------------------------------------------------
	
	function mod_topics()
	{
		global $std, $ibforums, $DB, $print;
		
		$perpage = 10;
		
		$start   = $ibforums->input['st'] ? $ibforums->input['st'] : 0;
		
		//--------------------------------------------------
		// How many topics must a man write down, before he is considered a man?
		//--------------------------------------------------
		
		$DB->query("SELECT COUNT(tid) as tcount FROM ibf_topics WHERE approved=0 and forum_id='".$this->forum['id']."'");
		$count = $DB->fetch_row();
		
		if ($count['tcount'] < 1)
		{
			$this->mod_error('cp_error_no_topics');
			return;
		}
		
		//+----------------------------------------------------------------
		// Generate the forum page span links
		//+----------------------------------------------------------------
		
		$pages = $std->build_pagelinks( array( 'TOTAL_POSS'  => $count['tcount'],
											   'PER_PAGE'    => $perpage,
											   'CUR_ST_VAL'  => $start,
											   'L_SINGLE'    => "",
											   'L_MULTI'     => $ibforums->lang['cp_pages'],
											   'BASE_URL'    => $this->base_url."&act=ModCP&CODE=modtopics&f=".$this->forum['id'],
									  )      );
									  
		$this->output .= $this->html->modtopics_start($pages, $this->forum['name'], $this->forum['id']);
		
		//+----------------------------------------------------------------
		// Get the topics to work on
		//+----------------------------------------------------------------
		
		$DB->query( "SELECT p.*, ".
				    "m.id,m.name,m.mgroup,m.email,m.joined,m.avatar,m.avatar_size,m.posts as member_posts,m.aim_name,m.icq_number,m.signature, m.website,m.yahoo,m.title,m.hide_email,m.msnname, ".
				    "g.g_id, g.g_title, g.g_icon, t.* ".
				    "FROM ibf_posts p, ibf_members m, ibf_groups g, ibf_topics t ".
				    "WHERE t.forum_id='".$this->forum['id']."' and t.approved=0 AND p.topic_id=t.tid AND p.new_topic=1 AND p.author_id=m.id AND g.g_id=m.mgroup ".
				    "ORDER BY t.tid ASC LIMIT $start, $perpage");
		
		while( $r = $DB->fetch_row() )
		{
			$member = $this->parse_member($r);
			
			$r['post_date'] = $std->get_date( $r['post_date'], 'LONG' );
			
			$this->output .= $this->html->mod_topic_title($r['title'], $r['tid']);
			$this->output .= $this->html->mod_postentry( array( 'msg' => $r, 'member' => $member ) );
			$this->output .= $this->html->mod_topic_spacer();
		}
		
		$this->output .= $this->html->modtopics_end();
		
	}
	
	//--------------------------------------------------
	// Do Pruney wooney
	//--------------------------------------------------
	
	function drink_prune_juice()
	{
		global $std, $ibforums, $DB, $print;
		
		$pass = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1)
		{
			$pass = 1;
		}
		else if ($this->moderator['mass_prune'] == 1)
		{
			$pass = 1;
		}
		else
		{
			$pass = 0;
		}
		
		if ($pass == 0)
		{
			$this->mod_error('cp_no_perms');
			return;
		}
		
		//-----------------------------------------------
		
		$db_query = "SELECT tid FROM ibf_topics WHERE approved=1 and forum_id='".$this->forum['id']."'";
		
		if ($ibforums->input['dateline'])
		{
			$db_query .= " AND last_post < {$ibforums->input['dateline']}";
		}
		
		if ($ibforums->input['starter'])
		{
			$db_query .= " AND starter_id='".$ibforums->input['starter']."'";
			
		}
		
		if ($ibforums->input['posts'])
		{
			$db_query .= " AND posts < '".$ibforums->input['posts']."'";
		}
		
		if ($ibforums->input['state'] != 'all')
		{
			if ($ibforums->input['state'])
			{
				$db_query .= " AND state='".$ibforums->input['state']."'";
			}
		}
		
		$batch = $DB->query($db_query);
		
		if ( ! $num_rows = $DB->get_num_rows() )
		{
			$this->mod_error('cp_error_no_topics');
			return;
		}
		
		//-----------------------------------------------
		// Right... to stop a potentially huge mySQL query
		// from forming with IN ( ), lets do this in batches
		// of 50...
		//-----------------------------------------------
		
		$tid_array = array();
		/*
		if ($num_rows > 50)
		{
			while ( $tid = $DB->fetch_row($batch) )
			{
				if ( count($tid_array) >= 50 )
				{
					$this->prune_batch( implode(",", $tid_array ) );
					$tid_array = array();
				}
				else
				{
					$tid_array[] = $tid['tid'];
				}
			}
		}
		else
		{
		*/
			while ( $tid = $DB->fetch_row() )
			{
				$tid_array[] = $tid['tid'];
			}
			
			$this->prune_batch( implode(",", $tid_array ) );
		//}
		
		// Recount the forum...
		
		$this->recount();
		
		$this->moderate_log("Pruned Forum");
		
		// Show results..
		
		$this->output .= $this->html->results( $ibforums->lang['cp_result_del'].$num_rows );
		
	}
	
	//--------------------------------------------------------------------------
	
	function prune_batch($tid_string)
	{
		global $std, $ibforums, $DB;
		
		// Check for attachments
		
		$DB->query("SELECT attach_id FROM ibf_posts WHERE topic_id IN ($tid_string)");
		
		while ( $attach = $DB->fetch_row() )
		{
			if ( is_file( $this->upload_dir."/".$attach['attach_id'] ) )
			{
				@unlink( $this->upload_dir."/".$attach['attach_id'] );
			}
		}
		
		// Remove polls
		
		$DB->query("DELETE FROM ibf_polls WHERE tid IN ($tid_string)");
		
		// Remove poll voters
		
		$DB->query("DELETE FROM ibf_voters WHERE tid IN ($tid_string)");
		
		// Remove the topics
		
		$DB->query("DELETE FROM ibf_topics WHERE tid IN ($tid_string)");
		
		// Remove the posts
		
		$DB->query("DELETE FROM ibf_posts WHERE topic_id IN ($tid_string)");
		
	}
	
	
	//--------------------------------------------------
	// Prune Forum start
	//--------------------------------------------------
	
	function prune_juice()
	{
		global $std, $ibforums, $DB, $print;
		
		$pass = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1)
		{
			$pass = 1;
		}
		else if ($this->moderator['mass_prune'] == 1)
		{
			$pass = 1;
		}
		else
		{
			$pass = 0;
		}
		
		if ($pass == 0)
		{
			$this->mod_error('cp_no_perms');
			return;
		}
		
		//-----------------------------------------------
		
		if ($ibforums->input['check'] == 1)
		{
		
			$link = "";
			$link_text = $ibforums->lang['cp_prune_dorem'];
			
			$DB->query("SELECT COUNT(tid) as tcount FROM ibf_topics WHERE approved=1 and forum_id='".$this->forum['id']."'");
			$tcount = $DB->fetch_row();
			
			$db_query = "SELECT COUNT(*) as count FROM ibf_topics WHERE approved=1 and forum_id='".$this->forum['id']."'";
			
			if ($ibforums->input['dateline'])
			{
				$date     = time() - $ibforums->input['dateline']*60*60*24;
				$db_query .= " AND last_post < $date";
				
				$link .= "&dateline=$date";
			}
			
			if ($ibforums->input['member'])
			{
				$DB->query("SELECT id FROM ibf_members WHERE name='".$ibforums->input['member']."'");
				
				if (! $mem = $DB->fetch_row() )
				{
					$this->mod_error('cp_error_no_mem');
					return;
				}
				else
				{
					$db_query .= " AND starter_id='".$mem['id']."'";
					$link     .= "&starter={$mem['id']}";
				}
			}
			
			if ($ibforums->input['posts'])
			{
				$db_query .= " AND posts < '".$ibforums->input['posts']."'";
				$link     .= "&posts={$ibforums->input['posts']}";
			}
			
			if ($ibforums->input['topic_type'] != 'all')
			{
				$db_query .= " AND state='".$ibforums->input['topic_type']."'";
				$link     .= "&state={$ibforums->input['topic_type']}";
			}
			
			$DB->query($db_query);
			$count = $DB->fetch_row();
			
			if ($ibforums->input['df'] == 'prune')
			{
				$link = "&act=ModCP&f={$this->forum['id']}&CODE=doprune&".$link;
			}
			else
			{
				if ($ibforums->input['df'] == $this->forum['id'])
				{
					$this->mod_error('cp_same_forum');
					return;
				}
				else if ($ibforums->input['df'] == -1)
				{
					$this->mod_error('cp_no_forum');
					return;
				}
				
				$link = "&act=ModCP&f={$this->forum['id']}&CODE=domove&df=".$ibforums->input['df'].$link;
				$link_text = $ibforums->lang['cp_prune_domove'];
			}
			
			$confirm_html = $this->html->prune_confirm( $tcount['tcount'], $count['count'], $link, $link_text );
			
		}
		
		
		$select = "<select name='topic_type' class='forminput'>";
		
		foreach( array( 'open', 'closed', 'link', 'all' ) as $type )
		{
			if ($ibforums->input['topic_type'] == $type)
			{
				$selected = ' selected';
			}
			else
			{
				$selected = '';
			}
			
			$select .= "<option value='$type'".$selected.">".$ibforums->lang['cp_pday_'.$type]."</option>";
		}
		
		$select .= "</select>\n";
		
		$forums = "<option value='prune'>{$ibforums->lang['cp_ac_prune']}</option>";
		
		$forums .= $std->build_forum_jump(0);
		
		
		if ($ibforums->input['df'])
		{
			$forums = preg_replace( "/<option value=\"".$ibforums->input['df']."\"/", "<option value=\"".$ibforums->input['df']."\" selected", $forums );
		}
		
		$this->output .= $this->html->prune_splash($this->forum, $forums, $select, $button, $confirm);
		
		if ($confirm_html)
		{
			$this->output = preg_replace( "/<!-- IBF\.CONFIRM -->/", "$confirm_html", $this->output );
		}
		
	}
	
	
	
	//--------------------------------------------------
	// Find a user to edit, dude.
	//--------------------------------------------------
	
	function find_user_one()
	{
		global $std, $ibforums, $DB, $print;
		
		$pass = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1)
		{
			$pass = 1;
		}
		else if ($this->moderator['edit_user'] == 1)
		{
			$pass = 1;
		}
		else
		{
			$pass = 0;
		}
		
		if ($pass == 0)
		{
			$this->mod_error('cp_no_perms');
			return;
		}
		
		$this->output .= $this->html->find_user();
	}
	
	function find_user_two()
	{
		global $std, $ibforums, $DB, $print;
		
		if ($ibforums->input['name'] == "")
		{
			$this->mod_error('cp_no_matches');
			return;
		}
		
		//---------------------------------
		// Query the DB for possible matches
		//---------------------------------
		
		$DB->query("SELECT id, name FROM ibf_members WHERE name LIKE '".$ibforums->input['name']."%' LIMIT 0,100");
		
		if ( $DB->get_num_rows() )
		{
			$select = "<select name='memberid' class='forminput'>";
			
			while ( $member = $DB->fetch_row() )
			{
				$select .= "\n<option value='".$member['id']."'>".$member['name']."</option>";
			}
			
			$select .= "</select>";
			
			$this->output .= $this->html->find_two($select);
		}
		else
		{
			$this->mod_error('cp_no_matches');
			return;
		}
	}
	
	function edit_user()
	{
		global $std, $ibforums, $DB, $print, $root_path;
		
		$pass = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1)
		{
			$pass = 1;
		}
		else if ($this->moderator['edit_user'] == 1)
		{
			$pass = 1;
		}
		else
		{
			$pass = 0;
		}
		
		if ($pass == 0)
		{
			$this->mod_error('cp_no_perms');
			return;
		}
		
		if ($ibforums->input['memberid'] == "")
		{
			$this->mod_error('cp_no_matches');
			return;
		}
		
		//--------------------------------------------------
		
		$DB->query("SELECT m.*, g.* FROM ibf_members m, ibf_groups g WHERE m.id='".$ibforums->input['memberid']."' AND m.mgroup=g.g_id");
		
		if (! $member = $DB->fetch_row() )
		{
			$this->mod_error('cp_no_matches');
			return;
		}
		
		//--------------------------------------------------
		// No editing of admins!
		//--------------------------------------------------
		
		if ($ibforums->member['g_access_cp'] != 1)
		{
			if ($member['g_access_cp'] == 1)
			{
				$this->mod_error('cp_admin_user');
				return;
			}
		}
		
		require $root_path."sources/lib/post_parser.php";
		
		$parser = new post_parser();
		
		$editable['signature'] = $parser->unconvert($member['signature']);
		$editable['location']  = $member['location'];
		$editable['interests'] = $member['interests'];
		$editable['website']   = $member['website'];
		$editable['id']        = $member['id'];
		$editable['name']      = $member['name'];
		
		$this->output .= $this->html->edit_user_form($editable);
	}
	
	//--------------------------------------------------
	
	function complete_user_edit()
	{
		global $std, $ibforums, $DB, $print, $root_path;
		
		$pass = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1)
		{
			$pass = 1;
		}
		else if ($this->moderator['edit_user'] == 1)
		{
			$pass = 1;
		}
		else
		{
			$pass = 0;
		}
		
		if ($pass == 0)
		{
			$this->mod_error('cp_no_perms');
			return;
		}
		
		if ($ibforums->input['memberid'] == "")
		{
			$this->mod_error('cp_no_matches');
			return;
		}
		
		//--------------------------------------------------
		
		$DB->query("SELECT m.*, g.* FROM ibf_members m, ibf_groups g WHERE m.id='".$ibforums->input['memberid']."' AND m.mgroup=g.g_id");
		
		if (! $member = $DB->fetch_row() )
		{
			$this->mod_error('cp_no_matches');
			return;
		}
		
		//--------------------------------------------------
		// No editing of admins!
		//--------------------------------------------------
		
		if ($ibforums->member['g_access_cp'] != 1)
		{
			if ($member['g_access_cp'] == 1)
			{
				$this->mod_error('cp_admin_user');
				return;
			}
		}
		
		require $root_path."sources/lib/post_parser.php";
		
		$parser = new post_parser();
		
		$ibforums->input['signature'] = $parser->convert(  array( 'TEXT'      => $ibforums->input['signature'],
																  'SMILIES'   => 0,
																  'CODE'      => $ibforums->vars['sig_allow_ibc'],
																  'HTML'      => 0,
																  'SIGNATURE' => 1
														)       );
									   
		if ($parser->error != "")
		{
			$std->Error( array( 'LEVEL' => 1, 'MSG' => $parser->error) );
		}
		
		
		$profile = array (
						   'signature'   => $ibforums->input['signature'],
						   'location'    => $ibforums->input['location'],
						   'interests'   => $ibforums->input['interests'],
						   'website'     => $ibforums->input['website']
						 );
		
		
		if ($ibforums->input['avatar'] == 1)
		{
			$profile['avatar']      = "";
			$profile['avatar_size'] = "";
		}
		
		$db_string = $DB->compile_db_update_string($profile);
		
		$DB->query("UPDATE ibf_members SET $db_string WHERE id='".$ibforums->input['memberid']."'");
		
		$this->moderate_log("Edited Profile for: {$member['name']}");
		
		$std->boink_it($ibforums->base_url."&act=ModCP&f={$ibforums->input['f']}&CODE=doedituser&memberid={$ibforums->input['memberid']}");
		exit();
	}

	
	//--------------------------------------------------
	// Show default ModCP screen
	//--------------------------------------------------
	
	function splash()
	{
		global $std, $ibforums, $DB, $print;
		
		// Get the counts for pending topics and posts and other assorted stuff etc and ok.
		
		$DB->query("SELECT COUNT(tid) as count FROM ibf_topics WHERE approved <> 1 and forum_id='".$this->forum['id']."'");
		$row = $DB->fetch_row();
		
		$tcount = $row['count'] ? $row['count'] : 0;
		
		//-------------------------------
		
		$DB->query("SELECT COUNT(pid) as pcount FROM ibf_posts WHERE queued=1 and new_topic <> 1 and forum_id='".$this->forum['id']."'");
		$row = $DB->fetch_row();
		
		$pcount = $row['pcount'] ? $row['pcount'] : 0;
		
		//-------------------------------
	
		$this->output .= $this->html->splash($tcount, $pcount, $this->forum['name']);
	}
	
	
	

	
	/*************************************************/
	
	function do_move() {
		global $std, $ibforums, $DB, $print;
		
		$pass = 0;
		
		if ($ibforums->member['g_is_supmod'] == 1)
		{
			$pass = 1;
		}
		else if ($this->moderator['mass_prune'] == 1)
		{
			$pass = 1;
		}
		else
		{
			$pass = 0;
		}
		
		if ($pass == 0)
		{
			$this->mod_error('cp_no_perms');
			return;
		}
		
		//-----------------------------------------------
		
		$db_query = "SELECT tid FROM ibf_topics WHERE approved=1 and forum_id='".$this->forum['id']."'";
		
		if ($ibforums->input['dateline'])
		{
			$db_query .= " AND last_post < {$ibforums->input['dateline']}";
		}
		
		if ($ibforums->input['starter'])
		{
			$db_query .= " AND starter_id='".$ibforums->input['starter']."'";
			
		}
		
		if ($ibforums->input['posts'])
		{
			$db_query .= " AND posts < '".$ibforums->input['posts']."'";
		}
		
		if ($ibforums->input['state'] != 'all')
		{
			if ($ibforums->input['state'])
			{
				$db_query .= " AND state='".$ibforums->input['state']."'";
			}
		}
		
		$DB->query($db_query);
		
		if ( ! $num_rows = $DB->get_num_rows() )
		{
			$this->mod_error('cp_error_no_topics'); 
			return;
		}
		
		$tid_array = array();
		
		while ($row = $DB->fetch_row())
		{
			$tid_array[] = $row['tid'];
		}
		
		//----------------------------------
		
		$tid    = implode( ",", $tid_array );
		$source = $this->forum['id'];
		$moveto = $ibforums->input['df'];
		
		//-----------------------------------
		// Check for an attempt to move into a subwrap forum
		//-----------------------------------
		
		$DB->query("SELECT subwrap, id FROM ibf_forums WHERE id='$moveto'");
		
		$f = $DB->fetch_row();
		
		if ($f['subwrap'] == 1)
		{
			$this->mod_error('cp_error_no_subforum');
			return;
		}
		
		
		//----------------------------------
		// Update the topic
		//----------------------------------
		
		$DB->query("UPDATE ibf_topics SET forum_id='$moveto' WHERE forum_id='$source' AND tid IN($tid)");
		
		//----------------------------------
		// Update the posts
		//----------------------------------
		
		$DB->query("UPDATE ibf_posts SET forum_id='$moveto' WHERE forum_id='$source' AND topic_id IN($tid)");
		
		//----------------------------------
		// Update the polls
		//----------------------------------
		
		$DB->query("UPDATE ibf_polls SET forum_id='$moveto' WHERE forum_id='$source' AND tid IN($tid)");
		
		$this->moderate_log("Mass moved topics");
		
		// Resync the forums..
		
		$this->recount($source);
		
		$this->recount($moveto);
	
		// Show results..
		
		$this->output .= $this->html->results( $ibforums->lang['cp_result_move'].$num_rows );
		
	}
	
	
	

	
//+---------------------------------------------------------------------------------------------
	
	
	/*************************************************/
	// MODERATE LOG:
	// ---------------
	//
	// Function for adding the mod action to the DB
	//
	/*************************************************/
	
	function moderate_log($title = 'unknown') {
		global $std, $ibforums, $DB, $HTTP_REFERER, $QUERY_STRING;
		
		$db_string = $std->compile_db_string( array (
														'forum_id'    => $ibforums->input['f'],
														'topic_id'    => $ibforums->input['t'],
														'post_id'     => $ibforums->input['p'],
														'member_id'   => $ibforums->member['id'],
														'member_name' => $ibforums->member['name'],
														'ip_address'  => $ibforums->input['IP_ADDRESS'],
														'http_referer'=> $HTTP_REFERER,
														'ctime'       => time(),
														'topic_title' => "<i>Via Moderators CP</i>",
														'action'      => $title,
														'query_string'=> $QUERY_STRING,
													)
										    );
		
		$DB->query("INSERT INTO ibf_moderator_logs (" .$db_string['FIELD_NAMES']. ") VALUES (". $db_string['FIELD_VALUES'] .")");
		
	}
	
	
	/*************************************************/
	// Re Count topics for the forums:
	// ---------------
	//
	// Handles simple moderation functions, saves on
	// writing the same code over and over.
	// ASS_U_ME's that the requesting user has been
	// authenticated by this stage.
	//
	/*************************************************/
	
	function recount($fid="") {
		global $ibforums, $root_path, $DB, $std;
		
		if ($fid != "")
		{
			$this->forum['id'] = $fid;
		}
		
		$DB->query("SELECT COUNT(tid) as tcount from ibf_topics WHERE approved=1");
		$topics = $DB->fetch_row();
		
		$DB->query("SELECT COUNT(pid) as pcount from ibf_posts WHERE queued <> 1");
		$posts  = $DB->fetch_row();
		
		$DB->query("SELECT COUNT(tid) as tcount from ibf_topics WHERE approved=1 and forum_id='".$this->forum['id']."'");
		$f_topics = $DB->fetch_row();
		$f_topics['tcount'] = $f_topics['tcount'] < 1 ? 0 : $f_topics['tcount'];
		
		$DB->query("SELECT COUNT(pid) as pcount from ibf_posts WHERE queued=0 and forum_id='".$this->forum['id']."'");
		$f_posts  = $DB->fetch_row();
		$f_posts['pcount'] = $f_posts['pcount'] < 1 ? 0 : $f_posts['pcount'];
		
		$this->forum['topics'] = $f_topics['tcount'];
		$this->forum['posts']  = $f_posts['pcount'] - $f_topics['tcount'];
		
		$DB->query("SELECT tid, title, last_poster_id, last_poster_name, last_post FROM ibf_topics WHERE approved=1 and forum_id='".$this->forum['id']."' ORDER BY last_post DESC LIMIT 0,1");
		$last_post = $DB->fetch_row();
		
		// Reset this forums stats
		
		$db_string = $DB->compile_db_update_string( array (
															 'last_poster_id'   => $last_post['last_poster_id'],
															 'last_poster_name' => $last_post['last_poster_name'],
															 'last_post'        => $last_post['last_post'],
															 'last_title'       => $last_post['title'],
															 'last_id'          => $last_post['tid'],
															 'topics'           => $this->forum['topics'],
															 'posts'            => $this->forum['posts']
												 )        );
		
		$DB->query("UPDATE ibf_forums SET $db_string WHERE id='".$this->forum['id']."'");
		
		// Update the main board stats.
		
		$DB->query("UPDATE ibf_stats SET TOTAL_TOPICS='".$topics['tcount']."', TOTAL_REPLIES='".$posts['pcount']."'");
		
	}
	
	
	
		
	/*************************************************/
	
	function mod_error($error)
	{
		global $std, $ibforums, $DB, $print;
		
		$error = $ibforums->lang[$error];
	
		$this->output .= $this->html->mod_error($error);
	}
	
	function parse_member($member=array()) {
		global $ibforums, $std, $DB;
		
		$member['name'] = "<a href='{$this->base_url}&act=Profile&CODE=03&MID={$member['id']}'>{$member['name']}</a>";
	
		$member['avatar'] = $std->get_avatar( $member['avatar'], $ibforums->member['view_avs'], $member['avatar_size'] );
		
		$member['MEMBER_JOINED'] = $ibforums->lang['m_joined'].' '.$std->get_date( $member['joined'], 'JOINED' );
		
		$member['MEMBER_GROUP'] = $ibforums->lang['m_group'].' '.$member['g_title'];
		
		$member['MEMBER_POSTS'] = $ibforums->lang['m_posts'].' '.$member['member_posts'];
		
		$member['PROFILE_ICON'] = "<a href='{$this->base_url}&act=Profile&CODE=03&MID={$member['id']}'>{$ibforums->skin['P_PROFILE']}</a>&nbsp;";
		
		$member['MESSAGE_ICON'] = "<a href='{$this->base_url}&act=Msg&CODE=04&MID={$member['id']}'>{$ibforums->skin['P_MSG']}</a>&nbsp;";
		
		if (!$member['hide_email']) {
			$member['EMAIL_ICON'] = "<a href='{$this->base_url}&act=Mail&CODE=00&MID={$member['id']}'>{$ibforums->skin['P_EMAIL']}</a>&nbsp;";
		}
		
		if ( $member['website'] and $member['website'] = preg_match( "/^http:\/\/\S+$/", $member['WEBSITE'] ) ) {
			$member['WEBSITE_ICON'] = "<a href='{$member['website']}' target='_blank'>{$ibforums->skin['P_WEBSITE']}</a>&nbsp;";
		}
		
		if ($member['icq_number']) {
			$member['ICQ_ICON'] = "<a href=\"javascript:PopUp('{$this->base_url}&act=ICQ&MID={$member['id']}','Pager','450','330','0','1','1','1')\">{$ibforums->skin[P_ICQ]}</a>&nbsp;";
		}
		
		if ($member['aim_name']) {
			$member['AOL_ICON'] = "<a href=\"javascript:PopUp('{$this->base_url}&act=AOL&MID={$member['id']}','Pager','450','330','0','1','1','1')\">{$ibforums->skin[P_AOL]}</a>&nbsp;";
		}
		
		//-----------------------------------------------------
		
		return $member;
	
	}

    
}

?>
