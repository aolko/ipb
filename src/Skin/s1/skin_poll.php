<?php

class skin_poll {



function edit_link($tid, $fid) {
global $ibforums;
return <<<EOF
[ <a href="{$ibforums->base_url}&act=Mod&CODE=20&f=$fid&t=$tid">{$ibforums->lang['ba_edit']}</a> ]
EOF;
}

function delete_link($tid, $fid) {
global $ibforums;
return <<<EOF
[ <a href="{$ibforums->base_url}&act=Mod&CODE=22&f=$fid&t=$tid">{$ibforums->lang['ba_delete']}</a> ]
EOF;
}

function Render_row_form($votes, $id, $answer) {
global $ibforums;
return <<<EOF
    <tr>
    <td id='row1' colspan='3'><INPUT type="radio" name="poll_vote" value="$id">&nbsp;<b>$answer</b></td>
    </tr>
EOF;
}

function ShowPoll_header($tid, $edit, $delete) {
global $ibforums;
return <<<EOF
    <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}" method="post">
    <input type='hidden' name='act' value='Poll'>
    <input type='hidden' name='s' value='{$ibforums->session_id}'>
    <input type='hidden' name='t'    value='$tid'>
    <br>
    <table cellpadding='0' cellspacing='0' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
        <tr>
            <td>
                <table cellpadding='5' cellspacing='1' border='0' width='100%'>
                <tr>
                <td align='right' colspan='3' id='titlemedium'>$edit &nbsp; $delete</td>
                </tr>
                <tr>
                <td id='row1' valign='middle' align='left' width='50%'><b>{$ibforums->lang['poll_s_choices']}</b></td>
                <td id='row1' valign='middle' align='left' width='10%'><b>{$ibforums->lang['poll_s_votes']}</b></td>
                <td id='row1' valign='middle' align='left' width='40%'><b>{$ibforums->lang['poll_s_stats']}</b></td>
EOF;
}

function ShowPoll_footer($vote_button) {
global $ibforums;
return <<<EOF
                <tr>
                <td id='row2' align='center' colspan='3'>
                $vote_button
                </td></tr></table>
                </td></tr></table>
                </form>
EOF;
}

function Render_row_results($votes, $id, $answer, $percentage, $width) {
global $ibforums;
return <<<EOF
    <tr>
    <td id='row1'><b>$answer</b></td>
    <td id='row1'><b>$votes</b></td>
    <td id='row1'><img src='{$ibforums->vars['img_url']}/bar_left.gif' border='0' width='4' height='11' align='middle' alt=''><img src='{$ibforums->vars['img_url']}/bar.gif' border='0' width='$width' height='11' align='middle' alt=''><img src='{$ibforums->vars['img_url']}/bar_right.gif' border='0' width='4' height='11' align='middle' alt=''>&nbsp;[$percentage%]</td>
    </tr>
EOF;
}


}
?>