<?php


/*
+--------------------------------------------------------------------------
|   IBFORUMS SAFE MODE INSTALL SCRIPT v0.1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001, 2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > Script written by Matthew Mecham
|   > Date started: 30th March 2002
|
+--------------------------------------------------------------------------
*/

error_reporting  (E_ERROR | E_WARNING | E_PARSE);
set_magic_quotes_runtime(0);

//+---------------------------------------
// ENTER YOUR PATH TO THE DIRECTORY THIS SCRIPT
// IS IN.
//
// Tips:
//
// If you are using Windows and Apache, do not
// use backslashes, use normal forward slashes.
// You may need to remove the drive letter also
// Example: C:\apache\htdocs\ibforums\ will need
// to be: /apache/htdocs/ibforums/
//
// If you are using Windows and IIS, then you will 
// need to enter double backslashes.
//
// In all cases, please enter a trailing slash (or
// trailing backslashes...)
//+---------------------------------------

$root = "./";



//+---------------------------------------

$template = new template;
$std      = new installer;

$VARS = $std->parse_incoming();

//+---------------------------------------
// What are we doing then? Eh? I'm talking to you!
//+---------------------------------------

if ( file_exists($root.'install.lock') )
{
	install_error("This installer is locked!<br>Please (via FTP) remove the 'install.lock' file in this directory");
	exit();
}


switch($VARS['a'])
{
	case '1':
		do_setup_form();
		break;
		
	case '2':
		do_install();
		break;
		
	case '3':
		do_finish();
		break;
		
	default:
		do_intro();
		break;
}

function do_finish()
{
	global $std, $template, $root, $VARS, $SQL;
	
	// Attempt to lock the install..
	
	if ($FH = fopen( $root.'install.lock', 'w' ) )
	{
		fwrite( $FH, 'bleh', 4 );
		fclose($FH);
		
		@chmod( $root.'install.lock', 0666 );
		
		$template->print_top('Success!');
	
		$template->contents .= "<tr>
								  <td id='subtitle'>&#149;&nbsp;Success!</td>
								<tr>
								<td>
								  <table cellpadding='8' cellspacing='0' width='100%' align='center' border='0' id='tablewrap'>
								  <tr>
									<td>
								  <table width='100%' cellspacing='1' cellpadding='0' align='center' border='0' id='table1'>
								   <tr>
									<td>
									 <table width='100%' cellspacing='2' cellpadding='3' align='center' border='0'>
									 <tr>
									   <td>
											<b>The installation is now complete!</b>
											<br><br>
											Although the installer is now locked (to re-install, remove the file 'install.lock'), for added
											security, please remove the sm_install.php program before continuing.
											<br><br>
											<center><b><a href='index.php?act=Login&CODE=00'>CLICK HERE TO LOG IN!</a></center>
										</td>
									 </tr>
									</table>
								  </td>
								 </tr>
								</table>
							   </td>
							  </tr>
							 </table>";
	}
	else
	{
	$template->print_top('Success!');

	$template->contents .= "<tr>
							  <td id='warning'>&#149;&nbsp;WARNING!</td>
							<tr>
							<td>
							  <table cellpadding='8' cellspacing='0' width='100%' align='center' border='0' id='tablewrap'>
							  <tr>
								<td>
							  <table width='100%' cellspacing='1' cellpadding='0' align='center' border='0' id='table1'>
							   <tr>
								<td>
								 <table width='100%' cellspacing='2' cellpadding='3' align='center' border='0'>
								 <tr>
								   <td>
										<b>The installation is now complete!</b>
										<br><br>
										PLEASE REMOVE THE INSTALLER ('sm_install.php') BEFORE CONTINUING!
										<br>
										Failure to do so will enable ANYONE to delete your board at any time!
										<br><br>
										<center><b><a href='index.php?act=Login&CODE=00'>CLICK HERE TO LOG IN!</a></center>
									</td>
								 </tr>
								</table>
							  </td>
							 </tr>
							</table>
						   </td>
						  </tr>
						 </table>";
	}
						 
	$template->output();
	
	
	
}

//+---------------------------------------


function do_install()
{
	global $std, $template, $root, $VARS, $HTTP_POST_VARS;
	
	// Ok, lets check for blankies...
	
	$NEW_INFO = array();
	
	$need = array('board_url','sql_host','sql_database','sql_user','adminname','adminpassword','adminpassword2','email');
	
	//-----------------------------------
	
	foreach($need as $greed)
	{
		if ($VARS[ $greed ] == "")
		{
			install_error("You must complete all of the form with the sole exception of 'SQL Table prefix'");
		}
	}
	
	//-----------------------------------
	
	$VARS['board_url'] = preg_replace( "#/$#", "", $VARS['board_url'] );
	
	if ($VARS['sql_tbl_prefix'] == "")
	{
		$VARS['sql_tbl_prefix'] = 'ibf_';
	}
	
	//-----------------------------------
	// Did the admin passy and passy2 match?
	//-----------------------------------
	
	if ($VARS['adminpassword2'] != $VARS['adminpassword'])
	{
		install_error("Your passwords did not match");
	}
	
	/*if ( ! preg_match( "!^http://!", $VARS['board_url'] ) )
	{
		install_error("The board URL must start with 'http://'");
	}*/
	
	//-----------------------------------
	// IMPORT $INFO!
	//-----------------------------------
	
	if ($root == './')
	{
		$root = str_replace( '\\', '/', getcwd() ) . '/';
	}
	
	$require = $root."conf_global.php";
	
	if ( ! file_exists($require) )
	{
		install_error("Could not locate '$require'. You may need to enter a value for the root path in this installer script, to do this, simply open up this script in a text editor and enter a value in \$root - remember to add a trailing slash. NT users will need to use double backslashes");
	}
	
	//@chmod( "conf_global.php", 0666 );
	
	include($require);
	
	//echo("here");
	//exit();
	
	if ( count($INFO) < 1 )
	{
		install_error("Possible corruption in 'conf_global.php' ({$VARS['base_dir']}conf_global.php), please re-upload in ASCII format");
	}
	
	//-----------------------------------
	// Attempt a DB connection..
	//-----------------------------------
	
	if ( ! $connect_id = mysql_connect( $VARS['sql_host'],$HTTP_POST_VARS['sql_user'],$HTTP_POST_VARS['sql_pass'] ) )
	{
		install_error("Could not create a mySQL connection, please check the SQL values entered");
	}
	
		
	if ( ! mysql_select_db($VARS['sql_database'], $connect_id) )
	{
		install_error("mySQL could not locate a database called '{$VARS['sql_database']}' please check the value entered for this");
	}
	
	//-----------------------------------
	// Attempt to write the config file.
	//-----------------------------------
	
    $new  = array( 'base_dir'       => $root,
				   'board_url'      => $VARS['board_url'],
				   'sql_host'       => $VARS['sql_host'],
				   'sql_database'   => $VARS['sql_database'],
				   'sql_user'       => $HTTP_POST_VARS['sql_user'],
				   'sql_pass'       => $HTTP_POST_VARS['sql_pass'],
				   'sql_tbl_prefix' => $VARS['sql_tbl_prefix'],
				   
				   'html_dir'       => $root."html/",
				   'html_url'       => $VARS['board_url']."/html",
				   'upload_dir'     => $root."uploads",
				   'upload_url'     => $VARS['board_url']."/uploads",
				   'email_in'       => $VARS['email'],
				   'email_out'      => $VARS['email'],
				   'ban_names'      => "",
				   'ban_email'      => "",
				   'ban_ip'         => "",
				   'force_login'    => 0,
				   'load_limit'     => "",
				   'board_start'    => time(),
				   'installed'      => 1,
				   'guests_ava'     => 1,
				   'guests_img'		=> 1,
				   'guests_sig'		=> 1,
				   'print_headers'  => 0,
				   'guest_name_pre' => "Guest_",
				 );
					 
	 foreach( $new as $k => $v )
	 {
		 // Update the old...
		 
		 $v = preg_replace( "/'/", "\\'" , $v );
		 $v = preg_replace( "/\r/", ""   , $v );
		 
		 $INFO[ $k ] = $v;
	 }
	 
	 $file_string = "<?php\n";
		
	 foreach( $INFO as $k => $v )
	 {
		 if ($k == 'skin' or $k == 'languages')
		 {
			continue;
		 }
		 $file_string .= '$INFO['."'".$k."'".']'."\t\t\t=\t'".$v."';\n";
	 }
	 
	 $file_string .= "\n".'?'.'>';   // Question mark + greater than together break syntax hi-lighting in BBEdit 6 :p
	 
	 if ( $fh = fopen( $require, 'w' ) )
	 {
		 fputs($fh, $file_string, strlen($file_string) );
		 fclose($fh);
	 }
	 else
	 {
		 install_error("Could not write to 'conf_global.php'");
	 }
	 
	 //-----------------------------------
	 // Lets populate the database!
	 //-----------------------------------
	 
	 $SQL = get_sql();
	 
	 foreach( $SQL as $q )
	 {
	 	if ($VARS['sql_tbl_prefix'] != "ibf_")
        {
           $q = preg_replace("/ibf_(\S+?)([\s\.,]|$)/", $VARS['sql_tbl_prefix']."\\1\\2", $q);
        }
        
        if ( preg_match("/CREATE TABLE (\S+) \(/", $q, $match) )
        {
        	if ($match[1])
        	{
        		$the_query = "DROP TABLE if exists ".$match[1];
        		if (! mysql_query($the_query, $connect_id) )
        		{
        			install_error("mySQL Error: ".mysql_error());
        		}
        	}
        }
        	
        if ( ! mysql_query($q, $connect_id) )
        {
        	install_error("mySQL Error: ".mysql_error());
        }
	}
	
	// Insert the admin...
	
	$passy = md5($VARS['adminpassword']);
	$time  = time();
	
	$query = "INSERT INTO ".$VARS['sql_tbl_prefix']."members (id, name, mgroup, password, email, joined, ip_address, posts, title, last_visit, last_activity) ".
		     "VALUES(1, '{$VARS['adminname']}', 4, '$passy', '{$VARS['email']}', '$time', '127.0.0.1', '0', 'Administrator', '$time', '$time')";
		     
	if ( ! mysql_query($query, $connect_id) )
	{
		install_error("mySQL Error: ".mysql_error());
		
	}
	
	// ARE WE DONE? REALLY? COOL!!
	
	$template->print_top('Success!');
	
	$template->contents .= "<tr>
							  <td id='subtitle'>&#149;&nbsp;Success!</td>
							<tr>
							<td>
							  <table cellpadding='8' cellspacing='0' width='100%' align='center' border='0' id='tablewrap'>
							  <tr>
								<td>
							  <table width='100%' cellspacing='1' cellpadding='0' align='center' border='0' id='table1'>
							   <tr>
								<td>
								 <table width='100%' cellspacing='2' cellpadding='3' align='center' border='0'>
								 <tr>
								   <td>
								   		<b>Your board has now been installed!</b>
								   		<br><br>
								   		The installation process is almost complete..
								   		<br><br>
								   		<center><b><a href='sm_install.php?a=3'>CLICK HERE TO COMPLETE THE INSTALL</a></center>
								   	</td>
								 </tr>
								</table>
							  </td>
							 </tr>
							</table>
						   </td>
						  </tr>
						 </table>";
						 
	$template->output();
	
}




function do_setup_form()
{
	global $std, $template, $root, $HTTP_SERVER_VARS;
	
	$template->print_top('Set Up form');
	
	//--------------------------------------------------
	
	$this_url = str_replace( "/sm_install.php", "", $HTTP_SERVER_VARS['HTTP_REFERER']);
	
	if ( ! $this_url )
	{
		$this_url = substr($HTTP_SERVER_VARS['SCRIPT_NAME'],0, -15);
		
    	if ($this_url == '')
    	{
    		$this_url == '/';
    	}
    	$this_url = 'http://'.$HTTP_SERVER_VARS['SERVER_NAME'].$this_url; 
    } 
	
	
	//--------------------------------------------------
	
	$template->contents .= "<tr>
							  <td id='subtitle'>&#149;&nbsp;Your Server Environment</td>
							<tr>
							<td>
							  <form action='sm_install.php' method='POST'>
							  <input type='hidden' name='a' value='2'>
							  <table cellpadding='8' cellspacing='0' width='100%' align='center' border='0' id='tablewrap'>
							  <tr>
								<td>
							  <table width='100%' cellspacing='1' cellpadding='0' align='center' border='0' id='table1'>
							   <tr>
								<td>
								 <table width='100%' cellspacing='2' cellpadding='3' align='center' border='0'>
								 <tr>
								   <td colspan='2' id='tdrow1'>
								   		This section requires you to enter the paths and URL's for the board. If in doubt, please
								   		check with your webhost before asking for support.
								   	</td>
								 </tr>
								 
								 <tr>
								   <td width='40%' id='tdrow1'><b>The script URL</b><br>This is the URL (must start with http://) to the directory that this script is in</td>
								   <td width='60%' id='tdrow2'><input type='text' id='textinput' name='board_url' value='$this_url'></td>
								 </tr>
								 
								</table>
							  </td>
							 </tr>
							</table>
						   </td>
						  </tr>
						 </table>
						 
						 </td>
						 </tr>
						 <tr>
							  <td id='subtitle'>&#149;&nbsp;Your SQL Environment</td>
							<tr>
							<td>
							  <table cellpadding='8' cellspacing='0' width='100%' align='center' border='0' id='tablewrap'>
							  <tr>
								<td>
							  <table width='100%' cellspacing='1' cellpadding='0' align='center' border='0' id='table1'>
							   <tr>
								<td>
								 <table width='100%' cellspacing='2' cellpadding='3' align='center' border='0'>
								 <tr>
								   <td colspan='2' id='tdrow1'>
								   		This section requires you to enter your SQL information. If in doubt, please
								   		check with your webhost before asking for support. You may choose to enter an existing database name,
								   		if not - you must create a new database before continuing.
								   	</td>
								 </tr>
								 
								 <tr>
								   <td width='40%' id='tdrow1'><b>SQL Host</b><br>(localhost is usually sufficient)</td>
								   <td width='60%' id='tdrow2'><input type='text' id='textinput' name='sql_host' value='localhost'></td>
								 </tr>
								 
								 <tr>
								   <td width='40%' id='tdrow1'><b>SQL Database Name</b></td>
								   <td width='60%' id='tdrow2'><input type='text' id='textinput' name='sql_database' value=''></td>
								 </tr>
								 
								 <tr>
								   <td width='40%' id='tdrow1'><b>SQL Username</b></td>
								   <td width='60%' id='tdrow2'><input type='text' id='textinput' name='sql_user' value=''></td>
								 </tr>
								 
								 <tr>
								   <td width='40%' id='tdrow1'><b>SQL Password</b></td>
								   <td width='60%' id='tdrow2'><input type='text' id='textinput' name='sql_pass' value=''></td>
								 </tr>
								 
								 <tr>
								   <td width='40%' id='tdrow1'><b>SQL Table Prefix</b>(You can leave this blank)</td>
								   <td width='60%' id='tdrow2'><input type='text' id='textinput' name='sql_tbl_prefix' value=''></td>
								 </tr>
								 
								</table>
							  </td>
							 </tr>
							</table>
						   </td>
						  </tr>
						 </table>
						 
						 <tr>
							  <td id='subtitle'>&#149;&nbsp;Your Admin Account</td>
							<tr>
							<td>
							  <table cellpadding='8' cellspacing='0' width='100%' align='center' border='0' id='tablewrap'>
							  <tr>
								<td>
							  <table width='100%' cellspacing='1' cellpadding='0' align='center' border='0' id='table1'>
							   <tr>
								<td>
								 <table width='100%' cellspacing='2' cellpadding='3' align='center' border='0'>
								 <tr>
								   <td colspan='2' id='tdrow1'>
								   		This section requires information to create your administration account. Please
								   		enter the data carefully!
								   	</td>
								 </tr>
								 
								 <tr>
								   <td width='40%' id='tdrow1'><b>Username</b></td>
								   <td width='60%' id='tdrow2'><input type='text' id='textinput' name='adminname' value=''></td>
								 </tr>
								 
								 <tr>
								   <td width='40%' id='tdrow1'><b>Password</b></td>
								   <td width='60%' id='tdrow2'><input type='password' id='textinput' name='adminpassword' value=''></td>
								 </tr>
								 
								 <tr>
								   <td width='40%' id='tdrow1'><b>Retype your password</b></td>
								   <td width='60%' id='tdrow2'><input type='password' id='textinput' name='adminpassword2' value=''></td>
								 </tr>
								 
								 <tr>
								   <td width='40%' id='tdrow1'><b>Email Address</b></td>
								   <td width='60%' id='tdrow2'><input type='text' id='textinput' name='email' value=''></td>
								 </tr>
								 
								 <tr>
								 	<td colspan='2' id='tdrow1' align='center'><input type='submit' value='Process'></td>
								 </tr>
								</table>
							  </td>
							 </tr>
							</table>
						   </td>
						  </tr>
						 </table>
						 </form>
						 </td>
						 </tr>
						 
						 ";
						 
	$template->output();
						 
}

//+---------------------------------------

function do_intro()
{
	global $std, $template, $root;
	
	$template->print_top('Welcome');
	
	$template->contents .= "<tr>
							  <td id='subtitle'>&#149;&nbsp;Welcome!</td>
							<tr>
							<td>
							  <table cellpadding='8' cellspacing='0' width='100%' align='center' border='0' id='tablewrap'>
							  <tr>
								<td>
							  <table width='100%' cellspacing='1' cellpadding='0' align='center' border='0' id='table1'>
							   <tr>
								<td>
								 <table width='100%' cellspacing='2' cellpadding='3' align='center' border='0'>
								 <tr>
								   <td>
								   		<b>Welcome to the Invision Board Safe Mode Installer</b>
								   		<br><br>
								   		Before we go any further, please ensure that all the files have been uploaded, and that the 
								   		file 'conf_global.php' has suitable permissions to allow this script to write to it ( 0666 should be sufficient ).
								   		<br><br>
								   		You will also need your SQL database name, your SQL username, your SQL password and SQL host (usually localhost).
								   		<br><br>
								   		Once you have clicked on proceed, you will be taken to a form to enter information the installer needs to set up your board.
								   		<br><br>
								   		<b>PLEASE NOTE: USING THIS INSTALLER WILL DELETE ANY CURRENT INVISION BOARD DATABASE IF YOU ARE USING THE SAME TABLE PREFIX</b>
								   	</td>
								 </tr>
								</table>
							  </td>
							 </tr>
							</table>
						   </td>
						  </tr>
						 </table>";
						 
	// Check to make sure that the config file is there and it's got suitable permissions to write to:
	
	$file = $root."conf_global.php";
	
	$warnings = array();
	
	if ( ! file_exists($file) )
	{
		$warnings[] = "Cannot locate the file 'conf_global.php'. This should be uploaded into the same directory as this script!";
	}
	
	if ( ! is_writeable($file) )
	{
		$warnings[] = "Cannot write to 'conf_global.php'. Please adjust the permissions to allow this script to write to the file. if in doubt, CHMOD via FTP to 0777";
	}
	
	$phpversion = phpversion();
	
	if ($phpversion < '4.0.0') {
		$warnings[] = "You cannot install Invision Board. Invision Board requires PHP Version 4.0.0 or better.";
	}
	
	if ( count($warnings) > 0 )
	{
	
		$err_string = "<ul><li>".implode( "<li>", $warnings )."</ul>";
	
		$template->contents .= "<tr>
							  <td id='warning'>&#149;&nbsp;WARNING!</td>
							<tr>
							<td>
							  <table cellpadding='8' cellspacing='0' width='100%' align='center' border='0' id='tablewrap'>
							  <tr>
								<td>
							  <table width='100%' cellspacing='1' cellpadding='0' align='center' border='0' id='table1'>
							   <tr>
								<td>
								 <table width='100%' cellspacing='2' cellpadding='3' align='center' border='0'>
								 <tr>
								   <td>
								   		<b>The following errors must be rectified before continuing!</b>
								   		<br><br>
								   		$err_string
								   	</td>
								 </tr>
								</table>
							  </td>
							 </tr>
							</table>
						   </td>
						  </tr>
						 </table>";
	}
	else
	{
		$template->contents .= "<tr><td align='center' style='font-size:18px'><br><b><a href='sm_install.php?a=1'>Proceed</a> &gt;&gt;</b></td></tr>";
	}
	
	
	$template->output();
}



function install_error($msg="")
{
	global $std, $template, $root;
	
	$template->print_top('Warning!');
	

	
	$template->contents .= "<tr>
						  <td id='warning'>&#149;&nbsp;WARNING!</td>
						<tr>
						<td>
						  <table cellpadding='8' cellspacing='0' width='100%' align='center' border='0' id='tablewrap'>
						  <tr>
							<td>
						  <table width='100%' cellspacing='1' cellpadding='0' align='center' border='0' id='table1'>
						   <tr>
							<td>
							 <table width='100%' cellspacing='2' cellpadding='3' align='center' border='0'>
							 <tr>
							   <td>
									<b>The following errors must be rectified before continuing!</b><br>Please go back and try again!
									<br><br>
									$msg
								</td>
							 </tr>
							</table>
						  </td>
						 </tr>
						</table>
					   </td>
					  </tr>
					 </table>";
	
	
	
	$template->output();
}

//+--------------------------------------------------------------------------
// CLASSES
//+--------------------------------------------------------------------------



class template
{
	var $contents = "";
	
	function output()
	{
		echo $this->contents;
		echo "   
				 </table>
				 <br><br><center><span id='copy'>&copy 2002 Invision Board (www.invisionboard.com)</span></center>
				 
				 </body>
				 </html>";
		exit();
	}
	
	//--------------------------------------

	function print_top($title="")
	{
	
		$this->contents = "<html>
		          <head><title>Invision Board Set Up :: $title </title>
		          <style type='text/css'>
		          	TABLE, TR, TD     { font-family:Verdana, Arial;font-size: 11px; color:#333333 }
					BODY      { font: 11px Verdana; color:#333333 }
					a:link, a:visited, a:active  { color:#000055 }
					a:hover                      { color:#333377;text-decoration:underline }
					
					#title  { font-size:10px; font-weight:bold; line-height:150%; color:#FFFFFF; height: 24px; background-image: url(html/sys-img/top_cell.gif); }
					#title  a:link, #title  a:visited, #title  a:active { text-decoration: underline; color : #FFFFFF; font-size:11px }
					
					#detail { font-family: Arial; font-size:11px; color: #333333 }
					
 					#large { font-family: verdana, arial; font-size:20px; color:#4C77B6; font-weight:bold; letter-spacing:-1px }
 					
					#subtitle { font-family: Verdana; font-size:22px; color:#4C77B6; font-weight:bold }
					
					#warning { font-family: Verdana; font-size:22px; color:#FF0000; font-weight:bold }
					
					#table1 {  background-color:#F1F1F1; width:100%; align:center; border:1px solid black }
					
					#tdrow1 { background-color:#F3F3EE }
					
					#tdrow2 { background-color:#EBEBE4 }
					
					#catrow  { font-size:10px; font-weight:bold; line-height:150%; color:#4C77B6; background-color:#C2CFDF; }
					
					#tablewrap {  border:1px dashed #777777; background-color:#EFEFEF }
					
					#copy { color:#555555; font-size:9px }
					
					#tdtop  { font-weight:bold; height:20px; line-height:150%; color:#FFFFFF; background-image: url(html/sys-img/top_cell.gif); }
					
					#green    { background-color: #caf2d9 }
					#red      { background-color: #f5cdcd }
					
					#button   { background-color: #4C77B6; color: #FFFFFF; font-family:Verdana, Arial; font-size:11px }
					
					#textinput { background-color: #EEEEEE; color:�#000000; font-family:Verdana, Arial; font-size:10px; width:100% }
					
					#dropdown { background-color: #EEEEEE; color:�#000000; font-family:Verdana, Arial; font-size:10px }
					
					#multitext { background-color: #EEEEEE; color:�#000000; font-family:Courier, Verdana, Arial; font-size:10px }
					
				  </style>
				  </head>
				 <body marginheight='0' marginwidth='0' leftmargin='0' topmargin='0' bgcolor='#FFFFFF'>
				 
				 <table width='100%' height='70' cellpadding='0' cellspacing='0' border='0'>
					<tr bgcolor='#4C77B6'>
						<td width='370' align='left' bgcolor='#4C77B6'><img src='html/sys-img/title.gif' width='370' height='70'></td>
					</tr>
				</table>
				<br>
				<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center'>
				 ";
				  	   
	}


}


class installer
{

	function parse_incoming()
    {
    	global $HTTP_GET_VARS, $HTTP_POST_VARS, $HTTP_CLIENT_IP, $REQUEST_METHOD, $REMOTE_ADDR, $HTTP_PROXY_USER, $HTTP_X_FORWARDED_FOR;
    	$return = array();
    	
		if( is_array($HTTP_GET_VARS) )
		{
			while( list($k, $v) = each($HTTP_GET_VARS) )
			{
				//$k = $this->clean_key($k);
				if( is_array($HTTP_GET_VARS[$k]) )
				{
					while( list($k2, $v2) = each($HTTP_GET_VARS[$k]) )
					{
						$return[$k][ $this->clean_key($k2) ] = $this->clean_value($v2);
					}
				}
				else
				{
					$return[$k] = $this->clean_value($v);
				}
			}
		}
		
		// Overwrite GET data with post data
		
		if( is_array($HTTP_POST_VARS) )
		{
			while( list($k, $v) = each($HTTP_POST_VARS) )
			{
				//$k = $this->clean_key($k);
				if ( is_array($HTTP_POST_VARS[$k]) )
				{
					while( list($k2, $v2) = each($HTTP_POST_VARS[$k]) )
					{
						$return[$k][ $this->clean_key($k2) ] = $this->clean_value($v2);
					}
				}
				else
				{
					$return[$k] = $this->clean_value($v);
				}
			}
		}
		
		return $return;
	}
    
    function clean_key($key) {
    
    	if ($key == "")
    	{
    		return "";
    	}
    	
    	$key = preg_replace( "/\.\./"           , ""  , $key );
    	$key = preg_replace( "/\_\_(.+?)\_\_/"  , ""  , $key );
    	$key = preg_replace( "/^([\w\.\-\_]+)$/", "$1", $key );
    	return $key;
    }
    
    function clean_value($val) {
    
    	if ($val == "")
    	{
    		return "";
    	}
    	
    	$val = preg_replace( "/&/"         , "&amp;"         , $val );
    	$val = preg_replace( "/<!--/"      , "&#60;&#33;--"  , $val );
    	$val = preg_replace( "/-->/"       , "--&#62;"       , $val );
    	$val = preg_replace( "/<script/i"  , "&#60;script"   , $val );
    	$val = preg_replace( "/>/"         , "&gt;"          , $val );
    	$val = preg_replace( "/</"         , "&lt;"          , $val );
    	$val = preg_replace( "/\"/"        , "&quot;"        , $val );
    	$val = preg_replace( "/\|/"        , "&#124;"        , $val );
    	$val = preg_replace( "/\n/"        , "<br>"          , $val ); // Convert literal newlines
    	$val = preg_replace( "/\\\$/"      , "&#036;"        , $val );
    	$val = preg_replace( "/\r/"        , ""              , $val ); // Remove literal carriage returns
    	$val = preg_replace( "/!/"         , "&#33;"         , $val );
    	$val = preg_replace( "/'/"         , "&#39;"         , $val ); // IMPORTANT: It helps to increase sql query safety.
    	$val = stripslashes($val);                                     // Swop PHP added backslashes
    	$val = preg_replace( "/\\\/"       , "&#092;"        , $val ); // Swop user inputted backslashes
    	return $val;
    }
   
}



// DATA AND STUFF, ETC

function get_sql()
{

$SQL = array();

$SQL[] = "CREATE TABLE ibf_admin_sessions (
  ID varchar(32) NOT NULL default '',
  IP_ADDRESS varchar(32) NOT NULL default '',
  MEMBER_NAME varchar(32) NOT NULL default '',
  MEMBER_ID varchar(32) NOT NULL default '',
  SESSION_KEY varchar(32) NOT NULL default '',
  LOCATION varchar(64) default 'index',
  LOG_IN_TIME int(10) NOT NULL default '0',
  RUNNING_TIME int(10) NOT NULL default '0',
  PRIMARY KEY  (ID)
)";


$SQL[] = "CREATE TABLE ibf_attachments (
  id bigint(20) NOT NULL auto_increment,
  mime_type varchar(128) default NULL,
  file_name varchar(64) default NULL,
  PRIMARY KEY  (id)
)";


$SQL[] = "CREATE TABLE ibf_badwords (
  wid int(3) NOT NULL auto_increment,
  type varchar(250) NOT NULL default '',
  swop varchar(250) default NULL,
  m_exact tinyint(1) default '0',
  PRIMARY KEY  (wid)
)";


$SQL[] = "CREATE TABLE ibf_categories (
  id tinyint(3) NOT NULL default '0',
  position tinyint(3) default NULL,
  state varchar(10) default NULL,
  name varchar(128) NOT NULL default '',
  description text,
  image varchar(128) default NULL,
  url varchar(128) default NULL,
  PRIMARY KEY  (id)
)";



$SQL[] = "CREATE TABLE ibf_contacts (
  id bigint(20) NOT NULL auto_increment,
  contact_id varchar(32) NOT NULL default '',
  member_id varchar(32) NOT NULL default '',
  contact_name varchar(32) NOT NULL default '',
  allow_msg tinyint(1) default NULL,
  contact_desc varchar(50) default NULL,
  PRIMARY KEY  (id)
)";



$SQL[] = "CREATE TABLE ibf_css (
  cssid int(10) NOT NULL auto_increment,
  css_name varchar(128) NOT NULL default '',
  css_text text,
  PRIMARY KEY  (cssid)
)";



$SQL[] = "CREATE TABLE ibf_emoticons (
  id smallint(3) NOT NULL auto_increment,
  typed varchar(32) NOT NULL default '',
  image varchar(128) NOT NULL default '',
  clickable smallint(2) NOT NULL default '1',
  PRIMARY KEY  (id)
)";



$SQL[] = "CREATE TABLE ibf_faq (
  id bigint(20) NOT NULL auto_increment,
  title varchar(128) NOT NULL default '',
  text text,
  description text NOT NULL,
  PRIMARY KEY  (id)
)";



$SQL[] = "CREATE TABLE ibf_forums (
  id smallint(5) NOT NULL default '0',
  topics mediumint(6) default NULL,
  posts mediumint(6) default NULL,
  last_post int(10) default NULL,
  last_poster_id varchar(32) default NULL,
  last_poster_name varchar(32) default NULL,
  name varchar(128) NOT NULL default '',
  description text,
  position tinyint(2) default NULL,
  use_ibc tinyint(1) default NULL,
  use_html tinyint(1) default NULL,
  status varchar(10) default NULL,
  start_perms varchar(32) default NULL,
  reply_perms varchar(32) default NULL,
  read_perms varchar(32) default NULL,
  password varchar(32) default NULL,
  category tinyint(2) NOT NULL default '0',
  last_title varchar(128) default NULL,
  last_id smallint(5) default NULL,
  sort_key varchar(32) default NULL,
  sort_order varchar(32) default NULL,
  prune tinyint(3) default NULL,
  show_rules tinyint(1) default NULL,
  use_attach tinyint(1) default NULL,
  preview_posts tinyint(1) default NULL,
  allow_poll tinyint(1) NOT NULL default '1',
  allow_pollbump tinyint(1) NOT NULL default '0',
  inc_postcount tinyint(1) NOT NULL default '1',
  skin_id int(10) default NULL,
  parent_id mediumint(5) default '-1',
  subwrap tinyint(1) default '0',
  PRIMARY KEY  (id),
  KEY category (category)
)";



$SQL[] = "CREATE TABLE ibf_groups (
  g_id int(3) unsigned NOT NULL auto_increment,
  g_view_board tinyint(1) default NULL,
  g_mem_info tinyint(1) default NULL,
  g_other_topics tinyint(1) default NULL,
  g_use_search tinyint(1) default NULL,
  g_email_friend tinyint(1) default NULL,
  g_invite_friend tinyint(1) default NULL,
  g_edit_profile tinyint(1) default NULL,
  g_post_new_topics tinyint(1) default NULL,
  g_reply_own_topics tinyint(1) default NULL,
  g_reply_other_topics tinyint(1) default NULL,
  g_edit_posts tinyint(1) default NULL,
  g_delete_own_posts tinyint(1) default NULL,
  g_open_close_posts tinyint(1) default NULL,
  g_delete_own_topics tinyint(1) default NULL,
  g_post_polls tinyint(1) default NULL,
  g_vote_polls tinyint(1) default NULL,
  g_use_pm tinyint(1) default NULL,
  g_is_supmod tinyint(1) default NULL,
  g_access_cp tinyint(1) default NULL,
  g_title varchar(32) NOT NULL default '',
  g_can_remove tinyint(1) default NULL,
  g_append_edit tinyint(1) default NULL,
  g_access_offline tinyint(1) default NULL,
  g_avoid_q tinyint(1) default NULL,
  g_avoid_flood tinyint(1) default NULL,
  g_icon varchar(64) default NULL,
  g_attach_max bigint(20) default NULL,
  g_avatar_upload tinyint(1) default '0',
  g_calendar_post tinyint(1) default '0',
  prefix varchar(250) default NULL,
  suffix varchar(250) default NULL,
  PRIMARY KEY  (g_id)
)";



$SQL[] = "CREATE TABLE ibf_images (
  imid int(10) NOT NULL auto_increment,
  setname varchar(125) NOT NULL default '',
  A_LOCKED_B varchar(250) default NULL,
  A_MOVED_B varchar(250) default NULL,
  A_POLL varchar(250) default NULL,
  A_POLLONLY_B varchar(250) default NULL,
  A_POST varchar(250) default NULL,
  A_REPLY varchar(250) default NULL,
  A_STAR varchar(250) default NULL,
  B_HOT varchar(250) default NULL,
  B_HOT_NN varchar(250) default NULL,
  B_LOCKED varchar(250) default NULL,
  B_MOVED varchar(250) default NULL,
  B_NEW varchar(250) default NULL,
  B_NORM varchar(250) default NULL,
  B_PIN varchar(250) default NULL,
  B_POLL varchar(250) default NULL,
  B_POLL_NN varchar(250) default NULL,
  C_LOCKED varchar(250) default NULL,
  C_OFF varchar(250) default NULL,
  C_OFF_CAT varchar(250) default NULL,
  C_OFF_RES varchar(250) default NULL,
  C_ON varchar(250) default NULL,
  C_ON_CAT varchar(250) default NULL,
  C_ON_RES varchar(250) default NULL,
  F_ACTIVE varchar(250) default NULL,
  F_NAV_SEP varchar(250) default NULL,
  F_NAV varchar(250) default NULL,
  F_STATS varchar(250) default NULL,
  GO_LAST_ON varchar(250) default NULL,
  GO_LAST_OFF varchar(250) default NULL,
  M_ADDMEM varchar(250) default NULL,
  M_DELETE varchar(250) default NULL,
  M_READ varchar(250) default NULL,
  M_REPLY varchar(250) default NULL,
  M_UNREAD varchar(250) default NULL,
  P_AOL varchar(250) default NULL,
  P_DELETE varchar(250) default NULL,
  P_EDIT varchar(250) default NULL,
  P_EMAIL varchar(250) default NULL,
  P_ICQ varchar(250) default NULL,
  P_MSG varchar(250) default NULL,
  P_PROFILE varchar(250) default NULL,
  P_QUOTE varchar(250) default NULL,
  P_WEBSITE varchar(250) default NULL,
  CAT_IMG varchar(250) default NULL,
  B_HOT_DOT varchar(250) default NULL,
  B_NEW_DOT varchar(250) default NULL,
  B_HOT_NN_DOT varchar(250) default NULL,
  B_NORM_DOT varchar(250) default NULL,
  B_POLL_DOT varchar(250) default NULL,
  B_POLL_NN_DOT varchar(250) default NULL,
  NEW_POST varchar(250) default NULL,
  PRIMARY KEY  (imid)
)";



$SQL[] = "CREATE TABLE ibf_member_extra (
  id bigint(20) NOT NULL default '0',
  notes text,
  links text,
  bio text,
  ta_size char(3) default NULL,
  PRIMARY KEY  (id)
)";



$SQL[] = "CREATE TABLE ibf_members (
  id bigint(10) NOT NULL default '0',
  name varchar(32) NOT NULL default '',
  mgroup tinyint(2) NOT NULL default '0',
  password varchar(32) NOT NULL default '',
  email varchar(60) NOT NULL default '',
  joined int(10) NOT NULL default '0',
  ip_address varchar(16) NOT NULL default '',
  avatar varchar(128) default NULL,
  avatar_size varchar(9) default NULL,
  posts mediumint(7) default '0',
  photo varchar(70) default NULL,
  aim_name varchar(40) default NULL,
  icq_number varchar(40) default NULL,
  location varchar(128) default NULL,
  signature text,
  website varchar(70) default NULL,
  yahoo varchar(32) default NULL,
  title varchar(64) default NULL,
  allow_admin_mails tinyint(1) default NULL,
  time_offset varchar(10) default NULL,
  interests text,
  hide_email varchar(8) default NULL,
  email_pm tinyint(1) default NULL,
  email_full tinyint(1) default NULL,
  skin smallint(5) default NULL,
  warn_level int(10) default NULL,
  language varchar(32) default NULL,
  msnname varchar(64) default NULL,
  last_post int(10) default NULL,
  allow_post tinyint(1) default '1',
  view_sigs tinyint(1) default '1',
  view_img tinyint(1) default '1',
  view_avs tinyint(1) default '1',
  view_pop tinyint(1) default '1',
  bday_day int(2) default NULL,
  bday_month int(2) default NULL,
  bday_year int(4) default NULL,
  new_msg tinyint(2) default NULL,
  msg_from_id varchar(32) default NULL,
  msg_msg_id int(10) default NULL,
  msg_total smallint(5) default NULL,
  vdirs text,
  show_popup tinyint(1) default NULL,
  validate_key varchar(32) default NULL,
  prev_group smallint(3) default '0',
  new_pass varchar(32) default NULL,
  misc varchar(128) default NULL,
  last_visit int(10) default '0',
  last_activity int(10) default '0',
  dst_in_use TINYINT (1) DEFAULT '0',
  PRIMARY KEY  (id),
  KEY name (name),
  KEY mgroup (mgroup)
)";


$SQL[] = "CREATE TABLE ibf_messages (
  msg_id bigint(20) NOT NULL auto_increment,
  msg_date int(10) default NULL,
  read_state tinyint(1) default NULL,
  title varchar(128) default NULL,
  message text,
  from_id varchar(32) default NULL,
  vid varchar(32) default NULL,
  member_id varchar(32) NOT NULL default '0',
  recipient_id varchar(32) default NULL,
  attach_type tinyint(128) default NULL,
  attach_file tinyint(128) default NULL,
  PRIMARY KEY  (msg_id),
  KEY member_id (member_id)
)";



$SQL[] = "CREATE TABLE ibf_moderator_logs (
  id bigint(20) NOT NULL auto_increment,
  forum_id int(5) default '0',
  topic_id bigint(20) default '0',
  post_id bigint(20) default '0',
  member_id varchar(32) NOT NULL default '',
  member_name varchar(32) NOT NULL default '',
  ip_address varchar(32) default NULL,
  http_referer varchar(255) default NULL,
  ctime int(10) default NULL,
  topic_title varchar(128) default NULL,
  action varchar(128) default NULL,
  query_string varchar(128) default NULL,
  PRIMARY KEY  (id)
)";


$SQL[] = "CREATE TABLE ibf_moderators (
  mid bigint(20) NOT NULL auto_increment,
  forum_id int(5) NOT NULL default '0',
  member_name varchar(32) NOT NULL default '',
  member_id varchar(32) NOT NULL default '0',
  edit_post tinyint(1) default NULL,
  edit_topic tinyint(1) default NULL,
  delete_post tinyint(1) default NULL,
  delete_topic tinyint(1) default NULL,
  view_ip tinyint(1) default NULL,
  open_topic tinyint(1) default NULL,
  close_topic tinyint(1) default NULL,
  mass_move tinyint(1) default NULL,
  mass_prune tinyint(1) default NULL,
  move_topic tinyint(1) default NULL,
  pin_topic tinyint(1) default NULL,
  unpin_topic tinyint(1) default NULL,
  post_q tinyint(1) default NULL,
  topic_q tinyint(1) default NULL,
  allow_warn tinyint(1) default NULL,
  edit_user tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (mid)
)";



$SQL[] = "CREATE TABLE ibf_polls (
  pid bigint(20) NOT NULL auto_increment,
  tid bigint(20) NOT NULL default '0',
  start_date int(10) default NULL,
  choices text,
  starter_id varchar(32) default NULL,
  votes bigint(20) default NULL,
  forum_id bigint(20) default NULL,
  PRIMARY KEY  (pid)
)";



$SQL[] = "CREATE TABLE ibf_posts (
  pid bigint(20) NOT NULL auto_increment,
  author_id int(10) NOT NULL default '0',
  author_name varchar(32) default NULL,
  use_sig varchar(8) default NULL,
  use_emo varchar(8) default NULL,
  ip_address varchar(32) default NULL,
  post_date int(10) default NULL,
  icon_id smallint(3) default NULL,
  post text,
  queued tinyint(1) default NULL,
  topic_id bigint(20) NOT NULL,
  forum_id int(10) NOT NULL,
  attach_id varchar(64) default NULL,
  attach_hits int(10) default NULL,
  attach_type varchar(128) default NULL,
  attach_file varchar(255) default NULL,
  post_title varchar(255) default NULL,
  new_topic tinyint(1) default '0',
  PRIMARY KEY  (pid),
  KEY forum_id (forum_id),
  KEY topic_id (topic_id,author_id),
  KEY author_id (author_id)
)";



$SQL[] = "CREATE TABLE ibf_rules (
  fid mediumint(6) NOT NULL default '0',
  title varchar(128) NOT NULL default '',
  body text,
  updated int(10) default NULL,
  show_all tinyint(1) default NULL,
  PRIMARY KEY  (fid)
)";


$SQL[] = "CREATE TABLE ibf_search_results (
  id varchar(32) NOT NULL default '',
  topics text NOT NULL,
  search_date int(12) NOT NULL default '0',
  max_hits int(3) NOT NULL default '0',
  sort_key varchar(32) NOT NULL default 'last_post',
  sort_order varchar(4) NOT NULL default 'desc'
)";


$SQL[] = "CREATE TABLE ibf_sessions (
  id varchar(32) NOT NULL default '0',
  member_name varchar(64) default NULL,
  member_pass varchar(32) default NULL,
  member_id varchar(32) default NULL,
  ip_address varchar(16) default NULL,
  start_session int(10) default NULL,
  browser varchar(64) default NULL,
  running_time int(10) default NULL,
  login_type tinyint(1) default NULL,
  location varchar(40) default NULL,
  member_group smallint(3) default NULL,
  PRIMARY KEY  (id)
)";


$SQL[] = "CREATE TABLE ibf_skins (
  uid int(10) NOT NULL auto_increment,
  sname varchar(100) NOT NULL default '',
  sid int(10) NOT NULL default '0',
  set_id int(5) NOT NULL default '0',
  tmpl_id int(10) NOT NULL default '0',
  img_id int(10) NOT NULL default '1',
  css_id int(10) NOT NULL default '1',
  tbl_width varchar(250) default NULL,
  tbl_border varchar(250) default NULL,
  hidden tinyint(1) NOT NULL default '0',
  default_set tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (uid)
)";



$SQL[] = "CREATE TABLE ibf_stats (
  TOTAL_REPLIES bigint(20) NOT NULL default '0',
  TOTAL_TOPICS bigint(20) NOT NULL default '0',
  LAST_MEM_NAME varchar(32) default NULL,
  LAST_MEM_ID varchar(32) default NULL,
  MOST_DATE int(10) default NULL,
  MOST_COUNT int(10) default '0',
  MEM_COUNT bigint(20) default '0'
)";



$SQL[] = "CREATE TABLE ibf_templates (
  tmid int(10) NOT NULL auto_increment,
  template mediumtext,
  name varchar(128) default NULL,
  PRIMARY KEY  (tmid)
)";


$SQL[] = "CREATE TABLE ibf_titles (
  id smallint(5) NOT NULL auto_increment,
  posts int(10) default NULL,
  title varchar(128) default NULL,
  pips varchar(128) default NULL,
  PRIMARY KEY  (id)
)";


$SQL[] = "CREATE TABLE ibf_tmpl_names (
  skid int(10) NOT NULL auto_increment,
  skname varchar(60) NOT NULL default 'Invision Board',
  author varchar(250) default '',
  email varchar(250) default '',
  url varchar(250) default '',
  PRIMARY KEY  (skid)
)";



$SQL[] = "CREATE TABLE ibf_topics (
  tid bigint(20) NOT NULL auto_increment,
  title varchar(70) NOT NULL default '',
  description varchar(70) default NULL,
  state varchar(8) default NULL,
  posts smallint(4) default NULL,
  starter_id varchar(32) default NULL,
  start_date int(10) default NULL,
  last_poster_id varchar(32) default NULL,
  last_post int(10) default NULL,
  icon_id tinyint(2) default NULL,
  starter_name varchar(32) default NULL,
  last_poster_name varchar(32) default NULL,
  poll_state varchar(8) default NULL,
  last_vote int(10) default NULL,
  views smallint(5) default NULL,
  forum_id smallint(5) NOT NULL,
  approved tinyint(1) default NULL,
  author_mode tinyint(1) default NULL,
  pinned tinyint(1) default NULL,
  moved_to varchar(64) default NULL,
  rating text,
  total_votes int(5) NOT NULL default '0',
  PRIMARY KEY  (tid),
  KEY forum_id (forum_id)
)";


$SQL[] = "CREATE TABLE ibf_tracker (
  trid bigint(20) NOT NULL auto_increment,
  member_id varchar(32) NOT NULL default '',
  topic_id bigint(20) NOT NULL default '0',
  start_date int(10) default NULL,
  last_sent int(10) NOT NULL default '0',
  PRIMARY KEY  (trid)
)";



$SQL[] = "CREATE TABLE ibf_voters (
  vid bigint(20) NOT NULL auto_increment,
  ip_address varchar(16) NOT NULL default '',
  vote_date int(10) NOT NULL default '0',
  tid bigint(20) NOT NULL default '0',
  member_id varchar(32) default NULL,
  forum_id bigint(20) default NULL,
  PRIMARY KEY  (vid)
)";

$SQL[] = "CREATE TABLE ibf_languages (
	lid bigint(20) NOT NULL auto_increment,
	ldir varchar(64) NOT NULL,
	lname varchar(250) NOT NULL,
	lauthor varchar(250) default NULL,
	lemail varchar(250) default NULL,
	PRIMARY KEY (lid)
)";


$SQL[] = "INSERT INTO ibf_categories (id, position, state, name, description, image, url) VALUES (-1, NULL, NULL, '-', NULL, NULL, NULL)";

$SQL[] = "INSERT INTO ibf_css (cssid, css_name, css_text) VALUES (1, 'Invision Style Sheet', NULL)";

$SQL[] = "INSERT INTO ibf_emoticons VALUES (1, ':mellow:', 'mellow.gif', 0)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (2, ':huh:', 'huh.gif', 1)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (3, '^_^', 'happy.gif', 0)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (4, ':o', 'ohmy.gif', 1)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (5, ';)', 'wink.gif', 1)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (6, ':P', 'tongue.gif', 1)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (7, ':D', 'biggrin.gif', 1)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (8, ':lol:', 'laugh.gif', 1)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (9, 'B)', 'cool.gif', 1)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (10, ':rolleyes:', 'rolleyes.gif', 1)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (11, '-_-', 'sleep.gif', 0)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (12, '&lt;_&lt;', 'dry.gif', 1)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (13, ':)', 'smile.gif', 1)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (14, ':wub:', 'wub.gif', 0)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (15, ':angry:', 'mad.gif', 1)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (16, ':(', 'sad.gif', 1)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (17, ':unsure:', 'unsure.gif', 1)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (18, ':wacko:', 'wacko.gif', 0)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (19, ':blink:', 'blink.gif', 1)";
$SQL[] = "INSERT INTO ibf_emoticons VALUES (20, ':ph34r:', 'ph34r.gif', 1)";

$SQL[] = "INSERT INTO ibf_faq (id, title, text, description) VALUES (1, 'Registration benefits', 'To be able to use all the features on this board, the administrator will probably require that you register for a member account. Registration is free and only takes a moment to complete.\r<br>\r<br>During registration, the adminstrator requires that you supply a valid email address. This is important as the administrator may require that you validate your registration via an email. If this is the case, you will be notified when registering.\r<br>In some cases, the administrator will need to approve your regsitration before you can use your member account fully. If this is the case you will be notified during registration.\r<br>\r<br>Once you have registered and logged in, you will have access to your private messenger and your control panel.\r<br>\r<br>For more information on these items, please see the relevant sections in this documentation.', 'How to register and the added benefits of being a registered member.')";
$SQL[] = "INSERT INTO ibf_faq (id, title, text, description) VALUES (2, 'Cookies and cookie usage', 'Using cookies is optional, but strongly recommended. Cookies are used to track topics, showing you which topics have new replies since your last visit and to automatically log you in when you return.\r<br>\r<br>This board will also allow you to remove the long session id\'s found in the URL\'s. You can do this by entering your control panel (click the link at the top of the board) and entering the \'Board Settings\' section. It is recommended that you enable this feature because you may experience problems if your internet connection shares a proxy and you share a link with another user. If in doubt, choose to hide the session id.\r<br>\r<br><b>Clearing Cookies</b>\r<br>\r<br>You can clear the cookies at any time by clicking on the link found at the bottom of the main board page (the first page you see when returning to the board). If this does not work for you, you may need to remove the cookies manually.\r<br>\r<br><u>Removing Cookies in Internet Explorer for Windows</u>\r<br>\r<br><ul>\r<br><li> Close all open Internet Explorer Windows\r<br><li> Click on the \'start\' button\r<br><li> Move up to \'Find\' and click on \'Files and Folders\'\r<br><li> When the new window appears, type in the domain name of the board you are using into the \'containing text\' field. (If the boards address was \'http://www.invisionboard.com/forums/index.php\' you would enter \'invisionboard.com\' without the quotes)\r<br><li> In the \'look in\' box, type in <b>C:&#92;Windows&#92;Cookies</b> and press \'Find Now\'\r<br><li> After it has finished searching, highlight all files (click on a file then press CTRL+A) and delete them.\r<br></ul>\r<br>\r<br><u>Removing Cookies in Internet Explorer for Macintosh</u>\r<br>\r<br><ul>\r<br><li> With Internet Explorer active, choose \'Edit\' and then \'Preferences\' from the Macintosh menu bar at the top of the screen\r<br><li> When the preferences panel opens, choose \'Cookies\' found in the \'Receiving Files\' section.\r<br><li> When the cookie pane loads, look for the domain name of the board (If the boards address was \'http://www.invisionboard.com/forums/index.php\' look for \'invisionboard.com\' or \'www.invisionboard.com\'\r<br><li> For each cookie, click on the entry and press the delete button.\r<br></ul>\r<br>\r<br>Your cookies should now be removed. In some cases you may need to restart your computer for the changes to take effect.', 'The benefits of using cookies and how to remove cookies set by this board.')";
$SQL[] = "INSERT INTO ibf_faq (id, title, text, description) VALUES (3, 'Recovering lost or forgotten passwords', 'Security is a big feature on this board, and to that end, all passwords are encrypted when you register.\r<br>This means that we cannot email your password to you as we hold no record of your \'uncrypted\' password. You can however, apply to have your password reset.\r<br>\r<br>To do this, click on the <a href=\'index.php?act=Reg&CODE=10\'>Lost Password link</a> found on the log in page.\r<br>\r<br>Further instruction is available from there.', 'How to reset your password if you\'ve forgotton it.')";
$SQL[] = "INSERT INTO ibf_faq (id, title, text, description) VALUES (4, 'Your Control Panel', 'Your control panel is your own private board console. You can change how the board looks and feels as well as your own information from here.\r<br>\r<br><b>Edit Profile Info</b>\r<br>\r<br>This section allows you to add or edit your contact information and enter some personal information if you choose. All of this information is optional and can be omitted.\r<br>\r<br><b>Edit Signature</b>\r<br>\r<br>A board \'signature\' is very similar to an email signature. This signature is attached to the foot of every message you post unless you choose to check the box that allows you to ommit the signature in the message you are posting. You may use BB Code if available and in some cases, pure HTML (if the board administrator allows it).\r<br>\r<br><b>Edit Avatar Settings</b>\r<br>\r<br>An avatar is a little image that appears under your username when you view a topic or post you authored. If the administrator allows, you may either choose from the board gallery, enter a URL to an avatar stored on your server or upload an avatar to use. You may also set the width of the avatar to ensure that it\'s sized in proportion.\r<br>\r<br><b>Email Settings</b>\r<br>\r<br><u>Hide my email address</u> allows you to deny the ability for other users to send you an email from the board.\r<br><u>Send me updates sent by the board administrator</u> will allow the administrator to include your email address in any mailings they send out - this is used mostly for important updates and community information.\r<br><u>Include a copy of the post when emailing me from a subscribed topic</u>, this allows you to have the new post included in any reply to topic notifications.\r<br>\r<br><b>Board Settings</b>\r<br>\r<br>From this section, you can set your timezone, choose to not see users signatures, avatars and posted images.\r<br>You can choose to get a pop up window informing you when you have a new message and hide the long session ID in links (see the \'Cookies\' help file for more infomation on this).\r<br>\r<br><b>Skins and Languages</b>\r<br>\r<br>If available, you can choose a skin style and language choice. This affects how the board is displayed so you may wish to preview the skin before submitting the form.\r<br>\r<br><b>Change Email Address</b>\r<br>\r<br>At any time, you can change the email address that is registered to your account. In some cases, you will need to revalidate your account after changing your email address. If this is the case, you will be notified before your email address change is processed.\r<br>\r<br><b>Change Password</b>\r<br>\r<br>You may change your password from this section. Please note that you will need to know your current password before you can change your password.\r<br>\r<br><b>View Subsciptions</b>\r<br>\r<br>This is where you manage your topic subscriptions. Please see the help file \'Email Notification of new messages\' for more information on how to subscribe to topics.', 'Editing contact information, personal information, avatars, signatures, board settings, languages and style choices.')";
$SQL[] = "INSERT INTO ibf_faq (id, title, text, description) VALUES (5, 'Email Notification of new messages', 'This board can notify you when a new reply is added to a topic. Many users find this useful to keep up to date on topics without the need to view the board to check for new messages.\r<br>\r<br>To do this, simply click on the \'Track This Topic\' link found at the top and bottom of any topic. When you click this link, the topic will be added to your subscriptions that are managed from your control panel.\r<br>\r<br>Please note that to avoid multiple emails being sent to your email address, you will only get notified once per day of new replies.', 'How to get emailed when a new reply is added to a topic.')";
$SQL[] = "INSERT INTO ibf_faq (id, title, text, description) VALUES (6, 'Your Personal Messenger', 'Your personal messenger acts much like an email account in that you can send and receive messages and store messages in folders.\r<br>\r<br><b>Send a new PM</b>\r<br>\r<br>This will allow you to send a message to another member. If you have names in your contact list, you can choose a name from it - or you may choose to enter a name in the relevant form field. This will be automatically filled in if you clicked a \'PM\' button on the board (from the memberlist or a post).\r<br>If the administrator allows, you may use BB Code and HTML in your private message. If you choose to check the \'Add a copy of this message to you sent items folder\' box, a copy of the message will be saved for you for later reference. If you check the \'Get notified when this message is read\' box, you will receive a PM informing you when the message was read for your records.\r<br>\r<br><b>Contact List</b>\r<br>\r<br>You may add in users names in this section, or edit any saved entries. You can also use this as a ban list, denying the named member the ability to message you.\r<br>Names entered in this section will appear in the drop down list when sending a new PM, allowing you to quickly choose the members name when sending a message.\r<br>\r<br><b>Edit Folders</b>\r<br>\r<br>You may rename, add or remove folders to store messages is, allowing you to organise your messages to your preference. You cannot remove \'Sent Items\' or \'Inbox\'.\r<br>\r<br><b>Go to Inbox</b>\r<br>\r<br>Your inbox is where all new messages are sent to. Clicking on the message title will show you the message in a similar format to the board topic view. You can also delete or move messages from your inbox.\r<br>\r<br><b>Archive Messages</b>\r<br>\r<br>If your messenger folders are full and you are unable to receive new messages, you can archive them off. This compiles the messages into a single HTML page or Microsoft &copy; Excel Format. This page is then emailed to your registered email address for your convenience.\r<br>\r<br>', 'How to send personal messages, edit your messenger folders and archive stored messages.')";
$SQL[] = "INSERT INTO ibf_faq (id, title, text, description) VALUES (7, 'Contacting the moderating team', 'If you need to contact a moderator or simply wish to view the complete administration team, you can click the link \'The moderating team\' found at the top of the main board page (the first page you see when visiting the board).\r<br>\r<br>This list will show you administrators (those who have administration control panel access), global moderators (those who can moderate in all forums) and the moderators of the individual forums.\r<br>\r<br>If you wish to contact someone about your member account, then contact an administrator - if you wish to contact someone about a post or topic, contact either a global moderator or the forum moderator.', 'Where to find a list of the board moderators and administrators.')";
$SQL[] = "INSERT INTO ibf_faq (id, title, text, description) VALUES (8, 'Viewing members profile information', 'You can view a members profile at any time by clicking on their name when it is underlined (as a link) or by clicking on their name in a post within a topic.\r<br>\r<br>This will show you their profile page which contains their contact information (if they have entered some) and their \'active stats\'.', 'How to view members contact information.')";
$SQL[] = "INSERT INTO ibf_faq (id, title, text, description) VALUES (9, 'Viewing todays active topics', 'You can view which new topics have new replies today by clicking on the \'Todays Active Topics\' link found on the main board page (the first page you see when visiting the board).\r<br>\r<br>You can set your own date critera, choosing to view all topics  with new replies during several date choices.', 'How to view all the topics which have a new reply today')";
$SQL[] = "INSERT INTO ibf_faq (id, title, text, description) VALUES (10, 'Searching Topics and Posts', 'The search feature is designed to allow you to quickly find topics and posts that contain the keywords you enter.\r<br>\r<br>The \'Simple Search\' option from the drop down box allows you to enter a single keyword or phrase to search by - the advanced option allows you to join keywords with \'AND\', \'OR\' to fine tune the search. Using this option will make the search slightly slower.\r<br>\r<br>The bottom section of the form allows you to further fine tune the search. You can choose a category to search in, or a forum - or choose all forums to search in.\r<br>\r<br>If you enter a name into the \'Search by member name\' section, all posts started or replied in by this member are returned.', 'How to use the search feature.')";
$SQL[] = "INSERT INTO ibf_faq (id, title, text, description) VALUES (11, 'Logging in and out', 'If you have chosen not to remember your log in details in cookies, or you are accessing the board on another computer, you will need to log into the board to access your member profile and post with your registered name.\r<br>\r<br>When you log in, you have the choice to save cookies that will log you in automatically when you return. Do not use this option on a shared computer for security.\r<br>\r<br>You can also choose to hide - this will keep your name from appearing in the active users list.\r<br>\r<br>Logging out is simply a matter of clicking on the \'Log Out\' link that is displayed when you are logged in. If you find that you are not logged out, you may need to manually remove your cookies. See the \'Cookies\' help file for more information.', 'How to log in and out from the board and how to remain anonymous and not be shown on the active users list.')";

$SQL[] = "INSERT INTO ibf_groups (g_id, g_view_board, g_mem_info, g_other_topics, g_use_search, g_email_friend, g_invite_friend, g_edit_profile, g_post_new_topics, g_reply_own_topics, g_reply_other_topics, g_edit_posts, g_delete_own_posts, g_open_close_posts, g_delete_own_topics, g_post_polls, g_vote_polls, g_use_pm, g_is_supmod, g_access_cp, g_title, g_can_remove, g_append_edit, g_access_offline, g_avoid_q, g_avoid_flood, g_icon, g_attach_max, g_avatar_upload, g_calendar_post, prefix, suffix) VALUES (4, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Admin', 0, 1, 1, 1, 1, '', 5000, 1, 1, '<span style=\'color:red\'>', '</span>')";
$SQL[] = "INSERT INTO ibf_groups (g_id, g_view_board, g_mem_info, g_other_topics, g_use_search, g_email_friend, g_invite_friend, g_edit_profile, g_post_new_topics, g_reply_own_topics, g_reply_other_topics, g_edit_posts, g_delete_own_posts, g_open_close_posts, g_delete_own_topics, g_post_polls, g_vote_polls, g_use_pm, g_is_supmod, g_access_cp, g_title, g_can_remove, g_append_edit, g_access_offline, g_avoid_q, g_avoid_flood, g_icon, g_attach_max, g_avatar_upload, g_calendar_post, prefix, suffix) VALUES (2, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 'Guests', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL)";
$SQL[] = "INSERT INTO ibf_groups (g_id, g_view_board, g_mem_info, g_other_topics, g_use_search, g_email_friend, g_invite_friend, g_edit_profile, g_post_new_topics, g_reply_own_topics, g_reply_other_topics, g_edit_posts, g_delete_own_posts, g_open_close_posts, g_delete_own_topics, g_post_polls, g_vote_polls, g_use_pm, g_is_supmod, g_access_cp, g_title, g_can_remove, g_append_edit, g_access_offline, g_avoid_q, g_avoid_flood, g_icon, g_attach_max, g_avatar_upload, g_calendar_post, prefix, suffix) VALUES (3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 0, 0, 'Members', 0, 1, 0, 0, 0, NULL, NULL, 1, 0, NULL, NULL)";
$SQL[] = "INSERT INTO ibf_groups (g_id, g_view_board, g_mem_info, g_other_topics, g_use_search, g_email_friend, g_invite_friend, g_edit_profile, g_post_new_topics, g_reply_own_topics, g_reply_other_topics, g_edit_posts, g_delete_own_posts, g_open_close_posts, g_delete_own_topics, g_post_polls, g_vote_polls, g_use_pm, g_is_supmod, g_access_cp, g_title, g_can_remove, g_append_edit, g_access_offline, g_avoid_q, g_avoid_flood, g_icon, g_attach_max, g_avatar_upload, g_calendar_post, prefix, suffix) VALUES (1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Validating', 0, 1, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL)";

$SQL[] = "INSERT INTO ibf_images (imid, setname, A_LOCKED_B, A_MOVED_B, A_POLL, A_POLLONLY_B, A_POST, A_REPLY, A_STAR, B_HOT, B_HOT_NN, B_LOCKED, B_MOVED, B_NEW, B_NORM, B_PIN, B_POLL, B_POLL_NN, C_LOCKED, C_OFF, C_OFF_CAT, C_OFF_RES, C_ON, C_ON_CAT, C_ON_RES, F_ACTIVE, F_NAV_SEP, F_NAV, F_STATS, GO_LAST_ON, GO_LAST_OFF, M_ADDMEM, M_DELETE, M_READ, M_REPLY, M_UNREAD, P_AOL, P_DELETE, P_EDIT, P_EMAIL, P_ICQ, P_MSG, P_PROFILE, P_QUOTE, P_WEBSITE, CAT_IMG, B_HOT_DOT, B_NEW_DOT, B_HOT_NN_DOT, B_NORM_DOT, B_POLL_DOT, B_POLL_NN_DOT, NEW_POST) VALUES (1, 'Invision Board Image Set', '<img src=\'style_images/1/t_closed.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/t_moved.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/t_poll.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/t_closed.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/t_new.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/t_reply.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/pip.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/f_hot.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/f_hot_no.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/f_closed.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/f_moved.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/f_norm.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/f_norm_no.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/f_pinned.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/f_poll.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/f_poll_no.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/bf_readonly.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/bf_nonew.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/bc_nonew.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/br_nonew.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/bf_new.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/bc_new.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/br_new.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/user.gif\' border=\'0\'  alt=\'\'>', ' ->', '<img src=\'style_images/1/nav.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/stats.gif\' border=\'0\'  alt=\'\'>', '', '', '<img src=\'style_images/1/msg_l_addmem.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/msg_l_delete.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/f_norm_no.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/msg_l_reply.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/f_norm.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/p_aim.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/p_delete.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/p_edit.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/p_email.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/p_icq.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/p_pm.gif\' border=\'0\'  alt=\'\'>', '[ Profile ]', '<img src=\'style_images/1/p_quote.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/p_www.gif\' border=\'0\'  alt=\'\'>', '<img src=\'style_images/1/nav_m.gif\' border=\'0\'  alt=\'\' width=\'8\' height=\'8\'>', '<img src=\'style_images/1/f_hot_dot.gif\' border=\'0\' alt=\'\'>', '<img src=\'style_images/1/f_norm_dot.gif\' border=\'0\' alt=\'\'>', '<img src=\'style_images/1/f_hot_no_dot.gif\' border=\'0\' alt=\'\'>', '<img src=\'style_images/1/f_norm_no_dot.gif\' border=\'0\' alt=\'\'>', '<img src=\'style_images/1/f_poll_dot.gif\' border=\'0\' alt=\'\'>', '<img src=\'style_images/1/f_poll_no_dot.gif\' border=\'0\' alt=\'\'>', '<img src=\'style_images/1/newpost.gif\' border=\'0\'  alt=\'Goto last unread\' title=\'Goto last unread\' hspace=2>')";
$SQL[] = "INSERT INTO ibf_members (id, name, mgroup, password, email, joined, ip_address, avatar, avatar_size, posts, photo, aim_name, icq_number, location, signature, website, yahoo, title, allow_admin_mails, time_offset, interests, hide_email, email_pm, email_full, skin, warn_level, language, msnname, last_post, allow_post, view_sigs, view_img, view_avs, view_pop, bday_day, bday_month, bday_year, new_msg, msg_from_id, msg_msg_id, msg_total, vdirs, show_popup, validate_key, prev_group, new_pass, misc, last_visit, last_activity) VALUES (0, 'Guest', 2, '', 'guest@ibforums.com', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0)";

$SQL[] = "INSERT INTO ibf_skins (uid, sname, sid, set_id, tmpl_id, img_id, css_id, tbl_width, tbl_border, hidden, default_set) VALUES (1, 'Invision Board', 0, 1, 1, 1, 1, '95%', '#999999', 0, 1)";

$SQL[] = "INSERT INTO ibf_stats (TOTAL_REPLIES, TOTAL_TOPICS, LAST_MEM_NAME, LAST_MEM_ID, MOST_DATE, MOST_COUNT, MEM_COUNT) VALUES (0, 0, NULL, '0', '0', '0', '0' )";

$SQL[] = "INSERT INTO ibf_templates (tmid, template, name) VALUES (1, '<html>    <head>      <title>                <% TITLE %>      </title>      <% GENERATOR %>      <% CSS %>      <% JAVASCRIPT %>          </head><body bgcolor=\'#FFFFFF\' leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" alink=\'#000000\' vlink=\'#000000\'><% BOARD HEADER %><% NAVIGATION %><% BOARD %><% STATS %><% COPYRIGHT %></body></html>', 'Invision Board Standard')";

$SQL[] = "INSERT INTO ibf_titles (id, posts, title, pips) VALUES (1, 0, 'Newbie', '1')";
$SQL[] = "INSERT INTO ibf_titles (id, posts, title, pips) VALUES (2, 10, 'Member', '2')";
$SQL[] = "INSERT INTO ibf_titles (id, posts, title, pips) VALUES (4, 30, 'Advanced Member', '3')";

$SQL[] = "INSERT INTO ibf_tmpl_names (skid, skname, author, email, url) VALUES (1, 'Invision Board Template Set', 'Invision Board', 'skins@invisionboard.com', 'http://www.invisionboard.com')";

$SQL[] = "INSERT INTO ibf_languages (ldir, lname, lauthor, lemail) VALUES ('en', 'English', 'Invision Board', 'languages@invisionboard.com')";

return $SQL;
}

?>