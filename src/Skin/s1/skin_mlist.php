<?php

class skin_mlist {



function Page_header($links) {
global $ibforums;
return <<<EOF
<script language='Javascript' type='text/javascript'>
		<!--
		function PopUp(url, name, width,height,center,resize,scroll,posleft,postop) {
			if (posleft != 0) { x = posleft }
			if (postop  != 0) { y = postop  }
		
			if (!scroll) { scroll = 1 }
			if (!resize) { resize = 1 }
		
			if ((parseInt (navigator.appVersion) >= 4 ) && (center)) {
			  X = (screen.width  - width ) / 2;
			  Y = (screen.height - height) / 2;
			}
			if (scroll != 0) { scroll = 1 }
		
			var Win = window.open( url, name, 'width='+width+',height='+height+',top='+Y+',left='+X+',resizable='+resize+',scrollbars='+scroll+',location=no,directories=no,status=no,menubar=no,toolbar=no');
	     }
		//-->
	</script>

<table width="{$ibforums->skin['tbl_width']}" border="0" align='center' cellspacing="0" cellpadding="4">
  <tr> 
    <td width="100%" height='38'>{$links[SHOW_PAGES]}</td>
  </tr>
</table>
<table width="{$ibforums->skin['tbl_width']}" border="0" align='center' cellspacing="1" cellpadding="0" bgcolor="{$ibforums->skin['tbl_border']}">
  <tr> 
    <td id='maintitleback' background='{$ibforums->vars['img_url']}/tile_back.gif'> 
      &nbsp;
    </td>
  </tr>
  <tr> 
    <td id='mainbg'> 
      <table width="100%" border="0" cellspacing="1" cellpadding="4">
				<tr> 
          <td nowrap background="{$ibforums->vars['img_url']}/tile_sub.gif" id='titlemedium' width="30%">{$ibforums->lang['member_name']}</td>
          <td nowrap background="{$ibforums->vars['img_url']}/tile_sub.gif" id='titlemedium' align="center" width="20%">{$ibforums->lang['member_level']}</td>
          <td nowrap background="{$ibforums->vars['img_url']}/tile_sub.gif" id='titlemedium' align="center" width="20%">{$ibforums->lang['member_group']}</td>
          <td nowrap background="{$ibforums->vars['img_url']}/tile_sub.gif" id='titlemedium' align="center" width="20%">{$ibforums->lang['member_joined']}</td>
          <td nowrap background="{$ibforums->vars['img_url']}/tile_sub.gif" id='titlemedium' align="center" width="10%">{$ibforums->lang['member_posts']}</td>
          <td nowrap background="{$ibforums->vars['img_url']}/tile_sub.gif" id='titlemedium' align="center">{$ibforums->lang['member_email']}</td>
          <td nowrap background="{$ibforums->vars['img_url']}/tile_sub.gif" id='titlemedium' align="center">{$ibforums->lang['member_aol']}</td>
          <td nowrap background="{$ibforums->vars['img_url']}/tile_sub.gif" id='titlemedium' align="center">{$ibforums->lang['member_icq']}</td>
        </tr>
EOF;
}

function end($links) {
global $ibforums;
return <<<EOF
<table width="{$ibforums->skin['tbl_width']}" border="0" align='center' cellspacing="0" cellpadding="4">
  <tr> 
    <td width="100%" height='38'>{$links[SHOW_PAGES]}</td>
  </tr>
</table>
EOF;
}

function no_results() {
global $ibforums;
return <<<EOF
No results
EOF;
}

function start() {
global $ibforums;
return <<<EOF
<!-- nothing here -->
EOF;
}

function Page_end() {
global $ibforums;
return <<<EOF
            <!-- End content Table -->
        
		<form action='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id};act=Members' method='POST'>
        <input type='hidden' name='act' value='Members'>
        <input type='hidden' name='s'   value='{$ibforums->session_id}'>
        <tr> 
          <td id='subtitle' colspan="9" align='center'>{$ibforums->lang['sorting_text']}&nbsp;<input type='submit' value='{$ibforums->lang['sort_submit']}' class='forminput'></td>
        </tr>
	</form>
        
      </table>
    </td>
  </tr>
</table>
EOF;
}

function show_row($member) {
global $ibforums;
return <<<EOF
              <!-- Entry for {$member[MEMBER_NAME]} -->
							<tr> 
								<td id='forum2' width="30%"><b><a href="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Profile&CODE=03&MID={$member[MEMBER_ID]}">{$member[MEMBER_NAME]}</a></b></td>
								<td id='forum2' nowrap width="20%">{$member[MEMBER_PIPS_IMG]}</td>
								<td id='forum1' align="center" width="20%">{$member[MEMBER_GROUP]}</td>
								<td id='forum2' nowrap align="center" width="20%">{$member[MEMBER_JOINED]}</td>
								<td id='forum2' align="center" width="10%">{$member[MEMBER_POSTS]}</td>
								<td id='forum1' align="center">{$member[MEMBER_EMAIL]}</td>
								<td id='forum1' align="center">{$member[AOLNAME]}</td>
								<td id='forum1' align="center">{$member[ICQNUMBER]}</td>
							</tr>
              <!-- End of Entry -->
EOF;
}


}
?>