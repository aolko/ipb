<?php

class skin_topic {



function mod_cp_link($data) {
global $ibforums;
return <<<EOF
( <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ModCP&f=$data'>{$ibforums->lang['mod_cp']}</a> )
EOF;
}

function report_link($data) {
global $ibforums;
return <<<EOF
&nbsp;&nbsp;<a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Report&f={$data[FORUM_ID]}&t={$data[TOPIC_ID]}&p={$data[POST_ID]}'>{$ibforums->lang['snitch_geezer_to_a_copper']}</a>
EOF;
}

function ip_show($data) {
global $ibforums;
return <<<EOF
{$ibforums->lang['ip']}: $data
EOF;
}

function golastpost_link($fid, $tid) {
global $ibforums;
return <<<EOF
( <a href='{$ibforums->base_url}&act=ST&f=$fid&t=$tid&view=getnewpost'>{$ibforums->lang['go_new_post']}</a> )
EOF;
}

function Mod_Panel($data, $fid, $tid, $modcp="&nbsp;") {
global $ibforums;
return <<<EOF
<br>
    <table cellpadding='0' cellspacing='0' border='0' width='{$ibforums->skin['tbl_width']}' align='center'>
      <tr>
      	  <td align='left'>$modcp</td>
          <td align='right'>
          <form method='POST' name='modform' action='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}'>
          <input type='hidden' name='s' value='{$ibforums->session_id}'>
          <input type='hidden' name='t' value='$tid'>
          <input type='hidden' name='f' value='$fid'>
          <input type='hidden' name='st' value='{$ibforums->input['st']}'>
          <input type='hidden' name='act' value='Mod'>
          <select name='CODE' class='forminput'>
          <option value='-1'>{$ibforums->lang['moderation_ops']}</option>
          $data
          </select>&nbsp;<input type='submit' value='{$ibforums->lang['jmp_go']}' class='forminput'>
          </form>
          </td>
        </tr>
    </table>
EOF;
}

function mod_wrapper($id="", $text="") {
global $ibforums;
return <<<EOF
<option value='$id'>$text</option>
EOF;
}

function TableFooter($data) {
global $ibforums;
return <<<EOF
      </table></td>
  </tr>
  <tr> 
    <td id='mainbg'>
			<table width='100%' border='0' cellspacing='1' cellpadding='4'>
        <tr>
          <td id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif' width='100%'>
			<table width='100%' border='0' cellspacing='0' cellpadding='0'>
              <tr>
                <td align='left'><b>{$ibforums->lang['topic_stats']}</b></td>
              	<td align='right'><b><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Track&f={$data['FORUM']['id']}&t={$data['TOPIC']['tid']}'>{$ibforums->lang['track_topic']}</a> |
									<a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Forward&f={$data['FORUM']['id']}&t={$data['TOPIC']['tid']}'>{$ibforums->lang['forward']}</a> |
									<a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Print&f={$data['FORUM']['id']}&t={$data['TOPIC']['tid']}'>{$ibforums->lang['print']}</a></b></td>
               </tr>
            </table>
		  </td>
        </tr>
      </table>
		</td>
  </tr>
</table>
<table width='{$ibforums->skin['tbl_width']}' align='center' border='0' cellspacing='0' cellpadding='4'>
  <tr> 
    <td width='100%'>{$data[TOPIC][SHOW_PAGES]}<br><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=SF&f={$data['FORUM']['id']}'>Back to {$data['FORUM']['name']}</a></td>
    <td align='right' nowrap>{$data[TOPIC][REPLY_BUTTON]}<a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Post&CODE=00&f={$data[FORUM]['id']}' title='{$ibforums->lang['start_new_topic']}'>{$ibforums->skin['A_POST']}</a>{$data[TOPIC][POLL_BUTTON]}</td>
  </tr>
</table>
<br>
<table width='{$ibforums->skin['tbl_width']}' align='center' border='0' cellspacing='0' cellpadding='3'>
  <tr> 
    <td width='100%' align='right'>{$data[FORUM]['JUMP']}</td>
  </tr>
</table>
<br>
EOF;
}

function RenderRow($data) {
global $ibforums;
return <<<EOF
		<!--Begin Msg Number {$data[POST]['pid']}-->
		 <tr>
		 <td valign='middle' id='posthead'><a name='entry{$data[POST]['pid']}'></a><span id='{$data[POST]['name_css']}'>{$data[POSTER]['name']}</span></td>
          <td id='posthead' valign='top'>
			<table width='100%' border='0' cellspacing='0' cellpadding='0'>
              <tr> 
                <td id='posthead'>{$data[POST]['post_icon']}<b>{$ibforums->lang['posted_on']}</b> {$data[POST]['post_date']}</td>
                <td id='posthead' align='right'>{$data[POST]['delete_button']}{$data[POST]['edit_button']}<a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Post&CODE=06&f={$ibforums->input[f]}&t={$ibforums->input[t]}&p={$data[POST]['pid']}'>{$ibforums->skin['P_QUOTE']}</a></td>
              </tr>
            </table>
		 </td>
        </tr>
		<tr>
          <td valign='top' id='post1'>
            	{$data[POSTER]['avatar']}<br><br>
				{$data[POSTER]['title']}<br>
				{$data[POSTER]['member_rank_img']}<br><br>
            	{$data[POSTER]['member_group']}<br>
            	{$data[POSTER]['member_posts']}<br>
            	{$data[POSTER]['member_number']}<br>
            	{$data[POSTER]['member_joined']}<br>
            	{$data[POSTER][WARN_GFX]}<br><br>
		        <img src='{$ibforums->vars['img_url']}/spacer.gif' alt='' width='160' height='1'><br> 
          </td>
          <td width='100%' valign='top' id='post1'><span id='postcolor'>{$data[POST]['post']} {$data[POST]['attachment']} {$data[POST]['signature']}</td>
        </tr>
        <tr>
		   <td id='postfoot' align='left'>&nbsp;</td>
		   <td id='postfoot' nowrap align='left'>
		    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
              <tr>
                <td id='postfoot' align='left' valign='middle' nowrap>{$data[POSTER]['message_icon']}{$data[POSTER]['email_icon']}{$data[POSTER]['website_icon']}{$data[POSTER]['icq_icon']}{$data[POSTER]['aol_icon']}{$data[POSTER]['yahoo_icon']}{$data[POSTER]['msn_icon']}</td>
                <td id='postfoot' valign='middle' align='right'>{$data[POST]['ip_address']}{$data[POST]['report_link']}</td>
		   		<td id='postfoot' valign='middle' align='right' width='2%'><a href='javascript:scroll(0,0);'><img src='{$ibforums->vars['img_url']}/p_up.gif' alt='Top' border='0'></a></td>
		      </tr>
		    </table>
		   </td>
        </tr>
        <tr> 
          <td id='postsep' colspan='2'><img src='{$ibforums->vars['img_url']}/spacer.gif' alt='' width='1' height='1'></td>
		</tr>
  <!-- end Message -->
EOF;
}

function PageTop($data) {
global $ibforums;
return <<<EOF
    <script language='javascript'>
    <!--
    function delete_post(theURL) {
       if (confirm('{$ibforums->lang['js_del_1']}')) {
          window.location.href=theURL;
       }
       else {
          alert ('{$ibforums->lang['js_del_2']}');
       } 
    }
    
    function PopUp(url, name, width,height,center,resize,scroll,posleft,postop) {
    if (posleft != 0) { x = posleft }
    if (postop  != 0) { y = postop  }

    if (!scroll) { scroll = 1 }
    if (!resize) { resize = 1 }

    if ((parseInt (navigator.appVersion) >= 4 ) && (center)) {
      X = (screen.width  - width ) / 2;
      Y = (screen.height - height) / 2;
    }
    if (scroll != 0) { scroll = 1 }

    var Win = window.open( url, name, 'width='+width+',height='+height+',top='+Y+',left='+X+',resizable='+resize+',scrollbars='+scroll+',location=no,directories=no,status=no,menubar=no,toolbar=no');
	}
    //-->
    </script>
<a name='top'></a>
<!-- Cgi-bot Start Forum page unique top -->
<table width='{$ibforums->skin['tbl_width']}' border='0' cellspacing='0' cellpadding='4' align='center'>
  <tr> 
    <td width='100%'>{$data['TOPIC']['SHOW_PAGES']}&nbsp;{$data['TOPIC']['go_new']}</td>
    <td align='right' nowrap>{$data[TOPIC][REPLY_BUTTON]}<a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Post&CODE=00&f={$data[FORUM]['id']}' title='{$ibforums->lang['start_new_topic']}'>{$ibforums->skin['A_POST']}</a>{$data[TOPIC][POLL_BUTTON]}</td>
  </tr>
</table>
<table width='{$ibforums->skin['tbl_width']}' border='0' cellspacing='1' cellpadding='0' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
  <tr> 
    <td background='{$ibforums->vars['img_url']}/tile_back.gif' id='maintitleback'>
			<table width='100%' border='0' cellspacing='0' cellpadding='3'>
        <tr> 
          <td><img src='{$ibforums->vars['img_url']}/nav_m.gif' alt='' width='8' height='8'></td>
          <td width='100%' id='maintitle'><b>{$data['TOPIC']['title']}</b>{$data['TOPIC']['description']}</td>
        </tr>
      </table>
		</td>
  </tr>
  <tr> 
	<td id='mainbg'>
	<table width='100%' border='0' cellspacing='1' cellpadding='4'>
	<tr> 
	  <td width='100%' id='titlemedium' nowrap background='{$ibforums->vars['img_url']}/tile_sub.gif' colspan="2"> 
		<table width='100%' border='0' cellspacing='0' cellpadding='0'>
		  <tr>
		    <td align='left'><b>&laquo; <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ST&f={$data[FORUM]['id']}&t={$data[TOPIC]['tid']}&view=old'>{$ibforums->lang['t_old']}</a> | <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ST&f={$data[FORUM]['id']}&t={$data[TOPIC]['tid']}&view=new'>{$ibforums->lang['t_new']}</a> &raquo;</b></td>
			<td align='right'><b>
				<a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Track&f={$data['FORUM']['id']}&t={$data['TOPIC']['tid']}'>{$ibforums->lang['track_topic']}</a> |
				<a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Forward&f={$data['FORUM']['id']}&t={$data['TOPIC']['tid']}'>{$ibforums->lang['forward']}</a> |
				<a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Print&f={$data['FORUM']['id']}&t={$data['TOPIC']['tid']}'>{$ibforums->lang['print']}</a></b></td>
			</tr>
		</table>
	  </td>
	</tr>

<!-- Cgi-bot End Forum page unique top -->
EOF;
}

function Show_attachments_img($data) {
global $ibforums;
return <<<EOF
<br><br><center><b>{$ibforums->lang['pic_attach']}</b></center><br>
<table cellpadding='4' cellspacing='0' border='0' width='50%' align='center' id='fancyborder'>
 <tr>
  <td valign='middle' align='center'><img src='{$ibforums->vars['upload_url']}/{$data['file_name']}' border='0' alt='{$ibforums->lang['pic_attach']}'></td>
 </tr>
</table>
<p>
EOF;
}

function Show_attachments($data) {
global $ibforums;
return <<<EOF
    <br><br>
     <table cellpadding='4' cellspacing='0' border='0' width='50%' align='center' id='fancyborder'>
      <tr>
       <td align='right' valign='middle' rowspan='2'><img src='{$ibforums->vars['mime_img']}/{$data['image']}' border='0' alt='User Attached Image'></td>
        <td align='left'><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Attach&type=post&id={$data['pid']}' target='_blank'>{$ibforums->lang['attach_dl']}</a></td>
      </tr>
      <tr>
      <td align='left' valign='middle' width='98%'>{$data['name']}  ( {$ibforums->lang['attach_hits']}: {$data['hits']} )</td>
      </tr>
     </table>
     <br><br>
EOF;
}


}
?>