<?php

/*
+--------------------------------------------------------------------------
|   IBFORUMS v1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001,2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > Import functions
|   > Module written by Matt Mecham
|   > Date started: 22nd April 2002
|
|	> Module Version Number: 1.0.0
+--------------------------------------------------------------------------
*/



$idx = new ad_langs();


class ad_langs {

	var $base_url;

	function ad_langs() {
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;

		switch($IN['code'])
		{
		
			case 'doimport':
				$this->doimport();
				break;
				
			case 'import':
				$this->import();
				break;
				
			case 'remove':
				$this->remove();
				break;
				
				
			//-------------------------
			default:
				$this->list_current();
				break;
		}
		
	}
	
	
	//---------------------------------------------
	// Remove archived files
	//---------------------------------------------
	
	function remove()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
	
		if ($IN['id'] == "")
		{
			$ADMIN->error("You did not select a tar-chive to import!");
		}
		
		$this->tar_with_path = $INFO['base_dir']."archive_in/".$IN['id'];
		
		if ( ! unlink($this->tar_with_path) )
		{
			$ADMIN->error("Could not remove that file, please check the CHMOD permissions");
		}
		
		$std->boink_it($ADMIN->base_url."&act=import");
		exit();
	
	
	}
	
	
	//---------------------------------------------
	// Import switcheroo
	//---------------------------------------------
	
	function import()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
	
		if ($IN['id'] == "")
		{
			$ADMIN->error("You did not select a tar-chive to import!");
		}
		
		$this->tar_with_path = $INFO['base_dir']."archive_in/".$IN['id'];
		
		$this->work_path     = $INFO['base_dir']."archive_in";
		
		if (! file_exists($this->tar_with_path) )
		{
			$ADMIN->error( "That archive is not found on the server, it may have been deleted by another admin");
		}
		
		$this->tar_file = $IN['id'];
		
		$this->name_translated = preg_replace( "/^(css|image|set|wrap|tmpl)-(.+?)\.(\S+)$/", "\\2", $this->tar_file );
		$this->name_translated = preg_replace( "/_/", " ", $this->name_translated );
		
		require $root_dir."sources/lib/tar.php";
		
		$this->tar = new tar();
		
		switch($IN['type'])
		{
			case 'css':
				$this->css_import();
				break;
				
			case 'wrap':
				$this->wrap_import();
				break;
				
			case 'image':
				$this->image_import();
				break;
				
			case 'tmpl':
				$this->template_import();
				break;
			case 'set':
				$this->set_import();
				break;
				
			//---------
			default:
				$ADMIN->error("Unrecognised archive type");
				break;
		}
	
	
	}
	
	function set_import()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		$templates_dir = $INFO['base_dir']."style_templates";
		$images_dir    = $INFO['base_dir']."style_images";
		$css_dir       = $INFO['base_dir']."style_sheets";
		$skins_dir     = $INFO['base_dir']."Skin";
		
		if (! is_writeable($images_dir) )
		{
			$ADMIN->error("Cannot write to the $images_dir directory, please set sufficient CHMOD permissions to allow this. IBF cannot do this for you.");
		}
		
		if (! is_writeable($css_dir) )
		{
			$ADMIN->error("Cannot write to the $css_dir directory, please set sufficient CHMOD permissions to allow this. IBF cannot do this for you.");
		}
		
		if (! is_dir($templates_dir) )
		{
			$ADMIN->error("Cannot write to the $templates_dir directory, It is not there.");
		}
		
		if (! is_writeable($templates_dir) )
		{
			$ADMIN->error("Cannot write to the $templates_dir directory, please set sufficient CHMOD permissions to allow this. IBF cannot do this for you.");
		}
		
		if (! is_dir($skins_dir) )
		{
			$ADMIN->error("Cannot write to the $skins_dir directory, It is not there.");
		}
		
		if (! is_writeable($skins_dir) )
		{
			$ADMIN->error("Cannot write to the $skins_dir directory, please set sufficient CHMOD permissions to allow this. IBF cannot do this for you.");
		}
		
		//------------------------------------------------------
		
		$this->tar->new_tar( $this->work_path, $this->tar_file );
		
		$files = $this->tar->list_files();
		
		if (! $this->check_archive($files) )
		{
			$ADMIN->error("That is not a valid tar-achive, please re-upload in binary and try again");
		}
		
		//------------------------------------------------------
		
		$DB->query("SELECT COUNT(*) as count FROM ibf_tmpl_names");
		
		$tmpl_count = $DB->fetch_row();
		
		//------------------------------------------------------
		// Attempt to create a new work directory
		//------------------------------------------------------
		
		$new_dir = preg_replace( "/^(.+?)\.tar$/", "\\1", $this->tar_file );
		
		$this->new_dir = $new_dir;
		
		if ( ! mkdir($this->work_path."/".$new_dir, 0777) )
		{
			$ADMIN->error("Directory creation failed, cannot import skin set. Please check the permission in 'archive_in'");
		}
		
		@chmod($this->work_path."/".$new_dir, 0777);
		
		$next_id = array( 'css' => 0, 'wrap' => 0, 'templates' => 0, 'images' => 0 );
		
		//------------------------------------------------------
		// Add a dummy entries into the DB to get the next insert ID
		//------------------------------------------------------
		
		$DB->query("INSERT INTO ibf_tmpl_names SET skname=\"".$this->name_translated." (Set Import).{$tmpl_count['count']}\"");
		
		$next_id['templates'] = $DB->get_insert_id();
		
		//------------------------------------------------------
		
		$DB->query("INSERT INTO ibf_css SET css_name=\"".$this->name_translated." (Set Import)\"");
		
		$next_id['css'] = $DB->get_insert_id();
		
		//------------------------------------------------------
		
		$DB->query("INSERT INTO ibf_templates SET name=\"".$this->name_translated." (Set Import)\"");
		
		$next_id['wrap'] = $DB->get_insert_id();
		
		//------------------------------------------------------
		
		$DB->query("INSERT INTO ibf_images SET setname=\"".$this->name_translated." (Set Import)\"");
		
		$next_id['images'] = $DB->get_insert_id();
		
		//------------------------------------------------------
		// Attempt to create the new directories
		//------------------------------------------------------
		
		if (! mkdir($templates_dir."/".$next_id['templates'], 0777) )
		{
			$this->import_error("Could not create a new directory in style_templates", $next_id);
		}
		
		@chmod($templates_dir."/".$next_id['templates'], 0777);
		
		//-----------------------------------------------------
		
		if (! mkdir($images_dir."/".$next_id['images'], 0777) )
		{
			$this->import_error("Could not create a new directory in style_images", $next_id);
		}
		
		@chmod($images_dir."/".$next_id['images'], 0777);
		
		//-----------------------------------------------------
		
		if (! mkdir($skins_dir."/s".$next_id['templates'], 0777) )
		{
			$this->import_error("Could not create a new directory in Skin", $next_id);
		}
		
		@chmod($skins_dir."/s".$next_id['templates'], 0777);
		
		//------------------------------------------------------
		
		$this->tar->extract_files($this->work_path."/".$new_dir);
		
		if ($this->tar->error != "")
		{
			$this->import_error($this->tar->error, $next_id);
		}
		
		//------------------------------------------------------
		// Make sure config file is present and correct and stuff
		//------------------------------------------------------
		
		if ( ! file_exists($this->work_path."/".$new_dir."/templates/config.php") )
		{
			$this->import_error("Master template set configuration file 'config.php' is missing from this archive, terminating import", $next_id);
		}
		
		//------------------------------------------------------
		
		if ( ! file_exists($this->work_path."/".$new_dir."/img_conf.inc") )
		{
			$this->import_error("Image and macro set configuration file 'img_conf.inc' is missing from this archive, terminating import", $next_id);
		}
		
		require $this->work_path."/".$new_dir."/img_conf.inc";
		
		$image_config = $config;
		
		unset($config);
		
		//------------------------------------------------------
		
		if ( file_exists($this->work_path."/".$new_dir."/templates_conf.inc") )
		{
			require $this->work_path."/".$new_dir."/templates_conf.inc";
			
			$template_config = array( 'author'     => stripslashes($config['author']),
									  'email'      => stripslashes($config['email']),
									  'url'        => stripslashes($config['url']),
									);
									
			$skin_set_config = array(
									  'tbl_width'  => stripslashes($config['tbl_width']),
									  'tbl_border' => stripslashes($config['tbl_border']),
									);
						   
			$db_string = $DB->compile_db_update_string( $template_config );
						   
			$DB->query("UPDATE ibf_tmpl_names SET $db_string WHERE skid='{$next_id['templates']}'");
		}
		
		//------------------------------------------------------
		// Import the CSS
		//------------------------------------------------------
		
		if ($FH = fopen( $this->work_path."/".$new_dir."/stylesheet.css", 'r' ) )
		{
			$css = fread($FH, filesize($this->work_path."/".$new_dir."/stylesheet.css") );
			fclose($FH);
			
			//-------------------------
			// Swop Binary to Ascii
		    //-------------------------
			
			$css = preg_replace( "/\r/", "\n", stripslashes($css) );
			
			$css_string = $DB->compile_db_update_string( array(
																 'css_name' => stripslashes( $this->name_translated )." (Import)",
																 'css_text' => $css,
													   )      );
												   
			$DB->query("UPDATE ibf_css SET $css_string WHERE cssid='{$next_id['css']}'");
			
			
			if ( $FHD = fopen( $INFO['base_dir']."style_sheets/stylesheet_".$next_id['css'].".css", 'w') )
			{
				fwrite($FHD, $css, strlen($css) );
				fclose($FHD);
			}
			else
			{
				$this->import_error("Could not write new style sheet to 'style_sheets' directory, please check the permission and re-try", $next_id);
			}
			
		}
		else
		{
			$this->import_error("Could not read the uploaded CSS archive file, please check the permissions on that file and try again", $next_id);
		}
		
		//------------------------------------------------------
		// Import the board wrapper
		//------------------------------------------------------
		
		if ($FH = fopen( $this->work_path."/".$new_dir."/wrapper.html", 'r' ) )
		{
			$text = fread($FH, filesize($this->work_path."/".$new_dir."/wrapper.html") );
			fclose($FH);
			
			//-------------------------
			// Swop Binary to Ascii
		    //-------------------------
			
			$text = preg_replace( "/\r/", "\n", stripslashes($text) );
			
			$wrap_string = $DB->compile_db_update_string( array(
																  'name'     => stripslashes( $this->name_translated )." (Import)",
																  'template' => $text,
														)      );
												   
			$DB->query("UPDATE ibf_templates SET $wrap_string WHERE tmid='{$next_id['wrap']}'");
			
		}
		else
		{
			$this->import_error("Could not read the uploaded board wrapper archive file, please check the permissions on that file and try again", $next_id);
		}
		
		//------------------------------------------------------
		// Attempt to copy over the image files
		//------------------------------------------------------
		
		if (! $ADMIN->copy_dir($this->work_path."/".$new_dir."/images", $images_dir."/".$next_id['images']) )
		{
			$this->import_error("Could not import images, terminating the import", $next_id);
		}
		
		//------------------------------------------------------
		// Fix up the images db table
		//------------------------------------------------------
		
		$new_array = array();
		
		foreach($image_config as $k => $v)
		{
			if ( ($k == 'imid') or ($k == 'setname') )
			{
				continue;
			}
			
			$v = stripslashes($v);
			
			$v = preg_replace( "#/%img_dir%/#", "/".$next_id['images']."/", $v );
			
			$new_array[ $k ] = $v;
		}
		
		$db_string = $DB->compile_db_update_string( $new_array );
		
		$DB->query("UPDATE ibf_images SET $db_string WHERE imid='{$next_id['images']}'");
		
		unset($db_string);
		
		//------------------------------------------------------
		// Attempt to copy over the template files
		//------------------------------------------------------
		
		if (! $ADMIN->copy_dir($this->work_path."/".$new_dir."/templates", $templates_dir."/".$next_id['templates']) )
		{
			$this->import_error("Could not import templates, terminating the import", $next_id);
		}
		
		$tmpl_string = $DB->compile_db_update_string( $template_config );
						   
		$DB->query("UPDATE ibf_tmpl_names SET $tmpl_string WHERE skid='{$next_id['templates']}'");
		
		//------------------------------------------------------
		// Add a new row to the skins table.
		//------------------------------------------------------
		
		$DB->query("SELECT MAX(sid) as new_id FROM ibf_skins");
			
		$set = $DB->fetch_row();
		
		$set['new_id']++;
		
		$new_name = stripslashes( $this->name_translated )." (Import)".$set['new_id'];
		
		$skin_string = $DB->compile_db_insert_string( array( 'sname'       => $new_name,
															 'sid'         => $set['new_id'],
															 'set_id'      => $next_id['templates'],
															 'tmpl_id'     => $next_id['wrap'],
															 'img_id'      => $next_id['images'],
															 'css_id'      => $next_id['css'],
															 'tbl_width'   => $skin_set_config['tbl_width'],
															 'tbl_border'  => $skin_set_config['tbl_border'],
															 'hidden'      => 0,
															 'default_set' => 0
												    )      );
							
		$DB->query("INSERT INTO ibf_skins (".$skin_string['FIELD_NAMES'].") VALUES(".$skin_string['FIELD_VALUES'].")");
		
		//------------------------------------------------------
		// Resynchronise the PHP files wid' da' HTML templates
		//------------------------------------------------------
		
		$ADMIN->rm_dir($this->work_path."/".$new_dir);
		
		$templates_dir .= "/" .$next_id['templates'];
		$skins_dir     .= "/s".$next_id['templates'];
		
		$errors = array();
		
		$errors = $this->rebuild_phpskin( $templates_dir, $skins_dir );
		
		if ( is_array($errors) )
		{
			if (count($errors > 0))
			{
				$ADMIN->html .= $SKIN->start_table("Errors and warnings");
			
				$ADMIN->html .= $SKIN->add_td_basic( implode("<br>", $errors) );
												 
				$ADMIN->html .= $SKIN->end_table();
			}
		}
		
		$ADMIN->done_screen("Skin set Imported", "Manage Skin sets", "act=sets" );
		
	}
	
	
	//-------------------------------------------------------------------
	
	//-------------------------------------------------------------------
	
	function template_import()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		$templates_dir = $INFO['base_dir']."style_templates";
		
		$skins_dir     = $INFO['base_dir']."Skin";
		
		if (! is_dir($templates_dir) )
		{
			$ADMIN->error("Cannot write to the $images_dir directory, It is not there.");
		}
		
		if (! is_writeable($templates_dir) )
		{
			$ADMIN->error("Cannot write to the $images_dir directory, please set sufficient CHMOD permissions to allow this. IBF cannot do this for you.");
		}
		
		if (! is_dir($skins_dir) )
		{
			$ADMIN->error("Cannot write to the $skins_dir directory, It is not there.");
		}
		
		if (! is_writeable($skins_dir) )
		{
			$ADMIN->error("Cannot write to the $skins_dir directory, please set sufficient CHMOD permissions to allow this. IBF cannot do this for you.");
		}
		
		//------------------------------------------------------
		
		$this->tar->new_tar( $this->work_path, $this->tar_file );
		
		$files = $this->tar->list_files();
		
		if (! $this->check_archive($files) )
		{
			$ADMIN->error("That is not a valid tar-achive, please re-upload in binary and try again");
		}
		
		//------------------------------------------------------
		// Add a dummy entry into the DB to get the next insert ID
		//------------------------------------------------------
		
		$DB->query("SELECT COUNT(*) as count FROM ibf_tmpl_names");
		
		$count = $DB->fetch_row();
		
		$DB->query("INSERT INTO ibf_tmpl_names SET skname=\"".$this->name_translated." (Import).{$count['count']}\"");
		
		$new_id = $DB->get_insert_id();
		
		if ($new_id == "")
		{
			$ADMIN->error("Could not write to the database");
		}
		
		//------------------------------------------------------
		// Attempt a mkdir
		//------------------------------------------------------
		
		if (! mkdir($templates_dir."/".$new_id, 0777) )
		{
			$DB->query("DELETE FROM ibf_tmpl_names WHERE skid='$new_id'");
			
			$ADMIN->error("Could not create a new directory in style_templates");
		}
		
		@chmod($templates_dir."/".$new_id, 0777);
		
		//-----------------------------------------------------
		
		if (! mkdir($skins_dir."/s".$new_id, 0777) )
		{
			$DB->query("DELETE FROM ibf_tmpl_names WHERE skid='$new_id'");
			
			$ADMIN->error("Could not create a new directory in Skin");
		}
		
		@chmod($skins_dir."/s".$new_id, 0777);
		
		//------------------------------------------------------
		
		$this->tar->extract_files($templates_dir."/".$new_id);
		
		if ($this->tar->error != "")
		{
			$DB->query("DELETE FROM ibf_tmpl_names WHERE skid='$new_id'");
			
			$ADMIN->error( $this->tar->error );
		}
		
		//------------------------------------------------------
		// Make sure config file is present and correct and stuff
		//------------------------------------------------------
		
		if ( ! file_exists($templates_dir."/".$new_id."/config.php") )
		{
			$DB->query("DELETE FROM ibf_tmpl_names WHERE skid='$new_id'");
			
			$ADMIN->error( "Master template set configuration file 'config.php' is missing from this archive, terminating import" );
		}
		
		//------------------------------------------------------
		
		if ( file_exists($templates_dir."/".$new_id."/conf.inc") )
		{
			require $templates_dir."/".$new_id."/conf.inc";
			
			$string = array( 'author' => stripslashes($config['author']),
							 'email'  => stripslashes($config['email']),
							 'url'    => stripslashes($config['url']),
						   );
						   
			$db_string = $DB->compile_db_update_string( $string );
						   
			$DB->query("UPDATE ibf_tmpl_names SET $db_string WHERE skid='$new_id'");
		}
		
		//------------------------------------------------------
		// Resynchronise the PHP files wid' da' HTML templates
		
		$templates_dir .= "/".$new_id;
		$skins_dir     .= "/s".$new_id;
		
		$errors = $this->rebuild_phpskin( $templates_dir, $skins_dir );
		
		if (count($errors > 0))
		{
			$ADMIN->html .= $SKIN->start_table("Errors and warnings");
		
			$ADMIN->html .= $SKIN->add_td_basic( implode("<br>", $errors) );
											 
			$ADMIN->html .= $SKIN->end_table();
		}
		
		$ADMIN->done_screen("Template set Imported", "Manage Template sets", "act=templ" );
		
	}
	
	
	//-------------------------------------------------------------------
	
	function image_import()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		$images_dir = $INFO['base_dir']."style_images";
		
		if (! is_dir($images_dir) )
		{
			$ADMIN->error("Cannot write to the $images_dir directory, It is not there.");
		}
		
		if (! is_writeable($images_dir) )
		{
			$ADMIN->error("Cannot write to the $images_dir directory, please set sufficient CHMOD permissions to allow this. IBF cannot do this for you.");
		}
		
		//------------------------------------------------------
		
		$this->tar->new_tar( $this->work_path, $this->tar_file );
		
		$files = $this->tar->list_files();
		
		if (! $this->check_archive($files) )
		{
			$ADMIN->error("That is not a valid tar-achive, please re-upload in binary and try again");
		}
		
		//------------------------------------------------------
		// Add a dummy entry into the DB to get the next insert ID
		//------------------------------------------------------
		
		$DB->query("INSERT INTO ibf_images SET setname=\"".$this->name_translated." (Import)\"");
		
		$new_id = $DB->get_insert_id();
		
		if ($new_id == "")
		{
			$ADMIN->error("Could not write to the database");
		}
		
		//------------------------------------------------------
		// Attempt a mkdir
		//------------------------------------------------------
		
		if (! mkdir($images_dir."/".$new_id, 0777) )
		{
			$DB->query("DELETE FROM ibf_images WHERE imid='$new_id'");
			
			$ADMIN->error("Could not create a new directory in style_images");
		}
		
		@chmod($images_dir."/".$new_id, 0777);
		
		//------------------------------------------------------
		
		$this->tar->extract_files($images_dir."/".$new_id);
		
		if ($this->tar->error != "")
		{
			$DB->query("DELETE FROM ibf_images WHERE imid='$new_id'");
			
			$ADMIN->error( $this->tar->error );
		}
		
		//------------------------------------------------------
		
		if ( ! file_exists($images_dir."/".$new_id."/conf.inc") )
		{
			$DB->query("DELETE FROM ibf_images WHERE imid='$new_id'");
			
			$ADMIN->error("Could not locate the configuration file!");
		}
		
		//------------------------------------------------------
		
		require $images_dir."/".$new_id."/conf.inc";
		
		$new_array = array();
		
		foreach($config as $k => $v)
		{
			if ( ($k == 'imid') or ($k == 'setname') )
			{
				continue;
			}
			
			$v = stripslashes($v);
			
			$v = preg_replace( "#/%img_dir%/#", "/$new_id/", $v );
			
			$new_array[ $k ] = $v;
		}
		
		//------------------------------------------------------
		
		// Update the DB
		
		$db_string = $DB->compile_db_update_string( $new_array );
		
		$DB->query("UPDATE ibf_images SET $db_string WHERE imid='$new_id'");
		
		@unlink($images_dir."/".$new_id."/conf.inc");
	
		$ADMIN->done_screen("Image & Macro Imported", "Manage Image & Macro sets", "act=image" );
		
	}
	
	//-------------------------------------------------------------------
	
	//-------------------------------------------------------------------
	
	function wrap_import()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		// Attempt to open the template file...
		
		if ($FH = fopen( $this->tar_with_path, 'r' ) )
		{
			$text = fread($FH, filesize($this->tar_with_path) );
			fclose($FH);
			
			//-------------------------
			// Swop Binary to Ascii
		    //-------------------------
			
			$text = preg_replace( "/\r/", "\n", stripslashes($text) );
			
			$string = $DB->compile_db_insert_string( array(
															 'name'     => stripslashes( $this->name_translated )." (Import)",
															 'template' => $text,
												   )      );
												   
			$DB->query("INSERT INTO ibf_templates (".$string['FIELD_NAMES'].") VALUES(".$string['FIELD_VALUES'].")");
			
		}
		else
		{
			$ADMIN->error("Could not read the uploaded board wrapper archive file, please check the permissions on that file and try again");
		}
		
		$ADMIN->done_screen("Board Wrapper Imported", "Manage Board Wrappers", "act=wrap" );
		
	}
	
	//-------------------------------------------------------------------
	
	
	function css_import()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		if (! is_dir($INFO['base_dir']."style_sheets") )
		{
			$ADMIN->error("Cannot write to the {$INFO['base_dir']}style_sheets directory, It is not there.");
		}
		
		if (! is_writeable($INFO['base_dir']."style_sheets") )
		{
			$ADMIN->error("Cannot write to the {$INFO['base_dir']}style_sheets directory, please set sufficient CHMOD permissions to allow this. IBF cannot do this for you.");
		}
		
		
		// Attempt to open the CSS file...
		
		if ($FH = fopen( $this->tar_with_path, 'r' ) )
		{
			$css = fread($FH, filesize($this->tar_with_path) );
			fclose($FH);
			
			//-------------------------
			// Swop Binary to Ascii
		    //-------------------------
			
			$css = preg_replace( "/\r/", "\n", stripslashes($css) );
			
			$string = $DB->compile_db_insert_string( array(
															 'css_name' => stripslashes( $this->name_translated )." (Import)",
															 'css_text' => $css,
												   )      );
												   
			$DB->query("INSERT INTO ibf_css (".$string['FIELD_NAMES'].") VALUES(".$string['FIELD_VALUES'].")");
			
			if ( $new_id = $DB->get_insert_id() )
			{
				if ( $FHD = fopen( $INFO['base_dir']."style_sheets/stylesheet_".$new_id.".css", 'w') )
				{
					fwrite($FHD, $css, strlen($css) );
					fclose($FHD);
				}
				else
				{
					$DB->query("DELETE FROM ibf_css WHERE cssid='$new_id'");
					$ADMIN->error("Could not write new style sheet to 'style_sheets' directory, please check the permission and re-try");
				}
			}
			else
			{
				$ADMIN->error("Could not write to the database.");
			}
		}
		else
		{
			$ADMIN->error("Could not read the uploaded CSS archive file, please check the permissions on that file and try again");
		}
		
		$ADMIN->done_screen("CSS information Imported", "Manage Uploaded Archives", "act=import" );
		
	}
	
	
	
	
	//----------------------------------------------------
	
	function check_archive($files)
	{
		if (count($files) > 0)
		{
			foreach($files as $giles)
			{
				if ( ! preg_match( "/^(?:[\(\)\:\;\~\.\w\d\+\-\_\/]+)$/", $giles) )
				{
					return FALSE;
				}
			}
		}
		else
		{
			return FALSE;
		}
		
		return TRUE;
	}
	
	
	//-------------------------------------------------------------
	// SHOW ALL LANGUAGE PACKS
	//-------------------------------------------------------------
	
	function list_current()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		$form_array = array();
	
		$ADMIN->page_detail = "You can select which archives to import onto your board in this section. All archives must be uploaded into the 'archive_in' directory";
		$ADMIN->page_title  = "Import Skin Archive Manager";
		
		
		
		//+-------------------------------
		
		$files = array();
		
		$dir = $INFO['base_dir']."/archive_in";
			
		if ( is_dir($dir) )
		{
			$handle = opendir($dir);
			
			while (($filename = readdir($handle)) !== false)
			{
				if (($filename != ".") && ($filename != ".."))
				{
					if (preg_match("/^(css|image|set|wrap|tmpl).+?\.(tar|html|css)$/", $filename))
					{
						$files[] = $filename;
					}
				}
			}
			
			closedir($handle);
			
		}
		
		//+-------------------------------
		
		$SKIN->td_header[] = array( "Name"         , "30%" );
		$SKIN->td_header[] = array( "Type"         , "20%" );
		$SKIN->td_header[] = array( "File Name"     , "30%" );
		$SKIN->td_header[] = array( "Import"       , "10%" );
		$SKIN->td_header[] = array( "Remove"       , "10%" );
		
		$ADMIN->html .= $SKIN->start_table( "Current Archives Uploaded" );
		
		if ( count($files) > 0 )
		{
			foreach($files as $file)
			{
			
				$type = array( 'css'   => 'Style Sheet',
							   'image' => 'Image & Macro set',
							   'set'   => 'Skin Set Collection',
							   'wrap'  => 'Board Wrapper',
							   'tmpl'  => 'Template set'
							 );
							 
				$rtype = preg_replace( "/^(css|image|set|wrap|tmpl).+?\.(\S+)$/"  , "\\1", $file );
				
				$rname = preg_replace( "/^(css|image|set|wrap|tmpl)-(.+?)\.(\S+)$/", "\\2", $file );
				
				$rname = preg_replace( "/_/", " ", $rname );
				
				
				$ADMIN->html .= $SKIN->add_td_row( array( "<b>$rname</b>",
														  "<center>{$type[$rtype]}</center>",
														  "<center>$file</center>",
														  "<center><a href='".$SKIN->base_url."&act=import&code=import&type=$rtype&id=$file'>Import</a></center>",
														  "<center><a href='".$SKIN->base_url."&act=import&code=remove&id=$file'>Remove</a></center>",
												 )      );
			}
			
		}
		
		$ADMIN->html .= $SKIN->end_table();
		
		
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->output();
	
	}
	
	function unconvert_tags($t="")
	{
		if ($t == "")
		{
			return "";
		}
		
		$t = preg_replace( "/{ibf\.script_url}/i"   , '{$ibforums->base_url}'         , $t);
		$t = preg_replace( "/{ibf\.session_id}/i"   , '{$ibforums->session_id}'       , $t);
		$t = preg_replace( "/{ibf\.skin\.(\w+)}/"   , '{$ibforums->skin[\''."\\1".'\']}'   , $t);
		$t = preg_replace( "/{ibf\.lang\.(\w+)}/"   , '{$ibforums->lang[\''."\\1".'\']}'   , $t);
		$t = preg_replace( "/{ibf\.vars\.(\w+)}/"   , '{$ibforums->vars[\''."\\1".'\']}'   , $t);
		$t = preg_replace( "/{ibf\.member\.(\w+)}/" , '{$ibforums->member[\''."\\1".'\']}' , $t);
		
		return $t;
		
	}
	
	function rebuild_phpskin($templates_dir, $skins_dir)
	{
	
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		$errors = array();
		
		require $templates_dir."/config.php";
		
		if ( $handle = opendir($templates_dir) )
		{
		
			while (($filename = readdir($handle)) !== false)
			{
				if (($filename != ".") && ($filename != ".."))
				{
					if ( preg_match( "/^index\./", $filename ) )
					{
						continue;
					}
					
					//-------------------------------------------
				
					if ( preg_match( "/\.html$/", $filename ) )
					{
						
						$name = preg_replace( "/\.html$/", "", $filename );
						
						
						if ($FHD = fopen($templates_dir."/".$filename, 'r') )
						{
							$text = fread($FHD, filesize($templates_dir."/".$filename) );
							fclose($FHD);
						}
						else
						{
							$errors[] = "Could not open $filename, skipping file...";
							continue;
						}
						
						//----------------------------------------------------
						
						$need = count($skin[$name]);
						$start = 0;
						$end   = 0;
						
						if ($need < 1)
						{
							$errors[] = "Error recalling function data for $filename, skipping...";
							continue;
						}
	
						// check to make sure the splitter tags are intact
						
						foreach($skin[$name] as $func_name => $data)
						{
							if ( preg_match("/<!--\|IBF\|$func_name\|START\|-->/", $text) )
							{
								$start++;
							}
							
							//+-------------------------------
							
							if ( preg_match("/<!--\|IBF\|$func_name\|END\|-->/", $text) )
							{
								$end++;
							}
						}
						
						if ($start != $end)
						{
							$errors[] = "Some start or end template splitter comments are missing in $filename, skipping file....";
							continue;
						}
						
						if ($start != $need)
						{
							$errors[] = "Some template splitter comments are missing in $filename, skipping file...";
							continue;
						}
						
						//+-------------------------------
						// Convert the tags back to php native
						//+-------------------------------
						
						$text = $this->unconvert_tags($text);
						
						//+-------------------------------
						// Start parsing the php skin file
						//+-------------------------------
						
						$final = "<"."?php\n\n".
								 "class $name {\n\n";
						
						foreach($skin[$name] as $func_name => $data)
						{
						
							$top = "\n\nfunction $func_name($data) {\n".
								   "global \$ibforums;\n".
								   "return <<<EOF\n";
								   
							$bum = "\nEOF;\n}\n";
						
							$text = preg_replace("/\s*<!--\|IBF\|$func_name\|START\|-->\s*\n/", "$top", $text);
							
							
							//+-------------------------------
							
							$text = preg_replace("/\s*<!--\|IBF\|$func_name\|END\|-->\s*\n/", "$bum", $text);
						}
						
						$end = "\n\n}\n?".">";
						
						$final .= $text.$end;
						
						if ($fh = fopen( $skins_dir."/".$name.".php", 'w' ) )
						{
							fwrite($fh, $final, strlen($final) );
							fclose($fh);
							@chmod( $skins_dir."/".$name.".php", 0777 );
						}
						else
						{
							$errors[] = "Could not save information to $phpskin, please ensure that the CHMOD permissions are correct.";
						}
						
						$end   = "";
						$final = "";
						$top   = "";
						
					} // if *.php
					
				} // if not dir
				
			} // while loop
			
			closedir($handle);
			
		}
		else
		{
			$errors[] = "Could not open the templates directory!";
		}
		
		return $errors;
	
	}
	
	
	function import_error( $error, $next_id)
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		$DB->query("DELETE FROM ibf_images WHERE imid='{$next_id['images']}'");
		$DB->query("DELETE FROM ibf_tmpl_names WHERE skid='{$next_id['templates']}'");
		$DB->query("DELETE FROM ibf_css WHERE cssid='{$next_id['css']}'");
		$DB->query("DELETE FROM ibf_templates WHERE tmid='{$next_id['wrap']}'");
		
		@rmdir($INFO['base_dir']."/style_images/".$next_id['images']);
		@rmdir($INFO['base_dir']."/style_templates/".$next_id['templates']);
		@rmdir($INFO['base_dir']."/Skin/s".$next_id['templates']);
		
		$ADMIN->rm_dir($this->work_path."/".$this->new_dir);
		
		$ADMIN->error( $error );
	
	}
	
	
}


?>