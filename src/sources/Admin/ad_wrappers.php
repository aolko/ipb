<?php

/*
+--------------------------------------------------------------------------
|   IBFORUMS v1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001,2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > Help Control functions
|   > Module written by Matt Mecham
|   > Date started: 2nd April 2002
|
|	> Module Version Number: 1.0.0
+--------------------------------------------------------------------------
*/


$idx = new ad_settings();


class ad_settings {

	var $base_url;

	function ad_settings() {
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;

		switch($IN['code'])
		{
			case 'wrapper':
				$this->list_wrappers();
				break;
				
			case 'add':
				$this->do_form('add');
				break;
				
			case 'edit':
				$this->do_form('edit');
				break;
				
			case 'doadd':
				$this->save_wrapper('add');
				break;
				
			case 'doedit':
				$this->save_wrapper('edit');
				break;
				
			case 'remove':
				$this->remove();
				break;
				
			case 'export':
				$this->export();
			
			//-------------------------
			default:
				$this->list_wrappers();
				break;
		}
		
	}
	
	function export()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing wrapper ID, go back and try again");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT * from ibf_templates WHERE tmid='".$IN['id']."'");
		
		if ( ! $row = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the information from the database");
		}
		
		//+-------------------------------
		
		$archive_dir = $INFO['base_dir']."/archive_out";
		
		require $root_dir."sources/lib/tar.php";
		
		if (!is_dir($archive_dir))
		{
			$ADMIN->error("Could not locate $archive_dir, is the directory there?");
		}
		
		if (!is_writeable($archive_dir))
		{
			$ADMIN->error("Cannot write in $archive_dir, CHMOD via FTP to 0755 or 0777 to enable this script to write into it. IBF cannot do this for you");
		}
		
		//+-------------------------------
		// Attempt to copy the files to the
		// working directory...
		//+-------------------------------
		
		$l_name = preg_replace( "/\s{1,}/", "_", $row['name'] );
		
		$file_name = "wrap-".$l_name.".html";
		
		$row['template'] = preg_replace("/\r/", "\n", $row['template'] );
		
		$FH = fopen($archive_dir."/".$file_name, 'w');
		fwrite($FH, $row['template'], strlen($row['template']));
		fclose($FH);
		
		$ADMIN->done_screen("Wrapper Export Created<br><br>You can download the html resource <a href='archive_out/$file_name' target='_blank'>here</a>", "Manage Board Wrappers", "act=wrap" );
		
		
	}
	
	
	//-------------------------------------------------------------
	// REMOVE WRAPPERS
	//-------------------------------------------------------------
	
	function remove()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		//+-------------------------------
		
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing wrapper ID, go back and try again");
		}
		
		$DB->query("DELETE FROM ibf_templates WHERE tmid='".$IN['id']."'");
		
		$std->boink_it($SKIN->base_url."&act=wrap");
			
		exit();
		
		
	}
	
	
	
	//-------------------------------------------------------------
	// ADD / EDIT WRAPPERS
	//-------------------------------------------------------------
	
	function save_wrapper( $type='add' )
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		//+-------------------------------
		
		if ($type == 'edit')
		{
			if ($IN['id'] == "")
			{
				$ADMIN->error("You must specify an existing wrapper ID, go back and try again");
			}
		}
		
		if ($IN['name'] == "")
		{
			$ADMIN->error("You must specify a name for this wrapper");
		}
		
		if ($IN['template'] == "")
		{
			$ADMIN->error("You can't have an empty template, can you?");
		}
		
		$tmpl = preg_replace( "!&lt/textarea>!", "/textarea>", stripslashes($HTTP_POST_VARS['template']) );
		
		if ( ! preg_match( "/<% BOARD %>/", $tmpl ) )
		{
			$ADMIN->error("You cannot remove the &lt% BOARD %> tag silly!");
		}
		
		if ( ! preg_match( "/<% COPYRIGHT %>/", $tmpl ) )
		{
			$ADMIN->error("You cannot remove the &lt% COPYRIGHT %> tag silly!");
		}
		
		$barney = array( 'name'     => stripslashes($HTTP_POST_VARS['name']),
						 'template' => $tmpl
					   );
					   
		if ($type == 'add')
		{
			$db_string = $DB->compile_db_insert_string( $barney );
			
			$DB->query("INSERT INTO ibf_templates (".$db_string['FIELD_NAMES'].") VALUES(".$db_string['FIELD_VALUES'].")");
			
			$std->boink_it($SKIN->base_url."&act=wrap");
			
			exit();
			
		}
		else
		{
			$db_string = $DB->compile_db_update_string( $barney );
			
			$DB->query("UPDATE ibf_templates SET $db_string WHERE tmid='".$IN['id']."'");
			
			$ADMIN->done_screen("Wrapper updated", "Manage Board Wrappers", "act=wrap" );
		}
		
		
	}
	
	//-------------------------------------------------------------
	// ADD / EDIT WRAPPERS
	//-------------------------------------------------------------
	
	function do_form( $type='add' )
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		//+-------------------------------
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing wrapper ID, go back and try again");
		}
		
		$DB->query("SELECT * from ibf_templates WHERE tmid='".$IN['id']."'");
		
		if ( ! $row = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the information from the database");
		}
		
		if ($type == 'add')
		{
			$code = 'doadd';
			$button = 'Create Wrapper';
			$row['name'] = $row['name'].".2";
		}
		else
		{
			$code = 'doedit';
			$button = 'Edit Wrapper';
		}
		
		//+-------------------------------
	
		$ADMIN->page_detail = "You may use HTML fully when adding or editing wrappers.";
		$ADMIN->page_title  = "Manage Board Wrappers";
		
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->js_no_specialchars();
		
		$ADMIN->html .= $SKIN->start_form( array( 1 => array( 'code'  , $code      ),
												  2 => array( 'act'   , 'wrap'      ),
												  3 => array( 'id'    , $IN['id']   ),
									     ), "theAdminForm", "onSubmit=\"return no_specialchars('wrapper')\""      );
									     
		//+-------------------------------
		
		$SKIN->td_header[] = array( "&nbsp;"  , "20%" );
		$SKIN->td_header[] = array( "&nbsp;"  , "80%" );

		//+-------------------------------
		
		$row['template'] = preg_replace( "/\/textarea>/", "&lt/textarea>", $row['template'] );  // Stop html killing the text area
		
		$ADMIN->html .= $SKIN->start_table( $button );
		
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"Wrapper Title",
													$SKIN->form_input('name', $row['name']),
									     )      );
									     
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"Content",
													$SKIN->form_textarea('template', $row['template'], "60", "15"),
									     )      );
												 
		$ADMIN->html .= $SKIN->end_form($button);
										 
		$ADMIN->html .= $SKIN->end_table();
		
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->output();
		
		
	}
	
	//-------------------------------------------------------------
	// SHOW WRAPPERS
	//-------------------------------------------------------------
	
	function list_wrappers()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		$form_array = array();
	
		$ADMIN->page_detail = "You may add/edit and remove the board wrapper templates.<br><br>Board wrapper refers to the main board template which allows additional HTML to be added to the header and footer of the board";
		$ADMIN->page_title  = "Manage Board Wrappers";
		
		//+-------------------------------
		
		$SKIN->td_header[] = array( "Title"        , "40%" );
		$SKIN->td_header[] = array( "Allocation"   , "30%" );
		$SKIN->td_header[] = array( "Export"       , "10%" );
		$SKIN->td_header[] = array( "Edit"         , "10%" );
		$SKIN->td_header[] = array( "Remove"       , "10%" );
		
		//+-------------------------------
		
		$DB->query("SELECT DISTINCT(w.tmid), w.name, s.sname from ibf_templates w, ibf_skins s WHERE s.tmpl_id=w.tmid ORDER BY w.name ASC");
		
		$used_ids = array();
		$show_array = array();
		
		if ( $DB->get_num_rows() )
		{
		
			$ADMIN->html .= $SKIN->start_table( "Current Wrappers In Use" );
			
			while ( $r = $DB->fetch_row() )
			{
			
				$show_array[ $r['tmid'] ] .= stripslashes($r['sname'])."<br>";
			
				if ( in_array( $r['tmid'], $used_ids ) )
				{
					continue;
				}
				
				$ADMIN->html .= $SKIN->add_td_row( array( "<b>".stripslashes($r['name'])."</b>",
														  "<#X-{$r['tmid']}#>",
														  "<center><a href='".$SKIN->base_url."&act=wrap&code=export&id={$r['tmid']}'>Export</a></center>",
														  "<center><a href='".$SKIN->base_url."&act=wrap&code=edit&id={$r['tmid']}'>Edit</a></center>",
														  "<i>Deallocate before removing</i>",
												 )      );
												   
				$used_ids[] = $r['tmid'];
				
				$form_array[] = array( $r['tmid'], $r['name'] );
				
			}
			
			foreach( $show_array as $idx => $string )
			{
				$string = preg_replace( "/<br>$/", "", $string );
				
				$ADMIN->html = preg_replace( "/<#X-$idx#>/", "$string", $ADMIN->html );
			}
			
			$ADMIN->html .= $SKIN->end_table();
		}
		
		if ( count($used_ids) > 0 )
		{
		
			$DB->query("SELECT tmid, name FROM ibf_templates WHERE tmid NOT IN(".implode(",",$used_ids).")");
		
			if ( $DB->get_num_rows() )
			{
			
				$SKIN->td_header[] = array( "Title"  , "70%" );
				$SKIN->td_header[] = array( "Export" , "10%" );
				$SKIN->td_header[] = array( "Edit"   , "10%" );
				$SKIN->td_header[] = array( "Remove" , "10%" );
			
				$ADMIN->html .= $SKIN->start_table( "Current Unallocated Wrappers" );
				
				$ADMIN->html .= $SKIN->js_checkdelete();
				
				
				while ( $r = $DB->fetch_row() )
				{
					
					$ADMIN->html .= $SKIN->add_td_row( array( "<b>".stripslashes($r['name'])."</b>",
															  "<center><a href='".$SKIN->base_url."&act=wrap&code=export&id={$r['tmid']}'>Export</a></center>",
															  "<center><a href='".$SKIN->base_url."&act=wrap&code=edit&id={$r['tmid']}'>Edit</a></center>",
															  "<center><a href='javascript:checkdelete(\"act=wrap&code=remove&id={$r['tmid']}\")'>Remove</a></center>",
													 )      );
													 
					$form_array[] = array( $r['tmid'], $r['name'] );
													   
				}
				
				$ADMIN->html .= $SKIN->end_table();
			}
		}
		
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->start_form( array( 1 => array( 'code'  , 'add'     ),
												  2 => array( 'act'   , 'wrap'    ),
									     )      );
		
		$SKIN->td_header[] = array( "&nbsp;"  , "40%" );
		$SKIN->td_header[] = array( "&nbsp;"  , "60%" );
		
		$ADMIN->html .= $SKIN->start_table( "Create New Wrapper" );
			
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->add_td_row( array( "<b>Base new wrapper on...</b>" ,
										  		  $SKIN->form_dropdown( "id", $form_array)
								 )      );
		
		$ADMIN->html .= $SKIN->end_form("Create new wrapper");
										 
		$ADMIN->html .= $SKIN->end_table();
		
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->output();
	
	}
	
	
}


?>