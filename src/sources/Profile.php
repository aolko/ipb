<?php

/*
+--------------------------------------------------------------------------
|   IBFORUMS v1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001,2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > User Profile functions
|   > Module written by Matt Mecham
|   > Date started: 28th February 2002
|
|	> Module Version Number: 1.0.0
+--------------------------------------------------------------------------
*/


$idx = new Profile;

class Profile {

    var $output     = "";
    var $page_title = "";
    var $nav        = array();
    var $html       = "";
    var $parser;

    var $member     = array();
    var $m_group    = array();
    
    var $jump_html  = "";
    var $parser     = "";
    
    var $links      = array();
    
    var $bio        = "";
    var $notes      = "";
    var $size       = "m";
    
    var $lib;
    
    function Profile() {
    	global $ibforums, $DB, $std, $print;
    
    	require "./sources/lib/post_parser.php";
        
        $this->parser = new post_parser();
        
    	//--------------------------------------------
    	// Make sure our code number is numerical only
    	//--------------------------------------------
    	
    	//$ibforums->input['CODE'] = preg_replace("/^([0-9]+)$/", "$1", $ibforums->input['CODE']);
    	
    	if ($ibforums->input['CODE'] == "") $ibforums->input['CODE'] = 00;
    	
    	//--------------------------------------------
    	// Require the HTML and language modules
    	//--------------------------------------------
    	
    	$ibforums->lang = $std->load_words($ibforums->lang, 'lang_profile'  , $ibforums->lang_id );
    	
    	require "./Skin/".$ibforums->skin_id."/skin_profile.php";
    	$this->html = new skin_profile;
    	
    	$this->base_url        = "{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}";
    	$this->base_url_nosess = "{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}";
    	
    	//--------------------------------------------
    	// Check viewing permissions, etc
		//--------------------------------------------
		
		$this->member  = $ibforums->member;
		$this->m_group = $ibforums->member;
		
    	
    	//--------------------------------------------
    	// What to do?
    	//--------------------------------------------
    	
    	
    	switch($ibforums->input['CODE']) {
    		case '03':
    			$this->view_profile();
    			break;
    		
    		//------------------------------
    		default:
    			$this->view_profile();
    			break;
    	}
    	
    	// If we have any HTML to print, do so...
    	
    	
    	$print->add_output("$this->output");
        $print->do_output( array( 'TITLE' => $this->page_title, 'JS' => 1, NAV => $this->nav ) );
    		
 	}
 	
 	function view_profile() {
 		global $ibforums, $DB, $std, $print;
 		
 		$info = array();
 		
 		if ($ibforums->member['g_mem_info'] != 1)
 		{
 			$std->Error( array( 'LEVEL' => 1, 'MSG' => 'no_permission' ) );
    	}
 		
 		//--------------------------------------------
    	// Check input..
    	//--------------------------------------------
    	
    	$id = preg_replace( "/^(\d+)$/", "\\1", $ibforums->input['MID'] );
    	
    	if ( empty($id) )
    	{
    		$std->Error( array( 'LEVEL' => 1, 'MSG' => 'incorrect_use' ) );
    	}
    	
    	//--------------------------------------------
    	// Prepare Query...
    	//--------------------------------------------
    	
    	$DB->query("SELECT m.*, g.g_id, g.g_title as group_title FROM ibf_members m, ibf_groups g WHERE m.id='$id' and m.mgroup=g.g_id");
    	$member = $DB->fetch_row();
    	
    	if (empty($member['id']))
    	{
    		$std->Error( array( 'LEVEL' => 1, 'MSG' => 'incorrect_use' ) );
    	}
    	
    	// Play it safe
    	
    	$member['password'] = "";
    	
    	//--------------------------------------------
    	// Find the most posted in forum that the viewing
    	// member has access to by this members profile
    	//--------------------------------------------
    	
    	$DB->query("SELECT id, read_perms FROM ibf_forums");
    	
    	$forum_ids = array('0');
    	
    	while ( $r = $DB->fetch_row() )
    	{
    		if ($r['read_perms'] == '*')
    		{
    			$forum_ids[] = $r['id'];
    		}
    		else if ( preg_match( "/(^|,)".$this->member['mgroup']."(,|$)/", $r['read_perms']) )
    		{
    			$forum_ids[] = $r['id'];
    		}
    	}
    	
    	$forum_id_str = implode( ",", $forum_ids );
    	
    	$percent = 0;
    	
    	$DB->query("SELECT DISTINCT(p.forum_id), f.name, COUNT(p.author_id) as f_posts FROM ibf_posts p, ibf_forums f ".
    			   "WHERE p.forum_id IN ($forum_id_str) AND p.author_id='".$member['id']."' AND p.forum_id=f.id GROUP BY p.forum_id ORDER BY f_posts DESC");
    			   
    	$favourite   = $DB->fetch_row();
    	
    	$DB->query("SELECT COUNT(pid) as total_posts FROM ibf_posts WHERE author_id='".$member['id']."'");
    	
    	$total_posts = $DB->fetch_row();
    	
    	$DB->query("SELECT TOTAL_TOPICS, TOTAL_REPLIES FROM ibf_stats");
    	
    	$stats = $DB->fetch_row();
    	
    	$board_posts = $stats['TOTAL_TOPICS'] + $stats['TOTAL_REPLIES'];
    	
    	if ($total_posts['total_posts'] > 0)
    	{
    		$percent = round( $favourite['f_posts'] / $total_posts['total_posts'] * 100 );
    	}
    	
    	if ($member['posts'])
    	{
    		$info['posts_day'] = round( $member['posts'] / (((time() - $member['joined']) / 86400)), 1);
    		$info['total_pct'] = sprintf( '%.2f', ( $member['posts'] / $board_posts * 100 ) );
    	}
    	
    	if ($info['posts_day'] > $member['posts'])
    	{
    		$info['posts_day'] = $member['posts'];
    	}
    	
    	$info['posts']       = $member['posts'] ? $member['posts'] : 0;
    	$info['name']        = $member['name'];
    	$info['mid']         = $member['id'];
    	$info['fav_forum']   = $favourite['name'];
    	$info['fav_id']      = $favourite['forum_id'];
    	$info['fav_posts']   = $favourite['f_posts'];
    	$info['percent']     = $percent;
    	$info['group_title'] = $member['group_title'];
    	$info['board_posts'] = $board_posts;
    	$info['joined']      = $std->get_date( $member['joined'], 'LONG' );
    	
    	$info['member_title'] = $member['title']     ? $member['title']      : $ibforums->lang['no_info'];
    	
    	$info['aim_name']    = $member['aim_name']   ? $member['aim_name']   : $ibforums->lang['no_info'];
    	$info['icq_number']  = $member['icq_number'] ? $member['icq_number'] : $ibforums->lang['no_info'];
    	$info['yahoo']       = $member['yahoo']      ? $member['yahoo']      : $ibforums->lang['no_info'];
    	$info['location']    = $member['location']   ? $member['location']   : $ibforums->lang['no_info'];
    	$info['interests']   = $member['interests']  ? $member['interests']  : $ibforums->lang['no_info'];
    	$info['msn_name']    = $member['msnname']    ? $member['msnname']    : $ibforums->lang['no_info'];
    	
    	$ibforums->vars['time_adjust'] = $ibforums->vars['time_adjust'] == "" ? 0 : $ibforums->vars['time_adjust'];
    	
    	if ($member['dst_in_use'] == 1)
    	{
    		$member['time_offset'] += 1;
    	}
    	
    	$info['local_time']  = $member['time_offset'] != "" ? gmdate( "h:i A", time() + ($member['time_offset']*3600) + ($ibforums->vars['time_adjust'] * 60) ) : $ibforums->lang['no_info'];
    	
    	$info['avatar']      = $std->get_avatar( $member['avatar'] , 1, $member['avatar_size'] );
    	
    	$info['signature']   = $member['signature'];
    	
    	if ( $member['website'] and preg_match( "/^http:\/\/\S+$/", $member['website'] ) ) {
			$info['homepage'] = "<a href='{$member['website']}' target='_blank'>{$member['website']}</a>";
		}
		else
		{
			$info['homepage'] = $ibforums->lang['no_info'];
		}
		
    	
    	if ($member['bday_month'])
    	{
    		$info['birthday'] = $member['bday_day']." ".$ibforums->lang[ 'M_'.$member['bday_month'] ]." ".$member['bday_year'];
    	}
    	else
    	{
    		$info['birthday'] = $ibforums->lang['no_info'];
    	}
    	
    	
    	if (!$member['hide_email']) {
			$info['email'] = "<a href='{$this->base_url}&act=Mail&CODE=00&MID={$member['id']}'>{$ibforums->lang['click_here']}</a>";
		}
		else
		{
			$info['email'] = $ibforums->lang['private'];
		}
    	
    	$info['base_url']    = $this->base_url;
    	
    	$this->output .= $this->html->show_profile( $info );
    	
    	// Is this our profile?
    	
    	if ($member['id'] == $this->member['id'])
    	{
    		$this->output = preg_replace( "/<!--MEM OPTIONS-->/e", "\$this->html->user_edit(\$info)", $this->output );
    	}
    	
    	// Can mods edit this profile?
    	
    	if ($ibforums->member['id'])
        {
        	if ($ibforums->member['g_is_supmod'] == 1)
        	{
        		$pass = 1;
        	}
        	else
        	{
        		$DB->query("SELECT * FROM ibf_moderators WHERE edit_user=1 AND member_id='".$ibforums->member['id']."'");
        		if ( $this->moderator = $DB->fetch_row() )
        		{
        			$pass = 1;
        		}
        	}
        }
    	
    	
    	
 		$this->page_title = $ibforums->lang['page_title'];
 		$this->nav        = array( $ibforums->lang['page_title'] );
 		
 	}
 	
 	
 	
}

?>