<?php

$lang = array (

send					=>	"Send report now",
message					=>	"Your message",
title					=>	"Report a post to a moderator",
redirect					=>	"Report sent, returning you to the topic you came from",
in_topic					=>	"Topic Title",
subject					=>	"Mailer from Invision Board: Reporting post",
in_forum					=>	"Reported in forum",
ibauto					=>	"Invision Board Bot",
warn					=>	"This form is for reporting abusive behaviour (spamming, rude posts, etc) ONLY.<br>DO NOT use this form just to contact a moderator - these messages will be ignored!",
email					=>	"Your email",
name					=>	"Your name",
);
?>