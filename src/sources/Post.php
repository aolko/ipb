<?php

/*
+--------------------------------------------------------------------------
|   IBFORUMS v1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001,2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > Post core module
|   > Module written by Matt Mecham
|   > Date started: 14th February 2002
|
|   > Module Version 1.0.0
+--------------------------------------------------------------------------
*/


$idx = new Post;

class Post {

    var $output    = "";
    var $base_url  = "";
    var $html      = "";
    var $parser    = "";
    var $moderator = array();
    var $forum     = array();
    var $topic     = array();
    var $category  = array();
    var $mem_groups = array();
    var $mem_titles = array();
    var $obj        = array();
    var $email      = "";
    
    /***********************************************************************************/
	//
	// Our constructor, load words, load skin, print the topic listing
	//
	/***********************************************************************************/
    
    function Post() {
    
    
        global $ibforums, $DB, $std, $print, $skin_universal;
        
        require "./Skin/".$ibforums->skin_id."/skin_post.php";
        
        require "./sources/lib/post_parser.php";
        
        $this->parser = new post_parser(1);
        

        //--------------------------------------
		// Compile the language file
		//--------------------------------------
		
        $ibforums->lang = $std->load_words($ibforums->lang, 'lang_post', $ibforums->lang_id);

        $this->html     = new skin_post();
        
        //--------------------------------------
        // Check the input
        //--------------------------------------
        
        if ($ibforums->input['t'])
        {
        	$ibforums->input['t'] = $std->is_number($ibforums->input['t']);
        	if (! $ibforums->input['t'] )
        	{
        		$std->Error( array( LEVEL => 1, MSG => 'missing_files') );
        	}
        }
        
        if ($ibforums->input['p'])
        {
        	$ibforums->input['p'] = $std->is_number($ibforums->input['p']);
        	if (! $ibforums->input['p'] )
        	{
        		$std->Error( array( LEVEL => 1, MSG => 'missing_files') );
        	}
        }
        
        $ibforums->input['f'] = $std->is_number($ibforums->input['f']);
        if (! $ibforums->input['f'] )
        {
        	$std->Error( array( LEVEL => 1, MSG => 'missing_files') );
        }
        
        $ibforums->input['st'] = $ibforums->input['st'] ? $std->is_number($ibforums->input['st']) : 0;
        
        // Did the user press the "preview" button?
        
        $this->obj['preview_post'] = $ibforums->input['preview'];
        
        
        //--------------------------------------
        // Get the forum info based on the forum ID, get the category name, ID, and get the topic details
        //--------------------------------------
        
        $DB->query("SELECT f.*, c.id as cat_id, c.name as cat_name from ibf_forums f, ibf_categories c WHERE f.id=".$ibforums->input[f]." and c.id=f.category");
        
        $this->forum = $DB->fetch_row();
        
        if ($this->forum['read_perms'] != '*')
        {
			if (! preg_match("/(^|,)".$ibforums->member['mgroup']."(,|$)/", $this->forum['read_perms'] ) )
			{
				$std->Error( array( LEVEL => 1, MSG => 'no_view_topic') );
			}
        
        }
        
        // Is this forum switched off?
        
        if ( ! $this->forum['status'] )
        {
        	$std->Error( array( LEVEL => 1, MSG => 'forum_read_only') );
        }
        
        //--------------------------------------
        // Is this a password protected forum?
        //--------------------------------------
        
        $pass = 0;
		
		if ($this->forum['password'] != "")
		{
			if ( ! $c_pass = $std->my_getcookie('iBForum'.$this->forum['id']) )
			{
				$pass = 0;
			}
		
			if ( $c_pass == $this->forum['password'] )
			{
				$pass = 1;
			}
			else
			{
			    $pass = 0;
			}
		}
		else
		{
			$pass = 1;
		}
		
		if ($pass == 0)
		{
			$std->Error( array( LEVEL => 1, MSG => 'no_view_topic') );
		}
		
		//--------------------------------------
        
        if ($this->forum['parent_id'] > 0)
        {
        
        	$DB->query("SELECT f.id as forum_id, f.name as forum_name, c.id, c.name FROM ibf_forums f, ibf_categories c WHERE f.id='".$this->forum['parent_id']."' AND c.id=f.category");
        	
        	$row = $DB->fetch_row();
        	
        	$this->forum['cat_id']   = $row['id'];
        	$this->forum['cat_name'] = $row['name'];
        	
        }
        
        
        //--------------------------------------
        // Error out if we can not find the forum
        //--------------------------------------
        
        if (!$this->forum['id'])
        {
        	$std->Error( array( LEVEL => 1, MSG => 'missing_files') );
        }
        
        $this->base_url = "{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}";
        
        //--------------------------------------
        // Is this forum moderated?
        //--------------------------------------
        
        $this->obj['moderate'] = $this->forum['preview_posts'] ? 1 : 0;
        // Can we bypass it?
        if ($ibforums->member['g_avoid_q'])
        {
        	$this->obj['moderate'] = 0;
        }
        
        //--------------------------------------
        // Are we allowed to post at all?
        //--------------------------------------
        
        if ($ibforums->member['id'])
        {
        	if (!$ibforums->member['allow_post'])
        	{
        		$std->Error( array( LEVEL => 1, MSG => 'posting_off') );
        	}
        	
        	// Flood check..
        	
        	if ( $ibforums->input['CODE'] != "08" and $ibforums->input['CODE'] != "09" )
        	{
				if ( $ibforums->vars['flood_control'] > 0 )
				{
					if ($ibforums->member['g_avoid_flood'] != 1)
					{
						if ( time() - $ibforums->member['last_post'] < $ibforums->vars['flood_control'] )
						{
							$std->Error( array( 'LEVEL' => 1, 'MSG' => 'flood_control' , 'EXTRA' => $ibforums->vars['flood_control'] ) );
						}
					}
				}
				
			}
        	
        }
        
        if ($ibforums->member['id'] != 0 and $ibforums->member['g_is_supmod'] == 0)
        {
        	$DB->query("SELECT * from ibf_moderators WHERE member_id='".$ibforums->member['id']."' AND forum_id='".$this->forum['id']."'");
        	$this->moderator = $DB->fetch_row();
        }
        
        //--------------------------------------
        // Convert the code ID's into something
        // use mere mortals can understand....
        //--------------------------------------
        
        $this->obj['action_codes'] = array ( '00'  => array( '0'  , 'new_post'     ),
        									 '01'  => array( '1'  , 'new_post'     ),
        									 '02'  => array( '0'  , 'reply_post'   ),
        									 '03'  => array( '1'  , 'reply_post'   ),
        									 '06'  => array( '0'  , 'q_reply_post' ),
        									 '07'  => array( '1'  , 'q_reply_post' ),
        									 '08'  => array( '0'  , 'edit_post'    ),
        									 '09'  => array( '1'  , 'edit_post'    ),
        									 '10'  => array( '0'  , 'poll'         ),
        									 '11'  => array( '1'  , 'poll'         ),
        								   );
        								   
        // Make sure our input CODE element is legal.
        
        if (! isset($this->obj['action_codes'][ $ibforums->input['CODE'] ]) )
        {
        	$std->Error( array( LEVEL => 1, MSG => 'missing_files') );
        }
        
        // Require and run our associated library file for this action.
        // this imports an extended class for this Post class.
        
        require "./sources/lib/post_" . $this->obj['action_codes'][ $ibforums->input['CODE'] ][1] . ".php";
        
        $post_functions = new post_functions(&$this);
        
        // If the first CODE array bit is set to "0" - show the relevant form.
        // If it's set to "1" process the input.
        
        // We pass a reference to this classes object so we can manipulate this classes
        // data from our sub class.
        
        if ($this->obj['action_codes'][ $ibforums->input['CODE'] ][0])
        {
        	// Make sure we have a "Guest" Name..
        	
        	if (!$ibforums->member['id'])
        	{
        	
        		$ibforums->input['UserName'] = trim($ibforums->input['UserName']);
        		$ibforums->input['UserName'] = str_replace( "<br>", "", $ibforums->input['UserName']);
        		$ibforums->input['UserName'] = $ibforums->input['UserName'] ? $ibforums->input['UserName'] : 'Guest';
        		
        		if ($ibforums->input['UserName'] != 'Guest')
        		{
        			$DB->query("SELECT id FROM ibf_members WHERE LOWER(name)='".trim(strtolower($ibforums->input['UserName']))."'");
        			
        			if ( $DB->get_num_rows() )
        			{
        				$ibforums->input['UserName'] = $ibforums->vars['guest_name_pre'].$ibforums->input['UserName'].$ibforums->vars['guest_name_suf'];
        			}
        		}
        		
        	}
        	
        	//-------------------------------------------------------------------------
        	// Stop the user hitting the submit button in the hope that multiple topics
        	// or replies will be added. Or if the user accidently hits the button
        	// twice.
        	//-------------------------------------------------------------------------
        	
        	if ( $this->obj['preview_post'] == "" )
        	{
        	
				if ( preg_match( "/Post,.*,(01|03|07|11)$/", $ibforums->location ) )
				{
					if ( time() - $ibforums->lastclick < 2 )
					{
						if ( $ibforums->input['CODE'] == '01' or $ibforums->input['CODE'] == '11' )
						{
							// Redirect to the newest topic in the forum
							
							
							$DB->query("SELECT tid from ibf_topics WHERE forum_id='".$this->forum['id']."' AND approved=1 "
									  ."ORDER BY last_post DESC LIMIT 0,1");
									  
							$topic = $DB->fetch_row();
					
							$std->boink_it($ibforums->base_url."&act=ST&f=".$this->forum['id']."&t=".$topic['tid']);
							exit();
						}
						else
						{
							// It's a reply, so simply show the topic...
							
							$std->boink_it($ibforums->base_url."&act=ST&f=".$this->forum['id']."&t=".$ibforums->input['t']."&view=getlastpost");
							exit();
						}
					}
				}
        	
        	}
        	
        	//----------------------------------
       
        	$post_functions->process(&$this);
        }
        else
        {
        	$post_functions->show_form(&$this);
        }
        
        

	}
	
	/*****************************************************/
	// topic tracker
	// ------------------
	// Checks and sends out the emails as needed.
	/*****************************************************/
	
	function topic_tracker($tid="", $post="", $poster="")
	{
		global $ibforums, $DB, $std;
		
		require "./sources/lib/emailer.php";
		
		$this->email = new emailer();
		
		//--------------------------
	
		if ($tid == "")
		{
			return TRUE;
		}
	
		// Configure the date
		$getdate = time() - (60*60*24);
		
		// Get the email addy's, topic ids and email_full stuff - oh yeah.
		
		$DB->query("SELECT tr.trid, tr.topic_id, m.name, m.email, m.id, m.email_full, m.language, t.title, t.forum_id FROM ibf_tracker tr, ibf_topics t,ibf_members m WHERE tr.topic_id='$tid' AND tr.member_id=m.id AND t.tid=tr.topic_id AND (tr.last_sent < '$getdate' or tr.last_sent=0)");
		
		if (  $DB->get_num_rows() )
		{
			$trids = array();
			
			while ( $r = $DB->fetch_row() )
			{
			
				$r['language'] = $r['language'] ? $r['language'] : 'en';
				
				// We don't want to be notified of our own posts, so..
				
				if ($r['id'] == $ibforums->member['id'])
				{
					continue;
				}
				
				//-----------------------------------------------------
				
				if ($r['email_full'] == 1)
				{
				
					$this->email->get_template("subs_with_post", $r['language']);
			
					$this->email->build_message( array(
														'TOPIC_ID'        => $r['topic_id'],
														'FORUM_ID'        => $r['forum_id'],
														'TITLE'           => $r['title'],
														'NAME'            => $r['name'],
														'POSTER'          => $poster,
														'POST'            => $post,
													  )
												);
												
					$this->email->subject = $ibforums->lang['tt_subject'];
					$this->email->to      = $r['email'];
					$this->email->send_mail();
					
				}
				else
				{
				
					$this->email->get_template("subs_no_post", $r['language']);
			
					$this->email->build_message( array(
														'TOPIC_ID'        => $r['topic_id'],
														'FORUM_ID'        => $r['forum_id'],
														'TITLE'           => $r['title'],
														'NAME'            => $r['name'],
														'POSTER'          => $poster,
													  )
												);
												
					$this->email->subject = $ibforums->lang['tt_subject'];
					$this->email->to      = $r['email'];
					
					$this->email->send_mail();
					
				}
				
				$trids[] = $r['trid'];
			}
			
			// Update the DB wid' da new time, innit.
			
			$trid_string = implode( ",", $trids );
			
			if ( count($trids) > 0 )
			{
			
				$DB->query("UPDATE ibf_tracker SET last_sent='".time()."' WHERE trid IN(".$trid_string.")");
			}
			
		}
		
		return TRUE;
		
	}
	
	/*****************************************************/
	// compile post
	// ------------------
	// Compiles all the incoming information into an array
	// which is returned to the accessor
	/*****************************************************/
	
	function compile_post() {
		global $ibforums, $std, $REQUEST_METHOD, $HTTP_POST_VARS;
		
		$ibforums->vars['max_post_length'] = $ibforums->vars['max_post_length'] ? $ibforums->vars['max_post_length'] : 2140000;
		
		// sort out some of the form data, check for posting length, etc.
		// THIS MUST BE CALLED BEFORE CHECKING ATTACHMENTS
		
		$ibforums->input['enablesig'] = $ibforums->input['enablesig'] == 'yes' ? 1 : 0;
		$ibforums->input['enableemo'] = $ibforums->input['enableemo'] == 'yes' ? 1 : 0;
		
		// Do we have a valid post?
		
		if (strlen( trim($HTTP_POST_VARS['Post']) ) < 1) {
			$std->Error( array( LEVEL => 1, MSG => 'no_post') );
		}
		
		if (strlen( $HTTP_POST_VARS['Post'] ) > ($ibforums->vars['max_post_length']*1024)) {
			$std->Error( array( LEVEL => 1, MSG => 'post_too_long') );
		}
		
		$post = array(
						'author_id'   => $ibforums->member['id'] ? $ibforums->member['id'] : 0,
						'use_sig'     => $ibforums->input['enablesig'],
						'use_emo'     => $ibforums->input['enableemo'],
						'ip_address'  => $ibforums->input['IP_ADDRESS'],
						'post_date'   => time(),
						'icon_id'     => $ibforums->input['iconid'],
						'post'        => $this->parser->convert( array( TEXT    => $ibforums->input['Post'],
																		SMILIES => $ibforums->input['enableemo'],
																		CODE    => $this->forum['use_ibc'],
																		HTML    => $this->forum['use_html']
																	  )
															   ),
						'author_name' => $ibforums->member['id'] ? $ibforums->member['name'] : $ibforums->input['UserName'],
						'forum_id'    => $this->forum['id'],
						'topic_id'    => "",
						'queued'      => $this->obj['moderate'],
						'attach_id'   => "",
						'attach_hits' => "",
						'attach_type' => "",
					 );
					 
	    // If we had any errors, parse them back to this class
	    // so we can track them later.
	    
	    $this->obj['post_errors'] = $this->parser->error;
					 
		return $post;
	}
	
    /*****************************************************/
	// process upload
	// ------------------
	// checks for an entry in the upload field, and uploads
	// the file if it meets our criteria. This also inserts
	// a new row into the attachments database if successful
	/*****************************************************/
	
	function process_upload() {
	
		global $ibforums, $std, $HTTP_POST_FILES, $DB, $FILE_UPLOAD;
		
		//-------------------------------------------------
		// Set up some variables to stop carpals developing
		//-------------------------------------------------
		
		$FILE_NAME = $HTTP_POST_FILES['FILE_UPLOAD']['name'];
		$FILE_SIZE = $HTTP_POST_FILES['FILE_UPLOAD']['size'];
		$FILE_TYPE = $HTTP_POST_FILES['FILE_UPLOAD']['type'];
		
		// Naughty Opera adds the filename on the end of the
		// mime type - we don't want this.
		
		$FILE_TYPE = preg_replace( "/^(.+?);.*$/", "\\1", $FILE_TYPE );
		
		$attach_data = array( 'attach_id'   => "",
							  'attach_hits' => "",
							  'attach_type' => "",
							  'attach_file' => "",
							);
							
		//-------------------------------------------------					
		// Return if we don't have a file to upload
		//-------------------------------------------------
		
		// Naughty Mozilla likes to use "none" to indicate an empty upload field.
		// I love universal languages that aren't universal.
		
		if ($HTTP_POST_FILES['FILE_UPLOAD']['name'] == "" or !$HTTP_POST_FILES['FILE_UPLOAD']['name'] or ($HTTP_POST_FILES['FILE_UPLOAD']['name'] == "none") ) return $attach_data;
		
		//-------------------------------------------------
		// Return empty handed if we don't have permission to use
		// uploads
		//-------------------------------------------------
		
		if ( (!$this->forum['use_attach']) and ($ibforums->member['g_attach_max'] < 1) ) return $attach_data;
		
		//-------------------------------------------------
		// Load our mime types config file.
		//-------------------------------------------------
		
		require "./conf_mime_types.php";
		
		//-------------------------------------------------
		// Are we allowing this type of file?
		//-------------------------------------------------
		
		if ($mime_types[ $FILE_TYPE ][0] != 1)
		{
			$this->obj['post_errors'] = 'invalid_mime_type';
			return $attach_data;
		}
		
		//-------------------------------------------------
		// Check the file size
		//-------------------------------------------------
		
		if ($FILE_SIZE > ($ibforums->member['g_attach_max']*1024))
		{
			$std->Error( array( LEVEL => 1, MSG => 'upload_to_big') );
		}
		
		//-------------------------------------------------
		// Make the uploaded file safe
		//-------------------------------------------------
		
		$FILE_NAME = preg_replace( "/[^\w\.]/", "_", $FILE_NAME );
		
		$real_file_name = "post-".$this->forum['id']."-".time();  // Note the lack of extension!
		
		if (preg_match( "/\.(cgi|pl|js|asp|php|html|htm|jsp|jar)/", $FILE_NAME ))
		{
			$FILE_TYPE = 'text/plain';
		}
		
		//-------------------------------------------------
		// Add on the extension...
		//-------------------------------------------------
		
		$ext = '.ibf';
		
		switch($FILE_TYPE)
		{
			case 'image/gif':
				$ext = '.gif';
				break;
			case 'image/jpeg':
				$ext = '.jpg';
				break;
			case 'image/pjpeg':
				$ext = '.jpg';
				break;
			case 'image/x-png':
				$ext = '.png';
				break;
			default:
				$ext = '.ibf';
				break;
		}
		
		$real_file_name .= $ext;
		
		//-------------------------------------------------
		// If we are previewing the post, we don't want to
		// add the attachment to the database, so we return
		// the array with the filename. We would have returned
		// earlier if there was an error
		//-------------------------------------------------
		
		if ($this->obj['preview_post'])
		{
			return array( 'FILE_NAME' => $FILE_NAME );
		}
		
		//-------------------------------------------------
		// Copy the upload to the uploads directory
		//-------------------------------------------------
		
		if (! @move_uploaded_file( $HTTP_POST_FILES['FILE_UPLOAD']['tmp_name'], $ibforums->vars['upload_dir']."/".$real_file_name) )
		{
			$this->obj['post_errors'] = 'upload_failed';
			return $attach_data;
		}
		
		//-------------------------------------------------
		// set the array, and enter the info into the DB
		// We don't have an extension on the file in the
		// hope that it make it more difficult to execute
		// a script on our server.
		//-------------------------------------------------
		
		$attach_data['attach_id']   = $real_file_name;
		$attach_data['attach_hits'] = 0;
		$attach_data['attach_type'] = $FILE_TYPE;
		$attach_data['attach_file'] = $FILE_NAME;
		
		return $attach_data;
	}

		
	
	/*****************************************************/
	// check_upload_ability
	// ------------------
	// checks to make sure the requesting browser can accept
	// file uploads, also checks if the member group can
	// accept uploads and returns accordingly.
	/*****************************************************/
	
	function check_upload_ability() {
		global $ibforums;
		
		if ($this->forum['use_attach'] and $ibforums->member['g_attach_max'] > 0)
		{
			$this->obj['can_upload']   = 1;
			$this->obj['form_extra']   = " enctype='multipart/form-data'";
			$this->obj['hidden_field'] = "<input type='hidden' name='MAX_FILE_SIZE' value='".($ibforums->member['g_attach_max']*1024)."'>";
		}
		
	}
	
	/*****************************************************/
	// HTML: mod_options.
	// ------------------
	// Returns the HTML for the mod options drop down box
	/*****************************************************/
	
	function mod_options() {
		global $ibforums, $DB;
		
		$can_close = 0;
		$can_pin   = 0;
		
		$html = "<select id='forminput' name='mod_options' class='forminput'>\n<option value='nowt'>".$ibforums->lang['mod_nowt']."</option>\n";
		
		if ($ibforums->member['g_is_supmod'])
		{
			$can_close = 1;
			$can_pin   = 1;
		}
		else if ($ibforums->member['id'] != 0)
		{
			if ($this->moderator['mid'] != "" )
			{
				if ($this->moderator['close_topic'])
				{
					$can_close = 1;
				}
				if ($this->moderator['pin_topic'])
				{
					$can_pin   = 1;
				}
			}
		}
		else
		{
			return "";
		}
		
		if ($can_pin == 0 and $can_close == 0)
		{
			return "";
		}
		
		if ($can_pin)
		{
			$html .= "<option value='pin'>".$ibforums->lang['mod_pin']."</option>";
		}
		if ($can_close)
		{
			$html .= "<option value='close'>".$ibforums->lang['mod_close']."</option>";
		}
		
		return $this->html->mod_options($html);
	
	}

	/*****************************************************/
	// HTML: start form.
	// ------------------
	// Returns the HTML for the <FORM> opening tag
	/*****************************************************/
	
	function html_start_form($additional_tags=array()) {
		global $ibforums;
		
		$form = "<form action='{$this->base_url}' method='POST' name='REPLIER' onSubmit='return ValidateForm()'".$this->obj['form_extra'].">".
				"<input type='hidden' name='st' value='".$ibforums->input[st]."'>".
				"<input type='hidden' name='act' value='Post'>".
				"<input type='hidden' name='s' value='".$ibforums->session_id."'>".
				"<input type='hidden' name='f' value='".$this->forum['id']."'>".
				$this->obj['hidden_field'];
				
		// Any other tags to add?
		
		if (isset($additional_tags)) {
			foreach($additional_tags as $k => $v) {
				$form .= "\n<input type='hidden' name='{$v[0]}' value='{$v[1]}'>";
			}
		}
		
		return $form;
    }
		
	/*****************************************************/
	// HTML: name fields.
	// ------------------
	// Returns the HTML for either text inputs or membername
	// depending if the member is a guest.
	/*****************************************************/
	
	function html_name_field() {
		global $ibforums;
		
		return $ibforums->member['id'] ? $this->html->nameField_reg() : $this->html->nameField_unreg( $ibforums->input[UserName] );
	}
	
	/*****************************************************/
	// HTML: Post body.
	// ------------------
	// Returns the HTML for post area, code buttons and
	// post icons
	/*****************************************************/
	
	function html_post_body($raw_post="") {
		global $ibforums;
		
		$ibforums->lang['the_max_length'] = $ibforums->vars['max_post_length'] * 1024;
		
		return $this->html->postbox_buttons($raw_post);
		
	}
	
	/*****************************************************/
	// HTML: Post Icons
	// ------------------
	// Returns the HTML for post area, code buttons and
	// post icons
	/*****************************************************/
	
	function html_post_icons($post_icon="") {
		global $ibforums;
		
		if ($ibforums->input['iconid'])
		{
			$post_icon = $ibforums->input['iconid'];
		}
		
		$ibforums->lang['the_max_length'] = $ibforums->vars['max_post_length'] * 1024;
		
		$html = $this->html->PostIcons();
		
		if ($post_icon) {
			$html = preg_replace( "/name=[\"']iconid[\"']\s*value=[\"']$post_icon\s?[\"']/", "name='iconid' value='$post_icon' checked", $html );
			$html = preg_replace( "/name=[\"']iconid[\"']\s*value=[\"']0[\"']\s*checked/i"  , "name='iconid' value='0'", $html );
		}
		return $html;
	}
	
    /*****************************************************/
	// HTML: add smilie box.
	// ------------------
	// Inserts the clickable smilies box
	/*****************************************************/
	
	function html_add_smilie_box() {
		global $ibforums, $DB;
		
		$show_table = 0;
		$count      = 0;
		$smilies    = "<tr align='center'>\n";
		
		// Get the smilies from the DB
		
		$DB->query("SELECT * FROM ibf_emoticons WHERE clickable='1'");
		
		while ($elmo = $DB->fetch_row() ) {
		
			$show_table++;
			$count++;
			
			$smilies .= "<td><a href=\"javascript:emoticon('".$elmo['typed']."')\"><img src=\"".$ibforums->vars['EMOTICONS_URL']."/".$elmo['image']."\" alt='smilie' border='0'></a>&nbsp;</td>\n";
			
			if ($count == $ibforums->vars['emo_per_row']) {
				$smilies .= "</tr>\n\n<tr align='center'>";
				$count = 0;
			}
		}
		
		if ($count != $ibforums->vars['emo_per_row']) {
			for ($i = $count ; $i < $ibforums->vars['emo_per_row'] ; ++$i) {
				$smilies .= "<td>&nbsp;</td>\n";
			}
			$smilies .= "</tr>";
		}
		
		$table = $this->html->smilie_table();
		
		if ($show_table != 0) {
			$table = preg_replace( "/<!--THE SMILIES-->/", $smilies, $table );
			$this->output = preg_replace( "/<!--SMILIE TABLE-->/", $table, $this->output );
		}
	
	}
		
	/*****************************************************/
	// HTML: topic summary.
	// ------------------
	// displays the last 10 replies to the topic we're
	// replying in.
	/*****************************************************/
	
	function html_topic_summary($topic_id) {
		
		global $ibforums, $std, $DB;
		
		if (! $topic_id ) return;
		
		$cached_members = array();
		
		$this->output .= $this->html->TopicSummary_top();
		
		//--------------------------------------------------------------
		// Get the posts
		// This section will probably change at some point
		//--------------------------------------------------------------
		
		$post_query = $DB->query("SELECT post, pid, post_date, author_id, author_name FROM ibf_posts WHERE topic_id='$topic_id' and queued <> 1 ORDER BY pid DESC LIMIT 0,10");
		
		while ( $row = $DB->fetch_row($post_query) )
		{
			
		    $row['author'] = $row['author_name'];
			
			$row['date']   = $std->get_date( $row['post_date'], 'LONG' );
			
			$this->output .= $this->html->TopicSummary_body( $row );
		}
		
		$this->output .= $this->html->TopicSummary_bottom();
		
	}
	
	/*****************************************************/
	// Moderators log
	// ------------------
	// Simply adds the last action to the mod logs
	/*****************************************************/
	
	function moderate_log($title = 'unknown', $topic_title) {
		global $std, $ibforums, $DB, $HTTP_REFERER, $QUERY_STRING;
		
		$db_string = $std->compile_db_string( array (
														'forum_id'    => $ibforums->input['f'],
														'topic_id'    => $ibforums->input['t'],
														'post_id'     => $ibforums->input['p'],
														'member_id'   => $ibforums->member['id'],
														'member_name' => $ibforums->member['name'],
														'ip_address'  => $ibforums->input['IP_ADDRESS'],
														'http_referer'=> $HTTP_REFERER,
														'ctime'       => time(),
														'topic_title' => $topic_title,
														'action'      => $title,
														'query_string'=> $QUERY_STRING,
													)
										    );
		
		$DB->query("INSERT INTO ibf_moderator_logs (" .$db_string['FIELD_NAMES']. ") VALUES (". $db_string['FIELD_VALUES'] .")");
		
	}
        
}

?>