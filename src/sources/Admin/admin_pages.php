<?php

// Simple library that holds all the links for the admin cp

// CAT_ID => array(  PAGE_ID  => (PAGE_NAME, URL ) )

// $PAGES[ $cat_id ][$page_id][0] = Page name
// $PAGES[ $cat_id ][$page_id][1] = Url


$PAGES = array(

				0 => array (
							 1 => array( 'Basic Config', 'act=op&code=url'   ),
							 2 => array( 'Security & Privacy'      , 'act=op&code=secure'  ),
							 3 => array( 'Topics & Posts', 'act=op&code=post'    ),
							 4 => array( 'User Profiles'       , 'act=op&code=avatars' ),
							 5 => array( 'Date Formats'  , 'act=op&code=dates'   ),
							 6 => array( 'CPU Saving'    , 'act=op&code=cpu'     ),
							 7 => array( 'Cookies'       , 'act=op&code=cookie'  ),
							 8 => array( 'PM Set up'       , 'act=op&code=pm'    ),
							 9 => array( 'Board on/off'    , 'act=op&code=board' ),
							 10 =>array( 'News Set-up'    , 'act=op&code=news' ),
							 
							 
						   ),

				1 => array (
							 1 => array( 'New Category'  , 'act=cat&code=new'    ),
							 2 => array( 'New Forum'     , 'act=forum&code=newsp'    ),
							 3 => array( 'Manage'        , 'act=cat&code=edit'   ),
							 4 => array( 'Re-Order Categories' , 'act=cat&code=reorder'),
							 5 => array( 'Re-Order Forums', 'act=forum&code=reorder'),
							 6 => array( 'Moderators'    , 'act=mod'               ),
						   ),
						   
						   
				3 => array (
							1 => array ( 'Pre-Register'        , 'act=mem&code=add'  ),
							2 => array ( 'Edit User'           , 'act=mem&code=edit' ),
							3 => array ( 'Delete User(s)'      , 'act=mem&code=del'  ),
							4 => array ( 'Ban Settings'        , 'act=mem&code=ban'  ),
							5 => array ( 'User Title/Ranks'    , 'act=mem&code=title'),
							6 => array ( 'Manage User Groups'  , 'act=group'         ),
							7 => array ( 'Manage Registrations', 'act=mem&code=mod'  ),
						   ),
						   
				4 => array (
							1 => array( 'Manage Word Filters', 'act=op&code=bw'    ),
							2 => array( 'Manage Emoticons', 'act=op&code=emo'   ),
							3 => array( 'Manage Help Files', 'act=help'         ),
							4 => array( 'Recount Statistics', 'act=op&code=count'    ),
							5 => array( 'View Moderator Logs', 'act=modlog'    ),
						   ),
						   
				5 => array (
							1 => array( 'Manage Board Wrappers'   , 'act=wrap'                ),
							2 => array( 'Manage Skin Templates'   , 'act=templ'     ),
							3 => array( 'Manage Style Sheets'     , 'act=style'               ),
							4 => array( 'Manage Image&Macros'   , 'act=image'               ),
							5 => array( '<b>Manage Skin Sets</b>' , 'act=sets'      ),
							6 => array( 'Import Skin files'       , 'act=import'      ),
							
						   ),
						   
				6 => array (
							1 => array( 'Manage Languages' , 'act=lang'             ),
							2 => array( 'Import a Language', 'act=lang&code=import' ),
						   ),
				
							
						   
			   );
			   
			   
$CATS = array (   
				  0 => "Board Settings",
			      1 => 'Forum Control',
				  3 => 'Users and Groups',
				  4 => 'Administration',
				  5 => 'Skins & Templates',
				  6 => 'Languages',
			  );
			  
$DESC = array (
				  0 => "Edit forum settings such as cookie paths, security features, posting abilities, etc",
				  1 => "Create, edit, remove and re-order categories, forums and moderators",
				  3 => "Edit, register, remove and ban members. Set up member titles and ranks. Manage User Groups and moderated registrations",
				  4 => "Manage Help Files, Bad Word Filters and Emoticons",
				  5 => "Manage templates, skins, colours and images.",
				  6 => "Manage language sets",
			  );
			  
			  
?>