<?php

/*
+--------------------------------------------------------------------------
|   IBFORUMS v1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001,2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > Skin -> Image Macro functions
|   > Module written by Matt Mecham
|   > Date started: 4th April 2002
|
|	> Module Version Number: 1.0.0
+--------------------------------------------------------------------------
*/





$idx = new ad_settings();


class ad_settings {

	var $base_url;

	function ad_settings() {
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;

		switch($IN['code'])
		{
			case 'wrapper':
				$this->list_current();
				break;
				
			case 'add':
				$this->add_images();
				break;
				
			case 'edit':
				$this->do_form('edit');
				break;
				
			case 'doadd':
				$this->save_wrapper('add');
				break;
				
			case 'doedit':
				$this->save_wrapper('edit');
				break;
				
			case 'remove':
				$this->remove();
				break;
				
			case 'export':
				$this->export();
			
			//-------------------------
			default:
				$this->list_current();
				break;
		}
		
	}
	
	
	
	function export()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing image set ID, go back and try again");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT * from ibf_images WHERE imid='".$IN['id']."'");
		
		if ( ! $row = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the information from the database");
		}
		
		//+-------------------------------
		
		$archive_dir   = $INFO['base_dir']."/archive_out";
		$images_dir    = $INFO['base_dir']."/style_images/".$row['imid'];
		
		require $root_dir."sources/lib/tar.php";
		
		if (!is_dir($archive_dir))
		{
			$ADMIN->error("Could not locate $archive_dir, is the directory there?");
		}
		
		if (!is_writeable($archive_dir))
		{
			$ADMIN->error("Cannot write in $archive_dir, CHMOD via FTP to 0755 or 0777 to enable this script to write into it. IBF cannot do this for you");
		}
		
		if (!is_dir($images_dir))
		{
			$ADMIN->error("Could not locate $images_dir, is the directory there?");
		}
		
		//+-------------------------------
		// Attempt to copy the files to the
		// working directory...
		//+-------------------------------
		
		$l_name = preg_replace( "/\s{1,}/", "_", $row['setname'] );
		
		$new_dir = "image-".$l_name;
		
		if ( ! $ADMIN->copy_dir($images_dir, $archive_dir."/".$new_dir) )
		{
			$ADMIN->error( $ADMIN->errors );
		}
		
		// Generate the config file..
		
		$file_content = "<?php\n\n";
		
		foreach($row as $k => $v)
		{
		
			$v = preg_replace( "#style_images/".$row['imid']."/#", "style_images/%img_dir%/", $v );
		
			$file_content .= "\n\$config['$k'] = \"".addslashes($v)."\";";
		}
		
		$file_content .= "\n\n?".">";
		
		$FH = fopen($archive_dir."/".$new_dir."/"."conf.inc", 'w');
		fwrite($FH, $file_content, strlen($file_content));
		fclose($FH);
		
		// Add files and write tarball
		
		$tar = new tar();
		
		$tar->new_tar( $archive_dir, $new_dir.".tar" );
		$tar->add_directory( $archive_dir."/".$new_dir );
		$tar->write_tar();
		
		// Check for errors.
		
		if ($tar->error != "")
		{
			$ADMIN->error($tar->error);
		}
		
		// remove original unarchived directory
		
		$ADMIN->rm_dir($archive_dir."/".$new_dir);
		
		$ADMIN->done_screen("Template Pack Export Created<br><br>You can download the tar-chive <a href='archive_out/{$new_dir}.tar' target='_blank'>here</a>", "Manage Template Sets", "act=templ" );
		
		
	}
	
	//-------------------------------------------------------------
	// Add images..
	//-------------------------------------------------------------
	
	
	function add_images()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing image set ID, go back and try again");
		}
		
		//-------------------------------------
		
		if ( ! is_writeable($root_path.'style_images') )
		{
			$ADMIN->error("The directory 'style_images' is not writeable by this script. Please check the permissions on that directory. CHMOD to 0777 if in doubt and try again");
		}
		
		//-------------------------------------
		
		if ( ! is_dir($root_path.'style_images/'.$IN['id']) )
		{
			$ADMIN->error("Could not locate the original image set to copy, please check and try again");
		}
		
		//-------------------------------------
		
		$DB->query("SELECT * FROM ibf_images WHERE imid='".$IN['id']."'");
		
		//-------------------------------------
		
		if ( ! $row = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query that image set from the DB, so there");
		}
		
		//-------------------------------------
		
		$row['setname'] = $row['setname'].".2";
		
		// Insert a new row into the DB...
		
		$final = array();
		
		foreach($row as $k => $v)
		{
			if ($k == 'imid')
			{
				continue;
			}
			else
			{
				$final[ $k ] = $v;
			}
		}
		
		$db_string = $DB->compile_db_insert_string( $final );
		
		$DB->query("INSERT INTO ibf_images (".$db_string['FIELD_NAMES'].") VALUES(".$db_string['FIELD_VALUES'].")");
		
		$new_id = $DB->get_insert_id();
		
		//-------------------------------------
		
		if ( ! $ADMIN->copy_dir( $INFO['base_dir'].'style_images/'.$IN['id'] , $INFO['base_dir'].'style_images/'.$new_id ) )
		{
			$DB->query("DELETE FROM ibf_images WHERE imid='$new_id'");
			
			$ADMIN->error( $ADMIN->errors );
		}
		
		//-------------------------------------
		// Pass to edit / add form...
		//-------------------------------------
		
		$this->do_form('add', $new_id);
	
	}
	
	//-------------------------------------------------------------
	// REMOVE WRAPPERS
	//-------------------------------------------------------------
	
	function remove()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		//+-------------------------------
		
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing image set ID, go back and try again");
		}
		
		$dir_to_remove = $INFO['base_dir']."style_images/".$IN['id'];
		
		if ( SAFE_MODE_ON == 0 )
		{
			if ( $ADMIN->rm_dir( $dir_to_remove ) )
			{
			
				$DB->query("DELETE FROM ibf_images WHERE imid='".$IN['id']."'");
				
				$std->boink_it($SKIN->base_url."&act=image");
				exit();
			}
			else
			{
				$ADMIN->error("Could not remove the image pack files, please check the CHMOD permissions to ensure that this script has the correct permissions to allow this");
			}
		}
		else
		{
			$DB->query("DELETE FROM ibf_images WHERE imid='".$IN['id']."'");
			$ADMIN->info_screen("Safe mode has been detected and this script cannot remove the images and directories for you, please remove the directory '$dir_to_remove' via FTP. The data has been removed from the database successfully");
		}
	}
	
	
	
	//-------------------------------------------------------------
	// ADD / EDIT IMAGE SETS
	//-------------------------------------------------------------
	
	function save_wrapper()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP, $HTTP_POST_VARS;
		
		//+-------------------------------
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing image set ID, go back and try again");
		}
	
		
		if ($IN['setname'] == "")
		{
			$ADMIN->error("You must specify a name for this image and macro set");
		}
		
		$barney = array();
		
		foreach ($IN as $k => $v)
		{
			if ( preg_match( "/^XX_(\S+)$/", $k, $match ) )
			{
				if ( isset($IN[ $match[0] ]) )
				{
					$v = preg_replace( "/&#39;/", "'", stripslashes($HTTP_POST_VARS[ $match[0] ]) );
				
					$barney[ $match[1] ] = $v;
				}
			}
		}
		
		if ( count($barney) < 10 )
		{
			$ADMIN->error("Oopsie, something has gone wrong - did you leave all the fields blank?");
		}
		
		$barney['setname'] = $IN['setname'];
		
		$db_string = $DB->compile_db_update_string( $barney );
		
		$DB->query("UPDATE ibf_images SET $db_string WHERE imid='".$IN['id']."'");
		
		$ADMIN->done_screen("Set updated", "Manage Image and Macro Sets", "act=image" );
		
		
		
	}
	
	//-------------------------------------------------------------
	// ADD / EDIT MACRO SETS
	//-------------------------------------------------------------
	
	function do_form( $type='add', $id="" )
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		//+-------------------------------
		
		if ($id != "")
		{
			$IN['id'] = $id;
		}
		
		//+-------------------------------
		
		if ($IN['id'] == "")
		{
			$ADMIN->error("You must specify an existing image set ID, go back and try again");
		}
		
		//+-------------------------------
		
		$DB->query("SELECT * from ibf_images WHERE imid='".$IN['id']."'");
		
		if ( ! $row = $DB->fetch_row() )
		{
			$ADMIN->error("Could not query the information from the database");
		}
		
		if ($type == 'add')
		{
			$button = 'Update Image & Macro set';
			
			foreach( $row as $k => $v )
			{
				$v = preg_replace( "#style_images/(\d+)/#", "style_images/".$IN['id']."/", $v );
				
				$row[$k] = $v;
			}
			
		}
		else
		{
			$button = 'Edit Image & Macro set';
		}
		
		//+-------------------------------
	
		$ADMIN->page_detail = "You may use edit the macros that power the images. You do not have to use an image, you may use text, flash or other objects.";
		$ADMIN->page_title  = "Manage Image & Macro sets";
		
		//+-------------------------------
		
		
		$ADMIN->html .= $SKIN->js_no_specialchars();
		
		
		$ADMIN->html .= $SKIN->start_form( array( 1 => array( 'code'  , 'doedit'    ),
												  2 => array( 'act'   , 'image'     ),
												  3 => array( 'id'    , $IN['id']   ),
									     ), "theAdminForm", "onSubmit=\"return no_specialchars('images')\""       );
									     
		//+-------------------------------
		
		$SKIN->td_header[] = array( "&nbsp;"   , "20%" );
		$SKIN->td_header[] = array( "&nbsp;"   , "80%" );

		//+-------------------------------
		
		$ADMIN->html .= $SKIN->start_table( "Image & Macro set title" );
		
		$ADMIN->html .= $SKIN->add_td_row( array( 
													"Image & Macro Set Title",
													$SKIN->form_input('setname', $row['setname']),
									     )      );
									     
		$ADMIN->html .= $SKIN->end_table();
									     
		//+-------------------------------
		
		$SKIN->td_header[] = array( "Macro"    , "20%" );
		$SKIN->td_header[] = array( "Source"   , "60%" );
		$SKIN->td_header[] = array( "Current"  , "20%" );

		//+-------------------------------
		
		$ADMIN->html .= $SKIN->start_table( "Current Macros" );
									     
		foreach($row as $k => $v)
		{
			if ($k != 'imid' && $k != 'setname')
			{
				if ( ! preg_match( "/^\d+$/", $k ) )
				{
				
					$real = preg_replace( "/'/", "&#39;", $v );
				
					$ADMIN->html .= $SKIN->add_td_row( array( 
														"$k",
														$SKIN->form_input('XX_'.$k, $real),
														"$v",
											 )      );
				}
			}
		}
									     

												 
		$ADMIN->html .= $SKIN->end_form($button);
										 
		$ADMIN->html .= $SKIN->end_table();
		
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->output();
		
		
	}
	
	//-------------------------------------------------------------
	// SHOW WRAPPERS
	//-------------------------------------------------------------
	
	function list_current()
	{
		global $IN, $root_path, $INFO, $DB, $SKIN, $ADMIN, $std, $MEMBER, $GROUP;
		
		$form_array = array();
	
		$ADMIN->page_detail = "Images and Macros are used in the skin templates as shorthand for an image or other style information. This enables you to use text links instead of buttons or flash movies for certain images. An image and macro set contains the actual images and the macros to power them.";
		$ADMIN->page_title  = "Manage Image and Macros";
		
		//+-------------------------------
		
		$SKIN->td_header[] = array( "Title"        , "40%" );
		$SKIN->td_header[] = array( "Allocation"   , "30%" );
		$SKIN->td_header[] = array( "Export"       , "10%" );
		$SKIN->td_header[] = array( "Edit"         , "10%" );
		$SKIN->td_header[] = array( "Remove"       , "10%" );
		
		//+-------------------------------
		
		$DB->query("SELECT DISTINCT(s.img_id), i.imid, i.setname, s.sname from ibf_images i, ibf_skins s WHERE s.img_id=i.imid ORDER BY i.setname ASC");
		
		$used_ids = array();
		$show_array = array();
		
		if ( $DB->get_num_rows() )
		{
		
			$ADMIN->html .= $SKIN->start_table( "Current Image & Macro sets In Use" );
			
			while ( $r = $DB->fetch_row() )
			{
			
				$show_array[ $r['imid'] ] .= stripslashes($r['sname'])."<br>";
			
				if ( in_array( $r['imid'], $used_ids ) )
				{
					continue;
				}
				
				$ADMIN->html .= $SKIN->add_td_row( array( "<b>".stripslashes($r['setname'])."</b>",
														  "<#X-{$r['imid']}#>",
														  "<center><a href='".$SKIN->base_url."&act=image&code=export&id={$r['imid']}'>Export</a></center>",
														  "<center><a href='".$SKIN->base_url."&act=image&code=edit&id={$r['imid']}'>Edit</a></center>",
														  "<i>Deallocate before removing</i>",
												 )      );
												   
				$used_ids[] = $r['imid'];
				
				$form_array[] = array( $r['imid'], $r['setname'] );
				
			}
			
			foreach( $show_array as $idx => $string )
			{
				$string = preg_replace( "/<br>$/", "", $string );
				
				$ADMIN->html = preg_replace( "/<#X-$idx#>/", "$string", $ADMIN->html );
			}
			
			$ADMIN->html .= $SKIN->end_table();
		}
		
		if ( count($used_ids) > 0 )
		{
		
			$DB->query("SELECT imid, setname FROM ibf_images WHERE imid NOT IN(".implode(",",$used_ids).")");
		
			if ( $DB->get_num_rows() )
			{
			
				$SKIN->td_header[] = array( "Title"  , "70%" );
				$SKIN->td_header[] = array( "Export" , "10%" );
				$SKIN->td_header[] = array( "Edit"   , "10%" );
				$SKIN->td_header[] = array( "Remove" , "10%" );
			
				$ADMIN->html .= $SKIN->start_table( "Current Unallocated Image & Macro sets" );
				
				$ADMIN->html .= $SKIN->js_checkdelete();
				
				while ( $r = $DB->fetch_row() )
				{
					
					$ADMIN->html .= $SKIN->add_td_row( array( "<b>".stripslashes($r['setname'])."</b>",
															  "<center><a href='".$SKIN->base_url."&act=image&code=export&id={$r['imid']}'>Export</a></center>",
															  "<center><a href='".$SKIN->base_url."&act=image&code=edit&id={$r['imid']}'>Edit</a></center>",
															  "<center><a href='javascript:checkdelete(\"act=image&code=remove&id={$r['imid']}\")'>Remove</a></center>",
													 )      ); 
													 
					$form_array[] = array( $r['imid'], $r['setname'] );
													   
				}
				
				$ADMIN->html .= $SKIN->end_table();
			}
		}
		
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->start_form( array( 1 => array( 'code'  , 'add'     ),
												  2 => array( 'act'   , 'image'    ),
									     )      );
		
		$SKIN->td_header[] = array( "&nbsp;"  , "40%" );
		$SKIN->td_header[] = array( "&nbsp;"  , "60%" );
		
		$ADMIN->html .= $SKIN->start_table( "Create New Image & Macro Set" );
			
		//+-------------------------------
		
		$ADMIN->html .= $SKIN->add_td_row( array( "<b>Base new Image & Macro set on...</b><br>(Note: This will also copy the images)" ,
										  		  $SKIN->form_dropdown( "id", $form_array)
								 )      );
		
		$ADMIN->html .= $SKIN->end_form("Create new Image & Macro set");
										 
		$ADMIN->html .= $SKIN->end_table();
		
		//+-------------------------------
		//+-------------------------------
		
		$ADMIN->output();
	
	}
	
	
}


?>