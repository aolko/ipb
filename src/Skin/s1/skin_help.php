<?php

class skin_help {



function row($entry) {
global $ibforums;
return <<<EOF
          <!-- Help Entry ID:{$entry[ID]} -->
          <tr>
            <td id='{$entry['CELL_COLOUR']}' style='height:28px'><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?act=Help&s={$ibforums->session_id}&CODE=01&HID={$entry['id']}'><b>{$entry['title']}</b></a><br>{$entry['description']}</td>
          </tr>
          <!-- End Help Entry -->
EOF;
}

function display($text) {
global $ibforums;
return <<<EOF
          <!-- Displaying Help Topic -->
          <tr>
            <td id='row1' colspan='2' id='postcolor'>$text</td>
          </tr>
          <!-- End Display -->
EOF;
}

function end() {
global $ibforums;
return <<<EOF
          </table>
          </td>
          </tr>
          </table>
EOF;
}

function no_results() {
global $ibforums;
return <<<EOF
                <tr>
                   <td id='row1' colspan='2'><b>{$ibforums->lang['no_results']}</b></td>
                 </tr>
EOF;
}

function start($one_text, $two_text, $three_text) {
global $ibforums;
return <<<EOF
     <table cellpadding=4 cellspacing='0' border='0' width='{$ibforums->skin['tbl_width']}' align='center'>
      <tr><td>$two_text</td></tr>
     </table>

     <table cellpadding=0 cellspacing='1' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
      <tr>
        <td>
          <table cellpadding='4' cellspacing='1' border='0' width='100%'>
          <tr>
          <td  align='left' colspan='2' id='titlemedium' background="{$ibforums->vars['img_url']}/tile_sub.gif">$one_text</td>
          </tr>
          <tr>
              <td id='row1' colspan='2'>
               <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?act=Help;s={$ibforums->session_id};CODE=02" method="post">
               <input type='hidden' name='act' value='Help'>
               <input type='hidden' name='CODE' value='02'>
               <input type='hidden' name='s' value='{$ibforums->session_id}'>
               {$ibforums->lang['search_txt']}&nbsp;&nbsp;<input type='text' maxlength='60' size='30' class='forminput' name='search_q'>&nbsp;<input type='submit' value='{$ibforums->lang['submit']}' class='forminput'>
              </form>
             </td>
           </tr>
           </table>
          </td>
         </tr>
      </table>
      <br>
     <table cellpadding=0 cellspacing='1' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
      <tr>
        <td>
          <table cellpadding='4' cellspacing='1' border='0' width='100%'>
           <tr>
             <td colspan='2' id='titlemedium' background="{$ibforums->vars['img_url']}/tile_sub.gif"><b>$three_text</b></td>
           </tr>
EOF;
}


}
?>