<?php

class skin_ucp {



function birthday($day,$month,$year) {
global $ibforums;
return <<<EOF
            <tr>
            <td id='row2' width='40%'><b>{$ibforums->lang['birthday']}</b></td>
            <td id='row2'>
            <select name='day' class='forminput'>{$day}</select> 
            <select name='month' class='forminput'>{$month}</select> 
            <select name='year' class='forminput'>{$year}</select>
            </td>
            </tr>
EOF;
}

function email_change($txt="") {
global $ibforums;
return <<<EOF
                   <td  colspan='2' id='category'><b>{$ibforums->lang['change_email_title']}</b></td>
                 </tr>

                 <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post" name='form1'>
                 <input type='hidden' name='act' value='UserCP'>
                 <input type='hidden' name='CODE' value='09'>
                 <input type='hidden' name='s' value='{$ibforums->session_id}'>
                 <tr>
                 	<td id='row1' colspan='2'>$txt</td>
                 </tr>
                 <tr>
                   <td id='row1' nowrap><b>{$ibforums->lang['ce_new_email']}</b></td>
                   <td id='row1' width='70%' align='left'><input type='text' name='in_email_1' value='' class='forminput'></td>
                 </tr>
                  <tr>
                   <td id='row1' nowrap><b>{$ibforums->lang['ce_new_email2']}</b></td>
                   <td id='row1' width='70%' align='left'><input type='text' name='in_email_2' value='' class='forminput'></td>
                 </tr>
                 <tr>
                     <td id='row2' align='center' colspan='2'><input type="submit" name='change_email' value="{$ibforums->lang['account_email_submit']}" class='forminput'></td>
                 </tr>
                 </form>
EOF;
}

function pass_change() {
global $ibforums;
return <<<EOF
				<form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post" name='form1'>
                 <input type='hidden' name='act' value='UserCP'>
                 <input type='hidden' name='CODE' value='29'>
                 <input type='hidden' name='s' value='{$ibforums->session_id}'>

                   <td colspan='2' id='category'><b>{$ibforums->lang['account_pass_title']}</b></td>
                 </tr>
                 <tr>
                   <td id='row1' colspan='2'>{$ibforums->lang['pass_change_text']}</td>
                 </tr>
                 <tr>
                   <td id='row2' nowrap><b>{$ibforums->lang['account_pass_old']}</b></td>
                   <td id='row2' align='left'><input type='password' name='current_pass' value='' class='forminput'></td>
                 </tr>
                 <tr>
                   <td id='row1' nowrap><b>{$ibforums->lang['account_pass_new']}</b></td>
                   <td id='row1' align='left'><input type='password' name='new_pass_1' value='' class='forminput'></td>
                 </tr>
                 <tr>
                   <td id='row1' nowrap><b>{$ibforums->lang['account_pass_new2']}</b></td>
                   <td id='row1' align='left'><input type='password' name='new_pass_2' value='' class='forminput'></td>
                 </tr>
                 <tr>
                    <td id='row2' align='center' colspan='2'><input type="submit" name='s_pass' value="{$ibforums->lang['account_pass_submit']}" class='forminput'></td>
                 </tr>
                 </form>
EOF;
}

function personal_splash() {
global $ibforums;
return <<<EOF
                   <td colspan='2' id='category'>{$ibforums->lang['personal_ops']}</td>                
                 </tr>
                 <tr>
                   <td id='row1' colspan='2'><b>{$ibforums->member['name']}</b>, {$ibforums->lang['personal_ops_txt']}</td>
                 </tr>
                 <tr>
                   <td id='row2' colspan='2'><span id='usermenu'><u><b><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=UserCP&CODE=01&MODE=2'>{$ibforums->lang['edit_profile']}</a></b></u></span><br><br>{$ibforums->lang['edit_profile_txt']}</td>
                 </tr>
EOF;
}

function member_title($title) {
global $ibforums;
return <<<EOF
                <tr>
                <td id='row1' width='40%'>{$ibforums->lang['member_title']}</td>
                <td id='row1'><input type='text' size='40' maxlength='120' name='member_title' value='$title' class='forminput'></td>
                </tr>
EOF;
}

function personal_avatar($data, $formextra="", $hidden_field="") {
global $ibforums;
return <<<EOF
                <script langauge='javascript'>
                <!--
                
                  function checkTheBox() {
                  	
                  	var isUrl = "{$ibforums->vars['avatar_url']}";
                  	
                  	if (isUrl == 1)
                  	{
                  		document.creator.choice[1].checked = true;
                  	}
                  	else
                  	{
                  		document.creator.choice[0].checked = true;
                  	}
                  }
                  
                  function showavatar(theURL) {
                    
                    document.creator.choice[0].checked = true;
                    
                    document.images.show_avatar.src=theURL+document.creator.gallery_list.options[document.creator.gallery_list.selectedIndex].value;
                  }
                  
                  function select_url() {
                  	document.creator.choice[1].checked = true;
                  }
                  
                  function select_upload() {
                  	document.creator.choice[1].checked = true;
                  	document.creator.url_avatar.value = "";
                  }
                  
                  function select_none() {
                  	document.creator.choice[2].checked = true;
                  }
                  
                //-->
                </script>
                
                <form action='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}' method='post' onload='checkTheBox' $formextra name='creator'>
                <input type='hidden' name='act' value='UserCP'>
                <input type='hidden' name='CODE' value='25'>
                <input type='hidden' name='s' value='{$ibforums->session_id}'>
                $hidden_field
                
                <td align='left' colspan='2' id='category'><b>{$ibforums->lang['av_current']}</b></td>
                </tr>
                <tr>
                <td id='row2' width='40%' valign='middle'>{$ibforums->lang['this_avatar']}</td>
                <td id='row2' width='60%' valign='middle'>{$data[CUR_AV]}</td>
                <tr>
                <td valign='left' colspan='2' id='category'><input type='radio' name='choice' value='gallery' onclick='return true;'><b>{$ibforums->lang['avatar_pre_title']}</b></td>
                </tr>
                <tr>
                <td id='row1' width='40%' valign='top'>{$ibforums->lang['avatar_pre_txt']}</td>
                <td id='row1' valign='top'>{$data[AVATARS]} &nbsp; &nbsp; {$data[SHOW_AVS]}</td>
                </tr>
EOF;
}

function avatar_upload_field($text="") {
global $ibforums;
return <<<EOF
				<tr>
                <td id='row1' width='40%' valign='top'><b>{$ibforums->lang['upload_avatar']}</b></td>
                <td id='row1'><input type='file' size='30' name='FILE_UPLOAD' class='forminput' onfocus='select_upload()' onclick='select_upload()'><br>$text</td>
                </tr>
EOF;
}

function personal_avatar_URL($Profile, $avatar, $allowed_ext) {
global $ibforums;
return <<<EOF
                <tr>
                <td valign='left' colspan='2' id='category'><input type='radio' name='choice' value='url' checked onclick='return true;'><b>{$ibforums->lang['avatar_url_title']}</b></td>
                </tr>
                <tr>
                <td id='row1' width='40%' valign='top'>{$ibforums->lang['avatar']}<br>{$ibforums->lang['avatar_url_ext']}<br><b>$allowed_ext</b></td>
                <td id='row1'><input type='text' size='55' maxlength='80' name='url_avatar' value='$avatar' class='forminput' onfocus='select_url()'></td>
                </tr>
                <!-- IBF.UPLOAD_AVATAR -->
                <tr>
                <td id='row1' width='40%' valign='top'>{$ibforums->lang['avatar_dims']}<br>{$ibforums->lang['maximum']} {$ibforums->lang['width']} = {$ibforums->vars['av_width']} {$ibforums->lang['pixels']}<br>{$ibforums->lang['maximum']} {$ibforums->lang['height']} = {$ibforums->vars['av_height']} {$ibforums->lang['pixels']})</td>
                <td id='row1'>{$ibforums->lang['width']} &nbsp; <input type='text' size='3' maxlength='3' name='Avatar_width' value='{$Profile[AVATAR_WIDTH]}' class='forminput' onfocus='select_url()'>&nbsp; x &nbsp; {$ibforums->lang['height']} &nbsp; <input type='text' size='3' maxlength='3' name='Avatar_height' value='{$Profile[AVATAR_HEIGHT]}' onfocus='select_url()' class='forminput'></td>
                </tr>
EOF;
}

function personal_avatar_end() {
global $ibforums;
return <<<EOF
                <tr>
                <td valign='left' colspan='2' id='category'><input type='radio' name='choice' value='none' onclick='return true;'><b>{$ibforums->lang['av_tt_one']}</b></td>
                </tr>
                <tr>
                <td id='row1' align='center' colspan='2'>{$ibforums->lang['av_tt_two']}</td>
                </tr> 
                <tr>
                <td id='row2' align='center' colspan='2'><input type="submit" value="{$ibforums->lang['avatar_pre_submit']}" class='forminput'></td>
                </tr>         
                </form>
EOF;
}

function personal_splash_av() {
global $ibforums;
return <<<EOF
                 <tr>
                   <td id='row2' colspan='2'><span id='usermenu'><b><u><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=UserCP&CODE=01&MODE=1'>{$ibforums->lang['avatar_ops']}</a></u></b></span><br><br>{$ibforums->lang['avatar_ops_txt']}</td>
                 </tr>
EOF;
}

function signature($sig, $t_sig) {
global $ibforums;
return <<<EOF
<script language="javascript1.2">
<!--

var MessageMax  = "{$ibforums->lang['the_max_length']}";
var Override    = "{$ibforums->lang['override']}";

function CheckLength() {
    MessageLength  = document.REPLIER.Post.value.length;
    message  = "";

        if (MessageMax > 0) {
            message = "{$ibforums->lang['js_max_length']} " + MessageMax + " {$ibforums->lang['js_characters']}.";
        } else {
            message = "";
        }
        alert(message + "\\n{$ibforums->lang['js_used']} " + MessageLength + " {$ibforums->lang['js_characters']}.");
}

function ValidateForm() {
    MessageLength  = document.REPLIER.Post.value.length;
    errors = "";

    if (MessageMax !=0) {
        if (MessageLength > MessageMax) {
            errors = "{$ibforums->lang['js_max_length']} " + MessageMax + " {$ibforums->lang['js_characters']}.\\n{$ibforums->lang['js_current']}: " + MessageLength;
        }
    }
    if (errors != "" && Override == "") {
        alert(errors);
        return false;
    } else {
        document.REPLIER.submit.disabled = true;
        return true;
    }
}



// IBC Code stuff
var text_enter_url      = "{$ibforums->lang['jscode_text_enter_url']}";
var text_enter_url_name = "{$ibforums->lang['jscode_text_enter_url_name']}";
var text_enter_image    = "{$ibforums->lang['jscode_text_enter_image']}";
var text_enter_email    = "{$ibforums->lang['jscode_text_enter_email']}";
var text_enter_flash    = "{$ibforums->lang['jscode_text_enter_flash']}";
var text_code           = "{$ibforums->lang['jscode_text_code']}";
var text_quote          = "{$ibforums->lang['jscode_text_quote']}";
var error_no_url        = "{$ibforums->lang['jscode_error_no_url']}";
var error_no_title      = "{$ibforums->lang['jscode_error_no_title']}";
var error_no_email      = "{$ibforums->lang['jscode_error_no_email']}";
var error_no_width      = "{$ibforums->lang['jscode_error_no_width']}";
var error_no_height     = "{$ibforums->lang['jscode_error_no_height']}";
//-->
</script>
<script language='Javascript' src='html/ibfcode.js'></script>

				<form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="POST" name='REPLIER'>
				<input type='hidden' name='act' value='UserCP'>
				<input type='hidden' name='CODE' value='23'>
				<input type='hidden' name='s' value='{$ibforums->session_id}'>
				<td id='title'><b>{$ibforums->lang['cp_current_sig']}</b></td>
				</tr>
				<tr>
				<td id='row1' align='center'>
					<table cellpadding='2' cellspacing='0' width='75%' align='center'>
						<tr>
							<td id='signature'>$sig</td>
						</tr>
					</table>
				</td>
				</tr>
				<tr>
                <td id='title'><b>{$ibforums->lang['cp_edit_sig']}</b></td>
                </tr>
                <tr>
                <td id='row1' align='center'>
                <table cellpadding='2' cellspacing='2' width='100%'>
                		<tr>
                			<td nowrap width='10%'>
							  <input type='button' accesskey='b' value=' B '       onClick='simpletag("B")'       class='codebuttons' title="BOLD: [Control / Alt] + b"      name='bold'   style="font-weight:bold">
							  <input type='button' accesskey='i' value=' I '       onClick='simpletag("I")'       class='codebuttons' title="ITALIC: [Control / Alt] + i"    name='italic' style="font-style:italic">
							  <input type='button' accesskey='u' value=' U '       onClick='simpletag("U")'       class='codebuttons' title="UNDERLINE: [Control / Alt] + u" name='under' style="text-decoration:underline">
							  
							  <select name='ffont' class='codebuttons' onchange="alterfont(this.options[this.selectedIndex].value, 'FONT')">
							  <option value='0'>{$ibforums->lang['ct_font']}</option>
							  <option value='Arial' style='font-family:Arial'>{$ibforums->lang['ct_arial']}</option>
							  <option value='Times' style='font-family:Times'>{$ibforums->lang['ct_times']}</option>
							  <option value='Courier' style='font-family:Courier'>{$ibforums->lang['ct_courier']}</option>
							  <option value='Impact' style='font-family:Impact'>{$ibforums->lang['ct_impact']}</option>
							  <option value='Geneva' style='font-family:Geneva'>{$ibforums->lang['ct_geneva']}</option>
							  <option value='Optima' style='font-family:Optima'>Optima</option>
							  </select><select name='fsize' class='codebuttons' onchange="alterfont(this.options[this.selectedIndex].value, 'SIZE')">
							  <option value='0'>{$ibforums->lang['ct_size']}</option>
							  <option value='7'>{$ibforums->lang['ct_sml']}</option>
							  <option value='14'>{$ibforums->lang['ct_lrg']}</option>
							  <option value='18'>{$ibforums->lang['ct_lest']}</option>
							  </select><select name='fcolor' class='codebuttons' onchange="alterfont(this.options[this.selectedIndex].value, 'COLOR')">
							  <option value='0'>{$ibforums->lang['ct_color']}</option>
							  <option value='blue' style='color:blue'>{$ibforums->lang['ct_blue']}</option>
							  <option value='red' style='color:red'>{$ibforums->lang['ct_red']}</option>
							  <option value='purple' style='color:purple'>{$ibforums->lang['ct_purple']}</option>
							  <option value='orange' style='color:orange'>{$ibforums->lang['ct_orange']}</option>
							  <option value='yellow' style='color:yellow'>{$ibforums->lang['ct_yellow']}</option>
							  <option value='gray' style='color:orange'>{$ibforums->lang['ct_grey']}</option>
							  <option value='green' style='color:green'>{$ibforums->lang['ct_green']}</option>
							  </select>
							</td>
							<td align='left'nowrap width='10%'><input type='button' accesskey='c' value=' x '       onClick='closelast()'          class='codebuttons' title="Close Current Tag: [Control / Alt] + c"      name='bold'   style="color:red"> Close Current Tag</td>
						 </tr>
						 <tr>
						    <td align='left'>
							  <input type='button' accesskey='h' value=' http:// ' onClick='tag_url()'            class='codebuttons' title="HYPERLINK: [Control / Alt] + h" style="text-decoration:underline;color:blue">
							  <input type='button' accesskey='g' value=' IMG '     onClick='tag_image()'          class='codebuttons' title="IMG: [Control / Alt] + g"       >
							  <input type='button' accesskey='e' value='  @  '     onClick='tag_email()'          class='codebuttons' title="EMAIL: [Control / Alt] + e"     style="text-decoration:underline;color:blue">
							  <input type='button' accesskey='q' value=' Quote '   onClick='simpletag("QUOTE")'   class='codebuttons' title="QUOTE: [Control / Alt] + q" name='quote'>
							  <input type='button' accesskey='p' value=' Code '    onClick='simpletag("CODE")'    class='codebuttons' title="CODE: [Control / Alt] + p"  name='code'>
							  <input type='button' accesskey='s' value=' SQL '     onClick='simpletag("SQL")'    class='codebuttons' title="SQL: [Control / Alt] + s"   name='code'>
							  <input type='button' accesskey='t' value=' HTML '     onClick='simpletag("HTML")'    class='codebuttons' title="HTML: [Control / Alt] + t"   name='code'>
							</td>
							<td align='left'>
							  <input type='button' accesskey='x' value=' X '       onClick='closeall()'          class='codebuttons' title="Close Current Tag: [Control / Alt] + x"      name='bold'   style="color:red;font-weight:bold"> Close All Tags
							</td>
						</tr>
					</table>
                </td>
                </tr>
                <tr>
                <td id='row1' align='center'><textarea cols='60' rows='12' wrap='soft' name='Post' tabindex='3' class='textinput'>$t_sig</textarea><br>(<a href='javascript:CheckLength()'>{$ibforums->lang['check_length']}</a>)</td>
                </tr>
                <tr>
                <td id='row1' align='center'><input type='submit' value='{$ibforums->lang['cp_submit_sig']}'></td>
                </tr>
                </form>
EOF;
}

function personal_panel($Profile) {
global $ibforums;
return <<<EOF
<script language="javascript">
<!--

var LocationMax  = "{$ibforums->vars['max_location_length']}";
var InterestMax  = "{$ibforums->vars['max_interest_length']}";

function CheckLength(Type) {
    LocationLength  = document.theForm.Location.value.length;
    InterestLength  = document.theForm.Interests.value.length;
    message  = "";

    if (Type == "location") {
        if (LocationMax !=0) {
            message = "{$ibforums->lang['js_location']}:\\n{$ibforums->lang['js_max']} " + LocationMax + " {$ibforums->lang['js_characters']}.";
        } else {
            message = "";
        }
        alert(message + "\\n{$ibforums->lang['js_used']} " + LocationLength + " {$ibforums->lang['js_so_far']}.");
    }
    if (Type == "interest") {
        if (InterestMax !=0) {
            message = "{$ibforums->lang['js_interests']}:\\n{$ibforums->lang['js_max']} " + InterestMax + " {$ibforums->lang['js_characters']}.";
        } else {
            message = "";
        }
        alert(message + "\\n{$ibforums->lang['js_used']} " + InterestLength + " {$ibforums->lang['js_so_far']}.");
    }
    
}

function ValidateProfile() {

    LocationLength  = document.theForm.Location.value.length;
    InterestLength  = document.theForm.Interests.value.length;

    errors = "";

    if (LocationMax !=0) {
        if (LocationLength > LocationMax) {
            errors = "{$ibforums->lang['js_location']}:\\n{$ibforums->lang['js_max']} " + LocationMax + " {$ibforums->lang['js_characters']}.\\n{$ibforums->lang['js_used']}: " + LocationLength;
        }
    }
    if (InterestMax !=0) {
        if (InterestLength > InterestMax) {
            errors = errors + "\\n{$ibforums->lang['js_interests']}:\\n{$ibforums->lang['js_max']} " + InterestMax + " {$ibforums->lang['js_characters']}.\\n{$ibforums->lang['js_used']}: " + InterestLength;
        }
    } 
    
    if (errors != "") {
        alert(errors);
        return false;
    } else {
        return true;
    }
}
//-->
</script>
<form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post" name='theForm' onSubmit='return ValidateProfile()'>
     <input type='hidden' name='act' value='UserCP'>
     <input type='hidden' name='CODE' value='21'>
     <input type='hidden' name='s' value='{$ibforums->session_id}'>

                <td valign='left' colspan='2' id='category'>{$ibforums->lang['profile_title']}</td>
                </tr>
                <!--{MEMBERTITLE}-->
                <!--{BIRTHDAY}-->
                <!-- for v1.1<tr>
                <td id='row1' width='40%'>{$ibforums->lang['photo']}</td>
                <td id='row1'><input type='text' size='40' maxlength='120' name='Photo' value='{$Profile['photo']}' class='forminput'></td>
                </tr>  -->
                <tr>
                <td id='row2' width='40%'>{$ibforums->lang['website']}</td>
                <td id='row2'><input type='text' size='40' maxlength='1200' name='WebSite' value='{$Profile['website']}' class='forminput'></td>
                </tr>  
                <tr>
                <td id='row1' width='40%'>{$ibforums->lang['icq']}</td>
                <td id='row1'><input type='text' size='40' maxlength='20' name='ICQNumber' value='{$Profile['icq_number']}' class='forminput'></td>
                </tr>
                <tr>
                <td id='row2' width='40%'>{$ibforums->lang['aol']}</td>
                <td id='row2'><input type='text' size='40' maxlength='30' name='AOLName' value='{$Profile['aim_name']}' class='forminput'></td>
                </tr>
                <tr>
                <td id='row1' width='40%'>{$ibforums->lang['yahoo']}</td>
                <td id='row1'><input type='text' size='40' maxlength='30' name='YahooName' value='{$Profile['yahoo']}' class='forminput'></td>
                </tr>
                <tr>
                <td id='row2' width='40%'>{$ibforums->lang['msn']}</td>
                <td id='row2'><input type='text' size='40' maxlength='30' name='MSNName' value='{$Profile['msnname']}' class='forminput'></td>
                </tr>
                <tr>
                <td id='row1' width='40%'>{$ibforums->lang['location']}<br>(<a href='javascript:CheckLength("location");'>{$ibforums->lang['check_length']}</a>)</td>
                <td id='row1'><input type='text' size='40' name='Location' value='{$Profile['location']}' class='forminput'></td>
                </tr>
                <tr>
                <td id='row2' width='40%' valign='top'>{$ibforums->lang['interests']}<br>(<a href='javascript:CheckLength("interest");'>{$ibforums->lang['check_length']}</a>)</td>
                <td id='row2'><textarea cols='60' rows='10' wrap='soft' name='Interests' class='forminput'>{$Profile['interests']}</textarea></td>
                </tr>
                <tr>
                <td id='row2' align='center' colspan='2'>
                <input type="submit" value="{$ibforums->lang['submit_profile']}" class='forminput'>
                </td></tr>
                </form>
EOF;
}

function Menu_bar($base_url) {
global $ibforums;
return <<<EOF
<br>
<table width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center' border="0" cellspacing="1" cellpadding="0">
  <tr> 
    <td id='maintitleback' background='{$ibforums->vars['img_url']}/tile_back.gif'> 
      &nbsp;
    </td>
  </tr>
  <tr> 
    <td> 
      <table id='mainbg' width="100%" border="0" cellspacing="1" cellpadding="4">
        <tr> 
          <td nowrap id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>&nbsp;</td>
          <td width="100%" nowrap id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>&nbsp;</td>
        </tr>
        <tr> 
          <td id='alt1' valign="top">
           <table cellpadding='0' cellspacing='0' width='100%' align='left' border='0'>
            <tr>
              <td>

						  <!-- Messenger -->
						  <b>{$ibforums->lang['m_messenger']}</b><br>
              <img src="{$ibforums->vars['img_url']}/dark_line.gif" alt="" width="155" height="7"><br>
              &#149; <a href='{$base_url}&act=Msg&CODE=04'>{$ibforums->lang['mess_new']}</a><br>
              &#149; <a href='{$base_url}&act=Msg&CODE=02'>{$ibforums->lang['mess_contact']}</a><br>
              &#149; <a href='{$base_url}&act=Msg&CODE=07'>{$ibforums->lang['mess_folders']}</a><br>
              &#149; <a href='{$base_url}&act=Msg&CODE=01'>{$ibforums->lang['mess_inbox']}</a><br>
              &#149; <a href='{$base_url}&act=Msg&CODE=14'>{$ibforums->lang['mess_archive']}</a><br>
              <br>

							<!-- Profile -->
              <b>{$ibforums->lang['m_personal']}</b><br>
              <img src="{$ibforums->vars['img_url']}/dark_line.gif" alt="" width="155" height="7"><br>
              &#149; <a href='{$base_url}&act=UserCP&CODE=01'>{$ibforums->lang['m_contact_info']}</a><br>
              <!-- &#149; <a href='#'>{$ibforums->lang['m_bio_info']}</a><br> -->
              &#149; <a href='{$base_url}&act=UserCP&CODE=22'>{$ibforums->lang['m_sig_info']}</a><br>
              &#149; <a href='{$base_url}&act=UserCP&CODE=24'>{$ibforums->lang['m_avatar_info']}</a><br>
              <!-- &#149; <a href='#'>{$ibforums->lang['m_edit_links']}</a><br> -->

							<!-- Options -->
              <br>
              <b>{$ibforums->lang['m_options']}</b><br>
              <img src="{$ibforums->vars['img_url']}/dark_line.gif" alt="" width="155" height="7"><br>
              &#149; <a href='{$base_url}&act=UserCP&CODE=02'>{$ibforums->lang['m_email_opt']}</a><br>
              &#149; <a href='{$base_url}&act=UserCP&CODE=04'>{$ibforums->lang['m_board_opt']}</a><br>
              &#149; <a href='{$base_url}&act=UserCP&CODE=06'>{$ibforums->lang['m_skin_lang']}</a><br>
              &#149; <a href='{$base_url}&act=UserCP&CODE=08'>{$ibforums->lang['m_email_change']}</a><br>
              &#149; <a href='{$base_url}&act=UserCP&CODE=28'>{$ibforums->lang['m_passy_opt']}</a><br>
              
							<!-- Topic Tracker -->
							<br>
              <b>{$ibforums->lang['m_tracker']}</b><br>
              <img src="{$ibforums->vars['img_url']}/dark_line.gif" alt="" width="155" height="7"><br>
              &#149; <a href='{$base_url}&act=UserCP&CODE=26'>{$ibforums->lang['m_view_subs']}</a>
             </td>
            </tr>
           </table>
          </td>
          <td valign='top' id='alt1'>
          <table cellpadding='4' cellspacing='0' width='100%' align='left' border='0'>
            <tr>
             
			     <!-- Start main CP area -->
EOF;
}

function CP_end() {
global $ibforums;
return <<<EOF
          <!-- end main CP area -->
        
       </table>
       </td>
      </tr>
      </table>
    </td>
  </tr>
</table>
EOF;
}

function splash($member) {
global $ibforums;
return <<<EOF
      <td id='subtitle' width="100%">{$ibforums->lang['stats_header']}</td>
        </tr>
        <tr> 
          <td id='alt1' valign="top" width="100%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="40%">{$ibforums->lang['email_address']}</td>
                <td width="60%">{$member[MEMBER_EMAIL]}</td>
              </tr>
              <tr> 
                <td width="40%">{$ibforums->lang['number_posts']}</td>
                <td width="60%">{$member[MEMBER_POSTS]}</td>
              </tr>
              <tr> 
                <td width="40%">{$ibforums->lang['registered']}</td>
                <td width="60%">{$member[DATE_REGISTERED]}</td>
              </tr>
              <tr> 
                <td width="40%">{$ibforums->lang['daily_average']}</td>
                <td width="60%">{$member[DAILY_AVERAGE]}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td id='subtitle' width="100%">{$ibforums->lang['messenger_summary']}</td>
        </tr>
        <tr> 
          <td id='alt1' valign="top" width="100%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="40%">{$ibforums->lang['total_messages']}</td>
                <td width="60%">{$member['total_messages']} {$member['full_percent']}</td>
              </tr>
              <tr> 
                <td width="40%">{$ibforums->lang['messages_left']}</td>
                <td width="60%">{$member['space_free']} {$member['full_messenger']}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td id='subtitle' width="100%">{$ibforums->lang['note_pad']}</td>
        </tr>
        <tr>
          <td id='alt1' valign="top" width="100%">
                <form name='notepad' action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post">
				<input type='hidden' name='act' value='UserCP'>
				<input type='hidden' name='s' value='{$ibforums->session_id}'>
				<input type='hidden' name='CODE' value='20'>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
				<td align="center">
				<textarea cols='65' rows='{$member['SIZE']}' wrap='soft' name='notes' class='forminput' style='width:100%'>{$member['NOTES']}</textarea>
				</td>
              </tr>
            </table>
            <br>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="50%">{$ibforums->lang['ta_size']}&nbsp;</td>
                <td nowrap width="50%"> 
				   <select name='ta_size' class='forminput'>
				   {$member['SIZE_CHOICE']}
				   </select>
                  <input type='submit' value='{$ibforums->lang['submit_notepad']}' class='forminput'>
                </td>
              </tr>
            </table>
            </form>
          </td>
        </tr>
EOF;
}

function settings_header($Profile, $time_select, $time, $dst_check) {
global $ibforums;
return <<<EOF
     <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post">
     <input type='hidden' name='act' value='UserCP'>
     <input type='hidden' name='CODE' value='05'>
     <input type='hidden' name='s' value='{$ibforums->session_id}'>

                  
                   <td colspan='2' id='category'><b>{$ibforums->lang['settings_time']}</b></td>
                 </tr>
                 <tr>
                   <td id='row1' colspan='2'><b>{$ibforums->lang['settings_time_txt']}</b><br><span id='highlight'>$time</span></td>
                 </tr>
                 <tr>
                   <td id='row1' colspan='2' align='left'>$time_select</td>
                 </tr>
                 <tr>
                   <td id='row1' nowrap>{$ibforums->lang['dst_box']}</td>
                   <td id='row1' width='70%' align='left'><input type='checkbox' class='forminput' name='DST' value='1' $dst_check></td>
                 </tr>
EOF;
}

function skin_lang_header($lang_select) {
global $ibforums;
return <<<EOF
	<script language='Javascript'>
      <!--
		function do_preview() {
			
			var f = document.prefs.u_skin;
			
			var base_url = "{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&";
			
			if (f.options[f.selectedIndex].value == -1) {
				return false;
			}
			
			window.open( base_url + 'skinid='+f.options[f.selectedIndex].value, 'Preview', 'width=800,height=600,top=0,left=0,resizable=1,scrollbars=1,location=no,directories=no,status=no,menubar=no,toolbar=no');
			
		}
	  -->
    </script>

     <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post" name='prefs'>
     <input type='hidden' name='act' value='UserCP'>
     <input type='hidden' name='CODE' value='07'>
     <input type='hidden' name='s' value='{$ibforums->session_id}'>

                   <td colspan='2' id='category'><b>{$ibforums->lang['settings_title']}</b></td>                
                 </tr>
                 <tr>
                   <td id='row1' nowrap><b>{$ibforums->lang['settings_lang_txt']}</b></td>
                   <td id='row1' align='left'>$lang_select</td>
                 </tr>
EOF;
}

function settings_skin($skin) {
global $ibforums;
return <<<EOF
                 </tr>
                   <td colspan='2' id='category'><b>{$ibforums->lang['settings_skin']}</b></td>
                 </tr>
                 <tr>
                   <td id='row1' nowrap><b>{$ibforums->lang['settings_skin_txt']}</b></td>
                   <td id='row1' align='left'>$skin &nbsp;&nbsp; <input type='button' value='{$ibforums->lang['cp_skin_preview']}' class='forminput' onClick='do_preview()'></td>
                 </tr>
EOF;
}

function skin_lang_end() {
global $ibforums;
return <<<EOF
                 <tr>
                   <td id='row2' colspan='2' align='center'><input type='submit' name='submit' value='{$ibforums->lang['settings_submit']}' class='forminput'></form></td>
                 </tr>
EOF;
}

function settings_end($data) {
global $ibforums;
return <<<EOF
                 <tr>
                   <td colspan='2' id='category'><b>{$ibforums->lang['settings_display']}</b></td>
                 </tr>
                 <tr>
                   <td id='row1' nowrap><b>{$ibforums->lang['settings_viewsig']}</b></td>
                   <td id='row1' width='70%' align='left'>{$data[SIG]}</td>
                 </tr>
                 <tr>
                   <td id='row1' nowrap><b>{$ibforums->lang['settings_viewimg']}</b></td>
                   <td id='row1' width='70%' align='left'>{$data[IMG]}</td>
                 </tr>
                 <tr>
                   <td id='row1' nowrap><b>{$ibforums->lang['settings_viewava']}</b></td>
                   <td id='row1' width='70%' align='left'>{$data[AVA]}</td>
                 </tr>
                 <tr>
                   <td id='row1' nowrap><b>{$ibforums->lang['settings_dopopup']}</b></td>
                   <td id='row1' width='70%' align='left'>{$data[POP]}</td>
                 </tr>
                 <tr>
                   <td id='row1' nowrap>{$ibforums->lang['hide_session_txt']}</td>
                   <td id='row1' width='70%' align='left'>{$data[SESS]}</td>
                 </tr>
                 <tr>
                   <td id='row2' colspan='2' align='center'><input type='submit' name='submit' value='{$ibforums->lang['settings_submit']}' class='forminput'></form></td>
                 </tr>
EOF;
}

function email($Profile) {
global $ibforums;
return <<<EOF
<form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post">
     <input type='hidden' name='act' value='UserCP'>
     <input type='hidden' name='CODE' value='03'>
     <input type='hidden' name='s' value='{$ibforums->session_id}'>
                   <td valign='left' colspan='2' id='category'><b>{$ibforums->lang['privacy_settings']}</b></td>
                 </tr>
                <tr>
                <td id='row1' align='right' valign='top'><input type='checkbox' name='hide_email' value='1' {$Profile['hide_email']}></td>
                <td id='row1' align='left' width='100%'>{$ibforums->lang['hide_email']}</td>
                </tr>  
                <tr>
                <td id='row1' align='right' valign='top'><input type='checkbox' name='admin_send' value='1' {$Profile['allow_admin_mails']}></td>
                <td id='row1' align='left'  width='100%'>{$ibforums->lang['admin_send']}</td>
                </tr>
                 <tr>
                   <td valign='left' colspan='2' id='category'><b>{$ibforums->lang['board_prefs']}</b></td>
                 </tr>
                <tr>
                <td id='row1' align='right' valign='top'><input type='checkbox' name='send_full_msg' value='1' {$Profile['email_full']}></td>
                <td id='row1' align='left'  width='100%'>{$ibforums->lang['send_full_msg']}</td>
                </tr>
                <!--<tr>
                <td id='row1' align='right' valign='top'><input type='checkbox' name='pm_reminder' value='1' {$Profile['email_pm']}></td>
                <td id='row1' align='left'  width='100%'>{$ibforums->lang['pm_reminder']}</td>
                </tr>-->
                <tr>
                <td id='row2' align='center' colspan='2'>
                <input type="submit" value="{$ibforums->lang['submit_email']}" class='forminput'>
                </td></tr>
                </form>
EOF;
}

function subs_header() {
global $ibforums;
return <<<EOF
<!-- inbox folder -->
                     <script language='JavaScript'>
                     <!--
                     function CheckAll(cb) {
                         var fmobj = document.mutliact;
                         for (var i=0;i<fmobj.elements.length;i++) {
                             var e = fmobj.elements[i];
                             if ((e.name != 'allbox') && (e.type=='checkbox') && (!e.disabled)) {
                                 e.checked = fmobj.allbox.checked;
                             }
                         }
                     }
                     function CheckCheckAll(cb) {	
                         var fmobj = document.mutliact;
                         var TotalBoxes = 0;
                         var TotalOn = 0;
                         for (var i=0;i<fmobj.elements.length;i++) {
                             var e = fmobj.elements[i];
                             if ((e.name != 'allbox') && (e.type=='checkbox')) {
                                 TotalBoxes++;
                                 if (e.checked) {
                                     TotalOn++;
                                 }
                             }
                         }
                         if (TotalBoxes==TotalOn) {fmobj.allbox.checked=true;}
                         else {fmobj.allbox.checked=false;}
                     }
                     //-->
                     </script>
                 
                   <td align='left' nowrap id='pagetitle'>{$ibforums->lang['subs_header']}</td>
                   <td>
					  <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" name='mutliact' method="post">
					  <input type='hidden' name='act' value='UserCP'>
					  <input type='hidden' name='CODE' value='27'>
					  <input type='hidden' name='s'    value='{$ibforums->session_id}'>
                   </td>
                 </tr>
                 <tr>
                 <td valign='top' colspan='2' >
                 <table cellpadding='4' cellspacing='1' align='center' width='100%' bgcolor='{$ibforums->skin['tbl_border']}'>
                 <tr>
                   <td id='titlemedium' align='left' width='5%'>&nbsp;</td>
                   <td id='titlemedium' align='left' width='*'>{$ibforums->lang['subs_topic']}</td>
                   <td id='titlemedium' align='center' width='5%'>{$ibforums->lang['subs_replies']}</td>
                   <td id='titlemedium' align='center' width='5%'>{$ibforums->lang['subs_view']}</td>
                   <td id='titlemedium' align='left' width='20%'>{$ibforums->lang['subs_last_post']}</td>
                   <td align='center' width='5%' id='titlemedium'><input name="allbox" type="checkbox" value="Check All" onClick="CheckAll();"></td>
                 </tr>
EOF;
}

function subs_row($data) {
global $ibforums;
return <<<EOF
                 <tr>
                   <td id='row1' align='center' width='5%'>{$data['folder_icon']}</td>
                   <td id='row1' align='left'><span id='linkthru'><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ST&f={$data['forum_id']}&t={$data['tid']}'>{$data['title']}</a> ( <a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ST&f={$data['forum_id']}&t={$data['tid']}' target='_blank'>{$ibforums->lang['new_window']}</a> )</span><br><span id='desc'>{$data['description']}{$ibforums->lang['subs_start']} {$data['start_date']}</span></td>
                   <td id='row1' align='center'>{$data['posts']}</td>
                   <td id='row1' align='center'>{$data['views']}</td>
                   <td id='row1' align='left'>{$data['last_post_date']}<br>{$ibforums->lang['subs_by']} {$data['last_poster']}</td>
                   <td id='row2' align='center'><input type='checkbox' name='id-{$data['trid']}' value='yes' class='forminput'></td>
                 </tr>
EOF;
}

function subs_end() {
global $ibforums;
return <<<EOF
<tr>
 <td align='center' id='titlemedium' valign='middle' colspan='6'><input type='submit' class='forminput' value='{$ibforums->lang['subs_delete']}'>&nbsp;&nbsp;{$ibforums->lang['with_selected']}</td>
</tr>
</form>
</table></td></tr>
EOF;
}

function forum_jump($data, $menu_extra="") {
global $ibforums;
return <<<EOF
    <script language='Javascript'>
      <!--
		function jump_page(internal) {
			
			var f = document.jumpForm.themenu;
			
			var base_url = "{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&";
			
			if (f.options[f.selectedIndex].value == -1) {
				return false;
			}
			
			var l_data = f.options[f.selectedIndex].value;
			
			s_data = l_data.split("|");
			
			if (s_data[0] == 0) {
				window.location = base_url + s_data[1];
			}
			else {
				window.open( s_data[1], 'width=800,height=600,top=0,left=0,resizable=1,scrollbars=1,location=yes,directories=yes,status=yes,menubar=yes,toolbar=yes');
			}
			
		}
	  -->
    </script>
<br>
<table cellpadding='0' cellspacing='0' border='0' width='{$ibforums->skin['tbl_width']}' align='center'>
	<tr>
		<td align='left'>
		<form name='jumpForm'>
			<select name='themenu' onchange='jump_page("1")' class='forminput'>
    	            <option value='-1'>{$ibforums->lang['qc_header']}</option>
    	            <option value='0|act=Msg&CODE=00'>{$ibforums->lang['qc_messenger']}</option>
    				<option value='0|act=Msg&CODE=01&VID=in'>{$ibforums->lang['qc_inbox']}</option>
    				<option value='0|act=Msg&CODE=01&VID=sent'>{$ibforums->lang['qc_sent']}</option>
    				<option value='0|act=Msg&CODE=04'>{$ibforums->lang['qc_compose']}</option>
    				<option value='0|act=Msg&CODE=07'>{$ibforums->lang['qc_prefs']}</option>
    				<option value='-1'>--------------</option>
    				<option value='0|act=Search&CODE=00'>{$ibforums->lang['qc_search']}</option>
    				<option value=''>{$ibforums->lang['qc_home']}</option>
    				<option value='0|act=Help&CODE=00'>{$ibforums->lang['qc_help']}</option>
					$menu_extra    				
    		</select>
    	</form>
    	</td>
		<td align='right'>{$data}</td>
	</tr>
</table>
<br>
EOF;
}


}
?>