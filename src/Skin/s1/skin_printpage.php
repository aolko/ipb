<?php

class skin_printpage {



function pp_postentry($poster, $entry ) {
global $ibforums;
return <<<EOF
 <b>{$ibforums->lang['by']}: {$entry['author_name']}</b> {$this->class['LANG']['on']} {$entry['post_date']}
 <br><br>
 {$entry['post']}
 <hr>
EOF;
}

function pp_end() {
global $ibforums;
return <<<EOF
    <i>{$ibforums->lang['end']}</i>
    </td>
    </tr>
    </table>
EOF;
}

function pp_header($forum_name, $topic_title, $topic_starter) {
global $ibforums;
return <<<EOF
    <html>
    <head>
    <title>{$ibforums->vars['board_name']} [Powered by IB Forums]</title>
    </head>
    <body bgcolor='#FFFFFF' alink='#000000' vlink='#000000' link='#000000'>
    <table width='90%' border='0' align='center'>
    <tr>
    <td>
    <font face='arial' size='3' color='#000000'>
    <b>{$ibforums->lang['title']}</b>
    <br><br>
    -{$ibforums->vars['board_name']}<br>
    +--{$ibforums->lang['forum']}: $forum_name<br>
    +---{$ibforums->lang['topic']}: $topic_title {$ibforums->lang['start']} $topic_starter
    <hr noshade size='1' color='#000000'>
    <br>
    <br>
EOF;
}


}
?>