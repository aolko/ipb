<?php

/*
+--------------------------------------------------------------------------
|   IBFORUMS v1
|   ========================================
|   by Matthew Mecham and David Baxter
|   (c) 2001,2002 IBForums
|   http://www.ibforums.com
|   ========================================
|   Web: http://www.ibforums.com
|   Email: phpboards@ibforums.com
|   Licence Info: phpib-licence@ibforums.com
+---------------------------------------------------------------------------
|
|   > Edit post library
|   > Module written by Matt Mecham
|   > Date started: 19th February 2002
|
|	> Module Version Number: 1.0.0
+--------------------------------------------------------------------------
*/



class post_functions extends Post {

	var $nav               = array();
	var $title             = "";
	var $post              = array();
	var $topic             = array();
	var $upload            = array();
	var $moderator         = array( 'member_id' => 0, 'member_name' => "", 'edit_post' => 0 );
	var $orig_post         = array();

	function post_functions($class) {
	
		global $ibforums, $std, $DB;
		
		// Lets load the topic from the database before we do anything else.
		
		$DB->query("SELECT * FROM ibf_topics WHERE tid='".$ibforums->input['t']."'");
		$this->topic = $DB->fetch_row();
		
		// Is it legitimate?
		
		if (! $this->topic['tid'])
		{
			$std->Error( array( LEVEL => 1, MSG => 'missing_files') );
		}
		
		// Load the old post
		
		$DB->query("SELECT * FROM ibf_posts WHERE pid='".$ibforums->input['p']."'");
		$this->orig_post = $DB->fetch_row();
		
		if (! $this->orig_post['pid'])
		{
			$std->Error( array( LEVEL => 1, MSG => 'missing_files') );
		}
		
		// Load the moderator
		
		if ($ibforums->member['id'])
		{
			$DB->query("SELECT member_id, member_name, mid, edit_post FROM ibf_moderators WHERE member_id='".$ibforums->member['id']."' and forum_id='".$class->forum['id']."'");
			$this->moderator = $DB->fetch_row();
		}
		
		//-------------------------------------------------
		// Lets do some tests to make sure that we are
		// allowed to edit this topic
		//-------------------------------------------------
		
		$can_edit = 0;
		
		if ($ibforums->member['g_is_supmod'])
		{
			$can_edit = 1;
		}
		if ($this->moderator['edit_post'])
		{
			$can_edit = 1;
		}
		if ( ($this->orig_post['author_id'] == $ibforums->member['id']) and ($ibforums->member['g_edit_posts']) )
		{
			$can_edit = 1;
		}
		
		if ($can_edit != 1)
		{
			$std->Error( array( LEVEL => 1, MSG => 'not_op') );
		}
		
		// Is the topic locked?
		
		if (($this->topic['state'] != 'open') and (!$ibforums->member['g_is_supmod']))
		{
			$std->Error( array( LEVEL => 1, MSG => 'locked_topic') );
		}

	}
	
	function process($class) {
	
		global $ibforums, $std, $DB, $print;
		
		//-------------------------------------------------
		// Parse the post, and check for any errors.
		// overwrites saved post intentionally
		//-------------------------------------------------
		
		$this->post   = $class->compile_post();
		
		if ($class->obj['post_errors'] == "")
		{
			$this->upload = $class->process_upload();
		}
		
		if ( ($class->obj['post_errors'] != "") or ($class->obj['preview_post'] != "") )
		{
			// Show the form again
			$this->show_form($class);
		}
		else
		{
			$this->complete_edit($class);
		}
	}
	
	
	
	
	
	function complete_edit($class) {
		
		global $ibforums, $std, $DB, $print;
		
		$time = $std->get_date( time(), 'LONG' );
		
		//-------------------------------------------------
		// Reset some data
		//-------------------------------------------------
		
		$this->post['attach_id']   = $this->orig_post['attach_id'];
		$this->post['attach_type'] = $this->orig_post['attach_type'];
		$this->post['attach_hits'] = $this->orig_post['attach_hits'];
		$this->post['ip_address']  = $this->orig_post['ip_address'];
		$this->post['topic_id']    = $this->orig_post['topic_id'];
		$this->post['author_id']   = $this->orig_post['author_id'];
		$this->post['pid']         = $this->orig_post['pid'];
		$this->post['post_date']   = $this->orig_post['post_date'];
		$this->post['author_name'] = $this->orig_post['author_name'];
		
		// If the post icon has changed, update the topic post icon
		
		if ($this->orig_post['new_topic'] == 1)
		{
			if ($this->post['icon_id'] != $this->orig_post['icon_id'])
			{
				$DB->query("UPDATE ibf_topics SET icon_id='".$this->post['icon_id']."' WHERE tid='".$this->topic['tid']."'");
			}
		}
		//-------------------------------------------------
		// Update the database (ib_forum_post)
		//-------------------------------------------------
		
		if ($ibforums->member['g_append_edit'])
		{
			$this->post['post'] .= "\n\n<br><br><!--EDIT|".$ibforums->member['name']."|".$time."-->";
		}
		
		$db_string = $DB->compile_db_update_string( $this->post );
		
		$DB->query("UPDATE ibf_posts SET $db_string WHERE pid='".$this->post['pid']."'");

		
		//-------------------------------------------------
		// Redirect them back to the topic
		//-------------------------------------------------
		
		$print->redirect_screen( $ibforums->lang['post_edited'], "act=ST&f={$class->forum['id']}&t={$this->topic['tid']}&st={$ibforums->input['st']}#entry{$this->post['pid']}");
		
	}






	function show_form($class) {
	
		global $ibforums, $std, $DB, $print, $HTTP_POST_VARS;
		
		//-------------------------------------------------
		// Sort out the "raw" textarea input and make it safe incase
		// we have a <textarea> tag in the raw post var.
		//-------------------------------------------------
		
		$raw_post = isset($HTTP_POST_VARS['Post'])  ? $HTTP_POST_VARS['Post'] : $class->parser->unconvert($this->orig_post['post'], $class->forum['use_ibc'], $class->forum['use_html']);

		if (isset($raw_post)) {
			$raw_post = preg_replace( "/<\/textarea>/", "&lt;/textarea>", $raw_post );
			$raw_post = str_replace( '$', "&#036;", $raw_post );
			$raw_post = str_replace( '<%', "&lt;%"  , $raw_post );
			$raw_post = stripslashes($raw_post);
		}
		
		//-------------------------------------------------
		// Do we have any posting errors?
		//-------------------------------------------------
		
		if ($class->obj['post_errors'])
		{
			$class->output .= $class->html->errors( $ibforums->lang[ $class->obj['post_errors'] ]);
		}
		
		if ($class->obj['preview_post'])
		{
			$class->output .= $class->html->preview( $class->parser->convert( array( 'TEXT' => $this->post['post'], 'CODE' => $class->forum['use_ibc'], 'SMILIES' => $ibforums->input['enableemo'], 'HTML' => $class->forum['use_html']) ) );
		}
		
		$class->output .= $class->html_start_form( array( 1 => array( 'CODE', '09' ),
														  2 => array( 't'   , $this->topic['tid']),
														  3 => array( 'p'   , $ibforums->input['p'] ),
														  4 => array( 'st'  , $ibforums->input['st'] ),
														) );
														
		//---------------------------------------
		// START TABLE
		//---------------------------------------
		
		$class->output .= $class->html->table_structure();
		
		//---------------------------------------
		
		$start_table = $class->html->table_top( "{$ibforums->lang['top_txt_edit']} {$this->topic['title']}");
		
		$name_fields = $class->html_name_field();
		
		$post_box    = $class->html_post_body( $raw_post );
		
		$end_form    = $class->html->EndForm( $ibforums->lang['submit_edit'] );
		
		$post_icons  = $class->html_post_icons($this->orig_post['icon_id']);
		
		//---------------------------------------
		
		$class->output = preg_replace( "/<!--START TABLE-->/" , "$start_table"  , $class->output );
		$class->output = preg_replace( "/<!--NAME FIELDS-->/" , "$name_fields"  , $class->output );
		$class->output = preg_replace( "/<!--POST BOX-->/"    , "$post_box"     , $class->output );
		$class->output = preg_replace( "/<!--POST ICONS-->/"  , "$post_icons"   , $class->output );
		$class->output = preg_replace( "/<!--END TABLE-->/"   , "$end_form"     , $class->output );
		
		//---------------------------------------
		
		$class->html_add_smilie_box();
		
		$class->html_topic_summary($this->topic['tid']);
		
		$this->nav = array( "<a href='{$class->base_url}&act=SC&c={$class->forum['cat_id']}'>{$class->forum['cat_name']}</a>",
							"<a href='{$class->base_url}&act=SF&f={$class->forum['id']}'>{$class->forum['name']}</a>",
							"<a href='{$class->base_url}&act=ST&f={$class->forum['id']}&t={$this->topic['tid']}'>{$this->topic['title']}</a>",
						  );
						  
		$this->title = $ibforums->lang['editing_post'].' '.$this->topic['title'];
		
		$print->add_output("$class->output");
		
        $print->do_output( array( 'TITLE'    => $ibforums->vars['board_name']." -> ".$this->title,
        					 	  'JS'       => 1,
        					 	  'NAV'      => $this->nav,
        					  ) );
		
	}
	

}

?>