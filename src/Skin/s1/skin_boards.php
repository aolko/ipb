<?php

class skin_boards {



function stats_header() {
global $ibforums;
return <<<EOF
<!-- Board Stats -->
    <br>
    <table cellpadding='2' width='{$ibforums->skin['tbl_width']}' align='center' id='solidborder'>
    <tr>
     <td>
     
    <table cellpadding='0' cellspacing='0' border='0' width='100%' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
        <tr>
          <td>
            <table cellpadding='4' cellspacing='1' border='0' width='100%'>
            <tr>
			   <td id='titlemedium' colspan='2'>{$ibforums->lang['board_stats']}</td>
		   </tr>
EOF;
}

function ActiveUsers($active) {
global $ibforums;
return <<<EOF
        <tr>
           <td id='category' colspan='2'>$active[TOTAL] {$ibforums->lang['active_users']}</td>
    	</tr>
    	<tr>
          <td width="5%" id='forum1'>{$ibforums->skin['F_ACTIVE']}</td>
          <td id='forum2' width='95%'><b>{$active[GUESTS]}</b> {$ibforums->lang['guests']}, <b>$active[MEMBERS]</b> {$ibforums->lang['public_members']} <b>$active[ANON]</b> {$ibforums->lang['anon_members']} {$active[LINK]}<br>{$active[NAMES]}</td>
        </tr>
EOF;
}

function ShowStats($text) {
global $ibforums;
return <<<EOF
		   <tr>
		     <td id='category' colspan='2'>{$ibforums->lang['board_stats']}</td>
		   </tr>
		   <tr>
			 <td id='forum1' width='5%' valign='middle'>{$ibforums->skin['F_STATS']}</td>
			 <td id='forum2' width="95%" align='left'>$text<br>{$ibforums->lang['most_online']}</td>
		   </tr>
EOF;
}

function birthdays($birthusers="", $total="", $birth_lang="") {
global $ibforums;
return <<<EOF
        <tr>
           <td id='category' colspan='2'>{$ibforums->lang['birthday_header']}</td>
    	</tr>
    	<tr>
          <td id='forum1' width='5%' valign='middle'>{$ibforums->skin['F_ACTIVE']}</td>
          <td id='forum2' width='95%'><b>$total</b> $birth_lang<br>$birthusers</td>
        </tr>
EOF;
}

function stats_footer() {
global $ibforums;
return <<<EOF
         </table>
		</td>
	  </tr>
	 </table>
	 
	 </td>
	 </tr>
	 </table>
    <!-- Board Stats -->
EOF;
}

function BoardInformation() {
global $ibforums;
return <<<EOF
   <br>
   <table cellspacing='4' cellpadding='0' width='70%' border='0' align='center'>
      <tr>
       <td align='center'>[ <a href="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Login&CODE=06">{$ibforums->lang['d_delete_cookies']}</a> ] :: [ <a href="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Login&CODE=05">{$ibforums->lang['d_post_read']}</a> ]</td>
    </tr>
   </table>
EOF;
}

function CatHeader_Expanded($Data) {
global $ibforums;
return <<<EOF
<table width="{$ibforums->skin['tbl_width']}" align="center" border="0" cellspacing="1" cellpadding="0" bgcolor="{$ibforums->skin['tbl_border']}">
  <tr> 
    <td id='maintitleback' background='{$ibforums->vars['img_url']}/tile_back.gif'> 
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr> 
          <td>{$ibforums->skin['CAT_IMG']}</td>
          <td width="100%" id="maintitle"><a href="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&c={$Data['id']}">{$Data['name']}</a></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td id='mainbg'> 
      <table width="100%" border="0" cellspacing="1" cellpadding="4">
        <tr> 
          <td align="center" nowrap id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'><img src="{$ibforums->vars['img_url']}/spacer.gif" alt="" width="28" height="1"></td>
          <td width="59%" nowrap id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['cat_name']}</td>
          <td align="center" width="7%" nowrap id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['topics']}</td>
          <td align="center" width="7%" nowrap id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['replies']}</td>
          <td width="27%" nowrap id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['last_post_info']}</td>
        </tr>
EOF;
}

function subheader() {
global $ibforums;
return <<<EOF
	<table width="{$ibforums->skin['tbl_width']}" align="center" border="0" cellspacing="1" cellpadding="0" bgcolor="{$ibforums->skin['tbl_border']}">
	  <tr> 
		<td id='mainbg'> 
            <table width="100%" border="0" cellspacing="1" cellpadding="4">
			<tr> 
			  <td align="center" nowrap id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'><img src="{$ibforums->vars['img_url']}/spacer.gif" alt="" width="28" height="1"></td>
			  <td width="59%" nowrap id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['cat_name']}</td>
			  <td align="center" width="7%" nowrap id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['topics']}</td>
			  <td align="center" width="7%" nowrap id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['replies']}</td>
			  <td width="27%" nowrap id='titlemedium' background='{$ibforums->vars['img_url']}/tile_sub.gif'>{$ibforums->lang['last_post_info']}</td>
			</tr>
EOF;
}

function end_this_cat() {
global $ibforums;
return <<<EOF
<tr> 
          <td id='mainfoot' colspan="5">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
EOF;
}

function end_all_cats() {
global $ibforums;
return <<<EOF
	<!--</table>
   </td>
   </tr>
   </table>-->
EOF;
}

function newslink( $fid="", $title="", $tid="" ) {
global $ibforums;
return <<<EOF
<b>{$ibforums->vars['board_name']} {$ibforums->lang['newslink']} <a href='{$ibforums->base_url}&act=ST&f=$fid&t=$tid'>$title</a></b><br>
EOF;
}

function PageTop($lastvisit) {
global $ibforums;
return <<<EOF
	<table cellpadding='4' cellspacing='0' border='0' width='{$ibforums->skin['tbl_width']}' align='center'>
	 <tr>
	  <td align='left' valign='bottom'><!-- IBF.NEWSLINK -->{$ibforums->lang['welcome_back_text']} $lastvisit</td>
	  <td align='right' valign='bottom'>
		  <a href='{$ibforums->base_url}&act=Stats&CODE=leaders'>{$ibforums->lang['sm_forum_leaders']}</a> |
		  <a href='{$ibforums->base_url}&act=Search&CODE=getactive'>{$ibforums->lang['sm_todays_posts']}</a><br>
		  <a href='{$ibforums->base_url}&act=Stats'>{$ibforums->lang['sm_today_posters']}</a> |
		  <a href='{$ibforums->base_url}&act=Members&max_results=10&sort_key=posts&sort_order=desc'>{$ibforums->lang['sm_all_posters']}</a>
	  </td>
	 </tr>
	</table>
EOF;
}

function ForumRow($info) {
global $ibforums;
return <<<EOF
    <!-- Forum {$info['id']} entry -->
        <tr> 
          <td id="forum2" align="center">{$info['img_new_post']}</td>
          <td id="forum2" width="59%"><span id="linkthru"><b><a href="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=SF&f={$info['id']}">{$info['name']}</a></b></span><br><span id='desc'>{$info['description']}</span><br>{$info['moderator']}</td>
          <td id="forum1" align="center" width="7%">{$info['topics']}</td>
          <td id="forum1" align="center" width="7%">{$info['posts']}</td>
          <td id="forum1" width="27%">{$info['last_post']}<br>{$ibforums->lang['in']}: {$info['last_topic']}<br>{$ibforums->lang['by']}: {$info['last_poster']}</td>
        </tr>
    <!-- End of Forum {$info['id']} entry -->
EOF;
}


}
?>