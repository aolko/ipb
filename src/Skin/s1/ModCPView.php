<?php

class ModCPView {



function topic_none() {
global $ibforums;
return <<<EOF
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' colspan='2'><i>{$ibforums->lang['q_t_n']}</i></td>
            </tr>
EOF;
}

function endcp() {
global $ibforums;
return <<<EOF
      </td>
     </tr>
    </table>
   </td>
  </tr>
 </table>
EOF;
}

function post_none() {
global $ibforums;
return <<<EOF
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' colspan='2'><i>{$ibforums->lang['q_p_n']}</i></td>
            </tr>
EOF;
}

function end_forum_splash() {
global $ibforums;
return <<<EOF
       </table>
      </td>
     </tr>
    </table>
EOF;
}

function post_form_header($count) {
global $ibforums;
return <<<EOF
            <tr>
             <td bgcolor='{$ibforums->skin['TITLEBACK']}' id='titlemedium' colspan='2'>{$ibforums->lang['q_posts']} $count</td>
            </tr>
EOF;
}

function prune_form_header() {
global $ibforums;
return <<<EOF
            <tr>
             <td bgcolor='{$ibforums->skin['TITLEBACK']}' id='titlemedium' colspan='2'>{$ibforums->lang['search_prune']}</td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' colspan='2'>
               <i>{$ibforums->lang['define_search']}</i>
               <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post" name='theForm'>
               <input type='hidden' name='act' value='ModCP'>
               <input type='hidden' name='CODE' value='process_prune'>
               <input type='hidden' name='s' value='{$ibforums->session_id}'>
               <input type='hidden' name='f' value='$iB::IN{'f'}'>
            </td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['s_prune_cutoff']}</td>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><input type='text' name='DAYS' size='5' maxlength='10' class='forminput'></td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['s_prune_perpage']}</td>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><input type='text' name='PAGE' size='5' maxlength='10' value='20' class='forminput'></td>
            </tr>
            <tr>
            <td bgcolor='{$ibforums->skin['MISCBACK_TWO']}' colspan='2' align='center'><input type='button' class='forminput' value='{$ibforums->lang['prune_forum']}' onClick='check_prune();'></form></td>
            </tr>
EOF;
}

function show_post_results($data) {
global $ibforums;
return <<<EOF
            <tr>
            <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>
                <b>{$ibforums->lang['a_topic_results']}</b><hr noshade size=1 color='{$ibforums->skin['TABLE_BORDER_COL']}'>
                {$ibforums->lang['a_topic_deleted']} {$data[DELETED]}<br><br>
                {$ibforums->lang['a_topic_approved']} {$data[APPROVED]}<br><br>
                {$ibforums->lang['a_topic_left']} {$data[LEFT]}<br><br>
            </td>
            </tr>
EOF;
}

function topic_header($data) {
global $ibforums;
return <<<EOF
            <tr>
             <td><b>{$ibforums->lang['f_moderate']} {$data[FORUM_NAME]}</b><br>{$ibforums->lang['f_moderate_t']}
               <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post" name='theForm'>
               <input type='hidden' name='act' value='ModCP'>
               <input type='hidden' name='CODE' value='{$data[search_type]}'>
               <input type='hidden' name='s' value='{$ibforums->session_id}'>
               <input type='hidden' name='f' value='$iB::IN{'f'}'>
            </td>
            </tr>
            <tr>
              <td>
               <table cellspacing='1' cellpadding='0' border='0' width='95%' align='center' bgcolor='{$ibforums->skin['TABLE_BORDER_COL']}'>
                 <tr>
                  <td>
                    <table cellspacing='1' cellpadding='4' border='0' width='100%' align='center'>
                     <tr>
                       <td width='15%' bgcolor='{$ibforums->skin['TITLEBACK']}'  id='titlemedium'>{$ibforums->lang['tt_action']}</td>
                       <td width='45%' bgcolor='{$ibforums->skin['TITLEBACK']}'  id='titlemedium'>{$ibforums->lang['st_topic']}</td>
                       <td width='5%'  bgcolor='{$ibforums->skin['TITLEBACK']}'  id='titlemedium'>{$ibforums->lang['st_posts']}</td>
                       <td width='35%' bgcolor='{$ibforums->skin['TITLEBACK']}'  id='titlemedium'>{$ibforums->lang['st_last']}</td>
                     </tr>
EOF;
}

function open_form_header() {
global $ibforums;
return <<<EOF
            <tr>
             <td bgcolor='{$ibforums->skin['TITLEBACK']}' id='titlemedium' colspan='2'>{$ibforums->lang['search_openclose']} $count</td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' colspan='2'>
               <i>{$ibforums->lang['define_search']}</i>
               <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post" name='theForm'>
               <input type='hidden' name='act' value='ModCP'>
               <input type='hidden' name='CODE' value='process_open'>
               <input type='hidden' name='s' value='{$ibforums->session_id}'>
               <input type='hidden' name='f' value='$iB::IN{'f'}'>
            </td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['s_topics_cutoff']}</td>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><input type='text' name='DAYS' size='5' maxlength='10' class='forminput'></td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['s_sort']}</td>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><select name='ORDER' class='forminput'><option value='asc'>{$ibforums->lang['s_topics_asc']}</option><option value='desc' selected>{$ibforums->lang['s_topics_desc']}</option></select></td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['s_limit']}</td>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><input type='text' name='LIMIT' size='5' maxlength='10' class='forminput'></td>
            </tr>
            <tr>
            <td bgcolor='{$ibforums->skin['MISCBACK_TWO']}' colspan='2' align='center'><input type='submit' class='forminput' value='{$ibforums->lang['submit_topics']}'></form></td>
            </tr>
EOF;
}

function render_topic_post($data) {
global $ibforums;
return <<<EOF
                     <tr>
                      <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' valign='middle'><input type='hidden' name='POST_{$data[POST]}->{'ID'}' value='{$data[POST]}->{'POST_ID'}'><input type='hidden' name='TOPIC_{$data[POST]}->{'ID'}' value='{$data[TOPIC]}->{'TOPIC_ID'}'><select name='CHOICE_{$data[POST]}->{'ID'}' class='forminput'><option value='APPROVE' selected>{$ibforums->lang['c_approve']}</option>{$data[TOPIC]}->{'DELETE'}<option value='LEAVE' selected>{$ibforums->lang['c_leave']}</option></select></td>
                      <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' valign='top'>
                        <table cellspacing='1' cellpadding='4' border='0' width='100%' align='center'>
                        <tr>
                          <td valign='top' width='15%'>{$data[POST]}->{'MEMBER_NAME'}</td>
                          <td valign='top' width='85%'><b>{$ibforums->lang['ss_posted_on']}</b> {$data[POST]}->{'POST_DATE'}<hr noshade size=1 color='{$ibforums->skin['TABLE_BORDER_COL']}'>{$data[POST]}->{'POST'} ( <a href="javascript:PopUp('{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ModCP&f=$iB::IN{'f'}&CODE=readpost&ID={$data[POST]}->{'ID'}','ViewPost','400','200','0','1','1','1')">{$ibforums->lang['ss_read_more']}</a> )</td>
                        </tr>
                        </table>
                      </td>
                    </tr>
EOF;
}

function online_menu() {
global $ibforums;
return <<<EOF
    <tr>
     <td bgcolor='{$ibforums->skin['MISCBACK_TWO']}' id='controlpanel'>{$ibforums->lang['m_moderate']}</td>
    </tr>
    <tr>
     <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ModCP&f=$iB::IN{'f'}'>{$ibforums->lang['m_approve']}</a></td>
    </tr>
    <tr>
     <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ModCP&f=$iB::IN{'f'};CODE=prune'>{$ibforums->lang['m_prune']}</a></td>
    </tr>
    <tr>
     <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ModCP&f=$iB::IN{'f'};CODE=delete'>{$ibforums->lang['m_delete']}</a></td>
    </tr>
    <tr>
     <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ModCP&f=$iB::IN{'f'};CODE=open'>{$ibforums->lang['m_open']}</a></td>
    </tr>
    <tr>
     <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ModCP&f=$iB::IN{'f'};CODE=move'>{$ibforums->lang['m_move']}</a></td>
    </tr>
    <!-- To be added later
    <tr>
     <td bgcolor='{$ibforums->skin['MISCBACK_TWO']}' id='controlpanel'>{$ibforums->lang['m_prefs']}</td>
    </tr>
    <tr>
     <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><a href='{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ModCP&f=$iB::IN{'f'}'>{$ibforums->lang['m_email']}</a></td>
    </tr>
    -->
EOF;
}

function show_topic_results($data) {
global $ibforums;
return <<<EOF
            <tr>
            <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>
                <b>{$ibforums->lang['a_post_results']}</b><hr noshade size=1 color='{$ibforums->skin['TABLE_BORDER_COL']}'>
                {$ibforums->lang['a_post_deleted']} {$data[DELETED]}<br><br>
                {$ibforums->lang['a_post_approved']} {$data[APPROVED]}<br><br>
                {$ibforums->lang['a_post_left']} {$data[LEFT]}<br><br>
            </td>
            </tr>
EOF;
}

function main_splash() {
global $ibforums;
return <<<EOF
            <tr>
             <td><b>{$ibforums->lang['mod_forum_t']}</b><br>{$ibforums->lang['mod_forum']}</td>
            </tr>
            <tr>
              <td>
               <table cellspacing='1' cellpadding='0' border='0' width='95%' align='center' bgcolor='{$ibforums->skin['TABLE_BORDER_COL']}'>
                 <tr>
                  <td>
                    <table cellspacing='1' cellpadding='4' border='0' width='100%' align='center'>
                     <tr>
                       <td width='54%' bgcolor='{$ibforums->skin['TITLEBACK']}' id='titlemedium'>{$ibforums->lang['forum_name']}</td>
                       <td width='6%' bgcolor='{$ibforums->skin['TITLEBACK']}'  id='titlemedium'>{$ibforums->lang['forum_topics']}</td>
                       <td width='5%' bgcolor='{$ibforums->skin['TITLEBACK']}'  id='titlemedium'>{$ibforums->lang['forum_posts']}</td>
                       <td width='35%' bgcolor='{$ibforums->skin['TITLEBACK']}'  id='titlemedium'>{$ibforums->lang['last_post']}</td>
                     </tr>
EOF;
}

function move_form_header() {
global $ibforums;
return <<<EOF
            <tr>
             <td bgcolor='{$ibforums->skin['TITLEBACK']}' id='titlemedium' colspan='2'>{$ibforums->lang['search_move']}</td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' colspan='2'>
               <i>{$ibforums->lang['define_search']}</i>
               <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post" name='theForm'>
               <input type='hidden' name='act' value='ModCP'>
               <input type='hidden' name='CODE' value='process_move'>
               <input type='hidden' name='s' value='{$ibforums->session_id}'>
               <input type='hidden' name='f' value='$iB::IN{'f'}'>
            </td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['s_topics_cutoff']}</td>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><input type='text' name='DAYS' size='5' maxlength='10' class='forminput'></td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['s_sort']}</td>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><select name='ORDER' class='forminput'><option value='asc'>{$ibforums->lang['s_topics_asc']}</option><option value='desc' selected>{$ibforums->lang['s_topics_desc']}</option></select></td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['s_limit']}</td>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><input type='text' name='LIMIT' size='5' maxlength='10' class='forminput'></td>
            </tr>
            <tr>
            <td bgcolor='{$ibforums->skin['MISCBACK_TWO']}' colspan='2' align='center'><input type='submit' class='forminput' value='{$ibforums->lang['submit_topics']}'></form></td>
            </tr>
EOF;
}

function delete_form_header() {
global $ibforums;
return <<<EOF
            <tr>
             <td bgcolor='{$ibforums->skin['TITLEBACK']}' id='titlemedium' colspan='2'>{$ibforums->lang['search_delete']} $count</td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' colspan='2'>
               <i>{$ibforums->lang['define_search']}</i>
               <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post" name='theForm'>
               <input type='hidden' name='act' value='ModCP'>
               <input type='hidden' name='CODE' value='process_delete'>
               <input type='hidden' name='s' value='{$ibforums->session_id}'>
               <input type='hidden' name='f' value='$iB::IN{'f'}'>
            </td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['s_topics_cutoff']}</td>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><input type='text' name='DAYS' size='5' maxlength='10' class='forminput'></td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['s_sort']}</td>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><select name='ORDER' class='forminput'><option value='asc'>{$ibforums->lang['s_topics_asc']}</option><option value='desc' selected>{$ibforums->lang['s_topics_desc']}</option></select></td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['s_limit']}</td>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><input type='text' name='LIMIT' size='5' maxlength='10' class='forminput'></td>
            </tr>
            <tr>
            <td bgcolor='{$ibforums->skin['MISCBACK_TWO']}' colspan='2' align='center'><input type='submit' class='forminput' value='{$ibforums->lang['submit_topics']}'></form></td>
            </tr>
EOF;
}

function render_cat($data) {
global $ibforums;
return <<<EOF
     <tr>
       <td bgcolor="{$ibforums->skin['CAT_BACK']}" id='category' colspan='4'>{$data[CAT_NAME]}</td>
     </tr>
EOF;
}

function topic_footer() {
global $ibforums;
return <<<EOF
        <tr>
          <td bgcolor='{$ibforums->skin['MISCBACK_TWO']}' colspan='4' align='center'><input type='submit' value='{$ibforums->lang['process_records']}' class='forminput'></td>
        </tr> 
       </table>
      </td>
     </tr>
    </table>
EOF;
}

function show_move_results($data) {
global $ibforums;
return <<<EOF
            <tr>
            <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>
                <b>{$ibforums->lang['a_topic_results']}</b><hr noshade size=1 color='{$ibforums->skin['TABLE_BORDER_COL']}'>
                {$ibforums->lang['a_topic_d_left']} {$data[leave]}<br><br>
                {$ibforums->lang['a_topic_moved']} {$data[move]}<br><br>
            </td>
            </tr>
EOF;
}

function search_header($data) {
global $ibforums;
return <<<EOF
            <tr>
             <td><b>{$ibforums->lang['f_moderate']} {$data[FORUM_NAME]}</b><br>{$ibforums->lang['f_moderate_t']}
               <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post" name='theForm'>
               <input type='hidden' name='act' value='ModCP'>
               <input type='hidden' name='CODE' value='{$data[search_type]}'>
               <input type='hidden' name='s' value='{$ibforums->session_id}'>
               <input type='hidden' name='f' value='$iB::IN{'f'}'>
            </td>
            </tr>
            <tr>
              <td>
               <table cellspacing='1' cellpadding='0' border='0' width='95%' align='center' bgcolor='{$ibforums->skin['TABLE_BORDER_COL']}'>
                 <tr>
                  <td>
                    <table cellspacing='1' cellpadding='4' border='0' width='100%' align='center'>
                     <tr>
                       <td width='15%' bgcolor='{$ibforums->skin['TITLEBACK']}' id='titlemedium'>{$ibforums->lang['tt_action']}</td>
                       <td width='85%' bgcolor='{$ibforums->skin['TITLEBACK']}'  id='titlemedium'>{$ibforums->lang['tt_details']}</td>
                     </tr>
EOF;
}

function move_ops($html, $forum_name) {
global $ibforums;
return <<<EOF
                <tr>
                  <td><table cellspacing='0' cellpadding='4' border='0' width='100%' align='center'>
                <tr>
                <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['move_topics_from']} <b>$forum_name</b> {$ibforums->lang['to']}:
               <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post" name='theForm'>
               <input type='hidden' name='act' value='ModCP'>
               <input type='hidden' name='CODE' value='do_move'>
               <input type='hidden' name='s' value='{$ibforums->session_id}'>
               <input type='hidden' name='f' value='$iB::IN{'f'}'>
                </td>
                <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>$html</td>
                </tr>
                <tr>
                <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><b>{$ibforums->lang['delete_old']}</b></td>
                <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>
                  <select name='leave' class='forminput'>
                  <option value='leave_full'  selected>{$ibforums->lang['leave_locked']}
                  <option value='leave_empty'>{$ibforums->lang['leave_empty']}
                  <option value='delete'>{$ibforums->lang['dont_leave']}
                  </select>
                </td>
                </tr>
                </table>
                </td>
                </tr>
EOF;
}

function read_post($data) {
global $ibforums;
return <<<EOF
               <table cellspacing='1' cellpadding='0' border='0' width='100%' align='center' bgcolor='{$ibforums->skin['TABLE_BORDER_COL']}'>
                 <tr>
                  <td>
                    <table cellspacing='1' cellpadding='4' border='0' width='100%' align='center'>
                     <tr>
                        <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$data[POST]}</td>
                     </tr>
                    </table>
                   </td>
                 </tr>
                </table>
EOF;
}

function forum_splash($data) {
global $ibforums;
return <<<EOF
            <tr>
             <td><b>{$ibforums->lang['f_moderate']} {$data[FORUM_NAME]}</b><br>{$ibforums->lang['f_moderate_t']}</td>
            </tr>
            <tr>
              <td>
               <table cellspacing='1' cellpadding='0' border='0' width='95%' align='center' bgcolor='{$ibforums->skin['TABLE_BORDER_COL']}'>
                 <tr>
                  <td>
                    <table cellspacing='1' cellpadding='4' border='0' width='100%' align='center'>
EOF;
}

function show_open_results($data) {
global $ibforums;
return <<<EOF
            <tr>
            <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>
                <b>{$ibforums->lang['a_topic_results']}</b><hr noshade size=1 color='{$ibforums->skin['TABLE_BORDER_COL']}'>
                {$ibforums->lang['a_topic_open']} {$data[open]}<br><br>
                {$ibforums->lang['a_topic_close']} {$data[closed]}<br><br>
            </td>
            </tr>
EOF;
}

function render_topic_title($data) {
global $ibforums;
return <<<EOF
                    <tr>
                      <td bgcolor='{$ibforums->skin['MISCBACK_TWO']}' colspan='2'>{$ibforums->lang['ss_topic_title']} <b>{$data[TOPIC_TITLE]}</b></td>
                    </tr>
EOF;
}

function render_forum($data) {
global $ibforums;
return <<<EOF
     <tr>
       <td bgcolor="{$ibforums->skin['FORUM_COL_TWO']}"><span id="linkthru"><b><a href="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ModCP&f={$data[FORUM_ID]}">{$data[FORUM_NAME]}</a></b></span><br>{$ibforums->lang['topic_to_mod']}&nbsp;<b>{$data[TOPICS_NOT_APPROVED]}</b></td>
        <td bgcolor="{$ibforums->skin['FORUM_COL_ONE']}" align='center' valign='middle'>{$data[FORUM_TOPICS]}</td>
        <td bgcolor="{$ibforums->skin['FORUM_COL_ONE']}" align='center' valign='middle' id='highlight'>{$data[FORUM_POSTS]}</td>
        <td bgcolor="{$ibforums->skin['FORUM_COL_ONE']}" align="left" valign='middle'><span id='highlight'>{$data[FORUM_LAST_POST]}</span>
     </tr>
EOF;
}

function render_newtopic($data) {
global $ibforums;
return <<<EOF
                    <tr>
                      <td bgcolor='{$ibforums->skin['MISCBACK_TWO']}' colspan='2'>{$ibforums->lang['ss_topic_title']} <b>{$data[TOPIC]}->{'TOPIC_TITLE'}</b></td>
                    </tr>
                      <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' valign='middle'><input type='hidden' name='R_POST_{$data[TOPIC]}->{'TOPIC_ID'}' value='{$data[POST]}->{'POST_ID'}'><input type='hidden' name='POST_{$data[TOPIC]}->{'TOPIC_ID'}' value='{$data[POST]}->{'ID'}'><select name='CHOICE_{$data[TOPIC]}->{'TOPIC_ID'}' class='forminput'><option value='APPROVE' selected>{$ibforums->lang['c_approve']}</option>{$data[TOPIC]}->{'DELETE'}<option value='LEAVE' selected>{$ibforums->lang['c_leave']}</option></select></td>
                      <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' valign='top'>
                        <table cellspacing='1' cellpadding='4' border='0' width='100%' align='center'>
                        <tr>
                          <td valign='top' width='15%'><a href="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Profile&CODE=03&MID={$data[TOPIC]}->{'TOPIC_STARTER'}" target='_blank'><b>{$data[TOPIC]}->{'TOPIC_STARTER_N'}</b></a></td>
                          <td valign='top' width='85%'><b>{$ibforums->lang['ss_posted_on']}</b> {$data[TOPIC]}->{'TOPIC_START_DATE'}<hr noshade size=1 color='{$ibforums->skin['TABLE_BORDER_COL']}'>{$data[POST]}->{'POST'} ( <a href="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=Profile&CODE=03&MID={$data[TOPIC]}->{'{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ModCP&f=$iB::IN{'f'}&CODE=readpost&ID={$data[POST]}->{'ID'}','ViewPost','400','200','0','1','1','1')">{$ibforums->lang['ss_read_more']}</a> )</td>
                        </tr>
                        </table>
                      </td>
                    </tr>
EOF;
}

function topic_form() {
global $ibforums;
return <<<EOF
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' colspan='2'>
               <i>{$ibforums->lang['define_search']}</i>
               <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post" name='theForm'>
               <input type='hidden' name='act' value='ModCP'>
               <input type='hidden' name='CODE' value='topic_search'>
               <input type='hidden' name='s' value='{$ibforums->session_id}'>
               <input type='hidden' name='f' value='$iB::IN{'f'}'>
            </td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['s_topics_cutoff']}</td>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><input type='text' name='DAYS' size='5' maxlength='10' class='forminput'></td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['s_sort']}</td>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><select name='ORDER' class='forminput'><option value='asc'>{$ibforums->lang['s_topics_asc']}</option><option value='desc' selected>{$ibforums->lang['s_topics_desc']}</option></select></td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['s_limit']}</td>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><input type='text' name='LIMIT' size='5' maxlength='10' class='forminput'></td>
            </tr>
            <tr>
            <td bgcolor='{$ibforums->skin['MISCBACK_TWO']}' colspan='2' align='center'><input type='submit' name='TOPICS' class='forminput' value='{$ibforums->lang['submit_topics']}'></form></td>
            </tr>
EOF;
}

function render_opentopic($data) {
global $ibforums;
return <<<EOF
                 <tr>
                  <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' valign='middle'>{$data[SELECT]}</td>
                  <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><span id="linkthru"><a href="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}?s={$ibforums->session_id}&act=ST&f={$data[FORUM_ID]}&t={$data[TOPIC_ID]}" target='_blank'>{$data[TOPIC_TITLE]}</a></span><br>{$data[TOPIC_DESC]}</td>
                  <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' align='center' valign='middle'>{$data[TOPIC_POSTS]}</td>
                  <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><span id='highlight'>{$data[TOPIC_LAST_DATE]}</span><br>{$ibforums->lang['st_by']} {$data[LAST_POSTER]}</td>
                 </tr>
EOF;
}

function post_form() {
global $ibforums;
return <<<EOF
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}' colspan='2'>
               <i>{$ibforums->lang['define_search']}</i>
               <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post" name='theForm'>
               <input type='hidden' name='act' value='ModCP'>
               <input type='hidden' name='CODE' value='post_search'>
               <input type='hidden' name='s' value='{$ibforums->session_id}'>
               <input type='hidden' name='f' value='$iB::IN{'f'}'>
            </td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['s_posts_cutoff']}</td>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><input type='text' name='DAYS' size='5' maxlength='10' class='forminput'></td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['s_sort']}</td>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><select name='ORDER' class='forminput'><option value='asc'>{$ibforums->lang['s_topics_asc']}</option><option value='desc' selected>{$ibforums->lang['s_topics_desc']}</option></select></td>
            </tr>
            <tr>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>{$ibforums->lang['s_limit']}</td>
             <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><input type='text' name='LIMIT' size='5' maxlength='10' class='forminput'></td>
            </tr>
            <tr>
            <td bgcolor='{$ibforums->skin['MISCBACK_TWO']}' colspan='2' align='center'><input type='submit' name='POSTS' value='{$ibforums->lang['submit_posts']}' class='forminput'></form></td>
            </tr>
EOF;
}

function startcp() {
global $ibforums;
return <<<EOF
     <script langauge="javascript">
     <!--
        function check_prune() {
            var message = "{$ibforums->lang['prune_confirm']} "+document.theForm.DAYS.value+"\\n{$ibforums->lang['prune_confirm_b']}";
            if ( confirm( message ) ) {
                document.theForm.submit();
            } else {
                return false;
            }
        }
        function PopUp(url, name, width,height,center,resize,scroll,posleft,postop) {
            if (posleft != 0) { x = posleft }
            if (postop  != 0) { y = postop  }
            if (!scroll) { scroll = 1 }
            if (!resize) { resize = 1 }
            if ((parseInt (navigator.appVersion) >= 4 ) && (center)) {
              X = (screen.width  - width ) / 2;
              Y = (screen.height - height) / 2;
            }
            if (scroll != 0) { scroll = 1 }
            var Win = window.open( url, name, 'width='+width+',height='+height+',top='+Y+',left='+X+',resizable='+resize+',scrollbars='+scroll+',location=no,directories=no,status=no,menubar=no,toolbar=no');
        }
     //-->
     </script>
     <table cellpadding='0' cellspacing='1' border='0' width='{$ibforums->skin['TABLE_WIDTH']}' bgcolor='{$ibforums->skin['TABLE_BORDER_COL']}' align='center'>
      <tr>
        <td width='20%' bgcolor='{$ibforums->skin['MISCBACK_ONE']}' valign='top'>
          <table cellspacing='0' cellpadding='4' border='0' width='100%' align='center'>
            <tr>
             <td bgcolor='{$ibforums->skin['TITLEBACK']}' valign='left' id='titlelarge'>{$ibforums->lang['menu']}</td>
            </tr>
          <!--menu goes here-->
          </table>
        </td>
        <td width='80%' bgcolor='{$ibforums->skin['MISCBACK_ONE']}' valign='top'>
          <table cellspacing='0' cellpadding='4' border='0' width='100%' align='center'>
            <tr>
             <td bgcolor='{$ibforums->skin['TITLEBACK']}' valign='left' id='titlelarge'>{$ibforums->lang['display']}</td>
            </tr>
          <!--main content-->
EOF;
}

function show_delete_results($data) {
global $ibforums;
return <<<EOF
            <tr>
            <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'>
                <b>{$ibforums->lang['a_topic_results']}</b><hr noshade size=1 color='{$ibforums->skin['TABLE_BORDER_COL']}'>
                {$ibforums->lang['a_topic_d_delete']} {$data[delete]}<br><br>
            </td>
            </tr>
EOF;
}

function search_footer() {
global $ibforums;
return <<<EOF
        <tr>
          <td bgcolor='{$ibforums->skin['MISCBACK_TWO']}' colspan='2' align='center'><input type='submit' value='{$ibforums->lang['process_records']}' class='forminput'></td>
        </tr> 
       </table>
      </td>
     </tr>
    </table>
EOF;
}

function topic_form_header($count) {
global $ibforums;
return <<<EOF
            <tr>
             <td bgcolor='{$ibforums->skin['TITLEBACK']}' id='titlemedium' colspan='2'>{$ibforums->lang['q_topics']} $count</td>
            </tr>
EOF;
}

function offline_menu() {
global $ibforums;
return <<<EOF
    <tr>
     <td bgcolor='{$ibforums->skin['MISCBACK_ONE']}'><b>{$ibforums->lang['menu_off']}</b></td>
    </tr>
EOF;
}

function end_main_splash() {
global $ibforums;
return <<<EOF
       </table>
      </td>
     </tr>
    </table>
EOF;
}


}
?>