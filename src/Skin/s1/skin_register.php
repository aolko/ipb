<?php

class skin_register {



function lost_pass_form() {
global $ibforums;
return <<<EOF
     <br>
     <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post">
     <input type='hidden' name='act' value='Reg'>
     <input type='hidden' name='CODE' value='11'>
     <table cellpadding='0' cellspacing='4' border='0' width='{$ibforums->skin['tbl_width']}' align='center'>
     <tr>
        <td valign='middle' align='left'><b>{$ibforums->lang['lp_header']}</b><br><br>{$ibforums->lang['lp_text']}</td>
     </tr>
     </table>
     <br>
     <table cellpadding='0' cellspacing='0' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
        <tr>
            <td>
                <table cellpadding='3' cellspacing='1' border='0' width='100%'>
                <tr>
                <td valign='left' colspan='2' id='titlemedium'>{$ibforums->lang['complete_form']}</td>
                </tr>
                <tr>
                <td id='row1' width='40%'>{$ibforums->lang['lp_user_name']}</td>
                <td id='row1'><input type='text' size='32' maxlength='32' name='member_name' class='forminput'></td>
                </tr>
                <tr>
                <td id='row2' align='center' colspan='2'>
                <input type="submit" value="{$ibforums->lang['lp_send']}" class='forminput'>
                </td></tr></table>
                </td></tr></table>
                </form>
EOF;
}

function show_authorise($member) {
global $ibforums;
return <<<EOF
     <table cellpadding='0' cellspacing='0' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
        <tr>
            <td>
                <table cellpadding='3' cellspacing='1' border='0' width='100%'>
                <tr>
                <td valign='left' id='titlemedium'>{$ibforums->lang['registration_process']}</td>
                </tr>
                <tr>
                <td id='row1'>{$ibforums->lang['thank_you']} {$member['name']}. {$ibforums->lang['auth_text']}  {$member['email']}</td>
                </tr>
                </table>
            </td>
        </tr>
    </table>
EOF;
}

function show_preview($member) {
global $ibforums;
return <<<EOF
     <table cellpadding='0' cellspacing='0' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
        <tr>
            <td>
                <table cellpadding='3' cellspacing='1' border='0' width='100%'>
                <tr>
                <td valign='left' id='titlemedium'>{$ibforums->lang['registration_process']}</td>
                </tr>
                <tr>
                <td id='row1'>{$ibforums->lang['thank_you']} {$member['name']}. {$ibforums->lang['preview_reg_text']}</td>
                </tr>
                </table>
            </td>
        </tr>
    </table>
EOF;
}

function show_dumb_form($type="reg") {
global $ibforums;
return <<<EOF
    <script language='javascript'>
    <!--
    function Validate() {
        // Check for Empty fields
        if (document.REG.uid.value == "" || document.REG.aid.value == "") {
            alert ("{$ibforums->lang['js_blanks']}");
            return false;
        }

    }
    //-->
    </script>
     <br>
     <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post" name='REG' onSubmit='return Validate()'>
     <input type='hidden' name='act' value='Reg'>
     <input type='hidden' name='CODE' value='03'>
     <input type='hidden' name='type' value='$type'>
     <table cellpadding='0' cellspacing='4' border='0' width='{$ibforums->skin['tbl_width']}' align='center'>
     <tr>
        <td valign='middle' align='left'><b>{$ibforums->lang['dumb_header']}</b><br><br>{$ibforums->lang['dumb_text']}</td>
     </tr>
     </table>
     <br>
     <table cellpadding='0' cellspacing='0' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
        <tr>
            <td>
                <table cellpadding='3' cellspacing='1' border='0' width='100%'>
                <tr>
                <td valign='left' colspan='2' id='titlemedium'>{$ibforums->lang['complete_form']}</td>
                </tr>
                <tr>
                <td id='row1' width='40%'>{$ibforums->lang['user_id']}</td>
                <td id='row1'><input type='text' size='32' maxlength='32' name='uid' class='forminput'></td>
                </tr>
                <tr>
                <td id='row2' width='40%'>{$ibforums->lang['val_key']}</td>
                <td id='row2'><input type='text' size='32' maxlength='50' name='aid' class='forminput'></td>
                </tr>
                <tr>
                <td id='row2' align='center' colspan='2'>
                <input type="submit" value="{$ibforums->lang['dumb_submit']}" class='forminput'>
                </td></tr></table>
                </td></tr></table>
                </form>
EOF;
}

function ShowForm($data) {
global $ibforums;
return <<<EOF
    <script language='javascript'>
    <!--
    function Validate() {
        // Check for Empty fields
        if (document.REG.UserName.value == "" || document.REG.PassWord.value == "" || document.REG.PassWord_Check.value == "" || document.REG.EmailAddress.value == "") {
            alert ("{$ibforums->lang['js_blanks']}");
            return false;
        }

        // Have we checked the checkbox?

        if (document.REG.agree.checked == true) {
            return true;
        } else {
            alert ("{$ibforums->lang['js_no_check']}");
            return false;
        }
    }
    //-->
    </script>
     <br>
     <form action="{$ibforums->vars['board_url']}/index.{$ibforums->vars['php_ext']}" method="post" name='REG' onSubmit='return Validate()'>
     <input type='hidden' name='act' value='Reg'>
     <input type='hidden' name='CODE' value='02'>
     <table cellpadding='0' cellspacing='4' border='0' width='{$ibforums->skin['tbl_width']}' align='center'>
     <tr>
        <td valign='middle' align='left'>{$ibforums->lang['reg_header']}</b><br><br>{$data['TEXT']}</td>
     </tr>
     </table>
     <br>
     <table cellpadding='0' cellspacing='0' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
        <tr>
            <td>
                <table cellpadding='3' cellspacing='1' border='0' width='100%'>
                <tr>
                <td valign='left' colspan='2' id='titlemedium'>{$ibforums->lang['complete_form']}</td>
                </tr>
                <tr>
                <td id='row1' width='40%'>{$ibforums->lang['user_name']}</td>
                <td id='row1'><input type='text' size='32' maxlength='64' name='UserName' class='forminput'></td>
                </tr>
                <tr>
                <td id='row2' width='40%'>{$ibforums->lang['pass_word']}</td>
                <td id='row2'><input type='password' size='32' maxlength='32' name='PassWord' class='forminput'></td>
                </tr>
                <tr>
                <td id='row2' width='40%'>{$ibforums->lang['re_enter_pass']}</td>
                <td id='row2'><input type='password' size='32' maxlength='32' name='PassWord_Check' class='forminput'></td>
                </tr>
                <tr>
                <td id='row1' width='40%'>{$ibforums->lang['email_address']}</td>
                <td id='row1'><input type='text' size='32' maxlength='50' name='EmailAddress' class='forminput'></td>
                </tr>
                <tr>
                <td id='row1' width='40%'>{$ibforums->lang['email_address_two']}</td>
                <td id='row1'><input type='text' size='32' maxlength='50' name='EmailAddress_two' class='forminput'></td>
                </tr>
                </table>
            </td>
        </tr>
     </table>
     <br>
     <table cellpadding='0' cellspacing='0' border='0' width='{$ibforums->skin['tbl_width']}' bgcolor='{$ibforums->skin['tbl_border']}' align='center'>
        <tr>
            <td>
                <table cellpadding='3' cellspacing='1' border='0' width='100%'>
                <tr>
                <td  valign='left' id='titlemedium'>{$ibforums->lang['terms_service']}</td>
                </tr>
                <tr>
                <td id='row1' align='center'>{$ibforums->lang['terms_service_text']}<br>
                    <textarea cols='75' rows='9' wrap='soft' name='Post' class='textinput' style='font-size:10px'>{$data[RULES]}</textarea>
                    <br><br><b>{$ibforums->lang['agree_submit']}</b>&nbsp;<input type='checkbox' name='agree' value='1'>
                </td>
                </tr>
                <tr>
                <td id='row2' align='center'>
                <input type="submit" value="{$ibforums->lang['submit_form']}" class='forminput'>
                </td></tr></table>
                </td></tr></table>
                </form>
EOF;
}


}
?>